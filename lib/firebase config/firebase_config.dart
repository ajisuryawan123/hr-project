import 'package:firebase_core/firebase_core.dart';

class DefaultFirebaseConfig {
  static FirebaseOptions get platformOptions {
    /// if you want configuration for web from here
    /// and fill your data from web's google-services.json file ///
    // if (kIsWeb) {
    //   // Web
    //   return const FirebaseOptions(
    //     apiKey: '',
    //     authDomain: '',
    //     databaseURL:'',
    //     projectId: '',
    //     storageBucket: '',
    //     messagingSenderId: '',
    //     appId: '',
    //     measurementId: '',
    //   );

    /// also if you wont configuration for IOS from here
    /// and fill your data from IOS's google-services.json file ///
    // } else if (Platform.isIOS || Platform.isMacOS) {
    //   // iOS and MacOS
    //   return const FirebaseOptions(
    //     apiKey: '',
    //     authDomain: '',
    //     databaseURL:'',
    //     projectId: '',
    //     storageBucket: '',
    //     messagingSenderId: '',
    //     appId: '',
    //     measurementId: '',
    //   );
    // } else {
    //   // Android

    /// To do change your data from google-services.json file ///
    /// in this configuration we work for android only  ///
    return const FirebaseOptions(
      appId: '1:140568674039:android:0c86fcb789a4e717379df2',
      apiKey: 'AIzaSyC1PkqXFrvbbi6bqQPKi-AMrMMJRJbTBDQ',
      projectId: 'appfuxion-hr-project',
      androidClientId:
          '140568674039-vbf643v0eb9ofbdbblp4r40s94prri12.apps.googleusercontent.com',
      // we can find it from firebase/project setting /  Cloud Messaging
      messagingSenderId: '140568674039',
      // we can find it from firebase/ Authentication /Sign-in method
      authDomain: 'appfuxion-hr-project.firebaseapp.com',
    );
    // }
  }
}
