import 'package:flutter/material.dart';
import 'package:hr_project/dummy_data.dart';
import 'package:hr_project/page/approval_management/approval_management.dart';
import 'package:hr_project/page/attendance/attendance.dart';
import 'package:hr_project/page/calender/calender.dart';
import 'package:hr_project/page/leaderboard/leaderboard.dart';
import 'package:hr_project/page/leave/leave.dart';
import 'package:hr_project/page/log/log.dart';
import 'package:hr_project/page/more.dart';
import 'package:hr_project/page/payroll/payroll.dart';
import 'package:hr_project/page/profile/profile.dart';
import 'package:hr_project/page/time%20setting/time_setting.dart';
import 'package:hr_project/page/user%20management/user_management.dart';
import 'package:hr_project/styling_theme.dart';

class ListDummy {
  static List<DummyGroupedAttendance> listGroupAttend = [
    DummyGroupedAttendance(
        "Fuxion Spark App\n\n",
        "Need to fix admin portal and deploy",
        "08:52 AM",
        "06:49 PM",
        "On Time",
        "17\n",
        "Wed",
        "Attend",
        DateTime(2023, 4)),
    DummyGroupedAttendance("-\n\n", "-", "-", "-", "", "16\n", "Tue",
        "No Record", DateTime(2023, 4)),
    DummyGroupedAttendance(
        "Paid Leave\n\n",
        "Annual Leave (Holiday Entitlement)",
        "-",
        "-",
        "Leave",
        "15\n",
        "Mon",
        "Leave",
        DateTime(2023, 4)),
    DummyGroupedAttendance(
        "HR Project\n\n",
        "Integration Log Screen",
        "08:52 AM",
        "06:49 PM",
        "On Time",
        "14\n",
        "Mon",
        "Attend",
        DateTime(2023, 4)),
    DummyGroupedAttendance(
        "Fuxion Spark App\n\n",
        "Need to fix admin portal and deploy",
        "08:52 AM",
        "06:49 PM",
        "On Time",
        "13\n",
        "Mon",
        "Leave",
        DateTime(2023, 4)),
  ];

  static List<DummyGridMenu> gridMenu = [
    DummyGridMenu(IconAsset.clock(), "Attendance", AttendancePage()),
    DummyGridMenu(IconAsset.form(), "Form", LeavePage()),
    DummyGridMenu(IconAsset.log(), "Log", LogPage()),
    DummyGridMenu(IconAsset.profile(), "Profile", ProfilePage()),
    DummyGridMenu(IconAsset.payroll(), "Payroll", PayrollPage()),
    DummyGridMenu(IconAsset.chat(), "Chat", null),
    DummyGridMenu(IconAsset.date(), "Calendar", CalendarPage()),
    DummyGridMenu(IconAsset.more(), "More", MorePage()),
  ];

  static List<DummyGridMenu> gridMenuDetail1 = [
    DummyGridMenu(IconAsset.clock(), "Attendance", AttendancePage()),
    DummyGridMenu(IconAsset.form(), "Form", LeavePage()),
    DummyGridMenu(IconAsset.log(), "Log", LogPage()),
    DummyGridMenu(IconAsset.profile(), "Profile", ProfilePage()),
    DummyGridMenu(IconAsset.payroll(), "Payroll", PayrollPage()),
    DummyGridMenu(IconAsset.chat(), "Chat", null),
    DummyGridMenu(IconAsset.date(), "Calendar", CalendarPage()),
    DummyGridMenu(IconAsset.leaderboard(), "Leaderboard", LeaderboardPage()),
  ];

  static List<DummyGridMenu> gridMenuDetail2 = [
    DummyGridMenu(IconAsset.approval_management(), "Approval Management",
        ApprovalManagementPage()),
    DummyGridMenu(
        IconAsset.user_management(), "User Management", UserManagePage()),
    DummyGridMenu(IconAsset.time_setting(), "Time Settings", TimeSettingPage()),
  ];

  static List<DummyList> listMenu = [
    DummyList(
        ImageAsset.image1(1),
        "Malaysia",
        "Welcome Onboard!",
        "Welcome to the team! We are excited to have James Webb on board and look forward to working with you.",
        "Welcome to the team! We are excited to have James Webb on board and look forward to working with you.\n\nWe are thrilled to welcome James Webb as Senior Software Engineer to our team at Appfuxion Consulting! We look forward to your contributions to our organization."),
    DummyList(
        ImageAsset.image2(1),
        "All Countries",
        "Cha-ching! Payday has arrived",
        "Your hard work pays off! 💰💼💪 Enjoy your well-deserved salary slip, dear employee!",
        "Your hard work pays off! 💰💼💪 Enjoy your well-deserved salary slip, dear employee!"),
    DummyList(
        ImageAsset.image3(1),
        "Indonesia",
        "Welcome Onboard!",
        "Welcome to the team! We are excited to have Shania Susanti on board and look forward to working with you.",
        "Welcome to the team! We are excited to have Shania Susanti on board and look forward to working with you.\n\nWe are thrilled to welcome Shania Susanti as Human Resources Supervisor to our team at Appfuxion Consulting! We look forward to your contributions to our organization."),
    DummyList(
        ImageAsset.image4(1),
        "Malaysia",
        "Selamat Merayakan Hari Raya Idul Fitri ",
        "Semoga kemenangan ini membawa kebahagiaan untuk kita semua. Minal ‘aidin wal faidzin",
        "Semoga kemenangan ini membawa kebahagiaan untuk kita semua. Minal ‘aidin wal faidzin.\n\nTiada yang seindah ketika hati ini tersucikan tanpa kebencian, terbasuh oleh Ramadan dan keikhlasan hati untuk saling memaafkan. Selamat Hari Raya Idul Fitri 1444 H. Mohon maaf lahir dan batin. "),
    DummyList(
        ImageAsset.image5(1),
        "All Countries",
        "Happy New Year 2023!! 🥳🎉",
        "Embrace the upcoming year with open arms and create memories that will last a lifetime. ",
        "Embrace the upcoming year with open arms and create memories that will last a lifetime. "),
    DummyList(
        ImageAsset.image6(1),
        "All Countries",
        "Scheduled Maintenance",
        "May 20 between 09:00 PM and 11:00 PM GMT +7 we will perform a scheduled update of our infrastructure.",
        "May 20 between 09:00 PM and 11:00 PM GMT +7 we will perform a scheduled update of our infrastructure."),
  ];

  static List<DummyAttendance> listAttendance = [
    DummyAttendance(
        "Fuxion Spark App\n\n",
        "Need to fix admin portal and deploy",
        "08:52 AM",
        "-",
        "On Time",
        "17\n",
        "Wed",
        "Attend"),
    DummyAttendance("-\n\n", "-", "-", "-", "", "16\n", "Tue", "No Record"),
    DummyAttendance("Paid Leave\n\n", "Annual Leave (Holiday Entitlement)", "-",
        "-", "Leave", "15\n", "Mon", "Leave"),
  ];

  static List<DummyListLeave> listLeave = [
    DummyListLeave("Waiting Response", "08 Dec 23", "12 Dec 23", "5 days",
        "Paid Leave\n\n", "Annual Leave (Holiday Entitlement)"),
    DummyListLeave("Declined", "08 Dec 23", "09 Aug 23", "2 days",
        "Paid Leave\n\n", "Annual Leave (Holiday Entitlement)"),
    DummyListLeave("Approved ", "08 Dec 23", "10 Mar 23", "3 days",
        "Unpaid Leave\n\n", "Sick Leave"),
  ];

  static List<PieData> data = [
    PieData("On Time", 80, 125, Color(0xFF27AE60)),
    PieData("Late", 16, 8, Color(0xFFF44336)),
    PieData("No Record", 3, 8, Color(0xFF525F71)),
  ];

  static List<DummyGroupedLog> listGroupLog = [
    DummyGroupedLog(
        "Fuxion Spark App\n\n",
        "Need to fix admin portal and deploy",
        "08:52 AM",
        "06:49 PM",
        "On Time",
        "Bella Puti Wahyuni",
        DateTime(2023, 5, 17),
        AvatarAsset.avatar1(1)),
    DummyGroupedLog(
        "Paid Leave\n\n",
        "Annual Leave (Holiday Entitlement)",
        "-",
        "-",
        "Leave",
        "Hafshah Nasyidah",
        DateTime(2023, 5, 17),
        AvatarAsset.avatar2(1)),
    DummyGroupedLog(
        "Fuxion Spark App\n\n",
        "Need to fix admin portal and deploy",
        "-",
        "-",
        "Late",
        "Baktiadi Anggriawan",
        DateTime(2023, 5, 17),
        AvatarAsset.avatar3(1)),
    DummyGroupedLog(
        "Fuxion Spark App\n\n",
        "Need to fix admin portal and deploy",
        "-",
        "-",
        "Late",
        "Citra Hariyah",
        DateTime(2023, 5, 17),
        AvatarAsset.avatar4(1)),
    DummyGroupedLog(
        "Fuxion Spark App\n\n",
        "Need to fix admin portal and deploy",
        "-",
        "-",
        "Late",
        "Bagya Nugroho",
        DateTime(2023, 5, 17),
        AvatarAsset.avatar5(1)),
  ];

  static List<DummyAttendDetail> listOnTime = [
    DummyAttendDetail("08:52 AM", "06:14 PM", "Hafshah Nasyidah",
        DateTime(2023, 05, 17), AvatarAsset.avatar1(1)),
    DummyAttendDetail("08:42 AM", "06:12 PM", "Citra Hariyah",
        DateTime(2023, 05, 17), AvatarAsset.avatar2(1)),
    DummyAttendDetail("08:32 AM", "06:23 PM", "Bagya Nugroho",
        DateTime(2023, 05, 17), AvatarAsset.avatar3(1)),
    DummyAttendDetail("08:22 AM", "07:03 PM", "Baktiadi Anggriawan",
        DateTime(2023, 05, 17), AvatarAsset.avatar4(1)),
    DummyAttendDetail("08:12 AM", "06:14 PM", "Bella Syahrini Rahimah",
        DateTime(2023, 05, 16), AvatarAsset.avatar1(1)),
    DummyAttendDetail("08:02 AM", "07:09 PM", "Edi Budiyanto",
        DateTime(2023, 05, 16), AvatarAsset.avatar2(1)),
    DummyAttendDetail("07:52 AM", "05:18 PM", "Ayu Mayasari",
        DateTime(2023, 05, 16), AvatarAsset.avatar3(1)),
    DummyAttendDetail("07:42 AM", "05:23 PM", "Hesti Lailasari",
        DateTime(2023, 05, 16), AvatarAsset.avatar4(1)),
  ];

  static List<DummyAttendDetail> listLate = [
    DummyAttendDetail("12:52 AM", "05:18 PM", "Craig Dias",
        DateTime(2023, 05, 17), AvatarAsset.avatar1(1)),
    DummyAttendDetail("11:42 AM", "05:23 PM", "Livia Dias",
        DateTime(2023, 05, 17), AvatarAsset.avatar2(1)),
    DummyAttendDetail("11:32 AM", "06:12 PM", "Adison Torff",
        DateTime(2023, 05, 17), AvatarAsset.avatar3(1)),
    DummyAttendDetail("11:22 AM", "06:23 PM", "Brandon Siphron",
        DateTime(2023, 05, 17), AvatarAsset.avatar4(1)),
    DummyAttendDetail("11:12 AM", "07:03 PM", "Zain Calzoni",
        DateTime(2023, 05, 17), AvatarAsset.avatar5(1)),
    DummyAttendDetail("10:12 AM", "06:14 PM", "Alfonso Franci",
        DateTime(2023, 05, 16), AvatarAsset.avatar6(1)),
    DummyAttendDetail("10:02 AM", "07:09 PM", "Cristofer Philips",
        DateTime(2023, 05, 16), AvatarAsset.avatar5(1)),
    DummyAttendDetail("09:52 AM", "05:18 PM", "Maren Mango",
        DateTime(2023, 05, 16), AvatarAsset.avatar4(1)),
    DummyAttendDetail("09:42 AM", "05:23 PM", "Abram Lipshutz",
        DateTime(2023, 05, 16), AvatarAsset.avatar3(1)),
  ];

  static List<DummyAttendDetail> listNoRecord = [
    DummyAttendDetail(
        "-", "-", "Craig Dias", DateTime(2023, 05, 17), AvatarAsset.avatar7(1)),
    DummyAttendDetail("-", "-", "Desirae Donin", DateTime(2023, 05, 17),
        AvatarAsset.avatar8(1)),
    DummyAttendDetail("-", "-", "Livia Culhane", DateTime(2023, 05, 17),
        AvatarAsset.avatar9(1)),
    DummyAttendDetail("-", "-", "Gustavo Workman", DateTime(2023, 05, 16),
        AvatarAsset.avatar10(1)),
    DummyAttendDetail("-", "-", "Kaiya Botosh", DateTime(2023, 05, 15),
        AvatarAsset.avatar11(1)),
    DummyAttendDetail("-", "-", "Maria Septimus", DateTime(2023, 05, 15),
        AvatarAsset.avatar12(1)),
    DummyAttendDetail("-", "-", "Aspen Press", DateTime(2023, 05, 15),
        AvatarAsset.avatar13(1)),
    DummyAttendDetail("-", "-", "Jordyn Rosser", DateTime(2023, 05, 15),
        AvatarAsset.avatar14(1)),
  ];

  static List<DummyGroupedLog> listSearchLog = [
    DummyGroupedLog(
        "Fuxion Spark App\n\n",
        "Need to fix admin portal and deploy",
        "08:52 AM",
        "06:49 PM",
        "On Time",
        "Bella Puti Wahyuni",
        DateTime(2023, 5, 17),
        AvatarAsset.avatar1(1)),
    DummyGroupedLog(
        "Fuxion Spark App\n\n",
        "Need to fix admin portal and deploy",
        "08:16 AM",
        "05:15 PM",
        "On Time",
        "Bella Puti Wahyuni",
        DateTime(2023, 5, 16),
        AvatarAsset.avatar1(1)),
    DummyGroupedLog(
        "Fuxion Spark App\n\n",
        "Need to fix admin portal and deploy",
        "09:26 AM",
        "07:03 PM",
        "On Time",
        "Bella Puti Wahyuni",
        DateTime(2023, 5, 15),
        AvatarAsset.avatar1(1)),
  ];

  static List<DummyManagePendingHistory> listPending = [
    DummyManagePendingHistory(
        AvatarAsset.avatar15(1),
        "Maulana Ibrahim",
        "Software Engineer",
        "Waiting Response",
        "Annual Leave (Holiday\nEntitlement)",
        "2 days"),
    DummyManagePendingHistory(AvatarAsset.avatar16(1), "Chelsea Suci Susanti",
        "UI/UX Designer", "Waiting Response", "Sick Leave", "2 days"),
  ];

  static List<DummyManagePendingHistory> listHistory = [
    DummyManagePendingHistory(
        AvatarAsset.avatar17(1),
        "Ayu Mayasari",
        "Software Engineer",
        "Declined",
        "Annual Leave (Holiday\nEntitlement)",
        "2 days"),
    DummyManagePendingHistory(AvatarAsset.avatar18(1), "Edi Budiyanto",
        "UI/UX Designer", "Approved", "Sick Leave", "2 days"),
  ];

  static List<DummyLeaderboard> listLeaderboard = [
    DummyLeaderboard(AvatarAsset.avatar19(1), "Citra Hariyah",
        "Human Resource Manager", "citra@appfuxion.id", "98%", false),
    DummyLeaderboard(AvatarAsset.avatar20(1), "Ayu Mayasari",
        "BackEnd Engineer", "ayu@appfuxion.id", "95%", false),
    DummyLeaderboard(AvatarAsset.avatar21(1), "Edi Budiyanto",
        "Human Resource Manager", "edi@appfuxion.id", "94%", false),
    DummyLeaderboard(AvatarAsset.avatar22(1), "Hafshah Nasyidah",
        "UI/UX Designer", "nasyidah@appfuxion.id", "90%", false),
    DummyLeaderboard(AvatarAsset.avatar23(1), "Eka Prakasa", "BackEnd Engineer",
        "eka@appfuxion.id", "89%", false),
    DummyLeaderboard(AvatarAsset.avatar24(1), "Raditya Pratama",
        "BackEnd Engineer", "raditya@appfuxion.id", "89%", true),
    DummyLeaderboard(AvatarAsset.avatar25(1), "Hesti Lailasari",
        "Graphic Designer", "hesti@appfuxion.id", "83%", false),
    DummyLeaderboard(AvatarAsset.avatar26(1), "Edi Budiyanto",
        "Graphic Designer", "hesti@appfuxion.id", "83%", false),
  ];

  static List<DummyDropdown> listFormType = [
    DummyDropdown("Leave Request", "A"),
    DummyDropdown("Claim", "B"),
    DummyDropdown("Overtime", "C")
  ];

  static List<DummyDropdown> leaveManagementFormType = [
    DummyDropdown("Leave Approval", "A"),
    DummyDropdown("Claim Approval", "B"),
    DummyDropdown("Overtime Approval", "C")
  ];

  static List<DummyDropdown> listClaimType = [
    DummyDropdown("Meeting with Client", "A"),
    DummyDropdown("Business Meeting", "B"),
    DummyDropdown("Accommodation", "C")
  ];

  static List<DummyDropdown> listSPVApprove = [
    DummyDropdown("Argono Anggriawan", "A"),
    DummyDropdown("Anita Riyanti", "B"),
    DummyDropdown("Darijan Sihombing", "C"),
    DummyDropdown("Elvin Maryadi", "D"),
    DummyDropdown("Anita Agustina", "E"),
    DummyDropdown("Eli Nasyidah", "F"),
    DummyDropdown("Fitria Yuliarti", "G"),
    DummyDropdown("Bagya Nugroho", "H"),
  ];

  static List<DummyListClaim> listClaim = [
    DummyListClaim(
        "Waiting Response",
        "19 May 23",
        "RM 2,000",
        "Meeting with Client\n\n",
        "Expenses incurred during a productive business meeting with a potential client."),
    DummyListClaim("Approved", "12 Feb 23", "RM 3,500", "Business Meeting\n\n",
        "Reimbursement request for expenses incurred during a productive business meeting with a potential client."),
    DummyListClaim("Declined", "23 Jan 23", "RM 4,000", "Accommodation\n\n",
        "Business meeting with a potential client."),
  ];

  static List<DummyListOvertime> listOvertime = [
    DummyListOvertime(
        "19 May 2023",
        "06:00 PM",
        "08:00 PM",
        "MBanking Android Mobile App\n\n",
        "Project deadline",
        DateTime(2023, 05)),
    DummyListOvertime(
        "18 May 2023",
        "06:00 PM",
        "08:00 PM",
        "MBanking Android Mobile App\n\n",
        "Project deadline",
        DateTime(2023, 05)),
  ];

  static List<DummyDropdown> listOvertimeClaim = [
    DummyDropdown("Maybank", "A"),
  ];

  static List<DummyDropdown> listOvertimeProject = [
    DummyDropdown("MBanking Android Mobile App", "A"),
  ];

  static List<DummyDropdown> listAssesor1 = [
    DummyDropdown("Argono Anggriawan", "A"),
    DummyDropdown("Anita Riyanti", "B"),
    DummyDropdown("Darijan Sihombing", "C"),
    DummyDropdown("Elvin Maryadi", "D"),
    DummyDropdown("Anita Agustina", "E"),
    DummyDropdown("Eli Nasyidah", "F"),
    DummyDropdown("Fitria Yuliarti", "G"),
    DummyDropdown("Bagya Nugroho", "H"),
  ];
}
