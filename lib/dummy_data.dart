import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class DummyGridMenu {
  final SvgPicture icon;
  final String title;
  final Widget? directPage;

  DummyGridMenu(this.icon, this.title, this.directPage);
}

class DummyList {
  final Image image;
  final String type;
  final String title;
  final String subtitle;
  final String text;

  DummyList(this.image, this.type, this.title, this.subtitle, this.text);
}

class DummyDropdown {
  final String text;
  final String valueRadio;

  DummyDropdown(this.text, this.valueRadio);
}

class DummyCountry {
  final String text;
  final String valueRadio;
  final SvgPicture icon;

  DummyCountry(this.text, this.valueRadio, this.icon);
}

class DummyProfileMenuItem {
  final SvgPicture icon;
  final String title;
  final Widget? directPage;

  DummyProfileMenuItem(this.icon, this.title, this.directPage);
}

class DummyAttendance {
  final String title;
  final String contain;
  final String clock_in;
  final String clock_out;
  final String status;
  final String tgl;
  final String day;
  final String statusAttend;

  DummyAttendance(this.title, this.contain, this.clock_in, this.clock_out,
      this.status, this.tgl, this.day, this.statusAttend);
}

class DummyGroupedAttendance {
  final String title;
  final String contain;
  final String clock_in;
  final String clock_out;
  final String status;
  final String tgl;
  final String day;
  final String statusAttend;
  final DateTime datetime;

  DummyGroupedAttendance(
      this.title,
      this.contain,
      this.clock_in,
      this.clock_out,
      this.status,
      this.tgl,
      this.day,
      this.statusAttend,
      this.datetime);
}

class DummyListLeave {
  final String status;
  final String startDate;
  final String endDate;
  final String leftDay;
  final String title;
  final String contain;

  DummyListLeave(this.status, this.startDate, this.endDate, this.leftDay,
      this.title, this.contain);
}

class PieData {
  final String name;
  final double percent;
  final int value;
  final Color color;

  PieData(this.name, this.percent, this.value, this.color);
}

class DummyGroupedLog {
  final String title;
  final String contain;
  final String clock_in;
  final String clock_out;
  final String status;
  final String name;
  final DateTime datetime;
  final Image avatar;

  DummyGroupedLog(this.title, this.contain, this.clock_in, this.clock_out,
      this.status, this.name, this.datetime, this.avatar);
}

class DummyAttendDetail {
  final String clock_in;
  final String clock_out;
  final String name;
  final DateTime datetime;
  final Image avatar;

  DummyAttendDetail(
      this.clock_in, this.clock_out, this.name, this.datetime, this.avatar);
}

class DummyManagePendingHistory {
  final Image avatar;
  final String name;
  final String role;
  final String statusLeave;
  final String typeLeave;
  final String daysTotal;

  DummyManagePendingHistory(this.avatar, this.name, this.role, this.statusLeave,
      this.typeLeave, this.daysTotal);
}

class DummyLeaderboard {
  final Image avatar;
  final String name;
  final String role;
  final String email;
  final String score;
  final bool isMe;

  DummyLeaderboard(
      this.avatar, this.name, this.role, this.email, this.score, this.isMe);
}

class MyTabs {
  final Widget directAdd;

  MyTabs(this.directAdd);
}

class DummyListClaim {
  final String status;
  final String startDate;
  final String price;
  final String title;
  final String contain;

  DummyListClaim(
      this.status, this.startDate, this.price, this.title, this.contain);
}

class DummyListOvertime {
  final String startDate;
  final String startTime;
  final String endTime;
  final String title;
  final String contain;
  final DateTime datetime;

  DummyListOvertime(this.startDate, this.startTime, this.endTime, this.title,
      this.contain, this.datetime);
}
