import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hr_project/config/API%20config/leave_api/leave_detail.dart';
import 'package:hr_project/page/leave/preview_image.dart';
import 'package:hr_project/styling_theme.dart';
import 'package:hr_project/utils/unauthorized.dart';
import 'package:hr_project/widget/custom_appbar.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../model/leave-model/leave_detail.dart';

class LeaveDetailPage extends StatefulWidget {
  final String id_leave;
  const LeaveDetailPage({Key? key, required this.id_leave}) : super(key: key);

  @override
  State<LeaveDetailPage> createState() => _LeaveDetailPageState();
}

class _LeaveDetailPageState extends State<LeaveDetailPage> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  bool _isLoad = false;
  LeaveDetailModel? leaveDetailRes;

  detailLeave() async {
    setState(() {
      _isLoad = true;
    });
    final SharedPreferences prefs = await _prefs;
    var res = await leaveDetail_API(
        token: prefs.getString("accessToken")!, id_leave: widget.id_leave);
    leaveDetailRes = await LeaveDetailModel.fromJson(json.decode(res.body));
    if (leaveDetailRes!.errorMessage == "invalid authorization") {
      unauthorized(context);
    }
    setState(() {
      _isLoad = false;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    detailLeave();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFFF6F9FF),
        appBar: CustomAppbar(title: "Leave Details"),
        body: _isLoad
            ? Center(child: CircularProgressIndicator())
            : SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      requestDetail(),
                      SizedBox(height: CustomTheme.screenHeight! * 0.03),
                      requestRespon(),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  requestDetail() {
    return Container(
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8)),
          color: Colors.white),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Request Details",
              style: CustomTheme.body1(context,
                  color: Colors.black, fontWeight: CustomTheme.semibold)),
          SizedBox(height: CustomTheme.screenHeight! * 0.03),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Leave Request",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text(
                    leaveDetailRes!.data!.request!.type == "PAID"
                        ? "Paid Leave"
                        : "Unpaid Leave",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Leave Type",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text(leaveDetailRes!.data!.request!.leaveType!.name!,
                    textAlign: TextAlign.end,
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Reason",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text(leaveDetailRes!.data!.request!.reason!,
                    textAlign: TextAlign.end,
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Start Date",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text(
                    DateFormat('dd MMM yyyy', 'en_US')
                        .format(leaveDetailRes!.data!.request!.startDate!),
                    textAlign: TextAlign.end,
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("End Date",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text(
                    DateFormat('dd MMM yyyy', 'en_US')
                        .format(leaveDetailRes!.data!.request!.endDate!),
                    textAlign: TextAlign.end,
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Days Total",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Container(
                  padding: EdgeInsets.all(8),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      color: Color(0xFFF2F4F9)),
                  child: Text(
                      "${leaveDetailRes!.data!.request!.daysTotal} days",
                      style: CustomTheme.caption(context,
                          color: Color(0xFF5981EA),
                          fontWeight: CustomTheme.medium)),
                ),
              ],
            ),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: CustomTheme.screenWidth! * 0.3,
                  child: Text("Attachment",
                      style: CustomTheme.body2(context,
                          color: Colors.black, fontWeight: CustomTheme.medium)),
                ),
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PreviewImagePage(
                                  dataDetail: leaveDetailRes!)));
                    },
                    child: Text(
                        leaveDetailRes!.data!.request!.attachmentUrl!
                            .replaceFirst(
                                "https://api.appfuxion.id/files/", ""),
                        textAlign: TextAlign.end,
                        style: CustomTheme.body2(context,
                            color: Color(0xFF2F80ED),
                            fontWeight: CustomTheme.semibold)),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  requestRespon() {
    return Container(
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8)),
          color: Colors.white),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Request Response",
              style: CustomTheme.body1(context,
                  color: Colors.black, fontWeight: CustomTheme.semibold)),
          SizedBox(height: CustomTheme.screenHeight! * 0.03),
          Container(
            height: CustomTheme.screenHeight! * 0.05,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Assessor 1",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text(leaveDetailRes!.data!.assessors!.first!.name!,
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(8)),
                color:
                    leaveDetailRes!.data!.assessors!.first!.status == "WAITING"
                        ? Color(0xFFFFF4ED)
                        : leaveDetailRes!.data!.assessors!.first!.status ==
                                "DECLINED"
                            ? Color(0xFFFFEEEC)
                            : Color(0xFFE9F7EF)),
            child: Text(
                leaveDetailRes!.data!.assessors!.first!.status == "WAITING"
                    ? "Waiting Response"
                    : leaveDetailRes!.data!.assessors!.first!.status ==
                            "DECLINED"
                        ? "Declined"
                        : "Approved",
                style: CustomTheme.caption(context,
                    color: leaveDetailRes!.data!.assessors!.first!.status ==
                            "WAITING"
                        ? Color(0xFFEB7D2E)
                        : leaveDetailRes!.data!.assessors!.first!.status ==
                                "DECLINED"
                            ? Color(0xFFFF5245)
                            : Color(0xFF27AE60),
                    fontWeight: CustomTheme.medium)),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.05,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Reason",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text("",
                    textAlign: TextAlign.end,
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          SizedBox(height: CustomTheme.screenHeight! * 0.03),
          Divider(
              color: Color(0xFFD8E4EE),
              height: 0,
              thickness: 1,
              endIndent: 20,
              indent: 20),
          SizedBox(height: CustomTheme.screenHeight! * 0.03),
          Container(
            height: CustomTheme.screenHeight! * 0.05,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Assessor 2",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text(leaveDetailRes!.data!.assessors!.second!.name!,
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(8)),
                color:
                    leaveDetailRes!.data!.assessors!.second!.status == "WAITING"
                        ? Color(0xFFFFF4ED)
                        : leaveDetailRes!.data!.assessors!.second!.status ==
                                "DECLINED"
                            ? Color(0xFFFFEEEC)
                            : Color(0xFFE9F7EF)),
            child: Text(
                leaveDetailRes!.data!.assessors!.second!.status == "WAITING"
                    ? "Waiting Response"
                    : leaveDetailRes!.data!.assessors!.second!.status ==
                            "DECLINED"
                        ? "Declined"
                        : "Approved",
                style: CustomTheme.caption(context,
                    color: leaveDetailRes!.data!.assessors!.second!.status ==
                            "WAITING"
                        ? Color(0xFFEB7D2E)
                        : leaveDetailRes!.data!.assessors!.second!.status ==
                                "DECLINED"
                            ? Color(0xFFFF5245)
                            : Color(0xFF27AE60),
                    fontWeight: CustomTheme.medium)),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.05,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Reason",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text("",
                    textAlign: TextAlign.end,
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
