import 'dart:convert';
import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:hr_project/config/API%20config/leave_api/leave_assesor1_2.dart';
import 'package:hr_project/styling_theme.dart';
import 'package:hr_project/utils/unauthorized.dart';
import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

import '../../config/API config/leave_api/leave_submit.dart';
import '../../config/API config/leave_api/leave_type.dart';
import '../../model/leave-model/leave_assesor1_2.dart';
import '../../model/leave-model/leave_submit.dart';
import '../../model/leave-model/leave_type.dart';
import '../authenticate/login.dart';

class UnpaidPaidLeave extends StatefulWidget {
  final String type;
  const UnpaidPaidLeave({Key? key, required this.type}) : super(key: key);

  @override
  State<UnpaidPaidLeave> createState() => _UnpaidPaidLeaveState();
}

class _UnpaidPaidLeaveState extends State<UnpaidPaidLeave> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  TextEditingController reasonController = TextEditingController();
  LeaveSubmitModel? leaveSubmitRes;
  bool _isLoad = false;
  DateRangePickerController _datePickerStart = DateRangePickerController();
  DateRangePickerController _datePickerEnd = DateRangePickerController();
  String dateStart = "Choose Date";
  String dateEnd = "Choose Date";
  String start_date = "";
  String end_date = "";
  int? difference;
  String fileName = "Upload file";
  PlatformFile? _pickedImage;

  ///Leave Type Data
  int? itemLeavetype;
  String? _selectedLeavetype;
  LeaveTypeModel? leaveTypeRes;
  List<LeaveTypeData>? dataLeaveType;

  ///Assesor1 Data
  int? itemAssessor1;
  String? _selectedAssessor1;
  LeaveAssesor1_2Model? assesor1Res;
  List<Content>? dataAssessor1;

  ///Assesor2 Data
  int? itemAssessor2;
  String? _selectedAssessor2;
  LeaveAssesor1_2Model? assesor2Res;
  List<Content>? dataAssessor2;

  Future _selectImage() async {
    final result = await FilePicker.platform.pickFiles(
      allowMultiple: false,
      type: FileType.image,
    );

    if (result != null) {
      setState(() {
        _pickedImage = result.files.first;
        fileName = _pickedImage!.name;
      });
      print("NAME: " + _pickedImage!.name);
      print("BYTES: " + _pickedImage!.bytes.toString());
      print("SIZE: " + _pickedImage!.size.toString());
      print("EXTENSION: " + _pickedImage!.extension.toString());
      print("PATH: " + _pickedImage!.path.toString());
    }
  }

  leaveSubmit(File file) async {
    setState(() {
      _isLoad = true;
    });
    final SharedPreferences prefs = await _prefs;
    var res = await leaveSubmit_API(
        token: prefs.getString("accessToken")!,
        file: file,
        start_date: start_date,
        end_date: end_date,
        leave_type_id: itemLeavetype.toString(),
        reason: reasonController.text,
        first_assesor: itemAssessor1.toString(),
        second_assesor: itemAssessor2.toString(),
        type: widget.type == 'Paid' ? "PAID" : "UNPAID");
    leaveSubmitRes = await LeaveSubmitModel.fromJson(json.decode(res.body));
    if (leaveSubmitRes!.errorMessage == "invalid authorization") {
      unauthorized(context);
    }
    setState(() {
      _isLoad = false;
    });
    dialog(res);
  }

  leaveType() async {
    setState(() {
      _isLoad = true;
    });
    final SharedPreferences prefs = await _prefs;
    var res = await leaveType_API(token: prefs.getString("accessToken")!);
    leaveTypeRes = await LeaveTypeModel.fromJson(json.decode(res.body));
    if (leaveTypeRes!.errorMessage == "invalid authorization") {
      unauthorized(context);
    }
    if (leaveTypeRes!.errorMessage == "invalid authorization") {
      unauthorized(context);
    }
    await assesor1();
    await assesor2();
    setState(() {
      dataLeaveType = leaveTypeRes!.data;
      itemLeavetype = leaveTypeRes!.data![0].id;
      _selectedLeavetype = leaveTypeRes!.data![0].name;
      _isLoad = false;
    });
  }

  assesor1() async {
    final SharedPreferences prefs = await _prefs;
    var res = await leaveAssesor1_2API(
        token: prefs.getString("accessToken")!, type: 'Assesor1');
    assesor1Res = await LeaveAssesor1_2Model.fromJson(json.decode(res.body));
    if (assesor1Res!.errorMessage == "invalid authorization") {
      unauthorized(context);
    }
    setState(() {
      dataAssessor1 = assesor1Res!.data!.content;
      itemAssessor1 = assesor1Res!.data!.content![0].employee!.employeeId;
      _selectedAssessor1 = assesor1Res!.data!.content![0].employee!.fullName;
    });
  }

  assesor2() async {
    final SharedPreferences prefs = await _prefs;
    var res = await leaveAssesor1_2API(
        token: prefs.getString("accessToken")!, type: 'Assesor2');
    assesor2Res = await LeaveAssesor1_2Model.fromJson(json.decode(res.body));
    if (assesor2Res!.errorMessage == "invalid authorization") {
      unauthorized(context);
    }
    setState(() {
      dataAssessor2 = assesor1Res!.data!.content;
      itemAssessor2 = assesor1Res!.data!.content![0].employee!.employeeId;
      _selectedAssessor2 = assesor1Res!.data!.content![0].employee!.fullName;
    });
  }

  @override
  void initState() {
    super.initState();
    _datePickerStart.selectedDate = DateTime.now();
    _datePickerEnd.selectedDate = DateTime.now();
    leaveType();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFFF6F9FF),
        appBar: appBarwidget(),
        bottomSheet: customSheet(),
        body: _isLoad
            ? Center(child: CircularProgressIndicator())
            : SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(12, 25, 12, 12),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      dateSelection(),
                      SizedBox(height: CustomTheme.screenHeight! * 0.04),
                      form1(),
                      SizedBox(height: CustomTheme.screenHeight! * 0.04),
                      form2(),
                      SizedBox(height: CustomTheme.screenHeight! * 0.15),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  appBarwidget() {
    return AppBar(
      leading: IconButton(
        icon: Icon(Icons.close, color: Color(0xFF2F80ED)),
        onPressed: () => Navigator.of(context).pop(),
      ),
      centerTitle: true,
      backgroundColor: Color(0xFFF2C94C),
      title: Text(
        widget.type == "Unpaid" ? "Unpaid Leave" : "Paid Leave",
        style: CustomTheme.subtitle(context,
            color: Colors.black, fontWeight: CustomTheme.semibold),
      ),
    );
  }

  customSheet() {
    return Container(
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3),
          ),
        ],
      ),
      child: ElevatedButton(
          onPressed: () {
            leaveSubmit(File(_pickedImage!.path.toString()));
          },
          style: ElevatedButton.styleFrom(
              padding: EdgeInsets.symmetric(vertical: 12),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              minimumSize: Size(double.infinity, 0),
              backgroundColor: Color(0xFFEB7D2E)),
          child: Text(
            "Submit",
            style: CustomTheme.button(context,
                color: Colors.white, fontWeight: CustomTheme.medium),
          )),
    );
  }

  dateSelection() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Start Date *",
              style: CustomTheme.body1(context,
                  color: Colors.black, fontWeight: CustomTheme.semibold),
            ),
            SizedBox(height: CustomTheme.screenHeight! * 0.01),
            GestureDetector(
              onTap: () => showDateStart(context),
              child: Container(
                padding: EdgeInsets.all(6),
                width: CustomTheme.screenWidth! * 0.45,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    border: Border.all(width: 1, color: Color(0xFFB8CADB)),
                    color: Colors.white),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(dateStart,
                        style: CustomTheme.body1(context,
                            color: dateStart == "Choose Date"
                                ? Color(0xFFBDBDBD)
                                : Colors.black,
                            fontWeight: CustomTheme.medium)),
                    IconAsset.calender_grey()
                  ],
                ),
              ),
            ),
          ],
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "End Date *",
              style: CustomTheme.body1(context,
                  color: Colors.black, fontWeight: CustomTheme.semibold),
            ),
            SizedBox(height: CustomTheme.screenHeight! * 0.01),
            GestureDetector(
              onTap: () => showDateEnd(context),
              child: Container(
                padding: EdgeInsets.all(6),
                width: CustomTheme.screenWidth! * 0.45,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    border: Border.all(width: 1, color: Color(0xFFB8CADB)),
                    color: Colors.white),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(dateEnd,
                        style: CustomTheme.body1(context,
                            color: dateEnd == "Choose Date"
                                ? Color(0xFFBDBDBD)
                                : Colors.black,
                            fontWeight: CustomTheme.medium)),
                    IconAsset.calender_grey()
                  ],
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

  void showDateStart(context) {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(12)),
        ),
        context: context,
        builder: (context) {
          return Container(
            padding: EdgeInsets.all(12),
            height: CustomTheme.screenHeight! * 0.5,
            child: SfDateRangePicker(
              monthViewSettings: DateRangePickerMonthViewSettings(
                  viewHeaderStyle: DateRangePickerViewHeaderStyle(
                      textStyle: CustomTheme.body2(context,
                          color: Color(0xFFEB7D2E),
                          fontWeight: CustomTheme.medium))),
              selectionColor: Color(0xFFEB7D2E),
              startRangeSelectionColor: Color(0xFFEB7D2E),
              endRangeSelectionColor: Color(0xFFEB7D2E),
              rangeSelectionColor: Color(0xFFFFA15E),
              rangeTextStyle: CustomTheme.body2(context,
                  color: Colors.white, fontWeight: CustomTheme.medium),
              selectionTextStyle: CustomTheme.body2(context,
                  color: Colors.white, fontWeight: CustomTheme.medium),
              headerStyle: DateRangePickerHeaderStyle(
                  textAlign: TextAlign.center,
                  textStyle: CustomTheme.body1(context,
                      color: Colors.black, fontWeight: CustomTheme.semibold)),
              selectionMode: DateRangePickerSelectionMode.single,
              controller: _datePickerStart,
              showActionButtons: true,
              onSubmit: (value) {
                if (value is PickerDateRange) {
                  final DateTime rangeStartDate = value.startDate!;
                  final DateTime rangeEndDate = value.endDate!;
                } else if (value is DateTime) {
                  final DateTime selectedDate = value;
                  setState(() {
                    dateStart =
                        DateFormat('dd MMM yyyy', 'en_US').format(selectedDate);
                    start_date =
                        DateFormat('yyyy-MM-dd', 'en_US').format(selectedDate);
                  });
                  if (dateStart != "Choose Date" && dateEnd != "Choose Date") {
                    setState(() {
                      difference = daysBetween(_datePickerStart.selectedDate!,
                          _datePickerEnd.selectedDate!);
                    });
                  }
                } else if (value is List<DateTime>) {
                  final List<DateTime> selectedDates = value;
                } else if (value is List<PickerDateRange>) {
                  final List<PickerDateRange> selectedRanges = value;
                }
                Navigator.pop(context);
              },
              onCancel: () {
                Navigator.pop(context);
              },
            ),
          );
        });
  }

  void showDateEnd(context) {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(12)),
        ),
        context: context,
        builder: (context) {
          return Container(
            padding: EdgeInsets.all(12),
            height: CustomTheme.screenHeight! * 0.5,
            child: SfDateRangePicker(
              monthViewSettings: DateRangePickerMonthViewSettings(
                  viewHeaderStyle: DateRangePickerViewHeaderStyle(
                      textStyle: CustomTheme.body2(context,
                          color: Color(0xFFEB7D2E),
                          fontWeight: CustomTheme.medium))),
              selectionColor: Color(0xFFEB7D2E),
              startRangeSelectionColor: Color(0xFFEB7D2E),
              endRangeSelectionColor: Color(0xFFEB7D2E),
              rangeSelectionColor: Color(0xFFFFA15E),
              rangeTextStyle: CustomTheme.body2(context,
                  color: Colors.white, fontWeight: CustomTheme.medium),
              selectionTextStyle: CustomTheme.body2(context,
                  color: Colors.white, fontWeight: CustomTheme.medium),
              headerStyle: DateRangePickerHeaderStyle(
                  textAlign: TextAlign.center,
                  textStyle: CustomTheme.body1(context,
                      color: Colors.black, fontWeight: CustomTheme.semibold)),
              selectionMode: DateRangePickerSelectionMode.single,
              controller: _datePickerEnd,
              showActionButtons: true,
              onSubmit: (value) {
                if (value is PickerDateRange) {
                  final DateTime rangeStartDate = value.startDate!;
                  final DateTime rangeEndDate = value.endDate!;
                } else if (value is DateTime) {
                  final DateTime selectedDate = value;
                  setState(() {
                    dateEnd =
                        DateFormat('dd MMM yyyy', 'en_US').format(selectedDate);
                    end_date =
                        DateFormat('yyyy-MM-dd', 'en_US').format(selectedDate);
                  });
                  if (dateStart != "Choose Date" && dateEnd != "Choose Date") {
                    setState(() {
                      difference = daysBetween(_datePickerStart.selectedDate!,
                          _datePickerEnd.selectedDate!);
                    });
                  }
                } else if (value is List<DateTime>) {
                  final List<DateTime> selectedDates = value;
                } else if (value is List<PickerDateRange>) {
                  final List<PickerDateRange> selectedRanges = value;
                }
                Navigator.pop(context);
              },
              onCancel: () {
                Navigator.pop(context);
              },
            ),
          );
        });
  }

  form1() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("Days Total",
            style: CustomTheme.body1(context,
                color: Colors.black, fontWeight: CustomTheme.semibold)),
        SizedBox(height: CustomTheme.screenHeight! * 0.01),
        Container(
            height: CustomTheme.screenHeight! * 0.06,
            padding: EdgeInsets.all(6),
            width: CustomTheme.screenWidth! * 1,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                border: Border.all(width: 1, color: Color(0xFFB8CADB)),
                color: Color(0xFFF2F4F9)),
            child: Text(difference == null ? "" : difference.toString(),
                style: CustomTheme.body1(context,
                    color: Colors.black, fontWeight: CustomTheme.medium))),
        SizedBox(height: CustomTheme.screenHeight! * 0.04),
        Text("Leave Type *",
            style: CustomTheme.body1(context,
                color: Colors.black, fontWeight: CustomTheme.semibold)),
        SizedBox(height: CustomTheme.screenHeight! * 0.01),
        InkWell(
          onTap: () => showLeavetype(context),
          child: Container(
            padding: EdgeInsets.all(6),
            width: CustomTheme.screenWidth! * 1,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                border: Border.all(width: 1, color: Color(0xFFB8CADB)),
                color: Colors.white),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(_selectedLeavetype.toString(),
                    style: CustomTheme.body1(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Icon(Icons.keyboard_arrow_down_outlined,
                    color: Color(0xFFB8CADB))
              ],
            ),
          ),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.04),
        Text(
          "Reason *",
          style: CustomTheme.body1(context,
              color: Colors.black, fontWeight: CustomTheme.semibold),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.02),
        TextFormField(
          controller: reasonController,
          keyboardType: TextInputType.multiline,
          maxLines: null,
          style: CustomTheme.body1(context,
              color: Colors.black, fontWeight: CustomTheme.medium),
          decoration: InputDecoration(
            floatingLabelBehavior: FloatingLabelBehavior.never,
            filled: true,
            fillColor: Colors.white,
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(color: Color(0xFFB8CADB))),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(color: Color(0xFFB8CADB))),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(color: Color(0xFFB8CADB))),
          ),
        ),
      ],
    );
  }

  void showLeavetype(context) {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(12)),
        ),
        context: context,
        builder: (context) {
          return Container(
            padding: EdgeInsets.fromLTRB(12, 12, 12, 0),
            height: CustomTheme.screenHeight! * 0.75,
            child: Column(
              children: [
                Text("Select Leave Type",
                    style: CustomTheme.body1(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
                SizedBox(height: CustomTheme.screenHeight! * 0.03),
                Container(
                  height: CustomTheme.screenHeight! * 0.07,
                  child: TextFormField(
                    style: CustomTheme.body1(context,
                        color: Colors.black, fontWeight: CustomTheme.medium),
                    decoration: InputDecoration(
                      floatingLabelBehavior: FloatingLabelBehavior.never,
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 30, horizontal: 10),
                      labelText: "Search Leave Type",
                      labelStyle: CustomTheme.body1(context,
                          color: Color(0xFFBDBDBD),
                          fontWeight: CustomTheme.medium),
                      filled: true,
                      fillColor: Color(0xFFF2F4F9),
                      prefixIcon: Icon(Icons.search, color: Color(0xFFE0E0E0)),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                    ),
                  ),
                ),
                SizedBox(height: CustomTheme.screenHeight! * 0.03),
                ListView.builder(
                    shrinkWrap: true,
                    itemCount: dataLeaveType!.length,
                    itemBuilder: (context, index) {
                      return RadioListTile(
                        title: Text(dataLeaveType![index].name!,
                            style: CustomTheme.body1(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.medium)),
                        activeColor: Color(0xFFEB7D2E),
                        value: dataLeaveType![index].id,
                        groupValue: itemLeavetype,
                        onChanged: (value) {
                          setState(() {
                            itemLeavetype = value;
                            _selectedLeavetype = dataLeaveType![index].name;
                          });
                          Navigator.of(context).pop();
                        },
                      );
                    }),
              ],
            ),
          );
        });
  }

  void showAssessor1(context) {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(12)),
        ),
        context: context,
        builder: (context) {
          return Container(
            padding: EdgeInsets.fromLTRB(12, 12, 12, 0),
            height: CustomTheme.screenHeight! * 0.9,
            child: Column(
              children: [
                Text("Select Assessor 1",
                    style: CustomTheme.body1(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
                SizedBox(height: CustomTheme.screenHeight! * 0.03),
                Container(
                  height: CustomTheme.screenHeight! * 0.07,
                  child: TextFormField(
                    style: CustomTheme.body1(context,
                        color: Colors.black, fontWeight: CustomTheme.medium),
                    decoration: InputDecoration(
                      floatingLabelBehavior: FloatingLabelBehavior.never,
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 30, horizontal: 10),
                      labelText: "Search Assessor 1",
                      labelStyle: CustomTheme.body1(context,
                          color: Color(0xFFBDBDBD),
                          fontWeight: CustomTheme.medium),
                      filled: true,
                      fillColor: Color(0xFFF2F4F9),
                      prefixIcon: Icon(Icons.search, color: Color(0xFFE0E0E0)),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                    ),
                  ),
                ),
                SizedBox(height: CustomTheme.screenHeight! * 0.03),
                Expanded(
                  child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: dataAssessor1!.length,
                      itemBuilder: (context, index) {
                        return RadioListTile(
                          title: Text(dataAssessor1![index].employee!.fullName!,
                              style: CustomTheme.body1(context,
                                  color: Colors.black,
                                  fontWeight: CustomTheme.medium)),
                          activeColor: Color(0xFFEB7D2E),
                          value: dataAssessor1![index].employee!.employeeId,
                          groupValue: itemAssessor1,
                          onChanged: (value) {
                            setState(() {
                              itemAssessor1 = value;
                              _selectedAssessor1 =
                                  dataAssessor1![index].employee!.fullName!;
                            });
                            Navigator.of(context).pop();
                          },
                        );
                      }),
                ),
              ],
            ),
          );
        });
  }

  void showAssessor2(context) {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(12)),
        ),
        context: context,
        builder: (context) {
          return Container(
            padding: EdgeInsets.fromLTRB(12, 12, 12, 0),
            height: CustomTheme.screenHeight! * 0.9,
            child: Column(
              children: [
                Text("Select Assessor 2",
                    style: CustomTheme.body1(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
                SizedBox(height: CustomTheme.screenHeight! * 0.03),
                Container(
                  height: CustomTheme.screenHeight! * 0.07,
                  child: TextFormField(
                    style: CustomTheme.body1(context,
                        color: Colors.black, fontWeight: CustomTheme.medium),
                    decoration: InputDecoration(
                      floatingLabelBehavior: FloatingLabelBehavior.never,
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 30, horizontal: 10),
                      labelText: "Search Assessor 2",
                      labelStyle: CustomTheme.body1(context,
                          color: Color(0xFFBDBDBD),
                          fontWeight: CustomTheme.medium),
                      filled: true,
                      fillColor: Color(0xFFF2F4F9),
                      prefixIcon: Icon(Icons.search, color: Color(0xFFE0E0E0)),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                    ),
                  ),
                ),
                SizedBox(height: CustomTheme.screenHeight! * 0.03),
                Expanded(
                  child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: dataAssessor2!.length,
                      itemBuilder: (context, index) {
                        return RadioListTile(
                          title: Text(dataAssessor2![index].employee!.fullName!,
                              style: CustomTheme.body1(context,
                                  color: Colors.black,
                                  fontWeight: CustomTheme.medium)),
                          activeColor: Color(0xFFEB7D2E),
                          value: dataAssessor2![index].employee!.employeeId,
                          groupValue: itemAssessor2,
                          onChanged: (value) {
                            setState(() {
                              itemAssessor2 = value;
                              _selectedAssessor2 =
                                  dataAssessor2![index].employee!.fullName!;
                            });
                            Navigator.of(context).pop();
                          },
                        );
                      }),
                ),
              ],
            ),
          );
        });
  }

  form2() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("Attachment",
            style: CustomTheme.body1(context,
                color: Colors.black, fontWeight: CustomTheme.semibold)),
        SizedBox(height: CustomTheme.screenHeight! * 0.01),
        InkWell(
          onTap: _selectImage,
          child: DottedBorder(
            color: Color(0xFFB8CADB),
            dashPattern: [8, 4],
            radius: Radius.circular(5),
            child: Container(
              padding: EdgeInsets.all(6),
              width: CustomTheme.screenWidth! * 1,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  color: Colors.white),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width * 0.7,
                    child: Text(fileName,
                        style: CustomTheme.body1(context,
                            color: fileName == "Upload file"
                                ? Color(0xFFBDBDBD)
                                : Colors.black,
                            fontWeight: CustomTheme.medium)),
                  ),
                  Icon(Icons.attach_file, color: Color(0xFFB8CADB))
                ],
              ),
            ),
          ),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.04),
        Text("Assesor 1 *",
            style: CustomTheme.body1(context,
                color: Colors.black, fontWeight: CustomTheme.semibold)),
        SizedBox(height: CustomTheme.screenHeight! * 0.01),
        InkWell(
          onTap: () => showAssessor1(context),
          child: Container(
            padding: EdgeInsets.all(6),
            width: CustomTheme.screenWidth! * 1,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                border: Border.all(width: 1, color: Color(0xFFB8CADB)),
                color: Colors.white),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(_selectedAssessor1.toString(),
                    style: CustomTheme.body1(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Icon(Icons.keyboard_arrow_down_outlined,
                    color: Color(0xFFB8CADB))
              ],
            ),
          ),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.04),
        Text("Assesor 2 *",
            style: CustomTheme.body1(context,
                color: Colors.black, fontWeight: CustomTheme.semibold)),
        SizedBox(height: CustomTheme.screenHeight! * 0.01),
        InkWell(
          onTap: () => showAssessor2(context),
          child: Container(
            padding: EdgeInsets.all(6),
            width: CustomTheme.screenWidth! * 1,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                border: Border.all(width: 1, color: Color(0xFFB8CADB)),
                color: Colors.white),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(_selectedAssessor2.toString(),
                    style: CustomTheme.body1(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Icon(Icons.keyboard_arrow_down_outlined,
                    color: Color(0xFFB8CADB))
              ],
            ),
          ),
        ),
      ],
    );
  }

  dialog(Response? statusCode) {
    return showDialog(
      context: context,
      builder: (context) {
        return Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
          child: statusCode!.statusCode != 201
              ? Container(
                  padding: EdgeInsets.symmetric(horizontal: 12),
                  height: CustomTheme.screenHeight! * 0.25,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "${widget.type} Leave" + " Failed",
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold),
                      ),
                      SizedBox(height: CustomTheme.screenHeight! * 0.01),
                      Text(
                        leaveSubmitRes!.errorMessage ?? "",
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium),
                      ),
                      SizedBox(height: CustomTheme.screenHeight! * 0.05),
                      ElevatedButton(
                          onPressed: () {
                            Navigator.pop(context);
                            Navigator.pop(context, 'update');
                          },
                          style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.symmetric(vertical: 15),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              minimumSize: Size(double.infinity, 0),
                              backgroundColor: Color(0xFFEB7D2E)),
                          child: Text(
                            "Ok",
                            style: CustomTheme.button(context,
                                color: Colors.white,
                                fontWeight: CustomTheme.medium),
                          ))
                    ],
                  ),
                )
              : Container(
                  padding: EdgeInsets.symmetric(horizontal: 12),
                  height: CustomTheme.screenHeight! * 0.5,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                          height: CustomTheme.screenHeight! * 0.27,
                          child: LottieAsset.success()),
                      Text(
                        "${widget.type} Leave" + " Success",
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold),
                      ),
                      SizedBox(height: CustomTheme.screenHeight! * 0.01),
                      Text(
                        "Yeah! You successfully " + widget.type,
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium),
                      ),
                      SizedBox(height: CustomTheme.screenHeight! * 0.05),
                      ElevatedButton(
                          onPressed: () {
                            Navigator.pop(context);
                            Navigator.pop(context, 'update');
                          },
                          style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.symmetric(vertical: 15),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              minimumSize: Size(double.infinity, 0),
                              backgroundColor: Color(0xFFEB7D2E)),
                          child: Text(
                            "Ok",
                            style: CustomTheme.button(context,
                                color: Colors.white,
                                fontWeight: CustomTheme.medium),
                          ))
                    ],
                  ),
                ),
        );
      },
    );
  }

  int daysBetween(DateTime from, DateTime to) {
    from = DateTime(from.year, from.month, from.day);
    to = DateTime(to.year, to.month, to.day);
    return (to.difference(from).inHours / 24).round();
  }
}
