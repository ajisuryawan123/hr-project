import 'package:flutter/material.dart';
import 'package:hr_project/styling_theme.dart';
import 'package:http/http.dart';
import 'dart:io';
import 'package:path_provider/path_provider.dart';

import '../../model/leave-model/leave_detail.dart';

class PreviewImagePage extends StatefulWidget {
  final LeaveDetailModel dataDetail;
  const PreviewImagePage({Key? key, required this.dataDetail})
      : super(key: key);

  @override
  State<PreviewImagePage> createState() => _PreviewImagePageState();
}

class _PreviewImagePageState extends State<PreviewImagePage> {
  String? imageData;
  bool _isLoad = false;

  downloadImage() async {
    setState(() {
      _isLoad = true;
    });
    //comment out the next two lines to prevent the device from getting
    // the image from the web in order to prove that the picture is
    // coming from the device instead of the web.
    var url = widget.dataDetail.data!.request!.attachmentUrl!; // <-- 1
    var response = await get(Uri.parse(url)); // <--2
    var documentDirectory = await getApplicationDocumentsDirectory();
    var firstPath = documentDirectory.path + "/images";
    var filePathAndName = documentDirectory.path + '/images/pic.jpg';
    //comment out the next three lines to prevent the image from being saved
    //to the device to show that it's coming from the internet
    await Directory(firstPath).create(recursive: true); // <-- 1
    File file2 = new File(filePathAndName); // <-- 2
    file2.writeAsBytesSync(response.bodyBytes); // <-- 3
    setState(() {
      imageData = filePathAndName;
      _isLoad = false;
    });
  }

  @override
  initState() {
    downloadImage();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.close, color: Color(0xFF2F80ED)),
            onPressed: () => Navigator.of(context).pop(),
          ),
          centerTitle: true,
          backgroundColor: Color(0xFFF2C94C),
          title: Text(
            widget.dataDetail.data!.request!.attachmentUrl!
                .replaceFirst("https://api.appfuxion.id/files/", ""),
            style: CustomTheme.body1(context,
                color: Colors.black, fontWeight: CustomTheme.semibold),
          ),
        ),
        body: _isLoad
            ? Center(child: CircularProgressIndicator())
            : Center(child: Image.file(File(imageData!))),
      ),
    );
  }
}
