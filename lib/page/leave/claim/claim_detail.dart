import 'package:flutter/material.dart';

import '../../../styling_theme.dart';
import '../../../widget/custom_appbar.dart';
import '../preview_image.dart';

class DetailClaim extends StatefulWidget {
  const DetailClaim({Key? key}) : super(key: key);

  @override
  State<DetailClaim> createState() => _DetailClaimState();
}

class _DetailClaimState extends State<DetailClaim> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      backgroundColor: Color(0xFFF6F9FF),
      appBar: CustomAppbar(title: "Claim Details"),
      body:
          // _isLoad
          //     ? Center(child: CircularProgressIndicator()) :
          SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              requestDetail(),
              SizedBox(height: CustomTheme.screenHeight! * 0.03),
              requestRespon(),
            ],
          ),
        ),
      ),
    ));
  }

  requestDetail() {
    return Container(
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8)),
          color: Colors.white),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Request Details",
              style: CustomTheme.body1(context,
                  color: Colors.black, fontWeight: CustomTheme.semibold)),
          SizedBox(height: CustomTheme.screenHeight! * 0.03),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Claim Name",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text("Meeting with Client",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Claim Type",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text("Business Meeting",
                    textAlign: TextAlign.end,
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          Container(
            // height: CustomTheme.screenHeight! * 0.1,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Claim Details",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Container(
                  width: 150,
                  child: Text("Expenses incurred during a productive business meeting with a potential client.",
                      textAlign: TextAlign.end,
                      style: CustomTheme.body2(context,
                          color: Colors.black, fontWeight: CustomTheme.semibold)),
                ),
              ],
            ),
          ),

          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Claim Date",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text("08 Aug 2023",
                    textAlign: TextAlign.end,
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Total Amount Claimed",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Container(
                  padding: EdgeInsets.all(8),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      color: Color(0xFFF2F4F9)),
                  child: Text("RM 2,000",
                      style: CustomTheme.caption(context,
                          color: Color(0xFF5981EA),
                          fontWeight: CustomTheme.medium)),
                ),
              ],
            ),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: CustomTheme.screenWidth! * 0.3,
                  child: Text("Attachment",
                      style: CustomTheme.body2(context,
                          color: Colors.black, fontWeight: CustomTheme.medium)),
                ),
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      // Navigator.push(
                      //     context,
                      //     MaterialPageRoute(
                      //         builder: (context) => PreviewImagePage(
                      //             dataDetail: leaveDetailRes!)));
                    },
                    child: Text(
                        "https://api.appfuxion.id/files/",
                        // leaveDetailRes!.data!.request!.attachmentUrl!
                        //     .replaceFirst(
                        //         "https://api.appfuxion.id/files/", ""),
                        textAlign: TextAlign.end,
                        style: CustomTheme.body2(context,
                            color: Color(0xFF2F80ED),
                            fontWeight: CustomTheme.semibold)),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  requestRespon() {
    return Container(
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8)),
          color: Colors.white),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Request Response",
              style: CustomTheme.body1(context,
                  color: Colors.black, fontWeight: CustomTheme.semibold)),
          SizedBox(height: CustomTheme.screenHeight! * 0.03),
          Container(
            height: CustomTheme.screenHeight! * 0.05,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("SPV Approval",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text("Argono Anggriawan",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(8)),
                color:
                Color(0xFFFFF4ED)
                // leaveDetailRes!.data!.assessors!.first!.status == "WAITING"
                //     ? Color(0xFFFFF4ED)
                //     : leaveDetailRes!.data!.assessors!.first!.status ==
                //     "DECLINED"
                //     ? Color(0xFFFFEEEC)
                //     : Color(0xFFE9F7EF)
            ),
            child: Text(
                "WAITING",
                // leaveDetailRes!.data!.assessors!.first!.status == "WAITING"
                //     ? "Waiting Response"
                //     : leaveDetailRes!.data!.assessors!.first!.status ==
                //     "DECLINED"
                //     ? "Declined"
                //     : "Approved",
                style: CustomTheme.caption(context,
                    color: Color(0xFFEB7D2E),
                    // leaveDetailRes!.data!.assessors!.first!.status ==
                    //     "WAITING"
                    //     ? Color(0xFFEB7D2E)
                    //     : leaveDetailRes!.data!.assessors!.first!.status ==
                    //     "DECLINED"
                    //     ? Color(0xFFFF5245)
                    //     : Color(0xFF27AE60),
                    fontWeight: CustomTheme.medium)),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.05,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Reason",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text("we are happy to grant you",
                    textAlign: TextAlign.end,
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          SizedBox(height: CustomTheme.screenHeight! * 0.03),
          Divider(
              color: Color(0xFFD8E4EE),
              height: 0,
              thickness: 1,
              endIndent: 20,
              indent: 20),

        ],
      ),
    );
  }

}
