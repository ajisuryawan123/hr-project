import 'package:dotted_border/dotted_border.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

import '../../../list_dummy.dart';
import '../../../styling_theme.dart';

class ClaimForm extends StatefulWidget {
  const ClaimForm({Key? key}) : super(key: key);

  @override
  State<ClaimForm> createState() => _ClaimFormState();
}

class _ClaimFormState extends State<ClaimForm> {
  DateRangePickerController _dateClaim = DateRangePickerController();
  String claimDate = "Choose Date";
  String itemClaimType = "B";
  String selectedClaimType = "Business Meeting";
  String itemSPVApprove = "A";
  String selectedSPVApprove = "Argono Anggriawan";
  String fileName = "Upload file";
  PlatformFile? _pickedImage;

  Future _selectImage() async {
    final result = await FilePicker.platform.pickFiles(
      allowMultiple: false,
      type: FileType.image,
    );

    if (result != null) {
      setState(() {
        _pickedImage = result.files.first;
        fileName = _pickedImage!.name;
      });
      print("NAME: " + _pickedImage!.name);
      print("BYTES: " + _pickedImage!.bytes.toString());
      print("SIZE: " + _pickedImage!.size.toString());
      print("EXTENSION: " + _pickedImage!.extension.toString());
      print("PATH: " + _pickedImage!.path.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFFF6F9FF),
        appBar: appBarwidget(),
        bottomSheet: customSheet(),
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.fromLTRB(12, 25, 12, 12),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                form1(),
                SizedBox(height: CustomTheme.screenHeight! * 0.04),
                form2(),
                SizedBox(height: CustomTheme.screenHeight! * 0.15),
              ],
            ),
          ),
        ),
      ),
    );
  }

  appBarwidget() {
    return AppBar(
      leading: IconButton(
        icon: Icon(Icons.close, color: Color(0xFF2F80ED)),
        onPressed: () => Navigator.of(context).pop(),
      ),
      centerTitle: true,
      backgroundColor: Color(0xFFF2C94C),
      title: Text(
        "Form Claim",
        style: CustomTheme.subtitle(context,
            color: Colors.black, fontWeight: CustomTheme.semibold),
      ),
    );
  }

  customSheet() {
    return Container(
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3),
          ),
        ],
      ),
      child: ElevatedButton(
          onPressed: () {
            dialog();
          },
          style: ElevatedButton.styleFrom(
              padding: EdgeInsets.symmetric(vertical: 12),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              minimumSize: Size(double.infinity, 0),
              backgroundColor: Color(0xFFEB7D2E)),
          child: Text(
            "Submit",
            style: CustomTheme.button(context,
                color: Colors.white, fontWeight: CustomTheme.medium),
          )),
    );
  }

  form1() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Claim Name *",
          style: CustomTheme.body1(context,
              color: Colors.black, fontWeight: CustomTheme.semibold),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.02),
        Container(
          height: CustomTheme.screenHeight! * 0.06,
          child: TextFormField(
            style: CustomTheme.body2(context,
                color: Colors.black, fontWeight: CustomTheme.medium),
            decoration: InputDecoration(
              floatingLabelBehavior: FloatingLabelBehavior.never,
              contentPadding:
                  EdgeInsets.symmetric(vertical: 30, horizontal: 10),
              filled: true,
              fillColor: Colors.white,
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Color(0xFFB8CADB))),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Color(0xFFB8CADB))),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Color(0xFFB8CADB))),
            ),
          ),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.04),
        Text(
          "Start Date *",
          style: CustomTheme.body1(context,
              color: Colors.black, fontWeight: CustomTheme.semibold),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.01),
        GestureDetector(
          onTap: () => showDateStart(context),
          child: Container(
            padding: EdgeInsets.all(6),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                border: Border.all(width: 1, color: Color(0xFFB8CADB)),
                color: Colors.white),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(claimDate,
                    style: CustomTheme.body1(context,
                        color: claimDate == "Choose Date"
                            ? Color(0xFFBDBDBD)
                            : Colors.black,
                        fontWeight: CustomTheme.medium)),
                IconAsset.calender_grey()
              ],
            ),
          ),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.04),
        Text(
          "Claim Type *",
          style: CustomTheme.body1(context,
              color: Colors.black, fontWeight: CustomTheme.semibold),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.01),
        InkWell(
          onTap: () => showClaimType(context),
          child: Container(
            padding: EdgeInsets.all(6),
            width: CustomTheme.screenWidth! * 1,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                border: Border.all(width: 1, color: Color(0xFFB8CADB)),
                color: Colors.white),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(selectedClaimType,
                    style: CustomTheme.body1(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Icon(Icons.keyboard_arrow_down_outlined,
                    color: Color(0xFFB8CADB))
              ],
            ),
          ),
        ),
      ],
    );
  }

  void showDateStart(context) {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(12)),
        ),
        context: context,
        builder: (context) {
          return Container(
            padding: EdgeInsets.all(12),
            height: CustomTheme.screenHeight! * 0.5,
            child: SfDateRangePicker(
              monthViewSettings: DateRangePickerMonthViewSettings(
                  viewHeaderStyle: DateRangePickerViewHeaderStyle(
                      textStyle: CustomTheme.body2(context,
                          color: Color(0xFFEB7D2E),
                          fontWeight: CustomTheme.medium))),
              selectionColor: Color(0xFFEB7D2E),
              startRangeSelectionColor: Color(0xFFEB7D2E),
              endRangeSelectionColor: Color(0xFFEB7D2E),
              rangeSelectionColor: Color(0xFFFFA15E),
              rangeTextStyle: CustomTheme.body2(context,
                  color: Colors.white, fontWeight: CustomTheme.medium),
              selectionTextStyle: CustomTheme.body2(context,
                  color: Colors.white, fontWeight: CustomTheme.medium),
              headerStyle: DateRangePickerHeaderStyle(
                  textAlign: TextAlign.center,
                  textStyle: CustomTheme.body1(context,
                      color: Colors.black, fontWeight: CustomTheme.semibold)),
              selectionMode: DateRangePickerSelectionMode.single,
              controller: _dateClaim,
              showActionButtons: true,
              onSubmit: (value) {
                if (value is PickerDateRange) {
                  final DateTime rangeStartDate = value.startDate!;
                  final DateTime rangeEndDate = value.endDate!;
                } else if (value is DateTime) {
                  final DateTime selectedDate = value;
                  setState(() {
                    claimDate =
                        DateFormat('dd MMM yyyy', 'en_US').format(selectedDate);
                  });
                } else if (value is List<DateTime>) {
                  final List<DateTime> selectedDates = value;
                } else if (value is List<PickerDateRange>) {
                  final List<PickerDateRange> selectedRanges = value;
                }
                Navigator.pop(context);
              },
              onCancel: () {
                Navigator.pop(context);
              },
            ),
          );
        });
  }

  void showClaimType(context) {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(12)),
        ),
        context: context,
        builder: (context) {
          return Container(
            padding: EdgeInsets.fromLTRB(12, 12, 12, 0),
            height: CustomTheme.screenHeight! * 0.75,
            child: Column(
              children: [
                Text("Select Claim Type",
                    style: CustomTheme.body1(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
                SizedBox(height: CustomTheme.screenHeight! * 0.03),
                Container(
                  height: CustomTheme.screenHeight! * 0.07,
                  child: TextFormField(
                    style: CustomTheme.body1(context,
                        color: Colors.black, fontWeight: CustomTheme.medium),
                    decoration: InputDecoration(
                      floatingLabelBehavior: FloatingLabelBehavior.never,
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 30, horizontal: 10),
                      labelText: "Search claim",
                      labelStyle: CustomTheme.body1(context,
                          color: Color(0xFFBDBDBD),
                          fontWeight: CustomTheme.medium),
                      filled: true,
                      fillColor: Color(0xFFF2F4F9),
                      prefixIcon: Icon(Icons.search, color: Color(0xFFE0E0E0)),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                    ),
                  ),
                ),
                SizedBox(height: CustomTheme.screenHeight! * 0.03),
                ListView.builder(
                    shrinkWrap: true,
                    itemCount: ListDummy.listClaimType.length,
                    itemBuilder: (context, index) {
                      return RadioListTile(
                        title: Text(ListDummy.listClaimType[index].text,
                            style: CustomTheme.body1(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.medium)),
                        activeColor: Color(0xFFEB7D2E),
                        value: ListDummy.listClaimType[index].valueRadio,
                        groupValue: itemClaimType,
                        onChanged: (value) {
                          setState(() {
                            itemClaimType = value.toString();
                            selectedClaimType =
                                ListDummy.listClaimType[index].text;
                          });
                          Navigator.of(context).pop();
                        },
                      );
                    }),
              ],
            ),
          );
        });
  }

  void showSPVApproval(context) {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(12)),
        ),
        context: context,
        builder: (context) {
          return Container(
            padding: EdgeInsets.fromLTRB(12, 12, 12, 0),
            height: CustomTheme.screenHeight! * 0.75,
            child: Column(
              children: [
                Text("Select SPV Approval",
                    style: CustomTheme.body1(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
                SizedBox(height: CustomTheme.screenHeight! * 0.03),
                Container(
                  height: CustomTheme.screenHeight! * 0.07,
                  child: TextFormField(
                    style: CustomTheme.body1(context,
                        color: Colors.black, fontWeight: CustomTheme.medium),
                    decoration: InputDecoration(
                      floatingLabelBehavior: FloatingLabelBehavior.never,
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 30, horizontal: 10),
                      labelText: "Search assessor",
                      labelStyle: CustomTheme.body1(context,
                          color: Color(0xFFBDBDBD),
                          fontWeight: CustomTheme.medium),
                      filled: true,
                      fillColor: Color(0xFFF2F4F9),
                      prefixIcon: Icon(Icons.search, color: Color(0xFFE0E0E0)),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                    ),
                  ),
                ),
                SizedBox(height: CustomTheme.screenHeight! * 0.03),
                Expanded(
                  child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: ListDummy.listSPVApprove.length,
                      itemBuilder: (context, index) {
                        return RadioListTile(
                          title: Text(ListDummy.listSPVApprove[index].text,
                              style: CustomTheme.body1(context,
                                  color: Colors.black,
                                  fontWeight: CustomTheme.medium)),
                          activeColor: Color(0xFFEB7D2E),
                          value: ListDummy.listSPVApprove[index].valueRadio,
                          groupValue: itemSPVApprove,
                          onChanged: (value) {
                            setState(() {
                              itemSPVApprove = value.toString();
                              selectedSPVApprove =
                                  ListDummy.listSPVApprove[index].text;
                            });
                            Navigator.of(context).pop();
                          },
                        );
                      }),
                ),
              ],
            ),
          );
        });
  }

  form2() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Claim Details *",
          style: CustomTheme.body1(context,
              color: Colors.black, fontWeight: CustomTheme.semibold),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.02),
        TextFormField(
          keyboardType: TextInputType.multiline,
          maxLines: null,
          style: CustomTheme.body1(context,
              color: Colors.black, fontWeight: CustomTheme.medium),
          decoration: InputDecoration(
            floatingLabelBehavior: FloatingLabelBehavior.never,
            filled: true,
            fillColor: Colors.white,
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(color: Color(0xFFB8CADB))),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(color: Color(0xFFB8CADB))),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(color: Color(0xFFB8CADB))),
          ),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.04),
        Text("Attachment",
            style: CustomTheme.body1(context,
                color: Colors.black, fontWeight: CustomTheme.semibold)),
        SizedBox(height: CustomTheme.screenHeight! * 0.01),
        InkWell(
          onTap: _selectImage,
          child: DottedBorder(
            color: Color(0xFFB8CADB),
            dashPattern: [8, 4],
            radius: Radius.circular(5),
            child: Container(
              padding: EdgeInsets.all(6),
              width: CustomTheme.screenWidth! * 1,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  color: Colors.white),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width * 0.7,
                    child: Text(fileName,
                        style: CustomTheme.body1(context,
                            color: fileName == "Upload file"
                                ? Color(0xFFBDBDBD)
                                : Colors.black,
                            fontWeight: CustomTheme.medium)),
                  ),
                  Icon(Icons.attach_file, color: Color(0xFFB8CADB))
                ],
              ),
            ),
          ),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.04),
        Text(
          "Total Amount Claimed *",
          style: CustomTheme.body1(context,
              color: Colors.black, fontWeight: CustomTheme.semibold),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.02),
        Container(
          height: CustomTheme.screenHeight! * 0.06,
          child: TextFormField(
            style: CustomTheme.body2(context,
                color: Colors.black, fontWeight: CustomTheme.medium),
            decoration: InputDecoration(
              floatingLabelBehavior: FloatingLabelBehavior.never,
              contentPadding:
                  EdgeInsets.symmetric(vertical: 30, horizontal: 10),
              filled: true,
              fillColor: Colors.white,
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Color(0xFFB8CADB))),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Color(0xFFB8CADB))),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Color(0xFFB8CADB))),
            ),
          ),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.04),
        Text(
          "SPV Approval *",
          style: CustomTheme.body1(context,
              color: Colors.black, fontWeight: CustomTheme.semibold),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.01),
        InkWell(
          onTap: () => showSPVApproval(context),
          child: Container(
            padding: EdgeInsets.all(6),
            width: CustomTheme.screenWidth! * 1,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                border: Border.all(width: 1, color: Color(0xFFB8CADB)),
                color: Colors.white),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(selectedSPVApprove,
                    style: CustomTheme.body1(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Icon(Icons.keyboard_arrow_down_outlined,
                    color: Color(0xFFB8CADB))
              ],
            ),
          ),
        ),
      ],
    );
  }

  dialog() {
    return showDialog(
      context: context,
      builder: (context) {
        return Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
            height: CustomTheme.screenHeight! * 0.6,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                    height: CustomTheme.screenHeight! * 0.3,
                    child: LottieAsset.success()),
                Text(
                  "Request Sent",
                  style: CustomTheme.body1(context,
                      color: Colors.black, fontWeight: CustomTheme.semibold),
                ),
                SizedBox(height: CustomTheme.screenHeight! * 0.01),
                Text(
                  "Yeah! Your claim request has been sent.",
                  textAlign: TextAlign.center,
                  style: CustomTheme.body1(context,
                      color: Colors.black, fontWeight: CustomTheme.medium),
                ),
                SizedBox(height: CustomTheme.screenHeight! * 0.05),
                ElevatedButton(
                    onPressed: () {
                      Navigator.pop(context);
                      Navigator.pop(context);
                    },
                    style: ElevatedButton.styleFrom(
                        padding: EdgeInsets.symmetric(vertical: 15),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        minimumSize: Size(double.infinity, 0),
                        backgroundColor: Color(0xFFEB7D2E)),
                    child: Text(
                      "Ok",
                      style: CustomTheme.button(context,
                          color: Colors.white, fontWeight: CustomTheme.medium),
                    ))
              ],
            ),
          ),
        );
      },
    );
  }
}
