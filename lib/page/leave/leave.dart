import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:grouped_list/grouped_list.dart';
import 'package:hr_project/config/API%20config/leave_api/leave_history.dart';
import 'package:hr_project/list_dummy.dart';
import 'package:hr_project/page/leave/overtime/overtime_form.dart';
import 'package:hr_project/page/leave/unpaid_leave.dart';
import 'package:hr_project/styling_theme.dart';
import 'package:hr_project/utils/unauthorized.dart';
import 'package:hr_project/widget/claim_item_list.dart';
import 'package:hr_project/widget/clock_in_out_button.dart';
import 'package:hr_project/widget/custom_appbar.dart';
import 'package:hr_project/widget/leave_item_list.dart';
import 'package:hr_project/widget/stat_percent_header.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../dummy_data.dart';
import '../../model/leave-model/leave_history.dart';
import '../../widget/overtime_item_list.dart';
import 'claim/claim_form.dart';

class LeavePage extends StatefulWidget {
  const LeavePage({Key? key}) : super(key: key);

  @override
  State<LeavePage> createState() => _LeavePageState();
}

class _LeavePageState extends State<LeavePage> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  bool _isLoad = false;
  LeaveHistoryModel? leaveHistoryRes;
  List<Content>? contentHistory;
  String itemFormType = "A";
  String selectedFormType = "Leave Request";
  String unpaidLeave = "";
  String totalPaid = "";
  String remainPaid = "";

  leaveHistory() async {
    setState(() {
      _isLoad = true;
    });
    final SharedPreferences prefs = await _prefs;
    var res = await leaveHistroy_API(token: prefs.getString("accessToken")!);
    leaveHistoryRes = await LeaveHistoryModel.fromJson(json.decode(res.body));
    if (leaveHistoryRes!.errorMessage == "invalid authorization") {
      unauthorized(context);
    }
    setState(() {
      contentHistory = leaveHistoryRes!.data!.leavePagination!.content;
      unpaidLeave = leaveHistoryRes!.data!.leave!.unpaidLeaveTaken.toString();
      totalPaid = leaveHistoryRes!.data!.leave!.total.toString();
      remainPaid = leaveHistoryRes!.data!.leave!.remains.toString();
      _isLoad = false;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    leaveHistory();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFFF6F9FF),
        appBar: CustomAppbar(title: "Form Request"),
        body: _isLoad
            ? Center(child: CircularProgressIndicator())
            : itemFormType == "A"
                ? leaveRequest()
                : itemFormType == "B"
                    ? claimRequest()
                    : overtimeRequest(),
      ),
    );
  }

  leaveRequest() {
    return Column(
      children: [
        dropViewPage(),
        Padding(
          padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              topLeaveRequest(),
              SizedBox(height: CustomTheme.screenHeight! * 0.03),
              leave(),
            ],
          ),
        ),
      ],
    );
  }

  topLeaveRequest() {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8)),
          color: Colors.white),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(8),
            decoration: BoxDecoration(
              border: Border(
                  bottom: BorderSide(width: 1, color: Color(0xFFB8CADB))),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                CustomHeaderButton(
                    color: Color(0xFF5981EA),
                    icon: IconAsset.calender_white(),
                    text: "Unpaid\nLeave",
                    colorText: Colors.white,
                    onPress: () async {
                      await Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  UnpaidPaidLeave(type: "Unpaid")));

                      await leaveHistory();
                    },
                    customWidth: CustomTheme.screenWidth! * 0.4,
                    isCenter: false),
                CustomHeaderButton(
                    color: remainPaid == "0"
                        ? Color(0xFFD8E4EE)
                        : Color(0xFF5981EA),
                    icon: remainPaid == "0"
                        ? IconAsset.calender_grey()
                        : IconAsset.calender_white(),
                    text: "Paid\nLeave",
                    colorText:
                        remainPaid == "0" ? Color(0xFFB8CADB) : Colors.white,
                    onPress: remainPaid == "0"
                        ? null
                        : () async {
                            final result = await Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) =>
                                      UnpaidPaidLeave(type: "Paid"),
                                ));
                            print('result: ' + result);
                            await leaveHistory();
                          },
                    customWidth: CustomTheme.screenWidth! * 0.4,
                    isCenter: false),
              ],
            ),
          ),
          Row(
            children: [
              StatHeader(
                  isSecond: false,
                  pointValue: "",
                  titlePoint: "",
                  percentValue: unpaidLeave,
                  titleValue: "Unpaid Leave"),
              StatHeader(
                  isSecond: true,
                  pointValue: "${remainPaid}/${totalPaid}",
                  titlePoint: "Paid Leave Taken",
                  percentValue: "",
                  titleValue: ""),
            ],
          )
        ],
      ),
    );
  }

  leave() {
    return Container(
      height: CustomTheme.screenHeight! * 0.45,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Leave History",
              style: CustomTheme.body1(context,
                  color: Colors.black, fontWeight: CustomTheme.semibold)),
          SizedBox(height: CustomTheme.screenHeight! * 0.02),
          Expanded(
            child: ListView.separated(
              shrinkWrap: true,
              itemCount: contentHistory!.length,
              separatorBuilder: (BuildContext context, int index) =>
                  SizedBox(height: CustomTheme.screenHeight! * 0.03),
              itemBuilder: (BuildContext context, int index) {
                return LeaveItem(item: contentHistory![index]);
              },
            ),
          )
        ],
      ),
    );
  }

  claim() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("Claim History",
            style: CustomTheme.body1(context,
                color: Colors.black, fontWeight: CustomTheme.semibold)),
        SizedBox(height: CustomTheme.screenHeight! * 0.02),
        Container(
          height: MediaQuery.of(context).size.height * 0.57,
          child: ListView.separated(
            shrinkWrap: true,
            itemCount: ListDummy.listClaim.length,
            separatorBuilder: (BuildContext context, int index) =>
                SizedBox(height: CustomTheme.screenHeight! * 0.03),
            itemBuilder: (BuildContext context, int index) {
              return ClaimItem(item: ListDummy.listClaim[index]);
            },
          ),
        )
      ],
    );
  }

  dropViewPage() {
    return Container(
      padding: EdgeInsets.all(15),
      color: Colors.white,
      child: InkWell(
        onTap: () => showFormType(context),
        child: Container(
          padding: EdgeInsets.all(6),
          width: CustomTheme.screenWidth! * 1,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(5)),
              border: Border.all(width: 1, color: Color(0xFFB8CADB)),
              color: Colors.white),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(selectedFormType,
                  style: CustomTheme.body1(context,
                      color: Colors.black, fontWeight: CustomTheme.medium)),
              Icon(Icons.keyboard_arrow_down_outlined, color: Color(0xFFB8CADB))
            ],
          ),
        ),
      ),
    );
  }

  void showFormType(context) {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(12)),
        ),
        context: context,
        builder: (context) {
          return Container(
            padding: EdgeInsets.fromLTRB(12, 12, 12, 0),
            height: CustomTheme.screenHeight! * 0.75,
            child: Column(
              children: [
                Text("Select Form Request",
                    style: CustomTheme.body1(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
                SizedBox(height: CustomTheme.screenHeight! * 0.03),
                Container(
                  height: CustomTheme.screenHeight! * 0.07,
                  child: TextFormField(
                    style: CustomTheme.body1(context,
                        color: Colors.black, fontWeight: CustomTheme.medium),
                    decoration: InputDecoration(
                      floatingLabelBehavior: FloatingLabelBehavior.never,
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 30, horizontal: 10),
                      labelText: "Search form",
                      labelStyle: CustomTheme.body1(context,
                          color: Color(0xFFBDBDBD),
                          fontWeight: CustomTheme.medium),
                      filled: true,
                      fillColor: Color(0xFFF2F4F9),
                      prefixIcon: Icon(Icons.search, color: Color(0xFFE0E0E0)),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                    ),
                  ),
                ),
                SizedBox(height: CustomTheme.screenHeight! * 0.03),
                ListView.builder(
                    shrinkWrap: true,
                    itemCount: ListDummy.listFormType.length,
                    itemBuilder: (context, index) {
                      return RadioListTile(
                        title: Text(ListDummy.listFormType[index].text,
                            style: CustomTheme.body1(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.medium)),
                        activeColor: Color(0xFFEB7D2E),
                        value: ListDummy.listFormType[index].valueRadio,
                        groupValue: itemFormType,
                        onChanged: (value) {
                          setState(() {
                            itemFormType = value.toString();
                            selectedFormType =
                                ListDummy.listFormType[index].text;
                          });
                          Navigator.of(context).pop();
                        },
                      );
                    }),
              ],
            ),
          );
        });
  }

  claimRequest() {
    return Column(
      children: [
        dropViewPage(),
        Padding(
          padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              topClaim(),
              SizedBox(height: CustomTheme.screenHeight! * 0.03),
              claim(),
            ],
          ),
        ),
      ],
    );
  }

  topClaim() {
    return CustomHeaderButton(
        color: Color(0xFF5981EA),
        icon: IconAsset.calender_white(),
        text: "Claim Request",
        colorText: Colors.white,
        onPress: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => ClaimForm()));
        },
        customWidth: CustomTheme.screenWidth! * 1,
        isCenter: true);
  }

  overtimeRequest() {
    return Column(
      children: [
        dropViewPage(),
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            topOvertime(),
            SizedBox(height: CustomTheme.screenHeight! * 0.02),
            overtime(),
          ],
        ),
      ],
    );
  }

  topOvertime() {
    return Padding(
      padding: EdgeInsets.fromLTRB(15, 15, 15, 0),
      child: Container(
        padding: EdgeInsets.all(12),
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CustomHeaderButton(
                color: Color(0xFF5981EA),
                icon: IconAsset.timer(),
                text: "Overtime",
                colorText: Colors.white,
                onPress: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => OvertimeForm()));
                },
                customWidth: CustomTheme.screenWidth! * 1,
                isCenter: true),
            SizedBox(height: CustomTheme.screenHeight! * 0.02),
            Text("200 Hours 20 Mins",
                style: CustomTheme.headline4(context,
                    color: Colors.black, fontWeight: CustomTheme.semibold)),
            SizedBox(height: CustomTheme.screenHeight! * 0.01),
            Text("Total Over Time This Month",
                style: CustomTheme.body1(context,
                    color: Color(0xFF757575),
                    fontWeight: CustomTheme.semibold)),
          ],
        ),
      ),
    );
  }

  overtime() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(left: 20),
          child: Text("Claim History",
              style: CustomTheme.body1(context,
                  color: Colors.black, fontWeight: CustomTheme.semibold)),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.02),
        Container(
          height: MediaQuery.of(context).size.height * 0.45,
          child: GroupedListView<DummyListOvertime, DateTime>(
            shrinkWrap: true,
            elements: ListDummy.listOvertime,
            groupBy: (element) =>
                DateTime(element.datetime.year, element.datetime.month),
            groupComparator: (value1, value2) => value2.compareTo(value1),
            itemComparator:
                (DummyListOvertime item1, DummyListOvertime item2) =>
                    item1.datetime.compareTo(item2.datetime),
            order: GroupedListOrder.ASC,
            useStickyGroupSeparators: true,
            groupSeparatorBuilder: (value) => groupSeperatorItem(value),
            itemBuilder: (BuildContext context, DummyListOvertime element) =>
                groupItem(context, element),
          ),
        )
      ],
    );
  }

  groupSeperatorItem(DateTime value) {
    return Container(
      width: double.infinity,
      color: Color(0xFFE7F1FA),
      padding: EdgeInsets.all(12),
      child: Text(
        // '${value.month} ${value.year}',
        DateFormat('MMM yyyy', 'en_US').format(value),
        style: CustomTheme.body1(context,
            color: Colors.black, fontWeight: CustomTheme.semibold),
      ),
    );
  }

  groupItem(BuildContext context, DummyListOvertime element) {
    return OvertimeItem(item: element);
  }
}
