import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hr_project/config/API%20config/attendances-api/attendance_log.dart';
import 'package:hr_project/model/attendances-model/attendances_log.dart';
import 'package:hr_project/utils/unauthorized.dart';
import 'package:hr_project/widget/attend_detail_widget.dart';
import 'package:moment_dart/moment_dart.dart';

class NoRecordPage extends StatefulWidget {
  final DateTime startDate, endDate;
  const NoRecordPage({Key? key, required this.startDate, required this.endDate})
      : super(key: key);

  @override
  State<NoRecordPage> createState() => _NoRecordPageState();
}

class _NoRecordPageState extends State<NoRecordPage> {
  ScrollController _scrollController = ScrollController();

  int _pageNo = 0;
  bool _isLastPage = false;
  bool _isLoading = false;
  bool _isLoadingMore = false;

  List<Content> contents = [];
  AttendanceLogModel? attendanceLogRes;

  @override
  void initState() {
    super.initState();

    _scrollController.addListener(_scrollListener);

    fetchLog(_pageNo);
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _scrollListener() async {
    if (!_isLoadingMore && !_isLastPage) {
      if (_scrollController.position.extentAfter < 50) {
        setState(() {
          _pageNo += 1;
          _isLoadingMore = true;
        });

        AttendanceLogModel? res = await getLog(_pageNo);

        setState(() {
          contents.addAll(res?.data?.attendancePagination.content ?? []);
          _isLastPage = res?.data?.attendancePagination.last ?? false;
          _isLoadingMore = false;
        });
      }
    }
  }

  getLog(int pageNo) async {
    var res = await attendanceLog_API(
      pageNo: pageNo,
      startDate: Moment(widget.startDate).format('YYYY-MM-DD'),
      endDate: Moment(widget.endDate).format('YYYY-MM-DD'),
      attendanceStatus: 'NORECORD',
    );

    attendanceLogRes = AttendanceLogModel.fromJson(json.decode(res.body));

    if (res.statusCode == 401) {
      unauthorized(context);
    }

    return attendanceLogRes;
  }

  fetchLog(int pageNo) async {
    setState(() {
      _isLoading = true;
    });

    AttendanceLogModel? res = await getLog(pageNo);

    if (res?.errorCode == '200401') {
      unauthorized(context);
    }

    setState(() {
      contents = res?.data?.attendancePagination.content ?? [];
      _isLastPage = res?.data?.attendancePagination.last ?? false;
    });

    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: _isLoading
          ? Center(child: CircularProgressIndicator())
          : Container(
              color: Color(0xFFF6F9FF),
              child: ListView.builder(
                controller: _scrollController,
                itemCount: contents.length,
                itemBuilder: (BuildContext context, int index) {
                  return AttendDetailItem(element: contents[index]);
                },
              ),
              // GroupedListView<DummyGroupedAttendance, DateTime>(
              //   shrinkWrap: true,
              //   elements: ListDummy.listGroupAttend,
              //   groupBy: (element) =>
              //       DateTime(element.datetime.year, element.datetime.month),
              //   groupComparator: (value1, value2) => value2.compareTo(value1),
              //   itemComparator:
              //       (DummyGroupedAttendance item1, DummyGroupedAttendance item2) =>
              //           item1.datetime.compareTo(item2.datetime),
              //   order: GroupedListOrder.ASC,
              //   useStickyGroupSeparators: true,
              //   groupSeparatorBuilder: (value) => groupSeperatorItem(value),
              //   indexedItemBuilder: (BuildContext context,
              //           DummyGroupedAttendance element, int index) =>
              //       groupItem(context, element, index),
              // ),
            ),
    );

    // GroupedListView<DummyAttendDetail, DateTime>(
    //   shrinkWrap: true,
    //   elements: ListDummy.listOnTime,
    //   groupBy: (element) => DateTime(
    //       element.datetime.year, element.datetime.month, element.datetime.day),
    //   groupComparator: (value1, value2) => value2.compareTo(value1),
    //   itemComparator: (DummyAttendDetail item1, DummyAttendDetail item2) =>
    //       item1.datetime.compareTo(item2.datetime),
    //   order: GroupedListOrder.ASC,
    //   useStickyGroupSeparators: true,
    //   groupSeparatorBuilder: (value) => groupSeperatorItem(value),
    //   itemBuilder: (BuildContext context, DummyAttendDetail element) =>
    //       groupItem(context, element),
    // );
  }

  // groupSeperatorItem(DateTime value) {
  //   return Container(
  //     width: double.infinity,
  //     color: Color(0xFFE7F1FA),
  //     padding: EdgeInsets.all(12),
  //     child: Text(
  //       // '${value.month} ${value.year}',
  //       DateFormat('dd MMMM yyyy', 'en_US').format(value),
  //       style: CustomTheme.body1(context,
  //           color: Colors.black, fontWeight: CustomTheme.semibold),
  //     ),
  //   );
  // }

  // groupItem(BuildContext context, DummyAttendDetail element) {
  //   return AttendDetailItem(element: element);
  // }
}
