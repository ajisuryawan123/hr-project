import 'package:flutter/material.dart';
import 'package:hr_project/page/log/attendDetail/late.dart';
import 'package:hr_project/page/log/attendDetail/no_record.dart';
import 'package:hr_project/page/log/attendDetail/on_time.dart';
import 'package:hr_project/styling_theme.dart';

class AttendDetailPage extends StatefulWidget {
  final int initialIndexTab;
  final DateTime startDate, endDate;
  const AttendDetailPage(
      {Key? key,
      required this.initialIndexTab,
      required this.startDate,
      required this.endDate})
      : super(key: key);

  @override
  State<AttendDetailPage> createState() => _AttendDetailPageState();
}

class _AttendDetailPageState extends State<AttendDetailPage> {
  TabBar get _tabBar => TabBar(
        indicatorColor: Color(0xFF5981EA),
        indicatorWeight: 5,
        labelColor: Colors.black,
        labelStyle: CustomTheme.body1(
          context,
          color: Colors.black,
          fontWeight: CustomTheme.bold,
        ),
        unselectedLabelColor: Colors.black,
        unselectedLabelStyle: CustomTheme.body1(
          context,
          color: Colors.black,
          fontWeight: CustomTheme.regular,
        ),
        tabs: [
          Tab(text: "On Time"),
          Tab(text: "Late"),
          Tab(text: "No Record"),
        ],
      );

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: widget.initialIndexTab,
      length: 3,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Color(0xFFF6F9FF),
          appBar: custom_appbar(),
          body: TabBarView(
            children: [
              OnTimePage(
                startDate: widget.startDate,
                endDate: widget.endDate,
              ),
              LatePage(
                startDate: widget.startDate,
                endDate: widget.endDate,
              ),
              NoRecordPage(
                startDate: widget.startDate,
                endDate: widget.endDate,
              ),
            ],
          ),
        ),
      ),
    );
  }

  custom_appbar() {
    return AppBar(
      centerTitle: true,
      backgroundColor: Color(0xFFF2C94C),
      leading: IconButton(
        icon: Icon(Icons.close, color: Color(0xFF2F80ED)),
        onPressed: () => Navigator.of(context).pop(),
      ),
      title: Text(
        "Attendance Details",
        style: CustomTheme.subtitle(context,
            color: Colors.black, fontWeight: CustomTheme.semibold),
      ),
      bottom: PreferredSize(
        preferredSize: _tabBar.preferredSize,
        child: Material(
          color: Colors.white,
          child: _tabBar,
        ),
      ),
    );
  }
}
