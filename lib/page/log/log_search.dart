import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hr_project/config/API%20config/attendances-api/attendance_log.dart';
import 'package:hr_project/model/attendances-model/attendances_log.dart';
import 'package:hr_project/styling_theme.dart';
import 'package:hr_project/utils/datetime_format.dart';
import 'package:hr_project/utils/unauthorized.dart';
import 'package:moment_dart/moment_dart.dart';

class LogSearchPage extends StatefulWidget {
  final DateTime startDate;
  final DateTime endDate;
  const LogSearchPage({
    Key? key,
    required this.startDate,
    required this.endDate,
  }) : super(key: key);

  @override
  State<LogSearchPage> createState() => _LogSearchPageState();
}

class _LogSearchPageState extends State<LogSearchPage> {
  ScrollController _scrollController = ScrollController();
  TextEditingController _searchController = TextEditingController();

  String text =
      "Status log data need to be saved at the time of clock in Attendance for Log module to show the summary.";
  String? firstHalf;
  String? secondHalf;
  bool flag = true;
  int _pageNo = 0;
  bool _isLastPage = false;
  bool _isLoading = false;
  bool _isLoadingMore = false;

  Timer? _debounce;

  List<Content> contents = [];
  AttendanceLogModel? attendanceLogRes;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (text.length > 35) {
      firstHalf = text.substring(0, 35);
      secondHalf = text.substring(35, text.length);
    } else {
      firstHalf = text;
      secondHalf = "";
    }

    _scrollController.addListener(_scrollListener);

    fetchLog(_pageNo, '');
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _searchController.dispose();
    _debounce?.cancel();
    super.dispose();
  }

  void _scrollListener() async {
    if (!_isLoadingMore && !_isLastPage) {
      if (_scrollController.position.extentAfter < 50) {
        setState(() {
          _pageNo += 1;
          _isLoadingMore = true;
        });

        AttendanceLogModel? res = await getLog(_pageNo, _searchController.text);

        setState(() {
          contents.addAll(res?.data?.attendancePagination.content ?? []);
          _isLastPage = res?.data?.attendancePagination.last ?? false;
          _isLoadingMore = false;
        });
      }
    }
  }

  _onSearchChanged(String query) {
    setState(() {
      _isLoading = true;
    });

    if (_debounce?.isActive ?? false) _debounce?.cancel();

    _debounce = Timer(const Duration(milliseconds: 1500), () async {
      // do something with query
      setState(() {
        _pageNo = 0;
      });

      fetchLog(_pageNo, query);
      print('$_pageNo $query');
    });

    setState(() {
      _isLoading = false;
    });
  }

  getLog(int pageNo, String search) async {
    var res = await attendanceLog_API(
      pageNo: pageNo,
      startDate: _searchController.text.length > 0
          ? Moment.now().startOfMonth().format('YYYY-MM-DD')
          : Moment(widget.startDate).format('YYYY-MM-DD'),
      endDate: Moment(widget.endDate).format('YYYY-MM-DD'),
      searchByName: search,
    );

    attendanceLogRes = AttendanceLogModel.fromJson(json.decode(res.body));

    if (res.statusCode == 401) {
      unauthorized(context);
    }

    return attendanceLogRes;
  }

  fetchLog(int pageNo, String search) async {
    setState(() {
      _isLoading = true;
    });

    AttendanceLogModel? res = await getLog(pageNo, search);

    if (res?.errorCode == '200401') {
      unauthorized(context);
    }

    setState(() {
      contents = res?.data?.attendancePagination.content ?? [];
      _isLastPage = res?.data?.attendancePagination.last ?? false;
    });

    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        extendBodyBehindAppBar: true,
        backgroundColor: Color(0xFFF6F9FF),
        appBar: custom_appbar(),
        body: _isLoading
            ? Center(child: CircularProgressIndicator())
            : Container(
                height: CustomTheme.screenHeight,
                child: listlog(),
              ),
      ),
    );
  }

  custom_appbar() {
    return AppBar(
      centerTitle: true,
      backgroundColor: Color(0xFFF2C94C),
      leading: IconButton(
        icon: Icon(Icons.close, color: Color(0xFF2F80ED)),
        onPressed: () => Navigator.of(context).pop(),
      ),
      title: Container(
        height: CustomTheme.screenHeight! * 0.065,
        child: TextFormField(
          controller: _searchController,
          onChanged: _onSearchChanged,
          style: CustomTheme.body2(context,
              color: Colors.black, fontWeight: CustomTheme.medium),
          decoration: InputDecoration(
            floatingLabelBehavior: FloatingLabelBehavior.never,
            contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
            hintText: 'Search by name',
            filled: true,
            fillColor: Colors.white,
            prefixIcon: Icon(Icons.search, color: Color(0xFFE0E0E0)),
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(color: Colors.white)),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(color: Colors.white)),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(color: Colors.white)),
          ),
        ),
      ),
    );
  }

  listlog() {
    return contents.isNotEmpty
        ? ListView.builder(
            controller: _scrollController,
            itemCount: contents.length,
            itemBuilder: (BuildContext context, int index) {
              Content content = contents[index];
              return Column(
                children: [
                  SizedBox(
                      height: content.dateGroupLabel is String && index != 0
                          ? 10
                          : 0),
                  if (content.dateGroupLabel is String)
                    dateGroup(content.dateGroupLabel ?? ''),
                  Container(
                    margin: EdgeInsets.fromLTRB(20, 0, 20, 10),
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      border: Border.all(color: Color(0xFFD8E4EE)),
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      color: Colors.white,
                    ),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            // TODO: employee profile picture
                            IconAsset.avatar(1.25),
                            SizedBox(width: CustomTheme.screenWidth! * 0.03),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(content.employee.fullName,
                                          style: CustomTheme.body1(context,
                                              color: Colors.black,
                                              fontWeight: CustomTheme.bold)),
                                      content.attendanceStatus ==
                                              AttendanceStatus.NORECORD
                                          ? Container(
                                              padding: EdgeInsets.all(8),
                                              child: Text(' ',
                                                  style: CustomTheme.overline(
                                                      context,
                                                      color: Colors.black,
                                                      fontWeight:
                                                          CustomTheme.regular)),
                                            )
                                          : Container(
                                              padding: EdgeInsets.all(8),
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(8)),
                                                  color: Color(0xFFEEEFF1)),
                                              child: Text(
                                                  content.attendanceStatus ==
                                                          AttendanceStatus
                                                              .ONTIME
                                                      ? 'On Time'
                                                      : content.attendanceStatus ==
                                                              AttendanceStatus
                                                                  .LATE
                                                          ? 'Late'
                                                          : 'Leave',
                                                  style: CustomTheme.overline(
                                                      context,
                                                      color: Colors.black,
                                                      fontWeight:
                                                          CustomTheme.regular)),
                                            )
                                    ],
                                  ),
                                  clockInOut(content.clockIn, content.clockOut),
                                ],
                              ),
                            )
                          ],
                        ),
                        SizedBox(height: CustomTheme.screenHeight! * 0.01),
                        Container(
                            width: CustomTheme.screenWidth! * 1,
                            padding: EdgeInsets.symmetric(
                                horizontal: 8, vertical: 15),
                            decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8)),
                                border: Border.all(
                                    width: 1, color: Color(0xFFD8E4EE)),
                                color: Color(0xFFF3F6F9)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                    content.attendanceStatus ==
                                            AttendanceStatus.NORECORD
                                        ? '-'
                                        : content.project.projectName,
                                    style: CustomTheme.body1(context,
                                        color: Colors.black,
                                        fontWeight: CustomTheme.semibold)),
                                SizedBox(height: 10),
                                Text(
                                    content.attendanceStatus ==
                                            AttendanceStatus.NORECORD
                                        ? '-'
                                        : '${content.dailyTask}',
                                    style: CustomTheme.body1(context,
                                        color: Color(0xFF828282),
                                        fontWeight: CustomTheme.medium)),
                              ],
                            )),
                      ],
                    ),
                  ),
                ],
              );
            },
          )
        : Center(child: Text('No employee found'));
  }

  dateGroup(String dateGroupLabel) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 12),
      margin: EdgeInsets.only(bottom: 20),
      width: CustomTheme.screenWidth,
      decoration: BoxDecoration(color: Color(0xFFE7F1FA)),
      child: Text(
        dateGroupLabel,
        style: CustomTheme.body1(context,
            color: Color(0xFF333333), fontWeight: CustomTheme.bold),
      ),
    );
  }

  clockInOut(DateTime? clockIn, DateTime? clockOut) {
    return Row(
      children: [
        Expanded(
          flex: 3,
          child: Row(
            children: [
              IconAsset.door_enter_green(),
              SizedBox(width: CustomTheme.screenWidth! * 0.02),
              Text(
                datetime_format(clockIn, 'hh:mm A'),
                style: CustomTheme.body2(
                  context,
                  color: Colors.black,
                  fontWeight: CustomTheme.semibold,
                ),
              ),
            ],
          ),
        ),
        Expanded(
          flex: 2,
          child: Row(
            children: [
              IconAsset.door_exit_red(),
              SizedBox(width: CustomTheme.screenWidth! * 0.02),
              Text(
                datetime_format(clockOut, 'hh:mm A'),
                style: CustomTheme.body2(
                  context,
                  color: Colors.black,
                  fontWeight: CustomTheme.semibold,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  // groupSeperatorItem(DateTime value) {
  //   return Container(
  //     width: double.infinity,
  //     color: Color(0xFFE7F1FA),
  //     padding: EdgeInsets.all(12),
  //     child: Text(
  //       // '${value.month} ${value.year}',
  //       DateFormat('dd MMMM yyyy', 'en_US').format(value),
  //       style: CustomTheme.body1(context,
  //           color: Colors.black, fontWeight: CustomTheme.semibold),
  //     ),
  //   );
  // }

  // groupItem(BuildContext context, DummyGroupedLog element, int index) {
  //   return index == 0
  //       ? Container(
  //           margin: EdgeInsets.fromLTRB(25, 12, 25, 12),
  //           padding: EdgeInsets.fromLTRB(12, 0, 12, 12),
  //           decoration: BoxDecoration(
  //               borderRadius: BorderRadius.all(Radius.circular(8)),
  //               color: Colors.white),
  //           child: Column(
  //             children: [
  //               Row(
  //                 children: [
  //                   Container(
  //                       decoration: BoxDecoration(shape: BoxShape.circle),
  //                       child: element.avatar),
  //                   SizedBox(width: CustomTheme.screenWidth! * 0.03),
  //                   Expanded(
  //                     child: Column(
  //                       crossAxisAlignment: CrossAxisAlignment.start,
  //                       children: [
  //                         Row(
  //                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //                           children: [
  //                             Text(element.name,
  //                                 style: CustomTheme.body1(context,
  //                                     color: Colors.black,
  //                                     fontWeight: CustomTheme.semibold)),
  //                             Container(
  //                               padding: EdgeInsets.all(8),
  //                               decoration: BoxDecoration(
  //                                   borderRadius:
  //                                       BorderRadius.all(Radius.circular(8)),
  //                                   color: Color(0xFFEEEFF1)),
  //                               child: Text(element.status,
  //                                   style: CustomTheme.caption(context,
  //                                       color: Colors.black,
  //                                       fontWeight: CustomTheme.medium)),
  //                             )
  //                           ],
  //                         ),
  //                         Row(
  //                           children: [
  //                             IconAsset.door_enter_green(),
  //                             SizedBox(width: CustomTheme.screenWidth! * 0.02),
  //                             Text(element.clock_in,
  //                                 style: CustomTheme.body2(context,
  //                                     color: Colors.black,
  //                                     fontWeight: CustomTheme.medium)),
  //                             SizedBox(width: CustomTheme.screenWidth! * 0.13),
  //                             IconAsset.door_exit_red(),
  //                             SizedBox(width: CustomTheme.screenWidth! * 0.02),
  //                             Text(element.clock_out,
  //                                 style: CustomTheme.body2(context,
  //                                     color: Colors.black,
  //                                     fontWeight: CustomTheme.medium)),
  //                           ],
  //                         )
  //                       ],
  //                     ),
  //                   )
  //                 ],
  //               ),
  //               SizedBox(height: CustomTheme.screenHeight! * 0.01),
  //               Container(
  //                 width: CustomTheme.screenWidth! * 1,
  //                 padding: EdgeInsets.symmetric(horizontal: 8, vertical: 15),
  //                 decoration: BoxDecoration(
  //                     borderRadius: BorderRadius.all(Radius.circular(8)),
  //                     border: Border.all(width: 1, color: Color(0xFFD8E4EE)),
  //                     color: Color(0xFFF3F6F9)),
  //                 child: RichText(
  //                   textAlign: TextAlign.left,
  //                   text: TextSpan(
  //                     children: [
  //                       TextSpan(
  //                           text: element.title,
  //                           style: CustomTheme.body1(context,
  //                               color: Colors.black,
  //                               fontWeight: CustomTheme.semibold)),
  //                       TextSpan(
  //                           text: element.contain,
  //                           style: CustomTheme.body1(context,
  //                               color: Color(0xFF828282),
  //                               fontWeight: CustomTheme.medium)),
  //                     ],
  //                   ),
  //                 ),
  //               ),
  //               SizedBox(height: CustomTheme.screenHeight! * 0.02),
  //               Container(
  //                 width: CustomTheme.screenWidth! * 1,
  //                 padding: EdgeInsets.symmetric(horizontal: 8, vertical: 15),
  //                 decoration: BoxDecoration(
  //                     borderRadius: BorderRadius.all(Radius.circular(8)),
  //                     border: Border.all(width: 1, color: Color(0xFFD8E4EE)),
  //                     color: Color(0xFFF3F6F9)),
  //                 child: Column(
  //                   crossAxisAlignment: CrossAxisAlignment.start,
  //                   children: [
  //                     Text("HR Project",
  //                         style: CustomTheme.body1(context,
  //                             color: Colors.black,
  //                             fontWeight: CustomTheme.semibold)),
  //                     SizedBox(height: CustomTheme.screenHeight! * 0.02),
  //                     secondHalf!.isEmpty
  //                         ? Text(firstHalf!)
  //                         : Row(
  //                             children: <Widget>[
  //                               Expanded(
  //                                 child: Text(
  //                                     flag
  //                                         ? (firstHalf! + "...")
  //                                         : (firstHalf! + secondHalf!),
  //                                     style: CustomTheme.body1(context,
  //                                         color: Color(0xFF828282),
  //                                         fontWeight: CustomTheme.medium)),
  //                               ),
  //                               GestureDetector(
  //                                 onTap: () {
  //                                   setState(() {
  //                                     flag = !flag;
  //                                   });
  //                                 },
  //                                 child: Icon(flag
  //                                     ? Icons.keyboard_arrow_up_rounded
  //                                     : Icons.keyboard_arrow_down_rounded),
  //                               )
  //                             ],
  //                           ),
  //                   ],
  //                 ),
  //               ),
  //             ],
  //           ),
  //         )
  //       : element.contain == "-"
  //           ? Container(
  //               margin: EdgeInsets.fromLTRB(25, 12, 25, 12),
  //               padding: EdgeInsets.fromLTRB(12, 0, 12, 12),
  //               decoration: BoxDecoration(
  //                   borderRadius: BorderRadius.all(Radius.circular(8)),
  //                   color: Colors.white),
  //               child: Column(
  //                 children: [
  //                   Row(
  //                     children: [
  //                       Container(
  //                           decoration: BoxDecoration(shape: BoxShape.circle),
  //                           child: element.avatar),
  //                       SizedBox(width: CustomTheme.screenWidth! * 0.03),
  //                       Expanded(
  //                         child: Column(
  //                           crossAxisAlignment: CrossAxisAlignment.start,
  //                           children: [
  //                             Text(element.name,
  //                                 style: CustomTheme.body1(context,
  //                                     color: Colors.black,
  //                                     fontWeight: CustomTheme.semibold)),
  //                             SizedBox(
  //                                 height: CustomTheme.screenHeight! * 0.01),
  //                             Row(
  //                               children: [
  //                                 IconAsset.door_enter_green(),
  //                                 SizedBox(
  //                                     width: CustomTheme.screenWidth! * 0.02),
  //                                 Text(element.clock_in,
  //                                     style: CustomTheme.body2(context,
  //                                         color: Colors.black,
  //                                         fontWeight: CustomTheme.medium)),
  //                                 SizedBox(
  //                                     width: CustomTheme.screenWidth! * 0.13),
  //                                 IconAsset.door_exit_red(),
  //                                 SizedBox(
  //                                     width: CustomTheme.screenWidth! * 0.02),
  //                                 Text(element.clock_out,
  //                                     style: CustomTheme.body2(context,
  //                                         color: Colors.black,
  //                                         fontWeight: CustomTheme.medium)),
  //                               ],
  //                             )
  //                           ],
  //                         ),
  //                       )
  //                     ],
  //                   ),
  //                   SizedBox(height: CustomTheme.screenHeight! * 0.01),
  //                   Container(
  //                     width: CustomTheme.screenWidth! * 1,
  //                     padding:
  //                         EdgeInsets.symmetric(horizontal: 8, vertical: 15),
  //                     decoration: BoxDecoration(
  //                         borderRadius: BorderRadius.all(Radius.circular(8)),
  //                         border:
  //                             Border.all(width: 1, color: Color(0xFFD8E4EE)),
  //                         color: Color(0xFFF3F6F9)),
  //                     child: RichText(
  //                       textAlign: TextAlign.left,
  //                       text: TextSpan(
  //                         children: [
  //                           TextSpan(
  //                               text: element.title,
  //                               style: CustomTheme.body1(context,
  //                                   color: Colors.black,
  //                                   fontWeight: CustomTheme.semibold)),
  //                           TextSpan(
  //                               text: element.contain,
  //                               style: CustomTheme.body1(context,
  //                                   color: Color(0xFF828282),
  //                                   fontWeight: CustomTheme.medium)),
  //                         ],
  //                       ),
  //                     ),
  //                   ),
  //                 ],
  //               ),
  //             )
  //           : Container(
  //               margin: EdgeInsets.fromLTRB(25, 12, 25, 12),
  //               padding: EdgeInsets.fromLTRB(12, 0, 12, 12),
  //               decoration: BoxDecoration(
  //                   borderRadius: BorderRadius.all(Radius.circular(8)),
  //                   color: Colors.white),
  //               child: Column(
  //                 children: [
  //                   Row(
  //                     children: [
  //                       Container(
  //                           decoration: BoxDecoration(shape: BoxShape.circle),
  //                           child: element.avatar),
  //                       SizedBox(width: CustomTheme.screenWidth! * 0.03),
  //                       Expanded(
  //                         child: Column(
  //                           crossAxisAlignment: CrossAxisAlignment.start,
  //                           children: [
  //                             Row(
  //                               mainAxisAlignment:
  //                                   MainAxisAlignment.spaceBetween,
  //                               children: [
  //                                 Text(element.name,
  //                                     style: CustomTheme.body1(context,
  //                                         color: Colors.black,
  //                                         fontWeight: CustomTheme.semibold)),
  //                                 Container(
  //                                   padding: EdgeInsets.all(8),
  //                                   decoration: BoxDecoration(
  //                                       borderRadius: BorderRadius.all(
  //                                           Radius.circular(8)),
  //                                       color: Color(0xFFEEEFF1)),
  //                                   child: Text(element.status,
  //                                       style: CustomTheme.caption(context,
  //                                           color: Colors.black,
  //                                           fontWeight: CustomTheme.medium)),
  //                                 )
  //                               ],
  //                             ),
  //                             Row(
  //                               children: [
  //                                 IconAsset.door_enter_green(),
  //                                 SizedBox(
  //                                     width: CustomTheme.screenWidth! * 0.02),
  //                                 Text(element.clock_in,
  //                                     style: CustomTheme.body2(context,
  //                                         color: Colors.black,
  //                                         fontWeight: CustomTheme.medium)),
  //                                 SizedBox(
  //                                     width: CustomTheme.screenWidth! * 0.13),
  //                                 IconAsset.door_exit_red(),
  //                                 SizedBox(
  //                                     width: CustomTheme.screenWidth! * 0.02),
  //                                 Text(element.clock_out,
  //                                     style: CustomTheme.body2(context,
  //                                         color: Colors.black,
  //                                         fontWeight: CustomTheme.medium)),
  //                               ],
  //                             )
  //                           ],
  //                         ),
  //                       )
  //                     ],
  //                   ),
  //                   SizedBox(height: CustomTheme.screenHeight! * 0.01),
  //                   Container(
  //                     width: CustomTheme.screenWidth! * 1,
  //                     padding:
  //                         EdgeInsets.symmetric(horizontal: 8, vertical: 15),
  //                     decoration: BoxDecoration(
  //                         borderRadius: BorderRadius.all(Radius.circular(8)),
  //                         border:
  //                             Border.all(width: 1, color: Color(0xFFD8E4EE)),
  //                         color: Color(0xFFF3F6F9)),
  //                     child: RichText(
  //                       textAlign: TextAlign.left,
  //                       text: TextSpan(
  //                         children: [
  //                           TextSpan(
  //                               text: element.title,
  //                               style: CustomTheme.body1(context,
  //                                   color: Colors.black,
  //                                   fontWeight: CustomTheme.semibold)),
  //                           TextSpan(
  //                               text: element.contain,
  //                               style: CustomTheme.body1(context,
  //                                   color: Color(0xFF828282),
  //                                   fontWeight: CustomTheme.medium)),
  //                         ],
  //                       ),
  //                     ),
  //                   ),
  //                 ],
  //               ),
  //             );
  // }
}
