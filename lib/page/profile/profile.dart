import 'dart:io';

import 'package:flutter/material.dart';
import 'package:hr_project/dummy_data.dart';
import 'package:hr_project/page/authenticate/login.dart';
import 'package:hr_project/page/profile/change_client.dart';
import 'package:hr_project/page/profile/change_pass.dart';
import 'package:hr_project/page/profile/my_log.dart';
import 'package:hr_project/styling_theme.dart';
import 'package:hr_project/widget/custom_appbar.dart';
import 'package:hr_project/widget/profile_item.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../database local/db_helper.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  String? username;
  String? email;
  String? jobTitle;
  List<DummyProfileMenuItem> menuItem = [
    DummyProfileMenuItem(IconAsset.clipboard(), "My Log", MyLogPage()),
    DummyProfileMenuItem(
        IconAsset.badge_account(), "Change Project", ChangeClientPage()),
    DummyProfileMenuItem(IconAsset.lock(), "Change Password", ChangePassPage()),
  ];
  File? picCamImg;

  Future _takePicture(BuildContext context) async {
    final ImagePicker picker = ImagePicker();
    XFile? pickedImage;

    try {
      pickedImage = await picker.pickImage(
          source: ImageSource.gallery,
          imageQuality: 50,
          preferredCameraDevice: CameraDevice.rear);

      if (pickedImage != null) {
        print("PATH: " + File(pickedImage.path).toString());
        print("TYPE: " + File(pickedImage.mimeType.toString()).toString());
        print("NAME: " + File(pickedImage.name.toString()).toString());
        setState(() {
          picCamImg = File(pickedImage!.path);
        });
      } else {
        Navigator.pop(context);
        await ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text("No image was selected"),
          ),
        );
      }
    } catch (e) {
      print(e);
      print("error");
    }
  }

  Future _takeCamera(BuildContext context) async {
    final ImagePicker picker = ImagePicker();
    XFile? pickedImage;

    try {
      pickedImage = await picker.pickImage(
          source: ImageSource.camera,
          imageQuality: 50,
          preferredCameraDevice: CameraDevice.front);

      if (pickedImage != null) {
        print("PATH: " + File(pickedImage.path).toString());
        print("TYPE: " + File(pickedImage.mimeType.toString()).toString());
        print("NAME: " + File(pickedImage.name.toString()).toString());
        setState(() {
          picCamImg = File(pickedImage!.path);
        });
      } else {
        Navigator.pop(context);
        await ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text("No image was selected"),
          ),
        );
      }
    } catch (e) {
      print(e);
      print("error");
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loadUsername();
  }

  Future<void> _loadUsername() async {
    final SharedPreferences prefs = await _prefs;
    setState(() {
      username = prefs.getString('fullName') ?? "";
      email = prefs.getString('email') ?? "";
      jobTitle = prefs.getString('jobTitle') ?? "";
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFFF6F9FF),
        appBar: CustomAppbar(title: "Profile"),
        body: Stack(
          children: [
            Positioned.fill(
              child: Align(
                alignment: Alignment.topCenter,
                child: ImageAsset.headerBackground2(1),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 40),
              child: Column(
                children: [
                  header(),
                  SizedBox(height: CustomTheme.screenHeight! * 0.05),
                  menuProfile(),
                  SizedBox(height: CustomTheme.screenHeight! * 0.03),
                  logout(),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  header() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        GestureDetector(
          onTap: () {
            showOption();
          },
          child: Stack(
            children: [
              picCamImg != null
                  ? CircleAvatar(
                      radius: 40,
                      backgroundImage: FileImage(
                        File(picCamImg!.path),
                      ))
                  : IconAsset.avatar(1),
              Positioned(
                bottom: 0,
                right: 3,
                child: CircleAvatar(
                  backgroundColor: Color(0xFFEB7D2E),
                  radius: 12,
                  child:
                      Icon(Icons.photo_camera, size: 12, color: Colors.white),
                ),
              ),
            ],
          ),
        ),
        SizedBox(width: CustomTheme.screenWidth! * 0.07),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              username ?? "",
              style: CustomTheme.body1(context,
                  color: Colors.black, fontWeight: CustomTheme.semibold),
            ),
            SizedBox(height: CustomTheme.screenHeight! * 0.01),
            Text(
              jobTitle ?? "",
              style: CustomTheme.body2(context,
                  color: Colors.white, fontWeight: CustomTheme.medium),
            ),
            SizedBox(height: CustomTheme.screenHeight! * 0.003),
            Text(
              email ?? "",
              style: CustomTheme.body2(context,
                  color: Colors.white, fontWeight: CustomTheme.medium),
            ),
          ],
        ),
      ],
    );
  }

  menuProfile() {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(12)),
          color: Colors.white),
      child: Padding(
        padding: EdgeInsets.all(10),
        child: ListView.separated(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemCount: menuItem.length,
          separatorBuilder: (BuildContext context, int index) =>
              Divider(color: Color(0xFFD8E4EE), thickness: 1),
          itemBuilder: (BuildContext context, int index) {
            return ProfileItem(
                icon: menuItem[index].icon,
                title: menuItem[index].title,
                directPage: menuItem[index].directPage);
          },
        ),
      ),
    );
  }

  logout() {
    return GestureDetector(
      onTap: () async {
        showAlertDialog(context);
      },
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(12)),
            color: Colors.white),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
          child: Row(
            children: [
              IconAsset.log_out(),
              SizedBox(width: CustomTheme.screenWidth! * 0.03),
              Text("Log Out",
                  style: CustomTheme.body1(context,
                      color: Colors.black, fontWeight: CustomTheme.medium)),
            ],
          ),
        ),
      ),
    );
  }

  showAlertDialog(BuildContext context) {
    // set up the buttons
    Widget cancelButton = InkWell(
      child: Container(
          decoration: BoxDecoration(
            color: Color(0xffEB7D2E),
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(40.0),
                bottomRight: Radius.circular(40.0),
                topLeft: Radius.circular(40.0),
                bottomLeft: Radius.circular(40.0)),
          ),
          height: 40,
          margin: EdgeInsets.all(10),
          width: MediaQuery.of(context).size.width * 0.8,
          child: Center(
              child: Text(
            "Log Out",
            style: TextStyle(color: Colors.white),
          ))),
      onTap: () async {
        final SharedPreferences prefs = await _prefs;
        await prefs.clear();
        await ProjectDatabase.instance.deleteAll();
        // await ProjectDatabase.instance.close();
        await prefs.clear();
        await Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => LoginPage()));
      },
    );
    Widget continueButton = InkWell(
      child: Container(
          decoration: BoxDecoration(
            border: Border.all(color: Color(0xffEB7D2E)),
            // color: Color(0xffEB7D2E),
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(40.0),
                bottomRight: Radius.circular(40.0),
                topLeft: Radius.circular(40.0),
                bottomLeft: Radius.circular(40.0)),
          ),
          height: 40,
          margin: EdgeInsets.all(10),
          width: MediaQuery.of(context).size.width * 0.8,
          child: Center(
              child: Text(
            "Cancel",
            style: TextStyle(color: Color(0xffEB7D2E)),
          ))),
      onTap: () async {
        Navigator.pop(context);
      },
    );

    Widget actionKu = Column(
      children: [
        cancelButton,
        continueButton,
      ],
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Container(alignment: Alignment.center, child: Text("Log Out?")),
      content: Text("Are you sure you want to log out?"),
      actions: [actionKu],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  showOption() {
    return showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(15))),
        builder: (BuildContext context) {
          return Padding(
            padding: EdgeInsets.only(top: 15),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text('Change Profile Picture',
                    style: CustomTheme.subtitle(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                SizedBox(height: CustomTheme.screenHeight! * 0.02),
                ListTile(
                  title: Text('Take a photo',
                      style: CustomTheme.subtitle(context,
                          color: Colors.black, fontWeight: CustomTheme.medium)),
                  trailing: Icon(Icons.arrow_forward_ios_rounded,
                      size: 17, color: Color(0xFFB8CADB)),
                  onTap: () {
                    _takeCamera(context);
                  },
                ),
                Divider(
                    color: Color(0xFFD8E4EE),
                    height: 0,
                    thickness: 1,
                    endIndent: 20,
                    indent: 20),
                ListTile(
                  title: Text('Choose from gallery',
                      style: CustomTheme.subtitle(context,
                          color: Colors.black, fontWeight: CustomTheme.medium)),
                  trailing: Icon(Icons.arrow_forward_ios_rounded,
                      size: 17, color: Color(0xFFB8CADB)),
                  onTap: () {
                    Navigator.pop(context);
                    _takePicture(context);
                  },
                ),
              ],
            ),
          );
        });
  }
}
