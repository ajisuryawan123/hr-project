import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hr_project/styling_theme.dart';
import 'package:hr_project/utils/unauthorized.dart';
import 'package:hr_project/widget/custom_appbar.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../config/API config/employee-project-controller-api/employeeChangeProject.dart';
import '../../config/API config/employee-project-controller-api/employeeCurrentProject.dart';
import '../../config/API config/employee-project-controller-api/employeeProjectHistory.dart';
import '../../model/employee-project-controller-model/employeeChangeProject.dart';
import '../../model/employee-project-controller-model/employeeCurrentProject.dart';
import '../../model/employee-project-controller-model/employeeProjectHistory.dart';

class ChangeClientPage extends StatefulWidget {
  const ChangeClientPage({Key? key}) : super(key: key);

  @override
  State<ChangeClientPage> createState() => _ChangeClientPageState();
}

class _ChangeClientPageState extends State<ChangeClientPage> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  EmployeeProjectHistoryModel? employeeProjectHistoryRes;
  EmployeeCurrentProjectModel? employeeCurrentProjectRes;
  EmployeeChangeProjectModel? employeeChangeProjectRes;
  bool _isLoad = false;
  int? itemDrop;
  String? _selected;
  List<EmployeeHistoryData>? dataEmployeeHistory;

  projectHistory() async {
    final SharedPreferences prefs = await _prefs;
    var res = await employeeProjectHistory_API(
        token: prefs.getString("accessToken")!);
    employeeProjectHistoryRes =
        await EmployeeProjectHistoryModel.fromJson(json.decode(res.body));
    if (employeeProjectHistoryRes!.errorMessage == "invalid authorization") {
      unauthorized(context);
    } else {
      setState(() {
        dataEmployeeHistory = employeeProjectHistoryRes!.data!.data;
      });
    }
  }

  currentProject() async {
    setState(() {
      _isLoad = true;
    });
    final SharedPreferences prefs = await _prefs;
    var res = await employeeCurrentProject_API(
        token: prefs.getString("accessToken")!);
    employeeCurrentProjectRes =
        await EmployeeCurrentProjectModel.fromJson(json.decode(res.body));
    if (employeeCurrentProjectRes!.errorMessage == "invalid authorization") {
      unauthorized(context);
    } else {
      await projectHistory();
      setState(() {
        itemDrop = employeeCurrentProjectRes!.data!.data!.project!.projectId;
        _selected = employeeCurrentProjectRes!.data!.data!.project!.projectName;
        _isLoad = false;
      });
    }
  }

  changeProject() async {
    setState(() {
      _isLoad = true;
    });
    final SharedPreferences prefs = await _prefs;
    var res = await employeeChangeProject_API(
        token: prefs.getString("accessToken")!, project_id: itemDrop!);
    employeeChangeProjectRes =
        await EmployeeChangeProjectModel.fromJson(json.decode(res.body));
    if (employeeChangeProjectRes!.errorMessage == "invalid authorization") {
      unauthorized(context);
    } else {
      setState(() {
        _isLoad = false;
      });
      dialog(res);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    currentProject();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFFF6F9FF),
        appBar: CustomAppbar(title: "Change Project"),
        bottomSheet: customSheet(),
        body: _isLoad
            ? Center(child: CircularProgressIndicator())
            : Padding(
                padding: EdgeInsets.all(25),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Project",
                      style: CustomTheme.body1(context,
                          color: Colors.black,
                          fontWeight: CustomTheme.semibold),
                    ),
                    SizedBox(height: CustomTheme.screenHeight! * 0.02),
                    InkWell(
                      onTap: () => showModal(context),
                      child: Container(
                        padding: EdgeInsets.all(6),
                        width: CustomTheme.screenWidth! * 1,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            border:
                                Border.all(width: 1, color: Color(0xFFB8CADB)),
                            color: Colors.white),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(_selected.toString(),
                                style: CustomTheme.body1(context,
                                    color: Colors.black,
                                    fontWeight: CustomTheme.medium)),
                            Icon(Icons.keyboard_arrow_down_outlined,
                                color: Color(0xFFB8CADB))
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
      ),
    );
  }

  void showModal(context) {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(12)),
        ),
        context: context,
        builder: (context) {
          return Container(
            padding: EdgeInsets.fromLTRB(12, 12, 12, 0),
            height: CustomTheme.screenHeight! * 0.75,
            child: Column(
              children: [
                Text("Select Client",
                    style: CustomTheme.body1(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
                SizedBox(height: CustomTheme.screenHeight! * 0.03),
                Container(
                  height: CustomTheme.screenHeight! * 0.07,
                  child: TextFormField(
                    style: CustomTheme.body1(context,
                        color: Colors.black, fontWeight: CustomTheme.medium),
                    decoration: InputDecoration(
                      floatingLabelBehavior: FloatingLabelBehavior.never,
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 30, horizontal: 10),
                      labelText: "Search client",
                      labelStyle: CustomTheme.body1(context,
                          color: Color(0xFFBDBDBD),
                          fontWeight: CustomTheme.medium),
                      filled: true,
                      fillColor: Color(0xFFF2F4F9),
                      prefixIcon: Icon(Icons.search, color: Color(0xFFE0E0E0)),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                    ),
                  ),
                ),
                SizedBox(height: CustomTheme.screenHeight! * 0.03),
                ListView.builder(
                    shrinkWrap: true,
                    itemCount: dataEmployeeHistory!.length,
                    itemBuilder: (context, index) {
                      var item = dataEmployeeHistory![index].project!;
                      return RadioListTile(
                        title: Text(item.projectName.toString(),
                            style: CustomTheme.body1(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.medium)),
                        activeColor: Color(0xFFEB7D2E),
                        value: item.projectId,
                        groupValue: itemDrop,
                        onChanged: (value) {
                          setState(() {
                            itemDrop = value;
                            _selected = item.projectName;
                          });
                          Navigator.of(context).pop();
                        },
                      );
                    }),
              ],
            ),
          );
        });
  }

  customSheet() {
    return Container(
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3),
          ),
        ],
      ),
      child: ElevatedButton(
          onPressed: () {
            changeProject();
          },
          style: ElevatedButton.styleFrom(
              padding: EdgeInsets.symmetric(vertical: 12),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              minimumSize: Size(double.infinity, 0),
              backgroundColor: Color(0xFFEB7D2E)),
          child: Text(
            "Save",
            style: CustomTheme.button(context,
                color: Colors.white, fontWeight: CustomTheme.medium),
          )),
    );
  }

  dialog(Response? statusCode) {
    return showDialog(
      context: context,
      builder: (context) {
        return Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
          child: statusCode!.statusCode != 201
              ? Container(
                  padding: EdgeInsets.symmetric(horizontal: 12),
                  height: CustomTheme.screenHeight! * 0.25,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Change Project Failed",
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold),
                      ),
                      SizedBox(height: CustomTheme.screenHeight! * 0.01),
                      Text(
                        employeeChangeProjectRes!.errorMessage ?? "",
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium),
                      ),
                      SizedBox(height: CustomTheme.screenHeight! * 0.05),
                      ElevatedButton(
                          onPressed: () {
                            Navigator.pop(context);
                            Navigator.pop(context);
                          },
                          style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.symmetric(vertical: 15),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              minimumSize: Size(double.infinity, 0),
                              backgroundColor: Color(0xFFEB7D2E)),
                          child: Text(
                            "Ok",
                            style: CustomTheme.button(context,
                                color: Colors.white,
                                fontWeight: CustomTheme.medium),
                          ))
                    ],
                  ),
                )
              : Container(
                  padding: EdgeInsets.symmetric(horizontal: 12),
                  height: CustomTheme.screenHeight! * 0.5,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                          height: CustomTheme.screenHeight! * 0.27,
                          child: LottieAsset.success()),
                      Text(
                        "Change Project Success",
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold),
                      ),
                      SizedBox(height: CustomTheme.screenHeight! * 0.01),
                      Text(
                        "Yeah! You successfully change your project",
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium),
                      ),
                      SizedBox(height: CustomTheme.screenHeight! * 0.05),
                      ElevatedButton(
                          onPressed: () {
                            Navigator.pop(context);
                            Navigator.pop(context);
                          },
                          style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.symmetric(vertical: 15),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              minimumSize: Size(double.infinity, 0),
                              backgroundColor: Color(0xFFEB7D2E)),
                          child: Text(
                            "Ok",
                            style: CustomTheme.button(context,
                                color: Colors.white,
                                fontWeight: CustomTheme.medium),
                          ))
                    ],
                  ),
                ),
        );
      },
    );
  }
}
