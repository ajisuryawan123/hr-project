import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hr_project/config/API%20config/attendances-api/attendance_myLog.dart';
import 'package:hr_project/config/API%20config/attendances-api/attendance_myLogExport.dart';
import 'package:hr_project/dummy_data.dart';
import 'package:hr_project/model/attendances-model/attendances_log.dart';
import 'package:hr_project/styling_theme.dart';
import 'package:hr_project/utils/capitalize.dart';
import 'package:hr_project/utils/datetime_format.dart';
import 'package:hr_project/utils/unauthorized.dart';
import 'package:intl/intl.dart';
import 'package:moment_dart/moment_dart.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:permission_handler/permission_handler.dart';

class MyLogPage extends StatefulWidget {
  const MyLogPage({Key? key}) : super(key: key);

  @override
  State<MyLogPage> createState() => _MyLogPageState();
}

class _MyLogPageState extends State<MyLogPage> {
  ScrollController _scrollController = ScrollController();
  DateRangePickerController _datePickerController = DateRangePickerController();
  String text =
      "Status log data need to be saved at the time of clock in Attendance for Log module to show the summary.";
  String? firstHalf;
  String? secondHalf;
  DateTime _startDate = Moment.now().startOfMonth();
  DateTime _endDate = Moment.now();
  bool flag = false;
  bool _isLoading = false;
  bool _isLoadingMore = false;
  int _pageNo = 0;
  bool _isLastPage = false;

  int _attendanceCount = 0;
  List<Statistic> statistics = [];
  List<Content> contents = [];
  AttendanceLogModel? attendanceMyLogRes;

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  @override
  void initState() {
    super.initState();

    if (text.length > 35) {
      firstHalf = text.substring(0, 35);
      secondHalf = text.substring(35, text.length);
    } else {
      firstHalf = text;
      secondHalf = "";
    }

    _datePickerController.selectedRange =
        PickerDateRange(Moment.now().startOfMonth(), Moment.now());

    _scrollController.addListener(_scrollListener);

    fetchMyLog(_pageNo);
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _datePickerController.dispose();
    super.dispose();
  }

  void _scrollListener() async {
    if (!_isLoadingMore && !_isLastPage) {
      if (_scrollController.position.extentAfter < 50) {
        setState(() {
          _pageNo += 1;
          _isLoadingMore = true;
        });

        AttendanceLogModel? res = await getMyLog(_pageNo);

        setState(() {
          contents.addAll(res?.data?.attendancePagination.content ?? []);
          _isLastPage = res?.data?.attendancePagination.last ?? false;
          _isLoadingMore = false;
        });
      }
    }
  }

  getMyLog(int pageNo) async {
    var res = await attendanceMyLog_API(
      pageNo: pageNo,
      startDate: Moment(_startDate).format('YYYY-MM-DD'),
      endDate: Moment(_endDate).format('YYYY-MM-DD'),
    );

    attendanceMyLogRes = AttendanceLogModel.fromJson(json.decode(res.body));

    if (res.statusCode == 401) {
      unauthorized(context);
    }

    return attendanceMyLogRes;
  }

  fetchMyLog(int pageNo) async {
    setState(() {
      _isLoading = true;
    });

    AttendanceLogModel? res = await getMyLog(pageNo);

    if (res?.errorCode == '200401') {
      unauthorized(context);
    }

    int sum = 0;
    res?.data?.statistic.forEach((e) {
      sum += e.value;
    });

    setState(() {
      statistics = res?.data?.statistic ?? [];
      contents = res?.data?.attendancePagination.content ?? [];
      _attendanceCount = sum;
      _isLastPage = res?.data?.attendancePagination.last ?? false;
    });

    setState(() {
      _isLoading = false;
    });
  }

  exportMyLog() async {
    var status = await Permission.storage.status;
    if (status.isDenied) {
      // We didn't ask for permission yet or the permission has been denied before but not permanently.
      var response = await Permission.storage.request();
      if (response.isDenied) {
        Fluttertoast.showToast(
          msg: "Please allow storage permission to download",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.white,
          textColor: Colors.black,
          fontSize: 14.0,
        );
      }
    } else if (status.isGranted) {
      final SharedPreferences prefs = await _prefs;

      String email = prefs.getString("email")!;

      String fileName =
          "${email}_${Moment(_startDate).format('YYYY-MM-DD')}_${Moment(_endDate).format('YYYY-MM-DD')}_timesheet";
      String ext = ".xlsx";

      try {
        String path;

        if (Platform.isAndroid) {
          path = '/storage/emulated/0/Download';
        } else {
          path = (await getApplicationDocumentsDirectory()).path;
        }

        String filePath = '$path/$fileName$ext';

        var res = await attendanceMyLogExport_API(
          startDate: Moment(_startDate).format('YYYY-MM-DD'),
          endDate: Moment(_endDate).format('YYYY-MM-DD'),
        );

        File file = new File(filePath);

        file.writeAsBytes(res.bodyBytes);

        OpenFile.open(file.path);

        // Fluttertoast.showToast(
        //   msg: "Download $fileName$ext successful",
        //   toastLength: Toast.LENGTH_SHORT,
        //   gravity: ToastGravity.BOTTOM,
        //   timeInSecForIosWeb: 3,
        //   backgroundColor: Colors.white,
        //   textColor: Colors.black,
        //   fontSize: 14.0,
        // );
      } catch (e) {
        debugPrint(e.toString());
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFFF6F9FF),
        appBar: custom_appbar(),
        body: _isLoading
            ? Center(child: CircularProgressIndicator())
            : Stack(
                children: [
                  Positioned.fill(
                    child: Align(
                      alignment: Alignment.topCenter,
                      child: ImageAsset.headerBackground2(1),
                    ),
                  ),
                  Column(
                    children: [
                      calendar(),
                      listattend(),
                      // noList(),
                    ],
                  )
                ],
              ),
      ),
    );
  }

  custom_appbar() {
    return AppBar(
      centerTitle: true,
      iconTheme: IconThemeData(color: Color(0xFF2F80ED)),
      backgroundColor: Color(0xFFF2C94C),
      title: Text(
        "My Log",
        style: CustomTheme.subtitle(context,
            color: Colors.black, fontWeight: CustomTheme.semibold),
      ),
      actions: [
        GestureDetector(
          onTap: exportMyLog,
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 0, horizontal: 16),
            child: IconAsset.download(),
          ),
        )
      ],
    );
  }

  calendar_items() {
    return Column(
        children: statistics
            .map((statistic) => calendar_item(
                  statistic.groupingBy,
                  statistic.value,
                ))
            .toList());
  }

  Widget calendar_item(String groupBy, int count) {
    return Column(
      children: [
        SizedBox(height: CustomTheme.screenHeight! * 0.01),
        Row(
          children: [
            Container(
              width: CustomTheme.screenWidth! * 0.3,
              child: Text(
                "${groupBy == 'ONTIME' ? 'On Time' : groupBy == 'LATE' ? 'Late' : groupBy == 'LEAVE' ? 'Leave' : 'No Record'}",
                style: CustomTheme.body2(context,
                    color: Colors.black, fontWeight: CustomTheme.medium),
              ),
            ),
            Container(
              width: CustomTheme.screenWidth! * 0.3,
              child: Text(
                ": ${count} day${count > 1 ? 's' : ''}",
                style: CustomTheme.body2(context,
                    color: Colors.black, fontWeight: CustomTheme.medium),
              ),
            ),
          ],
        ),
      ],
    );
  }

  calendar() {
    return Padding(
      padding: EdgeInsets.fromLTRB(25, 25, 25, 0),
      child: Column(
        children: [
          GestureDetector(
            onTap: () => showModal(context),
            child: Container(
              padding: EdgeInsets.all(12),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  color: Colors.white),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  IconAsset.calender_blue(),
                  SizedBox(width: CustomTheme.screenWidth! * 0.02),
                  Text(
                    Moment(_startDate).format('DD MMMM YYYY') +
                        " - " +
                        Moment(_endDate).format('DD MMMM YYYY'),
                    style: CustomTheme.body1(context,
                        color: Colors.black, fontWeight: CustomTheme.medium),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: CustomTheme.screenHeight! * 0.03),
          Container(
            padding: EdgeInsets.all(15),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(8)),
                color: Colors.white),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                IconAsset.bulb(),
                SizedBox(width: CustomTheme.screenWidth! * 0.1),
                IntrinsicWidth(
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Container(
                            width: CustomTheme.screenWidth! * 0.3,
                            child: Text(
                              "Attendance",
                              style: CustomTheme.subtitle(context,
                                  color: Colors.black,
                                  fontWeight: CustomTheme.medium),
                            ),
                          ),
                          Container(
                            width: CustomTheme.screenWidth! * 0.3,
                            child: Text(
                              ": ${_attendanceCount} day${_attendanceCount > 1 ? 's' : ''}",
                              style: CustomTheme.subtitle(context,
                                  color: Colors.black,
                                  fontWeight: CustomTheme.semibold),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: CustomTheme.screenHeight! * 0.01),
                      Divider(thickness: 1, color: Color(0xFFEB7D2E)),
                      calendar_items(),
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  noList() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.all(12),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color(0xFFD8E4EE),
                Color(0xFFD8E4EE).withOpacity(0.0),
              ],
            ),
          ),
          child: IconAsset.leaves(),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.03),
        Text(
          "No data to show",
          style: CustomTheme.body1(context,
              color: Color(0xFF525F71), fontWeight: CustomTheme.semibold),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.01),
        Text(
          "Try to change date range",
          textAlign: TextAlign.center,
          style: CustomTheme.body1(context,
              color: Color(0xFFB8CADB), fontWeight: CustomTheme.medium),
        ),
      ],
    );
  }

  listattend() {
    return Expanded(
      child: Container(
        padding: EdgeInsets.only(top: 10),
        child: ListView.builder(
          controller: _scrollController,
          itemCount: contents.length,
          itemBuilder: (BuildContext context, int index) {
            return Column(
              children: [
                if (contents[index].dateGroupLabel is String)
                  dateGroup(contents[index].dateGroupLabel ?? ''),
                Container(
                  margin: EdgeInsets.fromLTRB(20, 0, 20, 10),
                  padding: EdgeInsets.fromLTRB(10, 2, 10, 10),
                  decoration: BoxDecoration(
                    border: Border.all(color: Color(0xFFD8E4EE)),
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    color: Colors.white,
                  ),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Container(
                            padding: EdgeInsets.all(12),
                            decoration: BoxDecoration(
                              color: Color(0xFFF2F4F9),
                              shape: BoxShape.circle,
                            ),
                            child: Column(
                              children: [
                                Text(
                                    Moment(contents[index].attendanceDate)
                                        .format('DD'),
                                    style: CustomTheme.headline3(context,
                                        color: Color(0xFF525F71),
                                        fontWeight: CustomTheme.bold)),
                                Text(
                                    Moment(contents[index].attendanceDate)
                                        .format('ddd'),
                                    style: CustomTheme.caption(context,
                                        color: Color(0xFF525F71),
                                        fontWeight: CustomTheme.regular)),
                              ],
                            ),
                          ),
                          SizedBox(width: CustomTheme.screenWidth! * 0.03),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                        contents[index].attendanceStatus ==
                                                AttendanceStatus.NORECORD
                                            ? 'No Record'
                                            : capitalize(contents[index]
                                                .event
                                                .toString()),
                                        style: CustomTheme.body1(context,
                                            color: Colors.black,
                                            fontWeight: CustomTheme.bold)),
                                    contents[index].attendanceStatus ==
                                            AttendanceStatus.NORECORD
                                        ? Container(
                                            padding: EdgeInsets.all(8),
                                            child: Text(' ',
                                                style: CustomTheme.overline(
                                                    context,
                                                    color: Colors.black,
                                                    fontWeight:
                                                        CustomTheme.regular)),
                                          )
                                        : Container(
                                            padding: EdgeInsets.all(8),
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(8)),
                                                color: Color(0xFFEEEFF1)),
                                            child: Text(
                                                contents[index]
                                                            .attendanceStatus ==
                                                        AttendanceStatus.ONTIME
                                                    ? 'On Time'
                                                    : contents[index]
                                                                .attendanceStatus ==
                                                            AttendanceStatus
                                                                .LATE
                                                        ? 'Late'
                                                        : 'Leave',
                                                style: CustomTheme.overline(
                                                    context,
                                                    color: Colors.black,
                                                    fontWeight:
                                                        CustomTheme.regular)),
                                          )
                                  ],
                                ),
                                clockInOut(contents[index].clockIn,
                                    contents[index].clockOut),
                              ],
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: CustomTheme.screenHeight! * 0.01),
                      Container(
                          width: CustomTheme.screenWidth! * 1,
                          padding:
                              EdgeInsets.symmetric(horizontal: 8, vertical: 15),
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8)),
                              border: Border.all(
                                  width: 1, color: Color(0xFFD8E4EE)),
                              color: Color(0xFFF3F6F9)),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                  contents[index].attendanceStatus ==
                                          AttendanceStatus.NORECORD
                                      ? '-'
                                      : contents[index].project.projectName,
                                  style: CustomTheme.body1(context,
                                      color: Colors.black,
                                      fontWeight: CustomTheme.semibold)),
                              SizedBox(height: 10),
                              Text(
                                  contents[index].attendanceStatus ==
                                          AttendanceStatus.NORECORD
                                      ? '-'
                                      : '${contents[index].dailyTask}',
                                  style: CustomTheme.body1(context,
                                      color: Color(0xFF828282),
                                      fontWeight: CustomTheme.medium)),
                            ],
                          )),
                    ],
                  ),
                ),
              ],
            );
          },
        ),
        // GroupedListView<DummyGroupedAttendance, DateTime>(
        //   shrinkWrap: true,
        //   elements: ListDummy.listGroupAttend,
        //   groupBy: (element) =>
        //       DateTime(element.datetime.year, element.datetime.month),
        //   groupComparator: (value1, value2) => value2.compareTo(value1),
        //   itemComparator:
        //       (DummyGroupedAttendance item1, DummyGroupedAttendance item2) =>
        //           item1.datetime.compareTo(item2.datetime),
        //   order: GroupedListOrder.ASC,
        //   useStickyGroupSeparators: true,
        //   groupSeparatorBuilder: (value) => groupSeperatorItem(value),
        //   indexedItemBuilder: (BuildContext context,
        //           DummyGroupedAttendance element, int index) =>
        //       groupItem(context, element, index),
        // ),
      ),
    );
  }

  dateGroup(String dateGroupLabel) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 12),
      margin: EdgeInsets.symmetric(vertical: 10),
      width: CustomTheme.screenWidth,
      decoration: BoxDecoration(color: Color(0xFFE7F1FA)),
      child: Text(
        dateGroupLabel,
        style: CustomTheme.body1(context,
            color: Color(0xFF333333), fontWeight: CustomTheme.bold),
      ),
    );
  }

  clockInOut(DateTime? clockIn, DateTime? clockOut) {
    return Row(
      children: [
        Expanded(
          flex: 3,
          child: Row(
            children: [
              IconAsset.door_enter_green(),
              SizedBox(width: CustomTheme.screenWidth! * 0.02),
              Text(
                datetime_format(clockIn, 'hh:mm A'),
                style: CustomTheme.body2(
                  context,
                  color: Colors.black,
                  fontWeight: CustomTheme.semibold,
                ),
              ),
            ],
          ),
        ),
        Expanded(
          flex: 2,
          child: Row(
            children: [
              IconAsset.door_exit_red(),
              SizedBox(width: CustomTheme.screenWidth! * 0.02),
              Text(
                datetime_format(clockOut, 'hh:mm A'),
                style: CustomTheme.body2(
                  context,
                  color: Colors.black,
                  fontWeight: CustomTheme.semibold,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  groupSeperatorItem(DateTime value) {
    return Container(
      width: double.infinity,
      color: Color(0xFFE7F1FA),
      padding: EdgeInsets.all(12),
      child: Text(
        // '${value.month} ${value.year}',
        DateFormat('MMMM yyyy', 'en_US').format(value),
        style: CustomTheme.body1(context,
            color: Colors.black, fontWeight: CustomTheme.semibold),
      ),
    );
  }

  groupItem(BuildContext context, DummyGroupedAttendance element, int index) {
    return index == 0
        ? Container(
            margin: EdgeInsets.fromLTRB(25, 12, 25, 12),
            padding: EdgeInsets.fromLTRB(12, 0, 12, 12),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              color: Colors.white,
            ),
            child: Column(
              children: [
                Row(
                  children: [
                    Container(
                      padding: EdgeInsets.all(12),
                      decoration: BoxDecoration(
                          color: Color(0xFFFFF4ED), shape: BoxShape.circle),
                      child: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          children: [
                            TextSpan(
                                text: element.tgl,
                                style: CustomTheme.headline4(context,
                                    color: Color(0xFFEB7D2E),
                                    fontWeight: CustomTheme.semibold)),
                            TextSpan(
                                text: element.day,
                                style: CustomTheme.body2(context,
                                    color: Color(0xFFEB7D2E),
                                    fontWeight: CustomTheme.medium)),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(width: CustomTheme.screenWidth! * 0.03),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(element.statusAttend,
                                  style: CustomTheme.body1(context,
                                      color: Colors.black,
                                      fontWeight: CustomTheme.semibold)),
                              Container(
                                padding: EdgeInsets.all(8),
                                decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(8)),
                                    color: Color(0xFFEEEFF1)),
                                child: Text(element.status,
                                    style: CustomTheme.caption(context,
                                        color: Colors.black,
                                        fontWeight: CustomTheme.medium)),
                              )
                            ],
                          ),
                          Row(
                            children: [
                              IconAsset.door_enter_green(),
                              SizedBox(width: CustomTheme.screenWidth! * 0.02),
                              Text(element.clock_in,
                                  style: CustomTheme.body2(context,
                                      color: Colors.black,
                                      fontWeight: CustomTheme.medium)),
                              SizedBox(width: CustomTheme.screenWidth! * 0.13),
                              IconAsset.door_exit_red(),
                              SizedBox(width: CustomTheme.screenWidth! * 0.02),
                              Text(element.clock_out,
                                  style: CustomTheme.body2(context,
                                      color: Colors.black,
                                      fontWeight: CustomTheme.medium)),
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                ),
                SizedBox(height: CustomTheme.screenHeight! * 0.01),
                Container(
                  width: CustomTheme.screenWidth! * 1,
                  padding: EdgeInsets.symmetric(horizontal: 8, vertical: 15),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      border: Border.all(width: 1, color: Color(0xFFD8E4EE)),
                      color: Color(0xFFF3F6F9)),
                  child: RichText(
                    textAlign: TextAlign.left,
                    text: TextSpan(
                      children: [
                        TextSpan(
                            text: element.title,
                            style: CustomTheme.body1(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.semibold)),
                        TextSpan(
                            text: element.contain,
                            style: CustomTheme.body1(context,
                                color: Color(0xFF828282),
                                fontWeight: CustomTheme.medium)),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: CustomTheme.screenHeight! * 0.02),
                Container(
                  width: CustomTheme.screenWidth! * 1,
                  padding: EdgeInsets.symmetric(horizontal: 8, vertical: 15),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      border: Border.all(width: 1, color: Color(0xFFD8E4EE)),
                      color: Color(0xFFF3F6F9)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("HR Project",
                          style: CustomTheme.body1(context,
                              color: Colors.black,
                              fontWeight: CustomTheme.semibold)),
                      SizedBox(height: CustomTheme.screenHeight! * 0.02),
                      secondHalf!.isEmpty
                          ? Text(firstHalf!)
                          : Row(
                              children: <Widget>[
                                Expanded(
                                  child: Text(
                                      !flag
                                          ? (firstHalf! + "...")
                                          : (firstHalf! + secondHalf!),
                                      style: CustomTheme.body1(context,
                                          color: Color(0xFF828282),
                                          fontWeight: CustomTheme.medium)),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      flag = !flag;
                                    });
                                  },
                                  child: Icon(flag
                                      ? Icons.keyboard_arrow_up_rounded
                                      : Icons.keyboard_arrow_down_rounded),
                                )
                              ],
                            ),
                    ],
                  ),
                ),
              ],
            ),
          )
        : element.contain == "-"
            ? Container(
                margin: EdgeInsets.fromLTRB(25, 12, 25, 12),
                padding: EdgeInsets.fromLTRB(12, 0, 12, 12),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    color: Colors.white),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Container(
                          padding: EdgeInsets.all(12),
                          decoration: BoxDecoration(
                              color: Color(0xFFF2F4F9), shape: BoxShape.circle),
                          child: RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(
                              children: [
                                TextSpan(
                                    text: element.tgl,
                                    style: CustomTheme.headline4(context,
                                        color: Color(0xFF525F71),
                                        fontWeight: CustomTheme.semibold)),
                                TextSpan(
                                    text: element.day,
                                    style: CustomTheme.body2(context,
                                        color: Color(0xFF525F71),
                                        fontWeight: CustomTheme.medium)),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(width: CustomTheme.screenWidth! * 0.03),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(element.statusAttend,
                                  style: CustomTheme.body1(context,
                                      color: Colors.black,
                                      fontWeight: CustomTheme.semibold)),
                              SizedBox(
                                  height: CustomTheme.screenHeight! * 0.01),
                              Row(
                                children: [
                                  IconAsset.door_enter_green(),
                                  SizedBox(
                                      width: CustomTheme.screenWidth! * 0.02),
                                  Text(element.clock_in,
                                      style: CustomTheme.body2(context,
                                          color: Colors.black,
                                          fontWeight: CustomTheme.medium)),
                                  SizedBox(
                                      width: CustomTheme.screenWidth! * 0.13),
                                  IconAsset.door_exit_red(),
                                  SizedBox(
                                      width: CustomTheme.screenWidth! * 0.02),
                                  Text(element.clock_out,
                                      style: CustomTheme.body2(context,
                                          color: Colors.black,
                                          fontWeight: CustomTheme.medium)),
                                ],
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: CustomTheme.screenHeight! * 0.01),
                    Container(
                      width: CustomTheme.screenWidth! * 1,
                      padding:
                          EdgeInsets.symmetric(horizontal: 8, vertical: 15),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          border:
                              Border.all(width: 1, color: Color(0xFFD8E4EE)),
                          color: Color(0xFFF3F6F9)),
                      child: RichText(
                        textAlign: TextAlign.left,
                        text: TextSpan(
                          children: [
                            TextSpan(
                                text: element.title,
                                style: CustomTheme.body1(context,
                                    color: Colors.black,
                                    fontWeight: CustomTheme.semibold)),
                            TextSpan(
                                text: element.contain,
                                style: CustomTheme.body1(context,
                                    color: Color(0xFF828282),
                                    fontWeight: CustomTheme.medium)),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              )
            : Container(
                margin: EdgeInsets.fromLTRB(25, 12, 25, 12),
                padding: EdgeInsets.fromLTRB(12, 0, 12, 12),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    color: Colors.white),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Container(
                          padding: EdgeInsets.all(12),
                          decoration: BoxDecoration(
                              color: Color(0xFFF2F4F9), shape: BoxShape.circle),
                          child: RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(
                              children: [
                                TextSpan(
                                    text: element.tgl,
                                    style: CustomTheme.headline4(context,
                                        color: Color(0xFF525F71),
                                        fontWeight: CustomTheme.semibold)),
                                TextSpan(
                                    text: element.day,
                                    style: CustomTheme.body2(context,
                                        color: Color(0xFF525F71),
                                        fontWeight: CustomTheme.medium)),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(width: CustomTheme.screenWidth! * 0.03),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(element.statusAttend,
                                      style: CustomTheme.body1(context,
                                          color: Colors.black,
                                          fontWeight: CustomTheme.semibold)),
                                  Container(
                                    padding: EdgeInsets.all(8),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(8)),
                                        color: Color(0xFFEEEFF1)),
                                    child: Text(element.status,
                                        style: CustomTheme.caption(context,
                                            color: Colors.black,
                                            fontWeight: CustomTheme.medium)),
                                  )
                                ],
                              ),
                              Row(
                                children: [
                                  IconAsset.door_enter_green(),
                                  SizedBox(
                                      width: CustomTheme.screenWidth! * 0.02),
                                  Text(element.clock_in,
                                      style: CustomTheme.body2(context,
                                          color: Colors.black,
                                          fontWeight: CustomTheme.medium)),
                                  SizedBox(
                                      width: CustomTheme.screenWidth! * 0.13),
                                  IconAsset.door_exit_red(),
                                  SizedBox(
                                      width: CustomTheme.screenWidth! * 0.02),
                                  Text(element.clock_out,
                                      style: CustomTheme.body2(context,
                                          color: Colors.black,
                                          fontWeight: CustomTheme.medium)),
                                ],
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: CustomTheme.screenHeight! * 0.01),
                    Container(
                      width: CustomTheme.screenWidth! * 1,
                      padding:
                          EdgeInsets.symmetric(horizontal: 8, vertical: 15),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          border:
                              Border.all(width: 1, color: Color(0xFFD8E4EE)),
                          color: Color(0xFFF3F6F9)),
                      child: RichText(
                        textAlign: TextAlign.left,
                        text: TextSpan(
                          children: [
                            TextSpan(
                                text: element.title,
                                style: CustomTheme.body1(context,
                                    color: Colors.black,
                                    fontWeight: CustomTheme.semibold)),
                            TextSpan(
                                text: element.contain,
                                style: CustomTheme.body1(context,
                                    color: Color(0xFF828282),
                                    fontWeight: CustomTheme.medium)),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              );
  }

  void showModal(context) {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(12)),
        ),
        context: context,
        builder: (context) {
          return Container(
            padding: EdgeInsets.all(12),
            height: CustomTheme.screenHeight! * 0.5,
            child: SfDateRangePicker(
              minDate: Moment.now().subtract(const Duration(days: 90)),
              maxDate: Moment.now(),
              // onSelectionChanged: _onSelectionChanged,
              monthViewSettings: DateRangePickerMonthViewSettings(
                  viewHeaderStyle: DateRangePickerViewHeaderStyle(
                      textStyle: CustomTheme.body2(context,
                          color: Color(0xFFEB7D2E),
                          fontWeight: CustomTheme.medium))),
              startRangeSelectionColor: Color(0xFFEB7D2E),
              endRangeSelectionColor: Color(0xFFEB7D2E),
              rangeSelectionColor: Color(0xFFFFA15E),
              rangeTextStyle: CustomTheme.body2(context,
                  color: Colors.white, fontWeight: CustomTheme.medium),
              selectionTextStyle: CustomTheme.body2(context,
                  color: Colors.white, fontWeight: CustomTheme.medium),
              headerStyle: DateRangePickerHeaderStyle(
                  textAlign: TextAlign.center,
                  textStyle: CustomTheme.body1(context,
                      color: Colors.black, fontWeight: CustomTheme.semibold)),
              selectionMode: DateRangePickerSelectionMode.range,
              controller: _datePickerController,
              showActionButtons: true,
              onSubmit: (value) async {
                debugPrint("VALUE: " + value.toString());

                if (value is PickerDateRange) {
                  final DateTime rangeStartDate = value.startDate!;
                  final DateTime rangeEndDate = value.endDate ?? rangeStartDate;

                  setState(() {
                    _startDate = rangeStartDate;
                    _endDate = rangeEndDate;
                    _pageNo = 0;
                  });

                  fetchMyLog(_pageNo);
                }

                Navigator.pop(context);
              },
              onCancel: () {
                Navigator.pop(context);
              },
            ),
          );
        });
  }

  // void _onSelectionChanged(DateRangePickerSelectionChangedArgs args) {}
}
