import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hr_project/config/API%20config/authentication-api/update_password.dart';
import 'package:hr_project/model/authentication-model/update_password_model.dart';
import 'package:hr_project/styling_theme.dart';
import 'package:hr_project/utils/unauthorized.dart';
import 'package:hr_project/widget/custom_appbar.dart';
import 'package:hr_project/widget/password_textfield.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ChangePassPage extends StatefulWidget {
  const ChangePassPage({Key? key}) : super(key: key);

  @override
  State<ChangePassPage> createState() => _ChangePassPageState();
}

class _ChangePassPageState extends State<ChangePassPage> {
  final GlobalKey<FormState> _form = GlobalKey<FormState>();
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  TextEditingController current_pass = TextEditingController();
  TextEditingController new_pass = TextEditingController();
  TextEditingController re_pass = TextEditingController();
  UpdatePasswordModel? changePassRes;
  bool _passwordVisible1 = false;
  bool _passwordVisible2 = false;
  bool _passwordVisible3 = false;
  bool _isLoad = false;

  changePass() async {
    setState(() {
      _isLoad = true;
    });
    final SharedPreferences prefs = await _prefs;
    var res = await updatePasswordAPI(
        token: prefs.getString("accessToken")!,
        old_password: current_pass.text,
        new_password: re_pass.text);
    changePassRes = await UpdatePasswordModel.fromJson(json.decode(res.body));
    setState(() {
      _isLoad = false;
    });
    dialog(res);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFFF6F9FF),
        appBar: CustomAppbar(title: "Change Password"),
        bottomSheet: customSheet(),
        body: _isLoad
            ? Center(child: CircularProgressIndicator())
            : Form(
                key: _form,
                child: Padding(
                  padding: EdgeInsets.all(25),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      PassTextField(
                          controller: current_pass,
                          passVisible: _passwordVisible1,
                          validateCheck: (val) {
                            if (val!.isEmpty) return 'This field cannot empty';
                            return null;
                          },
                          title: 'Current Password'),
                      PassTextField(
                          controller: new_pass,
                          passVisible: _passwordVisible2,
                          validateCheck: (val) {
                            if (val!.isEmpty) return 'This field cannot empty';
                            return null;
                          },
                          title: 'New Password'),
                      PassTextField(
                          controller: re_pass,
                          passVisible: _passwordVisible3,
                          validateCheck: (val) {
                            if (val!.isEmpty) return 'This field cannot empty';
                            if (val != new_pass.text)
                              return 'Password did not match';
                            return null;
                          },
                          title: 'Re-type New Password'),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  customSheet() {
    return Container(
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3),
          ),
        ],
      ),
      child: ElevatedButton(
          onPressed: () {
            if (_form.currentState!.validate()) {
              changePass();
            }
          },
          style: ElevatedButton.styleFrom(
              padding: EdgeInsets.symmetric(vertical: 12),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              minimumSize: Size(double.infinity, 0),
              backgroundColor: Color(0xFFEB7D2E)),
          child: Text(
            "Save",
            style: CustomTheme.button(context,
                color: Colors.white, fontWeight: CustomTheme.medium),
          )),
    );
  }

  dialog(Response? statusCode) {
    return showDialog(
      context: context,
      builder: (context) {
        return Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
          child: changePassRes!.errorCode != "200000"
              ? Container(
                  padding: EdgeInsets.symmetric(horizontal: 12),
                  height: CustomTheme.screenHeight! * 0.25,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Change Password Failed",
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold),
                      ),
                      SizedBox(height: CustomTheme.screenHeight! * 0.01),
                      Text(
                        changePassRes!.errorMessage ?? "",
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium),
                      ),
                      SizedBox(height: CustomTheme.screenHeight! * 0.05),
                      ElevatedButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.symmetric(vertical: 15),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              minimumSize: Size(double.infinity, 0),
                              backgroundColor: Color(0xFFEB7D2E)),
                          child: Text(
                            "Ok",
                            style: CustomTheme.button(context,
                                color: Colors.white,
                                fontWeight: CustomTheme.medium),
                          ))
                    ],
                  ),
                )
              : Container(
                  padding: EdgeInsets.symmetric(horizontal: 12),
                  height: CustomTheme.screenHeight! * 0.5,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                          height: CustomTheme.screenHeight! * 0.2,
                          child: LottieAsset.success()),
                      Text(
                        "Change Password Success",
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold),
                      ),
                      SizedBox(height: CustomTheme.screenHeight! * 0.01),
                      Text(
                        "Yeah! You successfully change your password",
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium),
                      ),
                      SizedBox(height: CustomTheme.screenHeight! * 0.05),
                      ElevatedButton(
                          onPressed: () {
                            Navigator.pop(context);
                            unauthorized(context);
                          },
                          style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.symmetric(vertical: 15),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              minimumSize: Size(double.infinity, 0),
                              backgroundColor: Color(0xFFEB7D2E)),
                          child: Text(
                            "Ok",
                            style: CustomTheme.button(context,
                                color: Colors.white,
                                fontWeight: CustomTheme.medium),
                          ))
                    ],
                  ),
                ),
        );
      },
    );
  }
}
