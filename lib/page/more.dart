import 'package:flutter/material.dart';
import 'package:hr_project/list_dummy.dart';
import 'package:hr_project/styling_theme.dart';
import 'package:hr_project/widget/custom_appbar.dart';
import 'package:hr_project/widget/grid_menu_home.dart';

class MorePage extends StatefulWidget {
  const MorePage({Key? key}) : super(key: key);

  @override
  State<MorePage> createState() => _MorePageState();
}

class _MorePageState extends State<MorePage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFFF6F9FF),
        appBar: CustomAppbar(title: "Menu"),
        body: Column(
          children: [
            top(),
            SizedBox(height: CustomTheme.screenHeight! * 0.05),
            bottom(),
          ],
        ),
      ),
    );
  }

  top() {
    return Container(
      padding: EdgeInsets.all(15),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Featured",
            style: CustomTheme.subtitle(context,
                color: Color(0xFF5981EA), fontWeight: CustomTheme.semibold),
          ),
          SizedBox(height: CustomTheme.screenHeight! * 0.05),
          GridView.builder(
            shrinkWrap: true,
            itemCount: ListDummy.gridMenuDetail1.length,
            physics: NeverScrollableScrollPhysics(),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 4,
              mainAxisSpacing: 20,
            ),
            itemBuilder: (BuildContext context, int index) {
              return GridMenuHome(
                  icon: ListDummy.gridMenuDetail1[index].icon,
                  title: ListDummy.gridMenuDetail1[index].title,
                  directPage: ListDummy.gridMenuDetail1[index].directPage);
            },
          ),
        ],
      ),
    );
  }

  bottom() {
    return Container(
      padding: EdgeInsets.all(15),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Management",
            style: CustomTheme.subtitle(context,
                color: Color(0xFF5981EA), fontWeight: CustomTheme.semibold),
          ),
          SizedBox(height: CustomTheme.screenHeight! * 0.05),
          GridView.builder(
            shrinkWrap: true,
            itemCount: ListDummy.gridMenuDetail2.length,
            physics: NeverScrollableScrollPhysics(),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 4,
            ),
            itemBuilder: (BuildContext context, int index) {
              return GridMenuHome(
                  icon: ListDummy.gridMenuDetail2[index].icon,
                  title: ListDummy.gridMenuDetail2[index].title,
                  directPage: ListDummy.gridMenuDetail2[index].directPage);
            },
          ),
        ],
      ),
    );
  }
}
