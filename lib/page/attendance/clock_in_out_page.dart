import 'dart:convert';
import 'package:hr_project/utils/unauthorized.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:hr_project/config/API%20config/attendances-api/attendances_clock_inout.dart';
import 'package:hr_project/styling_theme.dart';
import 'package:hr_project/widget/custom_appbar.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:slide_to_act/slide_to_act.dart';

import '../../database local/db_helper.dart';
import '../../database local/projectList-model.dart';
import '../../model/attendances-model/attendances_clockin-out.dart';

class ClockInOutPage extends StatefulWidget {
  final String appbarTitle;
  final String attendId;
  const ClockInOutPage(
      {Key? key, required this.appbarTitle, required this.attendId})
      : super(key: key);

  @override
  State<ClockInOutPage> createState() => _ClockInOutPageState();
}

class _ClockInOutPageState extends State<ClockInOutPage> {
  late List<TodayProjectList> todayProject = [];
  TextEditingController dailytask = TextEditingController();
  AttendancesClockInOutModel? attendInOutRes;
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  bool _isLoading = false;
  String gender = "wfo";
  int itemDrop = 4;
  String _selected = 'None';
  bool isDouble = false;

  List<Widget> listForm = [];
  String formattedTime = "";
  String formattedDate = "";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    final time = DateTime.now();
    formattedTime = DateFormat('hh : mm : ss a').format(time);
    formattedDate = DateFormat('EEEE, d MMMM yyyy').format(time);

    print(formattedTime); // Output: 07:49:54 AM
    listForm.add(Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        bottomDropdown(),
        SizedBox(height: CustomTheme.screenHeight! * 0.03),
        dailyTask(),
      ],
    ));
    refreshProject();
  }

  clock_in_out() async {
    setState(() {
      _isLoading = true;
    });
    final SharedPreferences prefs = await _prefs;
    var res = await attendancesClockInOut_API(
        type: widget.appbarTitle,
        dailyTask: dailytask.text,
        workplace: gender,
        projectId: itemDrop,
        token: prefs.getString("accessToken")!,
        clock_in: widget.appbarTitle == "Clock In"
            ? DateFormat("yyyy-MM-ddTHH:mm:ssZ").format(DateTime.now())
            : "",
        clock_out: widget.appbarTitle == "Clock Out"
            ? DateFormat("yyyy-MM-ddTHH:mm:ssZ").format(DateTime.now())
            : "",
        attendance_id:
            widget.appbarTitle == "Clock Out" ? widget.attendId : "");
    if (res.statusCode == 200) {
      attendInOutRes =
          AttendancesClockInOutModel.fromJson(json.decode(res.body));
      if (attendInOutRes!.errorMessage == "invalid authorization") {
        unauthorized(context);
      }
      setState(() {
        _isLoading = false;
      });
      dialog(res);
    } else {
      attendInOutRes =
          AttendancesClockInOutModel.fromJson(json.decode(res.body));
      setState(() {
        _isLoading = false;
      });
      dialog(res);
    }
  }

  Future refreshProject() async {
    this.todayProject = await ProjectDatabase.instance.readAll();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFFF6F9FF),
        appBar: CustomAppbar(title: widget.appbarTitle),
        bottomSheet: customSheet(),
        body: Stack(
          children: [
            SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.all(12),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    top(),
                    SizedBox(height: CustomTheme.screenHeight! * 0.03),
                    radioButton(),
                    SizedBox(height: CustomTheme.screenHeight! * 0.03),
                    ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: listForm.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: CustomTheme.screenHeight! * 0.03),
                            listForm[index],
                            SizedBox(height: CustomTheme.screenHeight! * 0.02),
                            // addNew(index),
                          ],
                        );
                      },
                    ),
                    // bottomDropdown(),
                    // SizedBox(height: CustomTheme.screenHeight! * 0.03),
                    // dailyTask(),
                    SizedBox(height: CustomTheme.screenHeight! * 0.15),
                  ],
                ),
              ),
            ),
            _isLoading
                ? Container(
                    color: Colors.white.withOpacity(0.5),
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  )
                : SizedBox.shrink()
          ],
        ),
      ),
    );
  }

  radioButton() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Workplace *",
          style: CustomTheme.body1(context,
              color: Colors.black, fontWeight: CustomTheme.semibold),
        ),
        RadioListTile(
          title: Text(
            "WFH (Work From Home)",
            style: CustomTheme.body1(context,
                color: Colors.black, fontWeight: CustomTheme.medium),
          ),
          activeColor: Color(0xFFEB7D2E),
          value: "wfh",
          groupValue: gender,
          onChanged: (value) {
            setState(() {
              gender = value.toString();
            });
          },
        ),
        RadioListTile(
          title: Text(
            "WFO (Work From Office)",
            style: CustomTheme.body1(context,
                color: Colors.black, fontWeight: CustomTheme.medium),
          ),
          value: "wfo",
          activeColor: Color(0xFFEB7D2E),
          groupValue: gender,
          onChanged: (value) {
            setState(() {
              gender = value.toString();
            });
          },
        ),
      ],
    );
  }

  customSheet() {
    return Container(
      height: CustomTheme.screenHeight! * 0.13,
      padding: EdgeInsets.all(15),
      color: Colors.white,
      // child: ElevatedButton(onPressed: () {}, child: Text("???")),
      child: SlideAction(
        sliderButtonIconPadding: 14,
        sliderButtonYOffset: 0,
        borderRadius: 5,
        elevation: 0,
        innerColor: Color(0xFFEB7D2E),
        outerColor: Color(0xFFFFA15E),
        submittedIcon: Icon(Icons.check, color: Colors.white),
        sliderButtonIcon:
            Icon(Icons.arrow_forward_rounded, color: Colors.white),
        text: widget.appbarTitle == "Clock Out"
            ? "SWIPE TO CLOCK OUT"
            : "SWIPE TO CLOCK IN",
        textStyle: CustomTheme.subtitle(context,
            color: Colors.white, fontWeight: CustomTheme.semibold),
        sliderRotate: false,
        onSubmit: () {
          clock_in_out();
        },
      ),
    );
  }

  bottomDropdown() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Today’s Project *",
          style: CustomTheme.body1(context,
              color: Colors.black, fontWeight: CustomTheme.semibold),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.02),
        InkWell(
          onTap: () => showModal(context),
          child: Container(
            padding: EdgeInsets.all(6),
            width: CustomTheme.screenWidth! * 1,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                border: Border.all(width: 1, color: Color(0xFFB8CADB)),
                color: Colors.white),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(_selected,
                    style: CustomTheme.body1(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Icon(Icons.keyboard_arrow_down_outlined,
                    color: Color(0xFFB8CADB))
              ],
            ),
          ),
        )
      ],
    );
  }

  void showModal(context) {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(12)),
        ),
        context: context,
        builder: (context) {
          return Container(
            padding: EdgeInsets.fromLTRB(12, 12, 12, 0),
            height: CustomTheme.screenHeight! * 0.75,
            child: Column(
              children: [
                Text("Select Project",
                    style: CustomTheme.body1(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
                SizedBox(height: CustomTheme.screenHeight! * 0.03),
                Container(
                  height: CustomTheme.screenHeight! * 0.07,
                  child: TextFormField(
                    style: CustomTheme.body1(context,
                        color: Colors.black, fontWeight: CustomTheme.medium),
                    decoration: InputDecoration(
                      floatingLabelBehavior: FloatingLabelBehavior.never,
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 30, horizontal: 10),
                      labelText: "Search project",
                      labelStyle: CustomTheme.body1(context,
                          color: Color(0xFFBDBDBD),
                          fontWeight: CustomTheme.medium),
                      filled: true,
                      fillColor: Color(0xFFF2F4F9),
                      prefixIcon: Icon(Icons.search, color: Color(0xFFE0E0E0)),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                    ),
                  ),
                ),
                SizedBox(height: CustomTheme.screenHeight! * 0.03),
                ListView.builder(
                    shrinkWrap: true,
                    itemCount: todayProject.length,
                    itemBuilder: (context, index) {
                      var item = todayProject[index];
                      return RadioListTile(
                        title: Text(item.nameProject,
                            style: CustomTheme.body1(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.medium)),
                        activeColor: Color(0xFFEB7D2E),
                        value: item.idProject,
                        groupValue: itemDrop,
                        onChanged: (value) {
                          setState(() {
                            itemDrop = value!;
                            _selected = item.nameProject;
                          });
                          Navigator.of(context).pop();
                          print("VALUE DROP: " + itemDrop.toString());
                        },
                      );
                    }),
              ],
            ),
          );
        });
  }

  dailyTask() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Daily Task *",
          style: CustomTheme.body1(context,
              color: Colors.black, fontWeight: CustomTheme.semibold),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.02),
        TextFormField(
          controller: dailytask,
          keyboardType: TextInputType.multiline,
          maxLines: null,
          style: CustomTheme.body1(context,
              color: Colors.black, fontWeight: CustomTheme.medium),
          decoration: InputDecoration(
            floatingLabelBehavior: FloatingLabelBehavior.never,
            filled: true,
            fillColor: Colors.white,
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(color: Color(0xFFB8CADB))),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(color: Color(0xFFB8CADB))),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(color: Color(0xFFB8CADB))),
          ),
        ),
      ],
    );
  }

  dialog(Response? statusCode) {
    return showDialog(
      context: context,
      builder: (context) {
        return Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
          child: attendInOutRes!.errorMessage ==
                      "User has already clocked in for today." ||
                  statusCode!.statusCode != 201
              ? Container(
                  padding: EdgeInsets.symmetric(horizontal: 12),
                  height: CustomTheme.screenHeight! * 0.25,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        widget.appbarTitle + " Failed",
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold),
                      ),
                      SizedBox(height: CustomTheme.screenHeight! * 0.01),
                      Text(
                        attendInOutRes!.errorMessage ?? "",
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium),
                      ),
                      SizedBox(height: CustomTheme.screenHeight! * 0.05),
                      ElevatedButton(
                          onPressed: () {
                            Navigator.pop(context);
                            Navigator.pop(context, 'update');
                            // page sebelumnya onactivityresult cek sudah belum clock in/out
                          },
                          style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.symmetric(vertical: 15),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              minimumSize: Size(double.infinity, 0),
                              backgroundColor: Color(0xFFEB7D2E)),
                          child: Text(
                            "Ok",
                            style: CustomTheme.button(context,
                                color: Colors.white,
                                fontWeight: CustomTheme.medium),
                          ))
                    ],
                  ),
                )
              : Container(
                  padding: EdgeInsets.symmetric(horizontal: 12),
                  height: CustomTheme.screenHeight! * 0.5,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                          height: CustomTheme.screenHeight! * 0.27,
                          child: LottieAsset.success()),
                      Text(
                        widget.appbarTitle + " Success",
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold),
                      ),
                      SizedBox(height: CustomTheme.screenHeight! * 0.01),
                      Text(
                        "Yeah! You successfully " + widget.appbarTitle,
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium),
                      ),
                      SizedBox(height: CustomTheme.screenHeight! * 0.05),
                      ElevatedButton(
                          onPressed: () {
                            Navigator.pop(context);
                            Navigator.pop(context, 'update');
                          },
                          style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.symmetric(vertical: 15),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              minimumSize: Size(double.infinity, 0),
                              backgroundColor: Color(0xFFEB7D2E)),
                          child: Text(
                            "Ok",
                            style: CustomTheme.button(context,
                                color: Colors.white,
                                fontWeight: CustomTheme.medium),
                          ))
                    ],
                  ),
                ),
        );
      },
    );
  }

  top() {
    return Card(
      color: Colors.white,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8))),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(15),
            decoration: BoxDecoration(
              border: Border(
                  bottom: BorderSide(width: 0.5, color: Color(0xFFB8CADB))),
            ),
            child: Center(
              child: Text(
                formattedTime,
                style: CustomTheme.headline3(context,
                    color: Color(0xFFEB7D2E), fontWeight: CustomTheme.semibold),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(15),
            decoration: BoxDecoration(
              border:
                  Border(top: BorderSide(width: 0.5, color: Color(0xFFB8CADB))),
            ),
            child: Center(
              child: Text(
                formattedDate,
                style: CustomTheme.subtitle(context,
                    color: Color(0xFF525F71), fontWeight: CustomTheme.semibold),
              ),
            ),
          ),
        ],
      ),
    );
  }

  addNew(int index) {
    return listForm.length > 1
        ? Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              GestureDetector(
                onTap: () {
                  setState(() {
                    listForm.add(Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        bottomDropdown(),
                        SizedBox(height: CustomTheme.screenHeight! * 0.03),
                        dailyTask(),
                      ],
                    ));
                  });
                },
                child: Row(
                  children: [
                    CircleAvatar(
                      radius: 10,
                      backgroundColor: Color(0xFFEB7D2E),
                      child: Icon(
                        Icons.add,
                        color: Colors.white,
                        size: 17,
                      ),
                    ),
                    SizedBox(width: CustomTheme.screenWidth! * 0.03),
                    Text(
                      "Add other project",
                      style: CustomTheme.body2(context,
                          color: Color(0xFFEB7D2E),
                          fontWeight: CustomTheme.semibold),
                    ),
                  ],
                ),
              ),
              GestureDetector(
                onTap: () {
                  setState(() {
                    // removeItemByIndex(index);
                    print("INDEX LIST KE-" + index.toString());
                    listForm.removeAt(index);
                  });
                },
                child: Text(
                  "Remove",
                  style: CustomTheme.body2(context,
                      color: Color(0xFFB8CADB), fontWeight: CustomTheme.medium),
                ),
              ),
            ],
          )
        : GestureDetector(
            onTap: () {
              setState(() {
                listForm.add(Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    bottomDropdown(),
                    SizedBox(height: CustomTheme.screenHeight! * 0.03),
                    dailyTask(),
                  ],
                ));
              });
            },
            child: Row(
              children: [
                CircleAvatar(
                  radius: 10,
                  backgroundColor: Color(0xFFEB7D2E),
                  child: Icon(
                    Icons.add,
                    color: Colors.white,
                    size: 17,
                  ),
                ),
                SizedBox(width: CustomTheme.screenWidth! * 0.03),
                Text(
                  "Add other project",
                  style: CustomTheme.body2(context,
                      color: Color(0xFFEB7D2E),
                      fontWeight: CustomTheme.semibold),
                ),
              ],
            ),
          );
  }
}
