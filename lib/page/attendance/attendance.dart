import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hr_project/config/API%20config/attendances-api/attendance_week.dart';
import 'package:hr_project/model/attendances-model/attendances_week.dart';
import 'package:hr_project/page/attendance/clock_in_out_page.dart';
import 'package:hr_project/styling_theme.dart';
import 'package:hr_project/utils/capitalize.dart';
import 'package:hr_project/utils/datetime_format.dart';
import 'package:hr_project/utils/unauthorized.dart';
import 'package:hr_project/widget/clock_in_out_button.dart';
import 'package:hr_project/widget/custom_appbar.dart';
import 'package:hr_project/widget/stat_percent_header.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../config/API config/attendances-api/attendance_today.dart';
import '../../model/attendances-model/attendances_today.dart';

class AttendancePage extends StatefulWidget {
  const AttendancePage({Key? key}) : super(key: key);

  @override
  State<AttendancePage> createState() => _AttendancePageState();
}

class _AttendancePageState extends State<AttendancePage> {
  bool isList = true;
  String text =
      "Status log data need to be saved at the time of clock in Attendance for Log module to show the summary.";
  String? firstHalf;
  String? secondHalf;
  bool flag = true;
  AttendanceTodayModel? attendTodayRes;
  AttendanceWeekModel? attendWeekRes;
  List<Attendance>? attendWeekList;
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  bool hasClockIn = false;
  bool hasClockOut = true;
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    // cek sudah clock in/out atau belum

    if (text.length > 35) {
      firstHalf = text.substring(0, 35);
      secondHalf = text.substring(35, text.length);
    } else {
      firstHalf = text;
      secondHalf = "";
    }
    checkClockIn();
  }

  checkClockIn() async {
    setState(() {
      _isLoading = true;
    });
    final SharedPreferences prefs = await _prefs;
    var res =
        await attendancesToday_API(token: prefs.getString("accessToken")!);
    attendTodayRes = AttendanceTodayModel.fromJson(json.decode(res.body));
    if (attendTodayRes!.errorMessage == "invalid authorization") {
      unauthorized(context);
    }
    if (attendTodayRes!.data != null) {
      setState(() {
        attendList();
        hasClockIn = true;
      });
    } else {
      setState(() {
        attendList();
        hasClockIn = false;
      });
    }
    if (attendTodayRes!.data != null) {
      if (attendTodayRes!.data!.clockOut != null) {
        setState(() {
          attendList();
          hasClockOut = true;
        });
      } else {
        setState(() {
          attendList();
          hasClockOut = false;
        });
      }
    } else {
      setState(() {
        attendList();
        hasClockOut = true;
      });
    }
  }

  attendList() async {
    final SharedPreferences prefs = await _prefs;
    var res = await attendancesWeek_API(token: prefs.getString("accessToken")!);
    attendWeekRes = AttendanceWeekModel.fromJson(json.decode(res.body));
    if (attendWeekRes!.data != null) {
      setState(() {
        attendWeekList = attendWeekRes!.data!.attendances;
        _isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFFF6F9FF),
        appBar: CustomAppbar(title: "Attendance"),
        body: _isLoading
            ? Center(child: CircularProgressIndicator())
            : SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      top(),
                      SizedBox(height: CustomTheme.screenHeight! * 0.03),
                      attendance(),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  top() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              color: Colors.white),
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.all(8),
                decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(width: 1, color: Color(0xFFB8CADB))),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CustomHeaderButton(
                        color:
                            hasClockIn ? Color(0xFFD8E4EE) : Color(0xFF27AE60),
                        icon: hasClockIn
                            ? IconAsset.door_exit_grey()
                            : IconAsset.door_enter(),
                        text: "Clock\nIn",
                        colorText:
                            hasClockIn ? Color(0xFFB8CADB) : Colors.white,
                        onPress: hasClockIn
                            ? null
                            : () async {
                                await Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => ClockInOutPage(
                                            appbarTitle: "Clock In",
                                            attendId:
                                                attendTodayRes!.data == null
                                                    ? ""
                                                    : attendTodayRes!
                                                        .data!.attendanceId!)));
                                await checkClockIn();
                              },
                        customWidth: CustomTheme.screenWidth! * 0.4,
                        isCenter: false),
                    CustomHeaderButton(
                        color:
                            hasClockOut ? Color(0xFFD8E4EE) : Color(0xFFF44336),
                        icon: hasClockOut
                            ? IconAsset.door_exit_grey()
                            : IconAsset.door_exit(),
                        text: "Clock\nOut",
                        colorText:
                            hasClockOut ? Color(0xFFB8CADB) : Colors.white,
                        onPress: hasClockOut
                            ? null
                            : () async {
                                await Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => ClockInOutPage(
                                            appbarTitle: "Clock Out",
                                            attendId:
                                                attendTodayRes!.data == null
                                                    ? ""
                                                    : attendTodayRes!
                                                        .data!.attendanceId!)));
                                await checkClockIn();
                              },
                        customWidth: CustomTheme.screenWidth! * 0.4,
                        isCenter: false),
                  ],
                ),
              ),
              Row(
                children: [
                  StatHeader(
                      isSecond: false,
                      pointValue: "",
                      titlePoint: "",
                      percentValue:
                          attendWeekRes!.data!.punctualityValue == null
                              ? "-%"
                              : "${attendWeekRes!.data!.punctualityValue}%",
                      titleValue: "Punctuality"),
                  StatHeader(
                      isSecond: true,
                      pointValue:
                          "${attendWeekRes!.data!.totalEmployeeClockIn}",
                      titlePoint: "Attendees",
                      percentValue: "",
                      titleValue: ""),
                ],
              )
            ],
          ),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.05),
        Container(
          padding: EdgeInsets.all(12),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              border: Border.all(width: 1, color: Color(0xFFFFA15E)),
              color: Color(0xFFFFFCF2)),
          child: Row(
            children: [
              IconAsset.bulb(),
              SizedBox(width: CustomTheme.screenWidth! * 0.02),
              Text(
                attendWeekRes!.data!.punctualityMessage == null
                    ? "-"
                    : attendWeekRes!.data!.punctualityMessage!,
                style: CustomTheme.body2(context,
                    color: Colors.black, fontWeight: CustomTheme.medium),
              ),
            ],
          ),
        )
      ],
    );
  }

  attendance() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text("This Week",
                style: CustomTheme.body1(context,
                    color: Colors.black, fontWeight: CustomTheme.semibold)),
            Container(
              padding: EdgeInsets.all(8),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(25)),
                  color: Color(0xFFEBF1FF)),
              child: Text(
                attendWeekRes!.data!.dateRange!,
                style: CustomTheme.body1(context,
                    color: Colors.black, fontWeight: CustomTheme.semibold),
              ),
            ),
          ],
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.03),
        list(),
        // noList(),
      ],
    );
  }

  noList() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.all(12),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color(0xFFD8E4EE),
                Color(0xFFD8E4EE).withOpacity(0.0),
              ],
            ),
          ),
          child: IconAsset.leaves(),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.03),
        Text(
          "Your record is empty",
          style: CustomTheme.body1(context,
              color: Color(0xFF525F71), fontWeight: CustomTheme.semibold),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.01),
        Text(
          "Let’s start your day by tap on the\nClock In button!",
          textAlign: TextAlign.center,
          style: CustomTheme.body1(context,
              color: Color(0xFFB8CADB), fontWeight: CustomTheme.medium),
        ),
      ],
    );
  }

  list() {
    return ListView.separated(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: attendWeekList!.length,
      separatorBuilder: (BuildContext context, int index) =>
          SizedBox(height: CustomTheme.screenHeight! * 0.02),
      itemBuilder: (BuildContext context, int index) {
        var item = attendWeekList![index];
        String clockIn = datetime_format(item.clockIn, 'hh:mm A');
        String clockOut = datetime_format(item.clockOut, 'hh:mm A');
        return index == 0
            ? Container(
                padding: EdgeInsets.fromLTRB(10, 4, 10, 10),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    color: Colors.white),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Container(
                          padding: EdgeInsets.all(12),
                          decoration: BoxDecoration(
                              color: Color(0xFFFFF4ED), shape: BoxShape.circle),
                          child: RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(
                              children: [
                                TextSpan(
                                    text: DateFormat('dd', 'en_US')
                                            .format(item.attendanceDate!) +
                                        "\n",
                                    style: CustomTheme.headline4(context,
                                        color: Color(0xFFEB7D2E),
                                        fontWeight: CustomTheme.semibold)),
                                TextSpan(
                                    text: DateFormat('E', 'en_US')
                                        .format(item.attendanceDate!),
                                    style: CustomTheme.body2(context,
                                        color: Color(0xFFEB7D2E),
                                        fontWeight: CustomTheme.medium)),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(width: CustomTheme.screenWidth! * 0.03),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                      item.attendanceStatus == 'NORECORD'
                                          ? 'No Record'
                                          : capitalize(item.event.toString()),
                                      style: CustomTheme.body1(context,
                                          color: Colors.black,
                                          fontWeight: CustomTheme.semibold)),
                                  item.attendanceStatus == 'NORECORD'
                                      ? Container(
                                          padding: EdgeInsets.all(8),
                                          child: Text(' ',
                                              style: CustomTheme.overline(
                                                  context,
                                                  color: Colors.black,
                                                  fontWeight:
                                                      CustomTheme.regular)),
                                        )
                                      : Container(
                                          padding: EdgeInsets.all(8),
                                          decoration: BoxDecoration(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(8)),
                                              color: Color(0xFFEEEFF1)),
                                          child: Text(
                                              item.attendanceStatus == 'ONTIME'
                                                  ? 'On Time'
                                                  : item.attendanceStatus ==
                                                          'LATE'
                                                      ? 'Late'
                                                      : 'Leave',
                                              style: CustomTheme.overline(
                                                  context,
                                                  color: Colors.black,
                                                  fontWeight:
                                                      CustomTheme.regular)),
                                        )
                                ],
                              ),
                              SizedBox(
                                  height: CustomTheme.screenHeight! * 0.01),
                              Row(
                                children: [
                                  Expanded(
                                    flex: 3,
                                    child: Row(
                                      children: [
                                        IconAsset.door_enter_green(),
                                        SizedBox(
                                            width: CustomTheme.screenWidth! *
                                                0.02),
                                        Text(
                                          clockIn,
                                          style: CustomTheme.body2(
                                            context,
                                            color: Colors.black,
                                            fontWeight: CustomTheme.semibold,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: Row(
                                      children: [
                                        IconAsset.door_exit_red(),
                                        SizedBox(
                                            width: CustomTheme.screenWidth! *
                                                0.02),
                                        Text(
                                          clockOut,
                                          style: CustomTheme.body2(
                                            context,
                                            color: Colors.black,
                                            fontWeight: CustomTheme.semibold,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: CustomTheme.screenHeight! * 0.01),
                    Container(
                      width: CustomTheme.screenWidth! * 1,
                      padding:
                          EdgeInsets.symmetric(horizontal: 8, vertical: 15),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          border:
                              Border.all(width: 1, color: Color(0xFFD8E4EE)),
                          color: Color(0xFFF3F6F9)),
                      child: RichText(
                        textAlign: TextAlign.left,
                        text: TextSpan(
                          children: [
                            TextSpan(
                                text: item.attendanceStatus == 'NORECORD'
                                    ? '-\n\n'
                                    : "${item.project!.projectName}\n\n",
                                style: CustomTheme.body1(context,
                                    color: Colors.black,
                                    fontWeight: CustomTheme.semibold)),
                            TextSpan(
                                text: item.dailyTask!,
                                style: CustomTheme.body1(context,
                                    color: Color(0xFF828282),
                                    fontWeight: CustomTheme.medium)),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              )
            : Container(
                padding: EdgeInsets.fromLTRB(10, 4, 10, 10),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    color: Colors.white),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Container(
                          padding: EdgeInsets.all(12),
                          decoration: BoxDecoration(
                              color: Color(0xFFF2F4F9), shape: BoxShape.circle),
                          child: RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(
                              children: [
                                TextSpan(
                                    text: DateFormat('dd', 'en_US')
                                            .format(item.attendanceDate!) +
                                        "\n",
                                    style: CustomTheme.headline4(context,
                                        color: Color(0xFF525F71),
                                        fontWeight: CustomTheme.semibold)),
                                TextSpan(
                                    text: DateFormat('E', 'en_US')
                                        .format(item.attendanceDate!),
                                    style: CustomTheme.body2(context,
                                        color: Color(0xFF525F71),
                                        fontWeight: CustomTheme.medium)),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(width: CustomTheme.screenWidth! * 0.03),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                      item.attendanceStatus == 'NORECORD'
                                          ? 'No Record'
                                          : capitalize(item.event.toString()),
                                      style: CustomTheme.body1(context,
                                          color: Colors.black,
                                          fontWeight: CustomTheme.semibold)),
                                  item.attendanceStatus == 'NORECORD'
                                      ? Container(
                                          padding: EdgeInsets.all(8),
                                          child: Text(' ',
                                              style: CustomTheme.overline(
                                                  context,
                                                  color: Colors.black,
                                                  fontWeight:
                                                      CustomTheme.regular)),
                                        )
                                      : Container(
                                          padding: EdgeInsets.all(8),
                                          decoration: BoxDecoration(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(8)),
                                              color: Color(0xFFEEEFF1)),
                                          child: Text(
                                              item.attendanceStatus == 'ONTIME'
                                                  ? 'On Time'
                                                  : item.attendanceStatus ==
                                                          'LATE'
                                                      ? 'Late'
                                                      : 'Leave',
                                              style: CustomTheme.overline(
                                                  context,
                                                  color: Colors.black,
                                                  fontWeight:
                                                      CustomTheme.regular)),
                                        )
                                ],
                              ),
                              SizedBox(
                                  height: CustomTheme.screenHeight! * 0.01),
                              Row(
                                children: [
                                  Expanded(
                                    flex: 3,
                                    child: Row(
                                      children: [
                                        IconAsset.door_enter_green(),
                                        SizedBox(
                                            width: CustomTheme.screenWidth! *
                                                0.02),
                                        Text(
                                          clockIn,
                                          style: CustomTheme.body2(
                                            context,
                                            color: Colors.black,
                                            fontWeight: CustomTheme.semibold,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: Row(
                                      children: [
                                        IconAsset.door_exit_red(),
                                        SizedBox(
                                            width: CustomTheme.screenWidth! *
                                                0.02),
                                        Text(
                                          clockOut,
                                          style: CustomTheme.body2(
                                            context,
                                            color: Colors.black,
                                            fontWeight: CustomTheme.semibold,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: CustomTheme.screenHeight! * 0.01),
                    Container(
                      width: CustomTheme.screenWidth! * 1,
                      padding:
                          EdgeInsets.symmetric(horizontal: 8, vertical: 15),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          border:
                              Border.all(width: 1, color: Color(0xFFD8E4EE)),
                          color: Color(0xFFF3F6F9)),
                      child: RichText(
                        textAlign: TextAlign.left,
                        text: TextSpan(
                          children: [
                            TextSpan(
                                text: item.attendanceStatus == 'NORECORD'
                                    ? '-\n\n'
                                    : "${item.project!.projectName}\n\n",
                                style: CustomTheme.body1(context,
                                    color: Colors.black,
                                    fontWeight: CustomTheme.semibold)),
                            TextSpan(
                                text: item.dailyTask == ""
                                    ? "-"
                                    : item.dailyTask!,
                                style: CustomTheme.body1(context,
                                    color: Color(0xFF828282),
                                    fontWeight: CustomTheme.medium)),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              );
      },
    );
  }
}
