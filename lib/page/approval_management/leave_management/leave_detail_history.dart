import 'package:flutter/material.dart';
import 'package:hr_project/styling_theme.dart';
import 'package:hr_project/widget/custom_appbar.dart';

class LeaveDetailHistory extends StatefulWidget {
  const LeaveDetailHistory({Key? key}) : super(key: key);

  @override
  State<LeaveDetailHistory> createState() => _LeaveDetailHistoryState();
}

class _LeaveDetailHistoryState extends State<LeaveDetailHistory> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFFF6F9FF),
        appBar: CustomAppbar(title: "Leave Details"),
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(12),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                employeeDetails(),
                SizedBox(height: CustomTheme.screenHeight! * 0.03),
                requestDetail(),
                SizedBox(height: CustomTheme.screenHeight! * 0.03),
                requestRespon(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  requestDetail() {
    return Container(
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8)),
          color: Colors.white),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Request Details",
              style: CustomTheme.body1(context,
                  color: Colors.black, fontWeight: CustomTheme.semibold)),
          SizedBox(height: CustomTheme.screenHeight! * 0.03),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Leave Request",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text("Paid Leave",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Leave Type",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text("Annual Leave (Holiday\nEntitlement)",
                    textAlign: TextAlign.end,
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Reason",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text("I’m going to trip with my\nfamily.",
                    textAlign: TextAlign.end,
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Start Date",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text("08 Aug 2023",
                    textAlign: TextAlign.end,
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("End Date",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text("09 Aug 2023",
                    textAlign: TextAlign.end,
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Days Total",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Container(
                  padding: EdgeInsets.all(8),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      color: Color(0xFFF2F4F9)),
                  child: Text("2 days",
                      style: CustomTheme.caption(context,
                          color: Color(0xFF5981EA),
                          fontWeight: CustomTheme.medium)),
                ),
              ],
            ),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Attachment",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                GestureDetector(
                  onTap: () {
                    // Navigator.push(
                    //     context,
                    //     MaterialPageRoute(
                    //         builder: (context) => PreviewImagePage()));
                  },
                  child: Text("IMG_12345.PNG",
                      textAlign: TextAlign.end,
                      style: CustomTheme.body1(context,
                          color: Color(0xFF2F80ED),
                          fontWeight: CustomTheme.semibold)),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  requestRespon() {
    return Container(
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8)),
          color: Colors.white),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Request Response",
              style: CustomTheme.body1(context,
                  color: Colors.black, fontWeight: CustomTheme.semibold)),
          SizedBox(height: CustomTheme.screenHeight! * 0.03),
          Container(
            height: CustomTheme.screenHeight! * 0.05,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Assessor 1",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text("James Webb",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(8)),
                color: Color(0xFFE9F7EF)),
            child: Text("Approved",
                style: CustomTheme.caption(context,
                    color: Color(0xFF27AE60), fontWeight: CustomTheme.medium)),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.05,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Reason",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text("we are happy to grant you\n2 days of paid leave",
                    textAlign: TextAlign.end,
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          SizedBox(height: CustomTheme.screenHeight! * 0.03),
          Divider(
              color: Color(0xFFD8E4EE),
              height: 0,
              thickness: 1,
              endIndent: 20,
              indent: 20),
          SizedBox(height: CustomTheme.screenHeight! * 0.03),
          Container(
            height: CustomTheme.screenHeight! * 0.05,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Assessor 2",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text("David Jackson",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(8)),
                color: Color(0xFFFFEEEC)),
            child: Text("Declined",
                style: CustomTheme.caption(context,
                    color: Color(0xFFFF5245), fontWeight: CustomTheme.medium)),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Reason",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text("I regret to inform you that I\ncannot approve your leave",
                    textAlign: TextAlign.end,
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
        ],
      ),
    );
  }

  employeeDetails() {
    return Container(
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8)),
          color: Colors.white),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Employee Details",
              style: CustomTheme.body1(context,
                  color: Colors.black, fontWeight: CustomTheme.semibold)),
          SizedBox(height: CustomTheme.screenHeight! * 0.03),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Employee Name",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text("Raditya  Pratama",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Role",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text("Software Engineer",
                    textAlign: TextAlign.end,
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Unpaid Leave Taken",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text("8 Days",
                    textAlign: TextAlign.end,
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Paid Leave Taken",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text("9 Days",
                    textAlign: TextAlign.end,
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Paid Leave Balance",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text("3 Days",
                    textAlign: TextAlign.end,
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
