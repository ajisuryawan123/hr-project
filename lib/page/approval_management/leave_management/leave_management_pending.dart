import 'package:flutter/material.dart';
import 'package:hr_project/list_dummy.dart';
import 'package:hr_project/styling_theme.dart';
import 'package:hr_project/widget/leave_pending_widget.dart';

class LeavePendingPage extends StatefulWidget {
  const LeavePendingPage({Key? key}) : super(key: key);

  @override
  State<LeavePendingPage> createState() => _LeavePendingPageState();
}

class _LeavePendingPageState extends State<LeavePendingPage> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: CustomTheme.screenHeight! * 0.07,
            child: TextFormField(
              style: CustomTheme.body1(context,
                  color: Colors.black, fontWeight: CustomTheme.medium),
              decoration: InputDecoration(
                floatingLabelBehavior: FloatingLabelBehavior.never,
                contentPadding:
                    EdgeInsets.symmetric(vertical: 30, horizontal: 10),
                labelText: "Search employee name",
                labelStyle: CustomTheme.body1(context,
                    color: Color(0xFFBDBDBD), fontWeight: CustomTheme.medium),
                filled: true,
                fillColor: Color(0xFFF2F4F9),
                prefixIcon: Icon(Icons.search, color: Color(0xFFE0E0E0)),
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: BorderSide(color: Color(0xFFF2F4F9))),
              ),
            ),
          ),
          SizedBox(height: CustomTheme.screenHeight! * 0.03),
          cardBody(),
        ],
      ),
    );
  }

  cardBody() {
    return ListView.separated(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: ListDummy.listPending.length,
      separatorBuilder: (BuildContext context, int index) =>
          SizedBox(height: CustomTheme.screenHeight! * 0.02),
      itemBuilder: (BuildContext context, int index) {
        return LeavePendingWidget(item: ListDummy.listPending[index]);
      },
    );
  }
}
