import 'package:flutter/material.dart';
import 'package:hr_project/styling_theme.dart';
import 'package:hr_project/widget/custom_appbar.dart';

class LeaveDetailPending extends StatefulWidget {
  const LeaveDetailPending({Key? key}) : super(key: key);

  @override
  State<LeaveDetailPending> createState() => _LeaveDetailPendingState();
}

class _LeaveDetailPendingState extends State<LeaveDetailPending> {
  String gender = "wfo";
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFFF6F9FF),
        appBar: CustomAppbar(title: "Leave Details"),
        bottomSheet: customSheet(),
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(12),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                employeeDetails(),
                SizedBox(height: CustomTheme.screenHeight! * 0.03),
                requestDetail(),
                SizedBox(height: CustomTheme.screenHeight! * 0.03),
                requestRespon(),
                SizedBox(height: CustomTheme.screenHeight! * 0.03),
                choice(),
                SizedBox(height: CustomTheme.screenHeight! * 0.03),
                reason(),
                SizedBox(height: CustomTheme.screenHeight! * 0.15),
              ],
            ),
          ),
        ),
      ),
    );
  }

  requestDetail() {
    return Container(
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8)),
          color: Colors.white),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Request Details",
              style: CustomTheme.body1(context,
                  color: Colors.black, fontWeight: CustomTheme.semibold)),
          SizedBox(height: CustomTheme.screenHeight! * 0.03),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Leave Request",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text("Paid Leave",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Leave Type",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text("Annual Leave (Holiday\nEntitlement)",
                    textAlign: TextAlign.end,
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Reason",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text("I’m going to trip with my\nfamily.",
                    textAlign: TextAlign.end,
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Start Date",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text("08 Aug 2023",
                    textAlign: TextAlign.end,
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("End Date",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text("09 Aug 2023",
                    textAlign: TextAlign.end,
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Days Total",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Container(
                  padding: EdgeInsets.all(8),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      color: Color(0xFFF2F4F9)),
                  child: Text("2 days",
                      style: CustomTheme.caption(context,
                          color: Color(0xFF5981EA),
                          fontWeight: CustomTheme.medium)),
                ),
              ],
            ),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Attachment",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                GestureDetector(
                  onTap: () {
                    // Navigator.push(
                    //     context,
                    //     MaterialPageRoute(
                    //         builder: (context) => PreviewImagePage()));
                  },
                  child: Text("IMG_12345.PNG",
                      textAlign: TextAlign.end,
                      style: CustomTheme.body1(context,
                          color: Color(0xFF2F80ED),
                          fontWeight: CustomTheme.semibold)),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  requestRespon() {
    return Container(
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8)),
          color: Colors.white),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Request Response",
              style: CustomTheme.body1(context,
                  color: Colors.black, fontWeight: CustomTheme.semibold)),
          SizedBox(height: CustomTheme.screenHeight! * 0.03),
          Container(
            height: CustomTheme.screenHeight! * 0.05,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Assessor 1",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text("James Webb",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(8)),
                color: Color(0xFFE9F7EF)),
            child: Text("Approved",
                style: CustomTheme.caption(context,
                    color: Color(0xFF27AE60), fontWeight: CustomTheme.medium)),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.05,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Reason",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text("we are happy to grant you\n2 days of paid leave",
                    textAlign: TextAlign.end,
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          SizedBox(height: CustomTheme.screenHeight! * 0.03),
          Divider(
              color: Color(0xFFD8E4EE),
              height: 0,
              thickness: 1,
              endIndent: 20,
              indent: 20),
          SizedBox(height: CustomTheme.screenHeight! * 0.03),
          Container(
            height: CustomTheme.screenHeight! * 0.05,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Assessor 2",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text("David Jackson",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(8)),
                color: Color(0xFFFFF4ED)),
            child: Text("Waiting Response",
                style: CustomTheme.caption(context,
                    color: Color(0xFFEB7D2E), fontWeight: CustomTheme.medium)),
          ),
          SizedBox(height: CustomTheme.screenHeight! * 0.01),
          Container(
            height: CustomTheme.screenHeight! * 0.04,
            child: Text("Reason",
                style: CustomTheme.body2(context,
                    color: Colors.black, fontWeight: CustomTheme.medium)),
          ),
        ],
      ),
    );
  }

  customSheet() {
    return Container(
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3),
          ),
        ],
      ),
      child: ElevatedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          style: ElevatedButton.styleFrom(
              padding: EdgeInsets.symmetric(vertical: 12),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              minimumSize: Size(double.infinity, 0),
              backgroundColor: Color(0xFFEB7D2E)),
          child: Text(
            "Submit",
            style: CustomTheme.button(context,
                color: Colors.white, fontWeight: CustomTheme.medium),
          )),
    );
  }

  employeeDetails() {
    return Container(
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8)),
          color: Colors.white),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Employee Details",
              style: CustomTheme.body1(context,
                  color: Colors.black, fontWeight: CustomTheme.semibold)),
          SizedBox(height: CustomTheme.screenHeight! * 0.03),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Employee Name",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text("Raditya  Pratama",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Role",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text("Software Engineer",
                    textAlign: TextAlign.end,
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Unpaid Leave Taken",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text("8 Days",
                    textAlign: TextAlign.end,
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Paid Leave Taken",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text("9 Days",
                    textAlign: TextAlign.end,
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
          Container(
            height: CustomTheme.screenHeight! * 0.06,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Paid Leave Balance",
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Text("3 Days",
                    textAlign: TextAlign.end,
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
              ],
            ),
          ),
        ],
      ),
    );
  }

  choice() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          child: ListTile(
            contentPadding: EdgeInsets.all(0),
            horizontalTitleGap: 0.0,
            title: Text(
              "Approve",
              style: CustomTheme.body1(context,
                  color: Colors.black, fontWeight: CustomTheme.medium),
            ),
            leading: Radio(
              activeColor: Color(0xFFEB7D2E),
              value: "wfh",
              groupValue: gender,
              onChanged: (value) {
                setState(() {
                  gender = value.toString();
                });
              },
            ),
          ),
        ),
        Expanded(
          child: ListTile(
            contentPadding: EdgeInsets.all(0),
            horizontalTitleGap: 0.0,
            title: Text(
              "Decline",
              style: CustomTheme.body1(context,
                  color: Colors.black, fontWeight: CustomTheme.medium),
            ),
            leading: Radio(
              value: "wfo",
              activeColor: Color(0xFFEB7D2E),
              groupValue: gender,
              onChanged: (value) {
                setState(() {
                  gender = value.toString();
                });
              },
            ),
          ),
        ),
      ],
    );
  }

  reason() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Reason *",
          style: CustomTheme.body1(context,
              color: Colors.black, fontWeight: CustomTheme.semibold),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.02),
        TextFormField(
          keyboardType: TextInputType.multiline,
          maxLines: null,
          style: CustomTheme.body1(context,
              color: Colors.black, fontWeight: CustomTheme.medium),
          decoration: InputDecoration(
            floatingLabelBehavior: FloatingLabelBehavior.never,
            filled: true,
            fillColor: Colors.white,
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(color: Color(0xFFB8CADB))),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(color: Color(0xFFB8CADB))),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(color: Color(0xFFB8CADB))),
          ),
        ),
      ],
    );
  }
}
