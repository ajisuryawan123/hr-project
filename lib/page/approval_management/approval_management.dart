import 'package:flutter/material.dart';
import 'package:hr_project/list_dummy.dart';
import 'package:hr_project/page/approval_management/leave_management/leave_management_history.dart';
import 'package:hr_project/page/approval_management/leave_management/leave_management_pending.dart';
import 'package:hr_project/styling_theme.dart';

class ApprovalManagementPage extends StatefulWidget {
  const ApprovalManagementPage({Key? key}) : super(key: key);

  @override
  State<ApprovalManagementPage> createState() => _ApprovalManagementPageState();
}

class _ApprovalManagementPageState extends State<ApprovalManagementPage> {
  String itemFormType = "A";
  String selectedFormType = "Leave Approval";
  int currentTabIndex = 0;

  Widget _tabBarViewContent(int currentTabIndex) {
    Widget content = Container();
    switch (currentTabIndex) {
      case 0:
        content = itemFormType == "A"
            ? LeavePendingPage()
            : itemFormType == "B"
                ? LeavePendingPage()
                : LeavePendingPage();
        break;
      case 1:
        content = itemFormType == "A"
            ? LeaveHistoryPage()
            : itemFormType == "B"
                ? LeaveHistoryPage()
                : LeaveHistoryPage();
        break;
      default:
        content = Container();
    }
    return content;
  }

  TabBar get _tabBar => TabBar(
        onTap: (value) {
          setState(() {
            currentTabIndex = value;
          });
        },
        indicatorColor: Color(0xFF5981EA),
        indicatorWeight: 5,
        tabs: [
          Tab(
              child: Text("Pending",
                  style: CustomTheme.body1(context,
                      color: Colors.black, fontWeight: CustomTheme.semibold))),
          Tab(
              child: Text("History",
                  style: CustomTheme.body1(context,
                      color: Colors.black, fontWeight: CustomTheme.semibold))),
        ],
      );

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Color(0xFFF6F9FF),
          appBar: custom_appbar(),
          body: SingleChildScrollView(
            child: Column(
              children: [
                dropViewPage(),
                PreferredSize(
                  preferredSize: _tabBar.preferredSize,
                  child: Material(
                    color: Colors.white,
                    child: Column(
                      children: [
                        _tabBar,
                      ],
                    ),
                  ),
                ),
                _tabBarViewContent(currentTabIndex),
              ],
            ),
          ),
        ),
      ),
    );
  }

  custom_appbar() {
    return AppBar(
      centerTitle: true,
      backgroundColor: Color(0xFFF2C94C),
      iconTheme: IconThemeData(color: Color(0xFF2F80ED)),
      title: Text(
        "Approval Management",
        style: CustomTheme.subtitle(context,
            color: Colors.black, fontWeight: CustomTheme.semibold),
      ),
    );
  }

  dropViewPage() {
    return Container(
      padding: EdgeInsets.fromLTRB(20, 20, 20, 10),
      color: Colors.white,
      child: InkWell(
        onTap: () => showFormType(context),
        child: Container(
          padding: EdgeInsets.fromLTRB(20, 10, 10, 10),
          width: CustomTheme.screenWidth! * 1,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(5)),
              border: Border.all(width: 1, color: Color(0xFFB8CADB)),
              color: Colors.white),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(selectedFormType,
                  style: CustomTheme.body1(context,
                      color: Colors.black, fontWeight: CustomTheme.medium)),
              Icon(Icons.keyboard_arrow_down_outlined, color: Color(0xFFB8CADB))
            ],
          ),
        ),
      ),
    );
  }

  void showFormType(context) {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(12)),
        ),
        context: context,
        builder: (context) {
          return Container(
            padding: EdgeInsets.fromLTRB(12, 12, 12, 0),
            height: CustomTheme.screenHeight! * 0.75,
            child: Column(
              children: [
                Text("Select Form Request",
                    style: CustomTheme.body1(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
                SizedBox(height: CustomTheme.screenHeight! * 0.03),
                Container(
                  height: CustomTheme.screenHeight! * 0.07,
                  child: TextFormField(
                    style: CustomTheme.body1(context,
                        color: Colors.black, fontWeight: CustomTheme.medium),
                    decoration: InputDecoration(
                      floatingLabelBehavior: FloatingLabelBehavior.never,
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 30, horizontal: 10),
                      labelText: "Search form",
                      labelStyle: CustomTheme.body1(context,
                          color: Color(0xFFBDBDBD),
                          fontWeight: CustomTheme.medium),
                      filled: true,
                      fillColor: Color(0xFFF2F4F9),
                      prefixIcon: Icon(Icons.search, color: Color(0xFFE0E0E0)),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                    ),
                  ),
                ),
                SizedBox(height: CustomTheme.screenHeight! * 0.03),
                ListView.builder(
                    shrinkWrap: true,
                    itemCount: ListDummy.leaveManagementFormType.length,
                    itemBuilder: (context, index) {
                      return RadioListTile(
                        title: Text(
                            ListDummy.leaveManagementFormType[index].text,
                            style: CustomTheme.body1(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.medium)),
                        activeColor: Color(0xFFEB7D2E),
                        value:
                            ListDummy.leaveManagementFormType[index].valueRadio,
                        groupValue: itemFormType,
                        onChanged: (value) {
                          setState(() {
                            itemFormType = value.toString();
                            selectedFormType =
                                ListDummy.leaveManagementFormType[index].text;
                          });
                          Navigator.of(context).pop();
                        },
                      );
                    }),
              ],
            ),
          );
        });
  }
}
