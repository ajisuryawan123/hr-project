import 'package:flutter/material.dart';
import 'package:hr_project/list_dummy.dart';
import 'package:hr_project/styling_theme.dart';
import 'package:hr_project/widget/custom_appbar.dart';

class LeaderboardPage extends StatefulWidget {
  const LeaderboardPage({Key? key}) : super(key: key);

  @override
  State<LeaderboardPage> createState() => _LeaderboardPageState();
}

class _LeaderboardPageState extends State<LeaderboardPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFFF6F9FF),
        appBar: CustomAppbar(title: "Leaderboard"),
        body: Stack(
          children: <Widget>[
            Positioned.fill(
              child: Align(
                alignment: Alignment.topCenter,
                child: ImageAsset.headerBackground(0.25),
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(12, 0, 12, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ImageAsset.image8(1),
                  Container(
                    height: CustomTheme.screenHeight! * 0.07,
                    child: TextFormField(
                      style: CustomTheme.body1(context,
                          color: Colors.black, fontWeight: CustomTheme.medium),
                      decoration: InputDecoration(
                        floatingLabelBehavior: FloatingLabelBehavior.never,
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 30, horizontal: 10),
                        labelText: "Search employee name",
                        labelStyle: CustomTheme.body1(context,
                            color: Color(0xFFBDBDBD),
                            fontWeight: CustomTheme.medium),
                        filled: true,
                        fillColor: Colors.white,
                        prefixIcon:
                            Icon(Icons.search, color: Color(0xFFE0E0E0)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8),
                            borderSide: BorderSide(color: Colors.white)),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8),
                            borderSide: BorderSide(color: Colors.white)),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8),
                            borderSide: BorderSide(color: Colors.white)),
                      ),
                    ),
                  ),
                  SizedBox(height: CustomTheme.screenHeight! * 0.01),
                  SizedBox(
                    height: CustomTheme.screenHeight! * 0.59,
                    child: ListView.separated(
                      shrinkWrap: true,
                      itemCount: ListDummy.listLeaderboard.length,
                      separatorBuilder: (BuildContext context, int index) =>
                          SizedBox(height: CustomTheme.screenHeight! * 0.01),
                      itemBuilder: (BuildContext context, int index) {
                        int rank = index + 1;
                        var data = ListDummy.listLeaderboard[index];
                        return index == 0
                            ? Card(
                                color: Color(0xFFF2C94C),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8)),
                                child: Padding(
                                  padding: EdgeInsets.all(12),
                                  child: Row(
                                    children: [
                                      IconAsset.medal1(),
                                      SizedBox(
                                          width:
                                              CustomTheme.screenWidth! * 0.01),
                                      Container(
                                          decoration: BoxDecoration(
                                              shape: BoxShape.circle),
                                          child: data.avatar),
                                      SizedBox(
                                          width:
                                              CustomTheme.screenWidth! * 0.03),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(data.name,
                                              style: CustomTheme.body1(context,
                                                  color: Colors.black,
                                                  fontWeight:
                                                      CustomTheme.semibold)),
                                          SizedBox(
                                              height:
                                                  CustomTheme.screenHeight! *
                                                      0.01),
                                          Text(data.role,
                                              style: CustomTheme.body2(context,
                                                  color: Colors.black,
                                                  fontWeight:
                                                      CustomTheme.medium)),
                                          SizedBox(
                                              height:
                                                  CustomTheme.screenHeight! *
                                                      0.01),
                                          Text(data.email,
                                              style: CustomTheme.body2(context,
                                                  color: Colors.black,
                                                  fontWeight:
                                                      CustomTheme.medium)),
                                        ],
                                      ),
                                      Expanded(child: Container()),
                                      Container(
                                        padding: EdgeInsets.all(8),
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(8)),
                                            color: Color(0xFFE6BF48)),
                                        child: Text(data.score,
                                            style: CustomTheme.body1(context,
                                                color: Colors.black,
                                                fontWeight:
                                                    CustomTheme.medium)),
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            : index == 1
                                ? Card(
                                    color: Color(0xFFD8E4EE),
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(8)),
                                    child: Padding(
                                      padding: EdgeInsets.all(12),
                                      child: Row(
                                        children: [
                                          IconAsset.medal2(),
                                          SizedBox(
                                              width: CustomTheme.screenWidth! *
                                                  0.01),
                                          Container(
                                              decoration: BoxDecoration(
                                                  shape: BoxShape.circle),
                                              child: data.avatar),
                                          SizedBox(
                                              width: CustomTheme.screenWidth! *
                                                  0.03),
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(data.name,
                                                  style: CustomTheme.body1(
                                                      context,
                                                      color: Colors.black,
                                                      fontWeight: CustomTheme
                                                          .semibold)),
                                              SizedBox(
                                                  height: CustomTheme
                                                          .screenHeight! *
                                                      0.01),
                                              Text(data.role,
                                                  style: CustomTheme.body2(
                                                      context,
                                                      color: Colors.black,
                                                      fontWeight:
                                                          CustomTheme.medium)),
                                              SizedBox(
                                                  height: CustomTheme
                                                          .screenHeight! *
                                                      0.01),
                                              Text(data.email,
                                                  style: CustomTheme.body2(
                                                      context,
                                                      color: Colors.black,
                                                      fontWeight:
                                                          CustomTheme.medium)),
                                            ],
                                          ),
                                          Expanded(child: Container()),
                                          Container(
                                            padding: EdgeInsets.all(8),
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(8)),
                                                color: Color(0xFFCDD9E2)),
                                            child: Text(data.score,
                                                style: CustomTheme.body1(
                                                    context,
                                                    color: Colors.black,
                                                    fontWeight:
                                                        CustomTheme.medium)),
                                          ),
                                        ],
                                      ),
                                    ),
                                  )
                                : index == 2
                                    ? Card(
                                        color: Color(0xFFFFE5D2),
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(8)),
                                        child: Padding(
                                          padding: EdgeInsets.all(12),
                                          child: Row(
                                            children: [
                                              IconAsset.medal3(),
                                              SizedBox(
                                                  width:
                                                      CustomTheme.screenWidth! *
                                                          0.01),
                                              Container(
                                                  decoration: BoxDecoration(
                                                      shape: BoxShape.circle),
                                                  child: data.avatar),
                                              SizedBox(
                                                  width:
                                                      CustomTheme.screenWidth! *
                                                          0.03),
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(data.name,
                                                      style: CustomTheme.body1(
                                                          context,
                                                          color: Colors.black,
                                                          fontWeight:
                                                              CustomTheme
                                                                  .semibold)),
                                                  SizedBox(
                                                      height: CustomTheme
                                                              .screenHeight! *
                                                          0.01),
                                                  Text(data.role,
                                                      style: CustomTheme.body2(
                                                          context,
                                                          color: Colors.black,
                                                          fontWeight:
                                                              CustomTheme
                                                                  .medium)),
                                                  SizedBox(
                                                      height: CustomTheme
                                                              .screenHeight! *
                                                          0.01),
                                                  Text(data.email,
                                                      style: CustomTheme.body2(
                                                          context,
                                                          color: Colors.black,
                                                          fontWeight:
                                                              CustomTheme
                                                                  .medium)),
                                                ],
                                              ),
                                              Expanded(child: Container()),
                                              Container(
                                                padding: EdgeInsets.all(8),
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(8)),
                                                    color: Color(0xFFF2DAC7)),
                                                child: Text(data.score,
                                                    style: CustomTheme.body1(
                                                        context,
                                                        color: Colors.black,
                                                        fontWeight: CustomTheme
                                                            .medium)),
                                              ),
                                            ],
                                          ),
                                        ),
                                      )
                                    : Card(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(8)),
                                        child: Padding(
                                          padding: EdgeInsets.all(12),
                                          child: Row(
                                            children: [
                                              data.isMe
                                                  ? CircleAvatar(
                                                      radius: 11,
                                                      backgroundColor:
                                                          Color(0xFFF44336),
                                                      child: Text(
                                                          rank.toString(),
                                                          style: CustomTheme.body2(
                                                              context,
                                                              color:
                                                                  Colors.white,
                                                              fontWeight:
                                                                  CustomTheme
                                                                      .medium)),
                                                    )
                                                  : CircleAvatar(
                                                      radius: 11,
                                                      backgroundColor:
                                                          Color(0xFFD8E4EE),
                                                      child: Text(
                                                          rank.toString(),
                                                          style: CustomTheme.body2(
                                                              context,
                                                              color: Color(
                                                                  0xFF93B1FF),
                                                              fontWeight:
                                                                  CustomTheme
                                                                      .medium)),
                                                    ),
                                              SizedBox(
                                                  width:
                                                      CustomTheme.screenWidth! *
                                                          0.04),
                                              Container(
                                                  decoration: BoxDecoration(
                                                      shape: BoxShape.circle),
                                                  child: data.avatar),
                                              SizedBox(
                                                  width:
                                                      CustomTheme.screenWidth! *
                                                          0.03),
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(data.name,
                                                      style: CustomTheme.body1(
                                                          context,
                                                          color: Colors.black,
                                                          fontWeight:
                                                              CustomTheme
                                                                  .semibold)),
                                                  SizedBox(
                                                      height: CustomTheme
                                                              .screenHeight! *
                                                          0.01),
                                                  Text(data.role,
                                                      style: CustomTheme.body2(
                                                          context,
                                                          color: Colors.black,
                                                          fontWeight:
                                                              CustomTheme
                                                                  .medium)),
                                                  SizedBox(
                                                      height: CustomTheme
                                                              .screenHeight! *
                                                          0.01),
                                                  Text(data.email,
                                                      style: CustomTheme.body2(
                                                          context,
                                                          color: Colors.black,
                                                          fontWeight:
                                                              CustomTheme
                                                                  .medium)),
                                                ],
                                              ),
                                              Expanded(child: Container()),
                                              Container(
                                                padding: EdgeInsets.all(8),
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(8)),
                                                    color: Color(0xFFF2F2F2)),
                                                child: Text(data.score,
                                                    style: CustomTheme.body1(
                                                        context,
                                                        color: Colors.black,
                                                        fontWeight: CustomTheme
                                                            .medium)),
                                              ),
                                            ],
                                          ),
                                        ),
                                      );
                      },
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
