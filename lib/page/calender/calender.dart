import 'package:flutter/material.dart';
import 'package:hr_project/styling_theme.dart';
import 'package:intl/intl.dart';
import 'package:table_calendar/table_calendar.dart';

class CalendarPage extends StatefulWidget {
  const CalendarPage({Key? key}) : super(key: key);

  @override
  State<CalendarPage> createState() => _CalendarPageState();
}

class _CalendarPageState extends State<CalendarPage> {
  DateTime _selectedDay = DateTime.now();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: customAppbar(),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              calendarWidget(),
              event(),
            ],
          ),
        ),
      ),
    );
  }

  customAppbar() {
    return AppBar(
      centerTitle: true,
      iconTheme: IconThemeData(color: Color(0xFF2F80ED)),
      backgroundColor: Color(0xFFF2C94C),
      title: Text(
        "Calendar",
        style: CustomTheme.subtitle(context,
            color: Colors.black, fontWeight: CustomTheme.semibold),
      ),
      actions: [
        Container(
          margin: EdgeInsets.only(right: 20),
          child: GestureDetector(
            onTap: () {
              showOption();
            },
            child: IconAsset.indonesia(),
          ),
        )
      ],
    );
  }

  calendarWidget() {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(12),
      child: TableCalendar(
        selectedDayPredicate: (day) {
          return isSameDay(_selectedDay, day);
        },
        onDaySelected: (selectedDay, focusedDay) {
          setState(() {
            _selectedDay = selectedDay;
          });
        },
        rowHeight: CustomTheme.screenHeight! * 0.06,
        daysOfWeekStyle: DaysOfWeekStyle(
          dowTextFormatter: (date, locale) =>
              DateFormat.E(locale).format(date)[0],
          weekdayStyle: CustomTheme.body1(context,
              color: Color(0xFFEB7D2E), fontWeight: CustomTheme.medium),
          weekendStyle: CustomTheme.body1(context,
              color: Color(0xFFEB7D2E), fontWeight: CustomTheme.medium),
        ),
        calendarStyle: CalendarStyle(
          isTodayHighlighted: false,
          selectedDecoration: BoxDecoration(
            color: Color(0xFFFBE5D5),
            shape: BoxShape.circle,
          ),
          selectedTextStyle: CustomTheme.body1(context,
              color: Colors.black, fontWeight: CustomTheme.semibold),
          todayTextStyle: CustomTheme.body1(context,
              color: Colors.black, fontWeight: CustomTheme.semibold),
          defaultTextStyle: CustomTheme.body1(context,
              color: Colors.black, fontWeight: CustomTheme.semibold),
          outsideTextStyle: CustomTheme.body1(context,
              color: Color(0xFFBDBDBD), fontWeight: CustomTheme.semibold),
          weekendTextStyle: CustomTheme.body1(context,
              color: Color(0xFFF44336), fontWeight: CustomTheme.semibold),
        ),
        headerStyle: HeaderStyle(
            headerPadding: EdgeInsets.only(bottom: 12),
            leftChevronVisible: true,
            rightChevronVisible: true,
            formatButtonVisible: false,
            titleCentered: true,
            leftChevronIcon:
                Icon(Icons.arrow_back_ios_rounded, color: Color(0xFFEB7D2E)),
            rightChevronIcon:
                Icon(Icons.arrow_forward_ios_rounded, color: Color(0xFFEB7D2E)),
            titleTextStyle: CustomTheme.body1(context,
                color: Colors.black, fontWeight: CustomTheme.semibold)),
        firstDay: DateTime.utc(2010, 10, 16),
        lastDay: DateTime.utc(2030, 3, 14),
        focusedDay: DateTime.now(),
      ),
    );
  }

  noList() {
    return Center(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(8),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color(0xFFD8E4EE),
                  Color(0xFFD8E4EE).withOpacity(0.0),
                ],
              ),
            ),
            child: IconAsset.calendar_event(),
          ),
          SizedBox(height: CustomTheme.screenHeight! * 0.03),
          Text(
            "There are currently no special events",
            style: CustomTheme.body1(context,
                color: Color(0xFF525F71), fontWeight: CustomTheme.semibold),
          ),
          SizedBox(height: CustomTheme.screenHeight! * 0.01),
          Text(
            "Use this time to focus on your tasks\nand achieve your goals.",
            textAlign: TextAlign.center,
            style: CustomTheme.body1(context,
                color: Color(0xFFB8CADB), fontWeight: CustomTheme.medium),
          ),
        ],
      ),
    );
  }

  event() {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.fromLTRB(12, 12, 12, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Event",
                style: CustomTheme.body1(context,
                    color: Colors.black, fontWeight: CustomTheme.semibold)),
            SizedBox(height: CustomTheme.screenHeight! * 0.03),
            noList(),
          ],
        ),
      ),
    );
  }

  showOption() {
    return showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(15))),
        builder: (BuildContext context) {
          return Padding(
            padding: EdgeInsets.only(top: 15),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text('Country Options',
                    style: CustomTheme.subtitle(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                SizedBox(height: CustomTheme.screenHeight! * 0.02),
                ListTile(
                    leading: IconAsset.indonesia(),
                    title: Text('Indonesia',
                        style: CustomTheme.subtitle(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium))),
                Divider(
                    color: Color(0xFFD8E4EE),
                    height: 0,
                    thickness: 1,
                    endIndent: 20,
                    indent: 20),
                ListTile(
                    leading: IconAsset.malaysia(),
                    title: Text('Malaysia',
                        style: CustomTheme.subtitle(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium))),
                Divider(
                    color: Color(0xFFD8E4EE),
                    height: 0,
                    thickness: 1,
                    endIndent: 20,
                    indent: 20),
                ListTile(
                    leading: IconAsset.singapore(),
                    title: Text('Singapore',
                        style: CustomTheme.subtitle(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium))),
              ],
            ),
          );
        });
  }
}
