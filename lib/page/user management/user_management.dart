import 'package:flutter/material.dart';
import 'package:hr_project/dummy_data.dart';
import 'package:hr_project/page/user%20management/add_new/add_newEmployee.dart';
import 'package:hr_project/page/user%20management/add_new/add_newJob.dart';
import 'package:hr_project/page/user%20management/user%20manage/employee.dart';
import 'package:hr_project/page/user%20management/user%20manage/job_title.dart';
import 'package:hr_project/styling_theme.dart';

class UserManagePage extends StatefulWidget {
  const UserManagePage({Key? key}) : super(key: key);

  @override
  State<UserManagePage> createState() => _UserManagePageState();
}

class _UserManagePageState extends State<UserManagePage>
    with TickerProviderStateMixin {
  TabController? controllerTab;
  List<MyTabs> directList = [
    MyTabs(AddNewEmployeePage()),
    MyTabs(AddNewJobPage()),
  ];
  Widget? currentDirect;

  TabBar get _tabBar => TabBar(
        controller: controllerTab,
        indicatorColor: Color(0xFF5981EA),
        indicatorWeight: 5,
        tabs: [
          Tab(
              child: Text("Employee",
                  style: CustomTheme.body1(context,
                      color: Colors.black, fontWeight: CustomTheme.semibold))),
          Tab(
              child: Text("Job Title",
                  style: CustomTheme.body1(context,
                      color: Colors.black, fontWeight: CustomTheme.semibold))),
        ],
      );

  @override
  void initState() {
    currentDirect = directList[0].directAdd;
    controllerTab = TabController(length: 2, vsync: this);
    controllerTab!.addListener(changeWidget); // Registering listener
    super.initState();
  }

  void changeWidget() {
    setState(() {
      currentDirect = directList[controllerTab!.index].directAdd;
    });
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Color(0xFFF6F9FF),
          appBar: custom_appbar(),
          body: TabBarView(
            controller: controllerTab,
            children: [
              EmployeePage(),
              JobTitlePage(),
            ],
          ),
        ),
      ),
    );
  }

  custom_appbar() {
    return AppBar(
      centerTitle: true,
      backgroundColor: Color(0xFFF2C94C),
      iconTheme: IconThemeData(color: Color(0xFF2F80ED)),
      title: Text(
        "User Management",
        style: CustomTheme.subtitle(context,
            color: Colors.black, fontWeight: CustomTheme.semibold),
      ),
      actions: [
        IconAsset.calender_blue(),
        SizedBox(width: CustomTheme.screenWidth! * 0.07),
        GestureDetector(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => currentDirect!));
            },
            child: IconAsset.add_square()),
      ],
      bottom: PreferredSize(
        preferredSize: _tabBar.preferredSize,
        child: Material(
          color: Colors.white,
          child: _tabBar,
        ),
      ),
    );
  }
}
