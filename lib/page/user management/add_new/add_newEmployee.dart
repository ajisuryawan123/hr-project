import 'package:flutter/material.dart';
import 'package:hr_project/styling_theme.dart';
import 'package:hr_project/widget/custom_appbar.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

import '../../../dummy_data.dart';

class AddNewEmployeePage extends StatefulWidget {
  const AddNewEmployeePage({Key? key}) : super(key: key);

  @override
  State<AddNewEmployeePage> createState() => _AddNewEmployeePageState();
}

class _AddNewEmployeePageState extends State<AddNewEmployeePage> {
  TextEditingController controller1 = TextEditingController();
  TextEditingController controller2 = TextEditingController();
  TextEditingController controller3 = TextEditingController();
  DateRangePickerController _datePickerStart = DateRangePickerController();
  String dateStart = "Choose Date";
  final _formKey = GlobalKey<FormState>();
  final successColor = Color(0xFFEB7D2E);
  final errorColor = Color(0xFFE0E0E0);
  bool isValid1 = false;
  bool isValid2 = false;
  bool isValid3 = false;

  @override
  void dispose() {
    controller1.dispose();
    controller2.dispose();
    controller3.dispose();
    super.dispose();
  }

  void _validateFields() {
    setState(() {
      isValid1 = _validate1(controller1.text);
      isValid2 = _validate2(controller2.text);
      isValid3 = _validate3(controller3.text);
    });
  }

  bool _validate1(String controller1) => controller1.isNotEmpty;
  bool _validate2(String controller2) => controller2.isNotEmpty;
  bool _validate3(String controller3) => controller3.isNotEmpty;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFFF6F9FF),
        appBar: CustomAppbar(title: "Add New Employee"),
        bottomSheet: customSheet(),
        body: Form(
          key: _formKey,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.all(12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  form1(),
                  SizedBox(height: CustomTheme.screenHeight! * 0.03),
                  form2(),
                  SizedBox(height: CustomTheme.screenHeight! * 0.13),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  customSheet() {
    return Container(
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3),
          ),
        ],
      ),
      child: ElevatedButton(
          onPressed: (isValid1 && isValid2 && isValid3)
              ? () {
                  Navigator.pop(context);
                }
              : null,
          style: ElevatedButton.styleFrom(
              padding: EdgeInsets.symmetric(vertical: 12),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              minimumSize: Size(double.infinity, 0),
              backgroundColor: (isValid1 && isValid2 && isValid3)
                  ? successColor
                  : errorColor),
          child: Text(
            "Save",
            style: CustomTheme.button(context,
                color: (isValid1 && isValid2 && isValid3)
                    ? Colors.white
                    : Color(0xFF828282),
                fontWeight: CustomTheme.medium),
          )),
    );
  }

  List<DummyDropdown> _itemsGender = [
    DummyDropdown("Male", "M"),
    DummyDropdown("Female", "f"),
  ];
  String itemDrop = "M";

  String _selectedGender = 'Male';

  void showModalGender(context) {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(12)),
        ),
        context: context,
        builder: (context) {
          return Container(
            padding: EdgeInsets.fromLTRB(12, 12, 12, 0),
            height: CustomTheme.screenHeight! * 0.75,
            child: Column(
              children: [
                Text("Select Gender",
                    style: CustomTheme.body1(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
                SizedBox(height: CustomTheme.screenHeight! * 0.03),
                // Container(
                //   height: CustomTheme.screenHeight! * 0.07,
                //   child: TextFormField(
                //     style: CustomTheme.body1(context,
                //         color: Colors.black, fontWeight: CustomTheme.medium),
                //     decoration: InputDecoration(
                //       floatingLabelBehavior: FloatingLabelBehavior.never,
                //       contentPadding:
                //       EdgeInsets.symmetric(vertical: 30, horizontal: 10),
                //       labelText: "Search client",
                //       labelStyle: CustomTheme.body1(context,
                //           color: Color(0xFFBDBDBD),
                //           fontWeight: CustomTheme.medium),
                //       filled: true,
                //       fillColor: Color(0xFFF2F4F9),
                //       prefixIcon: Icon(Icons.search, color: Color(0xFFE0E0E0)),
                //       focusedBorder: OutlineInputBorder(
                //           borderRadius: BorderRadius.circular(8),
                //           borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                //       enabledBorder: OutlineInputBorder(
                //           borderRadius: BorderRadius.circular(8),
                //           borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                //       border: OutlineInputBorder(
                //           borderRadius: BorderRadius.circular(8),
                //           borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                //     ),
                //   ),
                // ),
                // SizedBox(height: CustomTheme.screenHeight! * 0.03),
                ListView.builder(
                    shrinkWrap: true,
                    itemCount: _itemsGender.length,
                    itemBuilder: (context, index) {
                      return RadioListTile(
                        title: Text(_itemsGender[index].text,
                            style: CustomTheme.body1(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.medium)),
                        activeColor: Color(0xFFEB7D2E),
                        value: _itemsGender[index].valueRadio,
                        groupValue: itemDrop,
                        onChanged: (value) {
                          setState(() {
                            itemDrop = value.toString();
                            _selectedGender = _itemsGender[index].text;
                          });
                          Navigator.of(context).pop();
                        },
                      );
                    }),
              ],
            ),
          );
        });
  }

  List<DummyCountry> _itemsCountry = [
    DummyCountry("Indonesia", "I", IconAsset.indonesia()),
    DummyCountry("Malaysia", "M", IconAsset.malaysia()),
    DummyCountry("Singapura", "S", IconAsset.singapore()),
  ];
  String itemDropCountry = "I";

  String _selectedCountry = 'Indonesia';

  void showModalCountry(context) {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(12)),
        ),
        context: context,
        builder: (context) {
          return Container(
            padding: EdgeInsets.fromLTRB(12, 12, 12, 0),
            height: CustomTheme.screenHeight! * 0.75,
            child: Column(
              children: [
                Text("Select Placement",
                    style: CustomTheme.body1(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
                SizedBox(height: CustomTheme.screenHeight! * 0.03),
                // Container(
                //   height: CustomTheme.screenHeight! * 0.07,
                //   child: TextFormField(
                //     style: CustomTheme.body1(context,
                //         color: Colors.black, fontWeight: CustomTheme.medium),
                //     decoration: InputDecoration(
                //       floatingLabelBehavior: FloatingLabelBehavior.never,
                //       contentPadding:
                //       EdgeInsets.symmetric(vertical: 30, horizontal: 10),
                //       labelText: "Search client",
                //       labelStyle: CustomTheme.body1(context,
                //           color: Color(0xFFBDBDBD),
                //           fontWeight: CustomTheme.medium),
                //       filled: true,
                //       fillColor: Color(0xFFF2F4F9),
                //       prefixIcon: Icon(Icons.search, color: Color(0xFFE0E0E0)),
                //       focusedBorder: OutlineInputBorder(
                //           borderRadius: BorderRadius.circular(8),
                //           borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                //       enabledBorder: OutlineInputBorder(
                //           borderRadius: BorderRadius.circular(8),
                //           borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                //       border: OutlineInputBorder(
                //           borderRadius: BorderRadius.circular(8),
                //           borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                //     ),
                //   ),
                // ),
                // SizedBox(height: CustomTheme.screenHeight! * 0.03),
                ListView.builder(
                    shrinkWrap: true,
                    itemCount: _itemsCountry.length,
                    itemBuilder: (context, index) {
                      return RadioListTile(
                        title: Row(
                          children: [
                            _itemsCountry[index].icon,
                            SizedBox(width: CustomTheme.screenWidth! * 0.05),
                            Text(_itemsCountry[index].text,
                                style: CustomTheme.body1(context,
                                    color: Colors.black,
                                    fontWeight: CustomTheme.medium)),
                          ],
                        ),
                        activeColor: Color(0xFFEB7D2E),
                        value: _itemsCountry[index].valueRadio,
                        groupValue: itemDropCountry,
                        onChanged: (value) {
                          setState(() {
                            itemDropCountry = value.toString();
                            _selectedCountry = _itemsCountry[index].text;
                          });
                          Navigator.of(context).pop();
                        },
                      );
                    }),
              ],
            ),
          );
        });
  }

  List<DummyDropdown> _itemsJob = [
    DummyDropdown("FrontEnd Engineer", "FE"),
    DummyDropdown("BackEnd Engineer", "BE"),
    DummyDropdown("UI/UX Designer", "UI"),
  ];
  String itemDropJob = "FE";

  String _selectedJob = 'FrontEnd Engineer';

  void showModalJob(context) {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(12)),
        ),
        context: context,
        builder: (context) {
          return Container(
            padding: EdgeInsets.fromLTRB(12, 12, 12, 0),
            height: CustomTheme.screenHeight! * 0.75,
            child: Column(
              children: [
                Text("Select Job Title",
                    style: CustomTheme.body1(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
                SizedBox(height: CustomTheme.screenHeight! * 0.03),
                // Container(
                //   height: CustomTheme.screenHeight! * 0.07,
                //   child: TextFormField(
                //     style: CustomTheme.body1(context,
                //         color: Colors.black, fontWeight: CustomTheme.medium),
                //     decoration: InputDecoration(
                //       floatingLabelBehavior: FloatingLabelBehavior.never,
                //       contentPadding:
                //       EdgeInsets.symmetric(vertical: 30, horizontal: 10),
                //       labelText: "Search client",
                //       labelStyle: CustomTheme.body1(context,
                //           color: Color(0xFFBDBDBD),
                //           fontWeight: CustomTheme.medium),
                //       filled: true,
                //       fillColor: Color(0xFFF2F4F9),
                //       prefixIcon: Icon(Icons.search, color: Color(0xFFE0E0E0)),
                //       focusedBorder: OutlineInputBorder(
                //           borderRadius: BorderRadius.circular(8),
                //           borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                //       enabledBorder: OutlineInputBorder(
                //           borderRadius: BorderRadius.circular(8),
                //           borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                //       border: OutlineInputBorder(
                //           borderRadius: BorderRadius.circular(8),
                //           borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                //     ),
                //   ),
                // ),
                // SizedBox(height: CustomTheme.screenHeight! * 0.03),
                ListView.builder(
                    shrinkWrap: true,
                    itemCount: _itemsJob.length,
                    itemBuilder: (context, index) {
                      return RadioListTile(
                        title: Text(_itemsJob[index].text,
                            style: CustomTheme.body1(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.medium)),
                        activeColor: Color(0xFFEB7D2E),
                        value: _itemsJob[index].valueRadio,
                        groupValue: itemDropJob,
                        onChanged: (value) {
                          setState(() {
                            itemDropJob = value.toString();
                            _selectedJob = _itemsJob[index].text;
                          });
                          Navigator.of(context).pop();
                        },
                      );
                    }),
              ],
            ),
          );
        });
  }

  form1() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("Full Name *",
            style: CustomTheme.body1(context,
                color: Colors.black, fontWeight: CustomTheme.semibold)),
        SizedBox(height: CustomTheme.screenHeight! * 0.01),
        Container(
          height: CustomTheme.screenHeight! * 0.06,
          child: TextFormField(
            controller: controller1,
            onChanged: (value) => _validateFields(),
            style: CustomTheme.body2(context,
                color: Colors.black, fontWeight: CustomTheme.medium),
            decoration: InputDecoration(
              floatingLabelBehavior: FloatingLabelBehavior.never,
              contentPadding:
                  EdgeInsets.symmetric(vertical: 30, horizontal: 10),
              filled: true,
              fillColor: Colors.white,
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Color(0xFFB8CADB))),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Color(0xFFB8CADB))),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Color(0xFFB8CADB))),
            ),
          ),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.03),
        Text("Gender *",
            style: CustomTheme.body1(context,
                color: Colors.black, fontWeight: CustomTheme.semibold)),
        SizedBox(height: CustomTheme.screenHeight! * 0.01),
        GestureDetector(
          onTap: () {
            showModalGender(context);
          },
          child: Container(
            padding: EdgeInsets.all(6),
            width: CustomTheme.screenWidth! * 1,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                border: Border.all(width: 1, color: Color(0xFFB8CADB)),
                color: Colors.white),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(_selectedGender,
                    style: CustomTheme.body1(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Icon(Icons.keyboard_arrow_down_outlined,
                    color: Color(0xFFB8CADB))
              ],
            ),
          ),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.03),
        Text("Placement *",
            style: CustomTheme.body1(context,
                color: Colors.black, fontWeight: CustomTheme.semibold)),
        SizedBox(height: CustomTheme.screenHeight! * 0.01),
        GestureDetector(
          onTap: () {
            showModalCountry(context);
          },
          child: Container(
            padding: EdgeInsets.all(6),
            width: CustomTheme.screenWidth! * 1,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                border: Border.all(width: 1, color: Color(0xFFB8CADB)),
                color: Colors.white),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(_selectedCountry,
                    style: CustomTheme.body1(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Icon(Icons.keyboard_arrow_down_outlined,
                    color: Color(0xFFB8CADB))
              ],
            ),
          ),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.03),
        Text("Job Title *",
            style: CustomTheme.body1(context,
                color: Colors.black, fontWeight: CustomTheme.semibold)),
        SizedBox(height: CustomTheme.screenHeight! * 0.01),
        GestureDetector(
          onTap: () {
            showModalJob(context);
          },
          child: Container(
            padding: EdgeInsets.all(6),
            width: CustomTheme.screenWidth! * 1,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                border: Border.all(width: 1, color: Color(0xFFB8CADB)),
                color: Colors.white),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(_selectedJob,
                    style: CustomTheme.body1(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                Icon(Icons.keyboard_arrow_down_outlined,
                    color: Color(0xFFB8CADB))
              ],
            ),
          ),
        ),
      ],
    );
  }

  void showDateStart(context) {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(12)),
        ),
        context: context,
        builder: (context) {
          return Container(
            padding: EdgeInsets.all(12),
            height: CustomTheme.screenHeight! * 0.5,
            child: SfDateRangePicker(
              monthViewSettings: DateRangePickerMonthViewSettings(
                  viewHeaderStyle: DateRangePickerViewHeaderStyle(
                      textStyle: CustomTheme.body2(context,
                          color: Color(0xFFEB7D2E),
                          fontWeight: CustomTheme.medium))),
              selectionColor: Color(0xFFEB7D2E),
              startRangeSelectionColor: Color(0xFFEB7D2E),
              endRangeSelectionColor: Color(0xFFEB7D2E),
              rangeSelectionColor: Color(0xFFFFA15E),
              rangeTextStyle: CustomTheme.body2(context,
                  color: Colors.white, fontWeight: CustomTheme.medium),
              selectionTextStyle: CustomTheme.body2(context,
                  color: Colors.white, fontWeight: CustomTheme.medium),
              headerStyle: DateRangePickerHeaderStyle(
                  textAlign: TextAlign.center,
                  textStyle: CustomTheme.body1(context,
                      color: Colors.black, fontWeight: CustomTheme.semibold)),
              selectionMode: DateRangePickerSelectionMode.single,
              controller: _datePickerStart,
              showActionButtons: true,
              onSubmit: (value) {
                if (value is PickerDateRange) {
                  final DateTime rangeStartDate = value.startDate!;
                  final DateTime rangeEndDate = value.endDate!;
                } else if (value is DateTime) {
                  final DateTime selectedDate = value;
                  setState(() {
                    dateStart =
                        DateFormat('dd MMM yyyy', 'en_US').format(selectedDate);
                  });
                } else if (value is List<DateTime>) {
                  final List<DateTime> selectedDates = value;
                } else if (value is List<PickerDateRange>) {
                  final List<PickerDateRange> selectedRanges = value;
                }
                Navigator.pop(context);
              },
              onCancel: () {
                Navigator.pop(context);
              },
            ),
          );
        });
  }

  form2() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("Email Address *",
            style: CustomTheme.body1(context,
                color: Colors.black, fontWeight: CustomTheme.semibold)),
        SizedBox(height: CustomTheme.screenHeight! * 0.01),
        Container(
          height: CustomTheme.screenHeight! * 0.06,
          child: TextFormField(
            controller: controller2,
            onChanged: (value) => _validateFields(),
            style: CustomTheme.body2(context,
                color: Colors.black, fontWeight: CustomTheme.medium),
            decoration: InputDecoration(
              floatingLabelBehavior: FloatingLabelBehavior.never,
              contentPadding:
                  EdgeInsets.symmetric(vertical: 30, horizontal: 10),
              filled: true,
              fillColor: Colors.white,
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Color(0xFFB8CADB))),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Color(0xFFB8CADB))),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Color(0xFFB8CADB))),
            ),
          ),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.03),
        Text("Join Date *",
            style: CustomTheme.body1(context,
                color: Colors.black, fontWeight: CustomTheme.semibold)),
        SizedBox(height: CustomTheme.screenHeight! * 0.01),
        GestureDetector(
          onTap: () {
            showDateStart(context);
          },
          child: Container(
            padding: EdgeInsets.all(6),
            width: CustomTheme.screenWidth! * 1,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                border: Border.all(width: 1, color: Color(0xFFB8CADB)),
                color: Colors.white),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(dateStart,
                    style: CustomTheme.body1(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                IconAsset.calender_grey()
              ],
            ),
          ),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.03),
        Text("Paid Leave Balance *",
            style: CustomTheme.body1(context,
                color: Colors.black, fontWeight: CustomTheme.semibold)),
        SizedBox(height: CustomTheme.screenHeight! * 0.01),
        Container(
          height: CustomTheme.screenHeight! * 0.06,
          child: TextFormField(
            controller: controller3,
            onChanged: (value) => _validateFields(),
            style: CustomTheme.body2(context,
                color: Colors.black, fontWeight: CustomTheme.medium),
            decoration: InputDecoration(
              floatingLabelBehavior: FloatingLabelBehavior.never,
              contentPadding:
                  EdgeInsets.symmetric(vertical: 30, horizontal: 10),
              filled: true,
              fillColor: Colors.white,
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Color(0xFFB8CADB))),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Color(0xFFB8CADB))),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Color(0xFFB8CADB))),
            ),
          ),
        ),
      ],
    );
  }
}
