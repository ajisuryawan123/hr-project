import 'package:flutter/material.dart';
import 'package:hr_project/styling_theme.dart';
import 'package:hr_project/widget/custom_appbar.dart';

class AddNewJobPage extends StatefulWidget {
  const AddNewJobPage({Key? key}) : super(key: key);

  @override
  State<AddNewJobPage> createState() => _AddNewJobPageState();
}

class _AddNewJobPageState extends State<AddNewJobPage> {
  TextEditingController controller = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final successColor = Color(0xFFEB7D2E);
  final errorColor = Color(0xFFE0E0E0);
  bool isValid = false;

  @override
  void initState() {
    super.initState();
    controller.addListener(() {
      if (controller.text.isEmpty) {
        setState(() {
          isValid = false;
        });
      } else {
        setState(() {
          isValid = true;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFFF6F9FF),
        appBar: CustomAppbar(title: "Add New Job Title"),
        bottomSheet: customSheet(),
        body: Form(
          key: _formKey,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          child: Padding(
            padding: EdgeInsets.all(12),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                textField(),
                SizedBox(height: CustomTheme.screenHeight! * 0.03),
                dropdown(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  customSheet() {
    return Container(
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3),
          ),
        ],
      ),
      child: ElevatedButton(
          onPressed: !isValid
              ? null
              : () {
                  Navigator.pop(context);
                },
          style: ElevatedButton.styleFrom(
              padding: EdgeInsets.symmetric(vertical: 12),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              minimumSize: Size(double.infinity, 0),
              backgroundColor: isValid ? successColor : errorColor),
          child: Text(
            "Save",
            style: CustomTheme.button(context,
                color: isValid ? Colors.white : Color(0xFF828282),
                fontWeight: CustomTheme.medium),
          )),
    );
  }

  textField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("Job Title *",
            style: CustomTheme.body1(context,
                color: Colors.black, fontWeight: CustomTheme.semibold)),
        SizedBox(height: CustomTheme.screenHeight! * 0.01),
        Container(
          height: CustomTheme.screenHeight! * 0.06,
          child: TextFormField(
            controller: controller,
            style: CustomTheme.body2(context,
                color: Colors.black, fontWeight: CustomTheme.medium),
            decoration: InputDecoration(
              floatingLabelBehavior: FloatingLabelBehavior.never,
              contentPadding:
                  EdgeInsets.symmetric(vertical: 30, horizontal: 10),
              filled: true,
              fillColor: Colors.white,
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Color(0xFFB8CADB))),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Color(0xFFB8CADB))),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Color(0xFFB8CADB))),
            ),
          ),
        ),
      ],
    );
  }

  dropdown() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("Department",
            style: CustomTheme.body1(context,
                color: Colors.black, fontWeight: CustomTheme.semibold)),
        SizedBox(height: CustomTheme.screenHeight! * 0.01),
        Container(
          padding: EdgeInsets.all(6),
          width: CustomTheme.screenWidth! * 1,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(5)),
              border: Border.all(width: 1, color: Color(0xFFB8CADB)),
              color: Colors.white),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("Dept. Product",
                  style: CustomTheme.body1(context,
                      color: Colors.black, fontWeight: CustomTheme.medium)),
              Icon(Icons.keyboard_arrow_down_outlined, color: Color(0xFFB8CADB))
            ],
          ),
        ),
      ],
    );
  }
}
