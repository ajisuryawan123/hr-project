import 'package:flutter/material.dart';
import 'package:hr_project/styling_theme.dart';

class JobTitlePage extends StatefulWidget {
  const JobTitlePage({Key? key}) : super(key: key);

  @override
  State<JobTitlePage> createState() => _JobTitlePageState();
}

class _JobTitlePageState extends State<JobTitlePage> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.all(12),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: CustomTheme.screenHeight! * 0.07,
              child: TextFormField(
                style: CustomTheme.body1(context,
                    color: Colors.black, fontWeight: CustomTheme.medium),
                decoration: InputDecoration(
                  floatingLabelBehavior: FloatingLabelBehavior.never,
                  contentPadding:
                      EdgeInsets.symmetric(vertical: 30, horizontal: 10),
                  labelText: "Search employee name",
                  labelStyle: CustomTheme.body1(context,
                      color: Color(0xFFBDBDBD), fontWeight: CustomTheme.medium),
                  filled: true,
                  fillColor: Color(0xFFF2F4F9),
                  prefixIcon: Icon(Icons.search, color: Color(0xFFE0E0E0)),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                ),
              ),
            ),
            SizedBox(height: CustomTheme.screenHeight! * 0.03),
            cardBody(),
          ],
        ),
      ),
    );
  }

  cardBody() {
    return Column(
      children: [
        Card(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
          child: Padding(
            padding: EdgeInsets.all(12),
            child: Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Software Engineer",
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold)),
                    SizedBox(height: CustomTheme.screenHeight! * 0.01),
                    Text("Dept. Product",
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium)),
                  ],
                ),
                Expanded(child: Container()),
                Container(
                  padding: EdgeInsets.all(8),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      color: Color(0xFFF3F6F9)),
                  child: Icon(Icons.arrow_forward_ios_rounded,
                      size: 20, color: Color(0xFF828282)),
                ),
              ],
            ),
          ),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.02),
        Card(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
          child: Padding(
            padding: EdgeInsets.all(12),
            child: Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("UI/UX Designer",
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold)),
                    SizedBox(height: CustomTheme.screenHeight! * 0.01),
                    Text("Dept. Product",
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium)),
                  ],
                ),
                Expanded(child: Container()),
                Container(
                  padding: EdgeInsets.all(8),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      color: Color(0xFFF3F6F9)),
                  child: Icon(Icons.arrow_forward_ios_rounded,
                      size: 20, color: Color(0xFF828282)),
                ),
              ],
            ),
          ),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.02),
        Card(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
          child: Padding(
            padding: EdgeInsets.all(12),
            child: Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Talent Acquisition",
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold)),
                    SizedBox(height: CustomTheme.screenHeight! * 0.01),
                    Text("Dept. Human Resource",
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium)),
                  ],
                ),
                Expanded(child: Container()),
                Container(
                  padding: EdgeInsets.all(8),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      color: Color(0xFFF3F6F9)),
                  child: Icon(Icons.arrow_forward_ios_rounded,
                      size: 20, color: Color(0xFF828282)),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
