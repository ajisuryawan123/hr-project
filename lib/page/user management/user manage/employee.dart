import 'package:flutter/material.dart';
import 'package:hr_project/page/blank_page.dart';
import 'package:hr_project/page/user%20management/detail_user_manage.dart';
import 'package:hr_project/styling_theme.dart';

class EmployeePage extends StatefulWidget {
  const EmployeePage({Key? key}) : super(key: key);

  @override
  State<EmployeePage> createState() => _EmployeePageState();
}

class _EmployeePageState extends State<EmployeePage> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.all(12),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: CustomTheme.screenHeight! * 0.07,
              child: TextFormField(
                style: CustomTheme.body1(context,
                    color: Colors.black, fontWeight: CustomTheme.medium),
                decoration: InputDecoration(
                  floatingLabelBehavior: FloatingLabelBehavior.never,
                  contentPadding:
                      EdgeInsets.symmetric(vertical: 30, horizontal: 10),
                  labelText: "Search employee name",
                  labelStyle: CustomTheme.body1(context,
                      color: Color(0xFFBDBDBD), fontWeight: CustomTheme.medium),
                  filled: true,
                  fillColor: Color(0xFFF2F4F9),
                  prefixIcon: Icon(Icons.search, color: Color(0xFFE0E0E0)),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(color: Color(0xFFF2F4F9))),
                ),
              ),
            ),
            SizedBox(height: CustomTheme.screenHeight! * 0.03),
            cardBody(),
          ],
        ),
      ),
    );
  }

  cardBody() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GestureDetector(
          onTap: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => DetailUserManage()));
          },
          child: Card(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
            child: Padding(
              padding: EdgeInsets.all(12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Container(
                          decoration: BoxDecoration(shape: BoxShape.circle),
                          child: AvatarAsset.avatar15(1)),
                      SizedBox(width: CustomTheme.screenWidth! * 0.03),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Maulana Ibrahim",
                              style: CustomTheme.body1(context,
                                  color: Colors.black,
                                  fontWeight: CustomTheme.semibold)),
                          SizedBox(height: CustomTheme.screenHeight! * 0.01),
                          Text("Software Engineer",
                              style: CustomTheme.body1(context,
                                  color: Colors.black,
                                  fontWeight: CustomTheme.medium)),
                        ],
                      ),
                      Expanded(child: Container()),
                      Container(
                        padding: EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            color: Color(0xFFF3F6F9)),
                        child: Icon(Icons.arrow_forward_ios_rounded,
                            size: 20, color: Color(0xFF828282)),
                      ),
                    ],
                  ),
                  SizedBox(height: CustomTheme.screenHeight! * 0.02),
                  Divider(
                      color: Color(0xFFF2F4F9),
                      height: 0,
                      thickness: 1,
                      endIndent: 17,
                      indent: 17),
                  SizedBox(height: CustomTheme.screenHeight! * 0.02),
                  Container(
                    height: CustomTheme.screenHeight! * 0.07,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Paid Leave",
                            style: CustomTheme.body2(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.medium)),
                        Text("maulana@appfuxion.id",
                            style: CustomTheme.body2(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.medium)),
                      ],
                    ),
                  ),
                  Container(
                    height: CustomTheme.screenHeight! * 0.07,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Join Date",
                            style: CustomTheme.body2(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.medium)),
                        Text("12/04/2023",
                            textAlign: TextAlign.end,
                            style: CustomTheme.body2(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.medium)),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.02),
        GestureDetector(
          onTap: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => BlankPage()));
          },
          child: Card(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
            child: Padding(
              padding: EdgeInsets.all(12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Container(
                          decoration: BoxDecoration(shape: BoxShape.circle),
                          child: AvatarAsset.avatar18(1)),
                      SizedBox(width: CustomTheme.screenWidth! * 0.03),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Chelsea Suci Susanti",
                              style: CustomTheme.body1(context,
                                  color: Colors.black,
                                  fontWeight: CustomTheme.semibold)),
                          SizedBox(height: CustomTheme.screenHeight! * 0.01),
                          Text("UI/UX Designer",
                              style: CustomTheme.body1(context,
                                  color: Colors.black,
                                  fontWeight: CustomTheme.medium)),
                        ],
                      ),
                      Expanded(child: Container()),
                      Container(
                        padding: EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            color: Color(0xFFF3F6F9)),
                        child: Icon(Icons.arrow_forward_ios_rounded,
                            size: 20, color: Color(0xFF828282)),
                      ),
                    ],
                  ),
                  SizedBox(height: CustomTheme.screenHeight! * 0.02),
                  Divider(
                      color: Color(0xFFF2F4F9),
                      height: 0,
                      thickness: 1,
                      endIndent: 17,
                      indent: 17),
                  SizedBox(height: CustomTheme.screenHeight! * 0.02),
                  Container(
                    height: CustomTheme.screenHeight! * 0.07,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Paid Leave",
                            style: CustomTheme.body2(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.medium)),
                        Text("susanti@appfuxion.id",
                            style: CustomTheme.body2(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.medium)),
                      ],
                    ),
                  ),
                  Container(
                    height: CustomTheme.screenHeight! * 0.07,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Join Date",
                            style: CustomTheme.body2(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.medium)),
                        Text("12/04/2023",
                            textAlign: TextAlign.end,
                            style: CustomTheme.body2(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.medium)),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
