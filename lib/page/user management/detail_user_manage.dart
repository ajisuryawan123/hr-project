import 'package:flutter/material.dart';
import 'package:hr_project/styling_theme.dart';

class DetailUserManage extends StatefulWidget {
  const DetailUserManage({Key? key}) : super(key: key);

  @override
  State<DetailUserManage> createState() => _DetailUserManageState();
}

class _DetailUserManageState extends State<DetailUserManage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFFF6F9FF),
        appBar: custom_appbar(),
        body: Stack(
          children: <Widget>[
            Positioned.fill(
              child: Align(
                alignment: Alignment.topCenter,
                child: ImageAsset.headerBackground(0.25),
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(12, 75, 12, 0),
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.fromLTRB(12, 55, 12, 12),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        color: Colors.white),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Employee Details",
                            style: CustomTheme.body1(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.semibold)),
                        SizedBox(height: CustomTheme.screenHeight! * 0.02),
                        Container(
                          height: CustomTheme.screenHeight! * 0.06,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Paid Leave Balance",
                                  style: CustomTheme.body2(context,
                                      color: Colors.black,
                                      fontWeight: CustomTheme.medium)),
                              Text("12 Days",
                                  style: CustomTheme.body2(context,
                                      color: Colors.black,
                                      fontWeight: CustomTheme.semibold)),
                            ],
                          ),
                        ),
                        Container(
                          height: CustomTheme.screenHeight! * 0.06,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Employee Name",
                                  style: CustomTheme.body2(context,
                                      color: Colors.black,
                                      fontWeight: CustomTheme.medium)),
                              Text("Raditya  Pratama",
                                  textAlign: TextAlign.end,
                                  style: CustomTheme.body2(context,
                                      color: Colors.black,
                                      fontWeight: CustomTheme.semibold)),
                            ],
                          ),
                        ),
                        Container(
                          height: CustomTheme.screenHeight! * 0.06,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Gender",
                                  style: CustomTheme.body2(context,
                                      color: Colors.black,
                                      fontWeight: CustomTheme.medium)),
                              Text("Male",
                                  textAlign: TextAlign.end,
                                  style: CustomTheme.body2(context,
                                      color: Colors.black,
                                      fontWeight: CustomTheme.semibold)),
                            ],
                          ),
                        ),
                        Container(
                          height: CustomTheme.screenHeight! * 0.06,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Job Title",
                                  style: CustomTheme.body2(context,
                                      color: Colors.black,
                                      fontWeight: CustomTheme.medium)),
                              Text("Software Engineer",
                                  textAlign: TextAlign.end,
                                  style: CustomTheme.body2(context,
                                      color: Colors.black,
                                      fontWeight: CustomTheme.semibold)),
                            ],
                          ),
                        ),
                        Container(
                          height: CustomTheme.screenHeight! * 0.06,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Email Address",
                                  style: CustomTheme.body2(context,
                                      color: Colors.black,
                                      fontWeight: CustomTheme.medium)),
                              Text("raditya@appfuxion.com",
                                  textAlign: TextAlign.end,
                                  style: CustomTheme.body2(context,
                                      color: Colors.black,
                                      fontWeight: CustomTheme.semibold)),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: CustomTheme.screenHeight! * 0.02),
                  Container(
                    padding: EdgeInsets.all(12),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        color: Colors.white),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Leave Details",
                            style: CustomTheme.body1(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.semibold)),
                        SizedBox(height: CustomTheme.screenHeight! * 0.02),
                        Container(
                          height: CustomTheme.screenHeight! * 0.06,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Paid Leave Balance",
                                  style: CustomTheme.body2(context,
                                      color: Colors.black,
                                      fontWeight: CustomTheme.medium)),
                              Text("12 Days",
                                  style: CustomTheme.body2(context,
                                      color: Colors.black,
                                      fontWeight: CustomTheme.semibold)),
                            ],
                          ),
                        ),
                        Container(
                          height: CustomTheme.screenHeight! * 0.06,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Paid Leave Taken",
                                  style: CustomTheme.body2(context,
                                      color: Colors.black,
                                      fontWeight: CustomTheme.medium)),
                              Text("0 Days",
                                  textAlign: TextAlign.end,
                                  style: CustomTheme.body2(context,
                                      color: Colors.black,
                                      fontWeight: CustomTheme.semibold)),
                            ],
                          ),
                        ),
                        Container(
                          height: CustomTheme.screenHeight! * 0.06,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Unpaid Leave Taken",
                                  style: CustomTheme.body2(context,
                                      color: Colors.black,
                                      fontWeight: CustomTheme.medium)),
                              Text("2 Days",
                                  textAlign: TextAlign.end,
                                  style: CustomTheme.body2(context,
                                      color: Colors.black,
                                      fontWeight: CustomTheme.semibold)),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                margin: EdgeInsets.only(top: 20),
                decoration: BoxDecoration(
                  border: Border.all(width: 4, color: Colors.white),
                  borderRadius: BorderRadius.circular(100),
                ),
                child: AvatarAsset.avatar15_bigger(1),
              ),
            ),
          ],
        ),
      ),
    );
  }

  custom_appbar() {
    return AppBar(
      centerTitle: true,
      backgroundColor: Color(0xFFF2C94C),
      iconTheme: IconThemeData(color: Color(0xFF2F80ED)),
      title: Text(
        "Raditya  Pratama",
        style: CustomTheme.subtitle(context,
            color: Colors.black, fontWeight: CustomTheme.semibold),
      ),
      actions: [
        IconAsset.calender_blue(),
        SizedBox(width: CustomTheme.screenWidth! * 0.07),
        GestureDetector(
            onTap: () {
              showOption();
            },
            child: Icon(Icons.more_vert, color: Color(0xFF2F80ED))),
      ],
    );
  }

  showOption() {
    return showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(15))),
        builder: (BuildContext context) {
          return Padding(
            padding: EdgeInsets.only(top: 15),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text('Options',
                    style: CustomTheme.subtitle(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                SizedBox(height: CustomTheme.screenHeight! * 0.02),
                ListTile(
                  title: Text('Edit Employee',
                      style: CustomTheme.subtitle(context,
                          color: Colors.black, fontWeight: CustomTheme.medium)),
                  trailing: Icon(Icons.arrow_forward_ios_rounded,
                      size: 17, color: Color(0xFFB8CADB)),
                ),
                Divider(
                    color: Color(0xFFD8E4EE),
                    height: 0,
                    thickness: 1,
                    endIndent: 20,
                    indent: 20),
                ListTile(
                  title: Text('View Log',
                      style: CustomTheme.subtitle(context,
                          color: Colors.black, fontWeight: CustomTheme.medium)),
                  trailing: Icon(Icons.arrow_forward_ios_rounded,
                      size: 17, color: Color(0xFFB8CADB)),
                ),
                Divider(
                    color: Color(0xFFD8E4EE),
                    height: 0,
                    thickness: 1,
                    endIndent: 20,
                    indent: 20),
                ListTile(
                  title: Text('Delete Employee',
                      style: CustomTheme.subtitle(context,
                          color: Color(0xFFF44336),
                          fontWeight: CustomTheme.medium)),
                  trailing: Icon(Icons.arrow_forward_ios_rounded,
                      size: 17, color: Color(0xFFB8CADB)),
                  onTap: () {
                    Navigator.pop(context);
                    dialog();
                  },
                ),
              ],
            ),
          );
        });
  }

  dialog() {
    return showDialog(
      context: context,
      builder: (context) {
        return Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 15),
            height: CustomTheme.screenHeight! * 0.6,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                IconAsset.delete(),
                SizedBox(height: CustomTheme.screenHeight! * 0.02),
                Text('Delete Employee?',
                    style: CustomTheme.subtitle(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)),
                SizedBox(height: CustomTheme.screenHeight! * 0.02),
                Text('Are you sure want to delete this employee?',
                    style: CustomTheme.body2(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                SizedBox(height: CustomTheme.screenHeight! * 0.02),
                Row(
                  children: [
                    CircleAvatar(radius: 3, backgroundColor: Colors.black),
                    SizedBox(width: CustomTheme.screenWidth! * 0.02),
                    SizedBox(
                        width: CustomTheme.screenWidth! * 0.15,
                        child: Text("Name",
                            style: CustomTheme.body2(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.medium))),
                    SizedBox(
                        width: CustomTheme.screenWidth! * 0.02,
                        child: Text(":",
                            style: CustomTheme.body2(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.medium))),
                    SizedBox(
                        width: CustomTheme.screenWidth! * 0.4,
                        child: Text("Maulana Ibrahim",
                            textAlign: TextAlign.start,
                            style: CustomTheme.body2(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.semibold))),
                  ],
                ),
                SizedBox(height: CustomTheme.screenHeight! * 0.02),
                Row(
                  children: [
                    CircleAvatar(radius: 3, backgroundColor: Colors.black),
                    SizedBox(width: CustomTheme.screenWidth! * 0.02),
                    SizedBox(
                        width: CustomTheme.screenWidth! * 0.15,
                        child: Text("Role",
                            style: CustomTheme.body2(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.medium))),
                    SizedBox(
                        width: CustomTheme.screenWidth! * 0.02,
                        child: Text(":",
                            style: CustomTheme.body2(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.medium))),
                    SizedBox(
                        width: CustomTheme.screenWidth! * 0.4,
                        child: Text("Software Engineer",
                            textAlign: TextAlign.start,
                            style: CustomTheme.body2(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.semibold))),
                  ],
                ),
                SizedBox(height: CustomTheme.screenHeight! * 0.02),
                Row(
                  children: [
                    CircleAvatar(radius: 3, backgroundColor: Colors.black),
                    SizedBox(width: CustomTheme.screenWidth! * 0.02),
                    SizedBox(
                        width: CustomTheme.screenWidth! * 0.15,
                        child: Text("Email",
                            style: CustomTheme.body2(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.medium))),
                    SizedBox(
                        width: CustomTheme.screenWidth! * 0.02,
                        child: Text(":",
                            style: CustomTheme.body2(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.medium))),
                    SizedBox(
                        width: CustomTheme.screenWidth! * 0.4,
                        child: Text("maulana@appfuxion.id",
                            textAlign: TextAlign.start,
                            style: CustomTheme.body2(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.semibold))),
                  ],
                ),
                SizedBox(height: CustomTheme.screenHeight! * 0.02),
                Text('This action will remove the data permanently.',
                    textAlign: TextAlign.center,
                    style: CustomTheme.body2(context,
                        color: Color(0xFFF44336),
                        fontWeight: CustomTheme.medium)),
                SizedBox(height: CustomTheme.screenHeight! * 0.03),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: ElevatedButton(
                          onPressed: () {},
                          style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.symmetric(vertical: 12),
                              side: BorderSide(width: 1, color: Colors.black),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              minimumSize: Size(double.infinity, 0),
                              backgroundColor: Colors.white),
                          child: Text(
                            "Cancel",
                            style: CustomTheme.button(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.medium),
                          )),
                    ),
                    SizedBox(width: CustomTheme.screenWidth! * 0.03),
                    Expanded(
                      child: ElevatedButton(
                          onPressed: () {
                            Navigator.pop(context);
                            Navigator.pop(context);
                            Navigator.pop(context);
                          },
                          style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.symmetric(vertical: 12),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              minimumSize: Size(double.infinity, 0),
                              backgroundColor: Color(0xFFF44336)),
                          child: Text(
                            "Delete",
                            style: CustomTheme.button(context,
                                color: Colors.white,
                                fontWeight: CustomTheme.medium),
                          )),
                    ),
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
