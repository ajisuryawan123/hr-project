import 'package:flutter/material.dart';
import 'package:hr_project/styling_theme.dart';
import 'package:hr_project/widget/custom_appbar.dart';

class TimeSettingPage extends StatefulWidget {
  const TimeSettingPage({Key? key}) : super(key: key);

  @override
  State<TimeSettingPage> createState() => _TimeSettingPageState();
}

class _TimeSettingPageState extends State<TimeSettingPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: CustomAppbar(title: "Time Settings"),
        body: Padding(
          padding: EdgeInsets.all(12),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Business Day",
                  style: CustomTheme.body1(context,
                      color: Colors.black, fontWeight: CustomTheme.semibold)),
              SizedBox(height: CustomTheme.screenHeight! * 0.03),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Day",
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold),
                      ),
                      SizedBox(height: CustomTheme.screenHeight! * 0.01),
                      Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 6, vertical: 8),
                        width: CustomTheme.screenWidth! * 0.45,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            border:
                                Border.all(width: 1, color: Color(0xFFB8CADB)),
                            color: Color(0xFFF2F4F9)),
                        child: Text("Monday",
                            style: CustomTheme.body1(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.medium)),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Time",
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold),
                      ),
                      SizedBox(height: CustomTheme.screenHeight! * 0.01),
                      Container(
                        padding: EdgeInsets.all(6),
                        width: CustomTheme.screenWidth! * 0.45,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            border:
                                Border.all(width: 1, color: Color(0xFFB8CADB)),
                            color: Colors.white),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("09.00",
                                style: CustomTheme.body1(context,
                                    color: Colors.black,
                                    fontWeight: CustomTheme.medium)),
                            IconAsset.time_light_blue()
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(height: CustomTheme.screenHeight! * 0.03),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding:
                    EdgeInsets.symmetric(horizontal: 6, vertical: 8),
                    width: CustomTheme.screenWidth! * 0.45,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        border:
                        Border.all(width: 1, color: Color(0xFFB8CADB)),
                        color: Color(0xFFF2F4F9)),
                    child: Text("Tuesday",
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium)),
                  ),
                  Container(
                    padding: EdgeInsets.all(6),
                    width: CustomTheme.screenWidth! * 0.45,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        border:
                        Border.all(width: 1, color: Color(0xFFB8CADB)),
                        color: Colors.white),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("09.00",
                            style: CustomTheme.body1(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.medium)),
                        IconAsset.time_light_blue()
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(height: CustomTheme.screenHeight! * 0.03),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding:
                    EdgeInsets.symmetric(horizontal: 6, vertical: 8),
                    width: CustomTheme.screenWidth! * 0.45,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        border:
                        Border.all(width: 1, color: Color(0xFFB8CADB)),
                        color: Color(0xFFF2F4F9)),
                    child: Text("Wednesday",
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium)),
                  ),
                  Container(
                    padding: EdgeInsets.all(6),
                    width: CustomTheme.screenWidth! * 0.45,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        border:
                        Border.all(width: 1, color: Color(0xFFB8CADB)),
                        color: Colors.white),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("09.00",
                            style: CustomTheme.body1(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.medium)),
                        IconAsset.time_light_blue()
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(height: CustomTheme.screenHeight! * 0.03),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding:
                    EdgeInsets.symmetric(horizontal: 6, vertical: 8),
                    width: CustomTheme.screenWidth! * 0.45,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        border:
                        Border.all(width: 1, color: Color(0xFFB8CADB)),
                        color: Color(0xFFF2F4F9)),
                    child: Text("Thursday",
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium)),
                  ),
                  Container(
                    padding: EdgeInsets.all(6),
                    width: CustomTheme.screenWidth! * 0.45,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        border:
                        Border.all(width: 1, color: Color(0xFFB8CADB)),
                        color: Colors.white),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("09.00",
                            style: CustomTheme.body1(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.medium)),
                        IconAsset.time_light_blue()
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(height: CustomTheme.screenHeight! * 0.03),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding:
                    EdgeInsets.symmetric(horizontal: 6, vertical: 8),
                    width: CustomTheme.screenWidth! * 0.45,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        border:
                        Border.all(width: 1, color: Color(0xFFB8CADB)),
                        color: Color(0xFFF2F4F9)),
                    child: Text("Friday",
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium)),
                  ),
                  Container(
                    padding: EdgeInsets.all(6),
                    width: CustomTheme.screenWidth! * 0.45,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        border:
                        Border.all(width: 1, color: Color(0xFFB8CADB)),
                        color: Colors.white),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("09.00",
                            style: CustomTheme.body1(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.medium)),
                        IconAsset.time_light_blue()
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(height: CustomTheme.screenHeight! * 0.05),
              Text("Overtime",
                  style: CustomTheme.body1(context,
                      color: Colors.black, fontWeight: CustomTheme.semibold)),
              SizedBox(height: CustomTheme.screenHeight! * 0.04),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding:
                    EdgeInsets.symmetric(horizontal: 6, vertical: 8),
                    width: CustomTheme.screenWidth! * 0.45,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        border:
                        Border.all(width: 1, color: Color(0xFFB8CADB)),
                        color: Color(0xFFF2F4F9)),
                    child: Text("Saturday",
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium)),
                  ),
                  Container(
                    padding: EdgeInsets.all(6),
                    width: CustomTheme.screenWidth! * 0.45,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        border:
                        Border.all(width: 1, color: Color(0xFFB8CADB)),
                        color: Colors.white),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("09.00",
                            style: CustomTheme.body1(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.medium)),
                        IconAsset.time_light_blue()
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(height: CustomTheme.screenHeight! * 0.03),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding:
                    EdgeInsets.symmetric(horizontal: 6, vertical: 8),
                    width: CustomTheme.screenWidth! * 0.45,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        border:
                        Border.all(width: 1, color: Color(0xFFB8CADB)),
                        color: Color(0xFFF2F4F9)),
                    child: Text("Sunday",
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium)),
                  ),
                  Container(
                    padding: EdgeInsets.all(6),
                    width: CustomTheme.screenWidth! * 0.45,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        border:
                        Border.all(width: 1, color: Color(0xFFB8CADB)),
                        color: Colors.white),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("09.00",
                            style: CustomTheme.body1(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.medium)),
                        IconAsset.time_light_blue()
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
