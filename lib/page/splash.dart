import 'package:flutter/material.dart';
import 'package:hr_project/page/home.dart';
import 'package:hr_project/styling_theme.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'authenticate/login.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  String username = "";

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  // Load counter value from SharedPreferences
  Future<void> _loadUsername() async {
    final SharedPreferences prefs = await _prefs;
    setState(() {
      username = prefs.getString('accessToken') ?? "";
      if (username == "") {
        Future.delayed(Duration(seconds: 3), () {
          prefs.clear();
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (_) => LoginPage()));
        });
      } else {
        Future.delayed(Duration(seconds: 3), () {
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (_) => HomePage()));
        });
      }
    });
  }

  @override
  void initState() {
    _loadUsername();
    // TODO: implement initState

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    CustomTheme().init(context);
    return Scaffold(
      backgroundColor: Color(0xFF04155A),
      body: Center(
        child: CircleAvatar(
          backgroundColor: Color(0xFFFFFFFF),
          radius: 90,
          child: Padding(
            padding: EdgeInsets.all(30),
            child: ImageAsset.logoHR(1),
          ),
        ),
      ),
    );
  }
}
