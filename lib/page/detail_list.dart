import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

import '../styling_theme.dart';
import '../widget/custom_appbar.dart';

class MessageArguments {
  final RemoteMessage message;
  final bool openedApplication;
  MessageArguments(this.message, this.openedApplication);
}

class MessageView extends StatefulWidget {
  const MessageView({Key? key}) : super(key: key);

  @override
  State<MessageView> createState() => _MessageViewState();
}

class _MessageViewState extends State<MessageView> {
  @override
  Widget build(BuildContext context) {
    final MessageArguments args =
        ModalRoute.of(context)!.settings.arguments! as MessageArguments;
    RemoteMessage message = args.message;
    RemoteNotification? notification = message.notification;

    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFFF6F9FF),
        appBar: CustomAppbar(title: "Update"),
        body: Stack(
          children: [
            Positioned.fill(
              child: Align(
                alignment: Alignment.topCenter,
                child: ImageAsset.headerBackground(0.25),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(15),
              child: Column(
                children: [
                  if (notification != null) ...[
                    Container(
                        width: CustomTheme.screenWidth! * 0.75,
                        child: Image.network(notification.android!.imageUrl!)),
                    SizedBox(height: CustomTheme.screenHeight! * 0.03),
                    content(notification.title!, notification.body!),
                  ],
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  content(String? title, String? body) {
    return Container(
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8)),
          color: Colors.white),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(8)),
                color: Color(0xFFFFEEE2)),
            child: Text(
              "widget.data.type",
              style: CustomTheme.caption(context,
                  color: Color(0xFFEB7D2E), fontWeight: CustomTheme.medium),
            ),
          ),
          SizedBox(height: CustomTheme.screenHeight! * 0.01),
          Text(
            title!,
            style: CustomTheme.body1(context,
                color: Colors.black, fontWeight: CustomTheme.semibold),
          ),
          SizedBox(height: CustomTheme.screenHeight! * 0.02),
          Text(
            body!,
            style: CustomTheme.body2(context,
                color: Colors.black, fontWeight: CustomTheme.medium),
          ),
        ],
      ),
    );
  }
}
