import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hr_project/config/API%20config/authentication-api/change_password.dart';
import 'package:hr_project/model/authentication-model/change_password_model.dart';
import 'package:hr_project/page/authenticate/login.dart';
import 'package:hr_project/styling_theme.dart';

class CreateNewPassPage extends StatefulWidget {
  const CreateNewPassPage({Key? key, required String this.email})
      : super(key: key);

  final String email;

  @override
  State<CreateNewPassPage> createState() => _CreateNewPassPageState();
}

class _CreateNewPassPageState extends State<CreateNewPassPage> {
  late ChangePasswordModel result;
  TextEditingController your_password = TextEditingController();
  TextEditingController confirm_password = TextEditingController();
  bool _passwordVisible1 = false;
  bool _passwordVisible2 = false;
  bool _hasLowerUpperCase = false;
  bool _hasNumberSymbol = false;
  bool _hasMatchLength = false;
  bool _isPasswordSame = false;
  bool _error = false;
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.all(30),
              child: Column(
                children: [
                  ImageAsset.logoHR(1.25),
                  SizedBox(height: CustomTheme.screenHeight! * 0.05),
                  Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12)),
                    elevation: 12,
                    child: Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 30, vertical: 40),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          createNewPassAppBar(),
                          SizedBox(height: CustomTheme.screenHeight! * 0.05),
                          Text(
                            "Create your new password.",
                            style: CustomTheme.body2(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.medium),
                          ),
                          SizedBox(height: CustomTheme.screenHeight! * 0.03),
                          passField(),
                          SizedBox(height: CustomTheme.screenHeight! * 0.03),
                          requirementPass(),
                          SizedBox(height: CustomTheme.screenHeight! * 0.03),
                          ElevatedButton(
                              onPressed: () {
                                if (_isLoading) {
                                  return null;
                                }
                                return onReset();
                              },
                              style: ElevatedButton.styleFrom(
                                  padding: EdgeInsets.symmetric(vertical: 15),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20)),
                                  minimumSize: Size(double.infinity, 0),
                                  backgroundColor: Color(0xFFEB7D2E)),
                              child: _isLoading
                                  ? SizedBox(
                                      child: Center(
                                          child: CircularProgressIndicator(
                                        color: Colors.white,
                                        strokeWidth: 2,
                                      )),
                                      height: 16.0,
                                      width: 16.0,
                                    )
                                  : Text(
                                      "Reset Password",
                                      style: CustomTheme.button(context,
                                          color: Colors.white,
                                          fontWeight: CustomTheme.medium),
                                    ))
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void onReset() async {
    setState(() {
      _isLoading = true;
    });

    if (_isPasswordSame &&
        _hasLowerUpperCase &&
        _hasNumberSymbol &&
        _hasMatchLength) {
      setState(() {
        _error = false;
      });

      var res = await changePasswordAPI(
        email: widget.email,
        password: your_password.text,
      );
      result = ChangePasswordModel.fromJson(json.decode(res.body));

      if (res.statusCode == 200) {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (_) => LoginPage()));
      } else {
        Fluttertoast.showToast(
          msg: "Something went wrong",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.white,
          textColor: Colors.black,
          fontSize: 14.0,
        );
      }
    } else {
      if (_hasLowerUpperCase && _hasNumberSymbol && _hasMatchLength) {
        Fluttertoast.showToast(
          msg: "Passwords do not match",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.white,
          textColor: Colors.black,
          fontSize: 14.0,
        );
      }

      setState(() {
        _error = true;
      });
    }

    setState(() {
      _isLoading = false;
    });
  }

  void validatePassword() {
    RegExp regEx1 = new RegExp(r"(?=.*[a-z])(?=.*[A-Z])\w+");
    RegExp regEx2 = new RegExp(r'^(?=.*\d)(?=.*\W).*$');

    if (regEx1.hasMatch(your_password.text)) {
      setState(() {
        _hasLowerUpperCase = true;
      });
    } else {
      setState(() {
        _hasLowerUpperCase = false;
      });
    }

    if (regEx2.hasMatch(your_password.text)) {
      setState(() {
        _hasNumberSymbol = true;
      });
    } else {
      setState(() {
        _hasNumberSymbol = false;
      });
    }

    if (your_password.text.length >= 8) {
      setState(() {
        _hasMatchLength = true;
      });
    } else {
      setState(() {
        _hasMatchLength = false;
      });
    }

    if (your_password.text == confirm_password.text) {
      setState(() {
        _isPasswordSame = true;
      });
    } else {
      setState(() {
        _isPasswordSame = false;
      });
    }
  }

  passField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Your Password",
          style: CustomTheme.body2(context,
              color: Colors.black, fontWeight: CustomTheme.semibold),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.01),
        Container(
          height: CustomTheme.screenHeight! * 0.06,
          child: TextFormField(
            controller: your_password,
            obscureText: !_passwordVisible1,
            style: CustomTheme.body2(context,
                color: Colors.black, fontWeight: CustomTheme.medium),
            decoration: InputDecoration(
              floatingLabelBehavior: FloatingLabelBehavior.never,
              contentPadding:
                  EdgeInsets.symmetric(vertical: 30, horizontal: 10),
              filled: true,
              fillColor: _error ? Colors.red.shade50 : Colors.white,
              suffixIcon: IconButton(
                icon: Icon(
                  _passwordVisible1 ? Icons.visibility : Icons.visibility_off,
                  color: Color(0xFF828282),
                ),
                onPressed: () {
                  setState(() {
                    _passwordVisible1 = !_passwordVisible1;
                  });
                },
              ),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(
                      color: _error ? Colors.red : Color(0xFFBDBDBD))),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(
                      color: _error ? Colors.red : Color(0xFFBDBDBD))),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(
                      color: _error ? Colors.red : Color(0xFFBDBDBD))),
            ),
            onChanged: (value) => validatePassword(),
          ),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.03),
        Text(
          "Retype New Password",
          style: CustomTheme.body2(context,
              color: Colors.black, fontWeight: CustomTheme.semibold),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.01),
        Container(
          height: CustomTheme.screenHeight! * 0.06,
          child: TextFormField(
            controller: confirm_password,
            obscureText: !_passwordVisible2,
            style: CustomTheme.body2(context,
                color: Colors.black, fontWeight: CustomTheme.medium),
            decoration: InputDecoration(
              floatingLabelBehavior: FloatingLabelBehavior.never,
              contentPadding:
                  EdgeInsets.symmetric(vertical: 30, horizontal: 10),
              filled: true,
              fillColor: _error ? Colors.red.shade50 : Colors.white,
              suffixIcon: IconButton(
                icon: Icon(
                  _passwordVisible2 ? Icons.visibility : Icons.visibility_off,
                  color: Color(0xFF828282),
                ),
                onPressed: () {
                  setState(() {
                    _passwordVisible2 = !_passwordVisible2;
                  });
                },
              ),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(
                      color: _error ? Colors.red : Color(0xFFBDBDBD))),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(
                      color: _error ? Colors.red : Color(0xFFBDBDBD))),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(
                      color: _error ? Colors.red : Color(0xFFBDBDBD))),
            ),
            onChanged: (value) => validatePassword(),
          ),
        ),
      ],
    );
  }

  requirementPass() {
    return Container(
      padding: EdgeInsets.all(12),
      color: Color(0x1AF44336),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Your password needs to",
            style: CustomTheme.caption(context,
                color: Colors.black, fontWeight: CustomTheme.bold),
          ),
          SizedBox(height: CustomTheme.screenHeight! * 0.01),
          Row(
            children: [
              _hasLowerUpperCase
                  ? Icon(Icons.check, color: Colors.green, size: 14)
                  : Icon(Icons.close, color: Colors.red, size: 14),
              SizedBox(width: CustomTheme.screenWidth! * 0.02),
              Expanded(
                child: Text(
                  "Include both lower and upper case character",
                  style: CustomTheme.caption(context,
                      color: Colors.black, fontWeight: CustomTheme.regular),
                ),
              ),
            ],
          ),
          SizedBox(height: CustomTheme.screenHeight! * 0.01),
          Row(
            children: [
              _hasNumberSymbol
                  ? Icon(Icons.check, color: Colors.green, size: 14)
                  : Icon(Icons.close, color: Colors.red, size: 14),
              SizedBox(width: CustomTheme.screenWidth! * 0.02),
              Expanded(
                child: Text(
                  "Include at lease one number and symbol",
                  style: CustomTheme.caption(context,
                      color: Colors.black, fontWeight: CustomTheme.regular),
                ),
              ),
            ],
          ),
          SizedBox(height: CustomTheme.screenHeight! * 0.01),
          Row(
            children: [
              _hasMatchLength
                  ? Icon(Icons.check, color: Colors.green, size: 14)
                  : Icon(Icons.close, color: Colors.red, size: 14),
              SizedBox(width: CustomTheme.screenWidth! * 0.02),
              Expanded(
                child: Text(
                  "Be at least 8 characters long",
                  style: CustomTheme.caption(context,
                      color: Colors.black, fontWeight: CustomTheme.regular),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  createNewPassAppBar() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        IconButton(
          icon: Icon(Icons.arrow_back_rounded, color: Color(0xFF828282)),
          onPressed: () => Navigator.pop(context),
          padding: EdgeInsets.zero,
          constraints: BoxConstraints(),
        ),
        Text(
          "CREATE NEW PASSWORD",
          style: CustomTheme.body1(context,
              color: Colors.black, fontWeight: CustomTheme.medium),
        ),
        SizedBox(width: 24)
      ],
    );
  }
}
