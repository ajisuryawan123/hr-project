import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hr_project/config/API%20config/authentication-api/authenticate.dart';
import 'package:hr_project/page/authenticate/forgot_password.dart';
import 'package:hr_project/page/home.dart';
import 'package:hr_project/styling_theme.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../database local/db_helper.dart';
import '../../database local/projectList-model.dart';
import '../../model/authentication-model/authenticate_model.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  AuthenticateModel? authResult;
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  bool _passwordVisible = false;
  bool incorrect = false;
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  bool _isLoading = false;

  login() async {
    setState(() {
      _isLoading = true;
    });
    final SharedPreferences prefs = await _prefs;
    var res = await authenticateAPI(email: email.text, password: password.text);
    if (res.statusCode == 200) {
      authResult = AuthenticateModel.fromJson(json.decode(res.body));
      addProject(authResult);
      print("TOKEN: " + authResult!.data!.accessToken.toString());
      await prefs.setString('email', authResult!.data!.user!.email.toString());
      await prefs.setString(
          'fullName', authResult!.data!.user!.fullName.toString());
      await prefs.setString(
          'jobTitle', authResult!.data!.employee!.jobTitle.toString());
      await prefs.setString(
          'accessToken', authResult!.data!.accessToken.toString());
      await prefs.setInt(
          'employee_id', authResult!.data!.employee!.employeeId!);
      await Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => HomePage()));
      setState(() {
        _isLoading = false;
        incorrect = false;
      });
    } else {
      authResult = AuthenticateModel.fromJson(jsonDecode(res.body));
      setState(() {
        _isLoading = false;
        incorrect = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Stack(
          children: [
            SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.all(30),
                child: Center(
                  child: Column(
                    children: [
                      ImageAsset.logoHR(1.25),
                      SizedBox(height: CustomTheme.screenHeight! * 0.05),
                      loginSection(),
                    ],
                  ),
                ),
              ),
            ),
            _isLoading
                ? Container(
                    color: Colors.white.withOpacity(0.5),
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  )
                : SizedBox.shrink()
          ],
        ),
      ),
    );
  }

  formField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Your Email",
          style: CustomTheme.body2(context,
              color: Colors.black, fontWeight: CustomTheme.semibold),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.01),
        Container(
          height: CustomTheme.screenHeight! * 0.06,
          child: TextFormField(
            controller: email,
            style: CustomTheme.body2(context,
                color: incorrect ? Colors.red : Colors.black,
                fontWeight: CustomTheme.medium),
            decoration: InputDecoration(
              floatingLabelBehavior: FloatingLabelBehavior.never,
              contentPadding:
                  EdgeInsets.symmetric(vertical: 30, horizontal: 10),
              filled: true,
              fillColor: incorrect ? Colors.red.shade50 : Colors.white,
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(
                      color: incorrect ? Colors.red : Color(0xFFBDBDBD))),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(
                      color: incorrect ? Colors.red : Color(0xFFBDBDBD))),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(
                      color: incorrect ? Colors.red : Color(0xFFBDBDBD))),
            ),
          ),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.03),
        Text(
          "Password",
          style: CustomTheme.body2(context,
              color: Colors.black, fontWeight: CustomTheme.semibold),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.01),
        Container(
          height: CustomTheme.screenHeight! * 0.06,
          child: TextFormField(
            controller: password,
            obscureText: !_passwordVisible,
            style: CustomTheme.body2(context,
                color: incorrect ? Colors.red : Colors.black,
                fontWeight: CustomTheme.medium),
            decoration: InputDecoration(
              floatingLabelBehavior: FloatingLabelBehavior.never,
              contentPadding:
                  EdgeInsets.symmetric(vertical: 30, horizontal: 10),
              filled: true,
              fillColor: incorrect ? Colors.red.shade50 : Colors.white,
              suffixIcon: IconButton(
                icon: Icon(
                  _passwordVisible ? Icons.visibility : Icons.visibility_off,
                  color: Color(0xFF828282),
                ),
                onPressed: () {
                  setState(() {
                    _passwordVisible = !_passwordVisible;
                  });
                },
              ),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(
                      color: incorrect ? Colors.red : Color(0xFFBDBDBD))),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(
                      color: incorrect ? Colors.red : Color(0xFFBDBDBD))),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(
                      color: incorrect ? Colors.red : Color(0xFFBDBDBD))),
            ),
          ),
        ),
      ],
    );
  }

  incorrectUserpass() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(height: CustomTheme.screenHeight! * 0.03),
        Text(
          authResult == null
              ? "Email  data not found"
              : authResult!.errorMessage.toString(),
          style: CustomTheme.body2(context,
              color: Colors.red, fontWeight: CustomTheme.medium),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.03),
      ],
    );
  }

  loginSection() {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      elevation: 12,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 30),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "Login",
              style: CustomTheme.headline4(context,
                  color: Colors.black, fontWeight: CustomTheme.medium),
            ),
            incorrect
                ? incorrectUserpass()
                : SizedBox(height: CustomTheme.screenHeight! * 0.05),
            formField(),
            SizedBox(height: CustomTheme.screenHeight! * 0.03),
            GestureDetector(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (_) => ForgotPassword()));
              },
              child: Text(
                "FORGOT PASSWORD?",
                style: CustomTheme.caption(context,
                        color: Colors.black, fontWeight: CustomTheme.semibold)
                    .copyWith(decoration: TextDecoration.underline),
              ),
            ),
            SizedBox(height: CustomTheme.screenHeight! * 0.07),
            ElevatedButton(
                onPressed: () async {
                  if (email.text.isNotEmpty && password.text.isNotEmpty) {
                    login();
                  } else {
                    setState(() {
                      incorrect = true;
                    });
                  }
                },
                style: ElevatedButton.styleFrom(
                    padding: EdgeInsets.symmetric(vertical: 15),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    minimumSize: Size(double.infinity, 0),
                    backgroundColor: Color(0xFFEB7D2E)),
                child: Text(
                  "Login",
                  style: CustomTheme.button(context,
                      color: Colors.white, fontWeight: CustomTheme.medium),
                ))
          ],
        ),
      ),
    );
  }

  Future addProject(AuthenticateModel? authRes) async {
    var projectListData = authRes!.data!.projectList!;
    for (var projectData in projectListData) {
      final project_list = TodayProjectList(
          idProject: projectData.projectId!,
          nameProject: projectData.projectName!);
      await ProjectDatabase.instance.create(project_list);
    }
  }
}
