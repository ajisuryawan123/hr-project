import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hr_project/config/API%20config/authentication-api/forgot_password.dart';
import 'package:hr_project/model/authentication-model/forgot_password_model.dart';
import 'package:hr_project/page/authenticate/verification.dart';
import 'package:hr_project/styling_theme.dart';
import 'package:hr_project/utils/validate_email.dart';

class ForgotPassword extends StatefulWidget {
  const ForgotPassword({Key? key}) : super(key: key);

  @override
  State<ForgotPassword> createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  late ForgotPasswordModel result;
  TextEditingController email = TextEditingController();
  bool _isLoading = false;
  bool incorrect = false;
  String errorMessage = " ";

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(30),
            child: Center(
              child: Column(
                children: [
                  ImageAsset.logoHR(1.25),
                  SizedBox(height: CustomTheme.screenHeight! * 0.05),
                  forgotPasswordSection(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  emailField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Your Email",
          style: CustomTheme.body2(context,
              color: Colors.black, fontWeight: CustomTheme.semibold),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.01),
        Container(
          height: CustomTheme.screenHeight! * 0.06,
          child: TextFormField(
            controller: email,
            style: CustomTheme.body2(context,
                color: incorrect ? Colors.red : Colors.black,
                fontWeight: CustomTheme.medium),
            decoration: InputDecoration(
              floatingLabelBehavior: FloatingLabelBehavior.never,
              contentPadding:
                  EdgeInsets.symmetric(vertical: 30, horizontal: 10),
              filled: true,
              fillColor: incorrect ? Colors.red.shade50 : Colors.white,
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(
                      color: incorrect ? Colors.red : Color(0xFFBDBDBD))),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(
                      color: incorrect ? Colors.red : Color(0xFFBDBDBD))),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(
                      color: incorrect ? Colors.red : Color(0xFFBDBDBD))),
            ),
          ),
        ),
      ],
    );
  }

  incorrectUserPass() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(height: CustomTheme.screenHeight! * 0.02),
        Text(
          errorMessage,
          style: CustomTheme.body2(context,
              color: Colors.red, fontWeight: CustomTheme.medium),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.02),
      ],
    );
  }

  forgotPasswordSection() {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      elevation: 12,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 30, vertical: 40),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            navBarForgot(),
            incorrectUserPass(),
            Text(
              "Enter you registered email. We'll send you verification code to reset your password.",
              style: CustomTheme.body2(context,
                  color: Colors.black, fontWeight: CustomTheme.medium),
            ),
            SizedBox(height: CustomTheme.screenHeight! * 0.05),
            emailField(),
            SizedBox(height: CustomTheme.screenHeight! * 0.05),
            ElevatedButton(
                onPressed: () {
                  if (_isLoading) {
                    return null;
                  }

                  return sendButtonPressed();
                },
                style: ElevatedButton.styleFrom(
                    padding: EdgeInsets.symmetric(vertical: 15),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    minimumSize: Size(double.infinity, 0),
                    backgroundColor: Color(0xFFEB7D2E)),
                child: _isLoading
                    ? SizedBox(
                        child: Center(
                            child: CircularProgressIndicator(
                          color: Colors.white,
                          strokeWidth: 2,
                        )),
                        height: 16.0,
                        width: 16.0,
                      )
                    : Text(
                        "Send",
                        style: CustomTheme.button(context,
                            color: Colors.white,
                            fontWeight: CustomTheme.medium),
                      ))
          ],
        ),
      ),
    );
  }

  void sendButtonPressed() async {
    if (isEmailValid(email.text)) {
      setState(() {
        _isLoading = true;
      });

      var res = await forgotPasswordAPI(email: email.text);
      result = ForgotPasswordModel.fromJson(json.decode(res.body));

      if (res.statusCode == 200) {
        setState(() {
          incorrect = false;
        });

        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (_) => VerificationPage(email: email.text)));
      } else {
        if (result.errorCode == "200000") {
          setState(() {
            incorrect = false;
            errorMessage = " ";
          });
        }
        if (result.errorCode == "200401") {
          setState(() {
            incorrect = true;
            errorMessage = "Enter a valid email address.";
          });
        } else if (result.errorCode == "200400") {
          setState(() {
            incorrect = false;
            errorMessage = result.errorMessage;
          });
        } else {
          setState(() {
            incorrect = false;
            errorMessage = "Something went wrong.";
          });
        }
      }

      setState(() {
        _isLoading = false;
      });
    } else {
      setState(() {
        incorrect = true;
        errorMessage = "Enter a valid email address.";
      });
    }
  }

  navBarForgot() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        IconButton(
          icon: Icon(Icons.arrow_back_rounded, color: Color(0xFF828282)),
          onPressed: () => Navigator.pop(context),
          padding: EdgeInsets.zero,
          constraints: BoxConstraints(),
        ),
        Text(
          "FORGOT PASSWORD",
          style: CustomTheme.body1(context,
              color: Colors.black, fontWeight: CustomTheme.medium),
        ),
        SizedBox(width: 24)
      ],
    );
  }
}
