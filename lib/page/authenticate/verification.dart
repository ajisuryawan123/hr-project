import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hr_project/config/API%20config/authentication-api/forgot_password.dart';
import 'package:hr_project/config/API%20config/authentication-api/verify_otp.dart';
import 'package:hr_project/model/authentication-model/verify_otp_model.dart';
import 'package:hr_project/page/authenticate/create_newPass.dart';
import 'package:hr_project/styling_theme.dart';
import 'package:pinput/pinput.dart';
import 'package:fluttertoast/fluttertoast.dart';

class VerificationPage extends StatefulWidget {
  const VerificationPage({Key? key, required String this.email})
      : super(key: key);

  final String email;

  @override
  State<VerificationPage> createState() => _VerificationPageState();
}

class _VerificationPageState extends State<VerificationPage> {
  late VerifyOtpModel result;
  final pinController = TextEditingController();
  final focusNode = FocusNode();
  final formKey = GlobalKey<FormState>();
  bool _isLoading = false;
  bool incorrect = false;
  String errorMessage = " ";

  @override
  void dispose() {
    pinController.dispose();
    focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(30),
            child: Center(
              child: Column(
                children: [
                  ImageAsset.logoHR(1.25),
                  SizedBox(height: CustomTheme.screenHeight! * 0.05),
                  verificationSection(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void onVerify(String pin) async {
    setState(() {
      _isLoading = true;
    });

    var res = await verifyOtpAPI(email: widget.email, otp: pin);
    result = VerifyOtpModel.fromJson(json.decode(res.body));

    if (res.statusCode == 200) {
      setState(() {
        incorrect = false;
        errorMessage = " ";
      });

      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (_) => CreateNewPassPage(email: widget.email)));
    } else {
      if (result.errorCode == "200404") {
        setState(() {
          incorrect = true;
          errorMessage = "Please enter the correct verification code.";
        });
      } else {
        setState(() {
          incorrect = false;
          errorMessage = "Something went wrong.";
        });
      }
    }

    setState(() {
      _isLoading = false;
    });
  }

  void requestOtp() async {
    setState(() {
      _isLoading = true;
    });

    Fluttertoast.showToast(
        msg: "Requesting OTP",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.white,
        textColor: Colors.black,
        fontSize: 16.0);

    await forgotPasswordAPI(email: widget.email);

    setState(() {
      _isLoading = false;
    });
  }

  emailField() {
    const cursor = Color(0xFF828282);
    const focusedBorderColor = Color(0xFFE0E0E0);
    const fillColor = Color(0xFFF3F6F9);
    const borderColor = Color(0xFFE0E0E0);
    const errorColor = Colors.red;

    final defaultPinTheme = PinTheme(
      width: 56,
      height: 56,
      textStyle: CustomTheme.body1(context,
          color: Colors.black, fontWeight: CustomTheme.semibold),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        border: Border.all(color: borderColor),
      ),
    );

    return Form(
      key: formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Directionality(
            // Specify direction if desired
            textDirection: TextDirection.ltr,
            child: Pinput(
              controller: pinController,
              focusNode: focusNode,
              androidSmsAutofillMethod:
                  AndroidSmsAutofillMethod.smsUserConsentApi,
              defaultPinTheme: defaultPinTheme,
              // onClipboardFound: (value) {
              //   debugPrint('onClipboardFound: $value');
              //   pinController.setText(value);
              // },
              hapticFeedbackType: HapticFeedbackType.lightImpact,
              onCompleted: (pin) {
                onVerify(pin);

                debugPrint('onCompleted: $pin');
              },
              cursor: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    margin: EdgeInsets.only(bottom: 9),
                    width: 22,
                    height: 1,
                    color: cursor,
                  ),
                ],
              ),
              focusedPinTheme: defaultPinTheme.copyWith(
                decoration: defaultPinTheme.decoration!.copyWith(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(
                      color: incorrect ? errorColor : focusedBorderColor),
                ),
              ),
              submittedPinTheme: defaultPinTheme.copyWith(
                decoration: defaultPinTheme.decoration!.copyWith(
                  color: fillColor,
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(
                      color: incorrect ? errorColor : focusedBorderColor),
                ),
              ),
              errorPinTheme: defaultPinTheme.copyBorderWith(
                border: Border.all(color: Colors.red),
              ),
            ),
          ),
          SizedBox(height: CustomTheme.screenHeight! * 0.02),
          TextButton(
            onPressed: () {
              if (_isLoading) {
                return null;
              }

              return requestOtp();
            },
            child: Text(
              "Send new verification code",
              style: CustomTheme.caption(context,
                      color: Color(0xFFEB7D2E),
                      fontWeight: CustomTheme.semibold)
                  .copyWith(decoration: TextDecoration.underline),
            ),
          ),
          SizedBox(height: CustomTheme.screenHeight! * 0.02),
          ElevatedButton(
              onPressed: () {
                if (_isLoading) {
                  return null;
                }

                focusNode.unfocus();
                formKey.currentState!.validate();

                return onVerify(pinController.text);
              },
              style: ElevatedButton.styleFrom(
                  padding: EdgeInsets.symmetric(vertical: 15),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  minimumSize: Size(double.infinity, 0),
                  backgroundColor: Color(0xFFEB7D2E)),
              child: _isLoading
                  ? SizedBox(
                      child: Center(
                          child: CircularProgressIndicator(
                        color: Colors.white,
                        strokeWidth: 2,
                      )),
                      height: 16.0,
                      width: 16.0,
                    )
                  : Text(
                      "Next",
                      style: CustomTheme.button(context,
                          color: Colors.white, fontWeight: CustomTheme.medium),
                    ))
        ],
      ),
    );
  }

  verificationSection() {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      elevation: 12,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 30, vertical: 40),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            verificationAppBar(),
            incorrectSection(),
            Text(
              "We've sent a verification code to:",
              style: CustomTheme.body2(context,
                  color: Colors.black, fontWeight: CustomTheme.medium),
            ),
            SizedBox(height: 4),
            Text(
              widget.email,
              style: CustomTheme.body2(context,
                  color: Colors.black, fontWeight: CustomTheme.semibold),
            ),
            SizedBox(height: CustomTheme.screenHeight! * 0.03),
            emailField(),
          ],
        ),
      ),
    );
  }

  incorrectSection() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(height: CustomTheme.screenHeight! * 0.02),
        Text(
          errorMessage,
          textAlign: TextAlign.center,
          style: CustomTheme.body2(context,
              color: Colors.red, fontWeight: CustomTheme.medium),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.02),
      ],
    );
  }

  verificationAppBar() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        IconButton(
          icon: Icon(Icons.arrow_back_rounded, color: Color(0xFF828282)),
          onPressed: () => Navigator.pop(context),
          padding: EdgeInsets.zero,
          constraints: BoxConstraints(),
        ),
        Text(
          "VERIFICATION",
          style: CustomTheme.body1(context,
              color: Colors.black, fontWeight: CustomTheme.medium),
        ),
        SizedBox(width: 24)
      ],
    );
  }
}
