import 'package:flutter/material.dart';
import 'package:hr_project/styling_theme.dart';

class PayrollDetailPage extends StatefulWidget {
  final String titleAppbar;
  const PayrollDetailPage({Key? key, required this.titleAppbar})
      : super(key: key);

  @override
  State<PayrollDetailPage> createState() => _PayrollDetailPageState();
}

class _PayrollDetailPageState extends State<PayrollDetailPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFFF6F9FF),
        appBar: custom_appbar(),
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(12),
            child: Column(
              children: [
                info1(),
                SizedBox(height: CustomTheme.screenHeight! * 0.03),
                info2()
              ],
            ),
          ),
        ),
      ),
    );
  }

  custom_appbar() {
    return AppBar(
      centerTitle: true,
      iconTheme: IconThemeData(color: Color(0xFF2F80ED)),
      backgroundColor: Color(0xFFF2C94C),
      title: Text(
        widget.titleAppbar,
        style: CustomTheme.subtitle(context,
            color: Colors.black, fontWeight: CustomTheme.semibold),
      ),
      actions: [
        IconAsset.download(),
      ],
    );
  }

  info1() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.all(12),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              border: Border.all(width: 1, color: Color(0xFFB8CADB)),
              color: Colors.white),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Employee Details",
                  style: CustomTheme.body1(context,
                      color: Colors.black, fontWeight: CustomTheme.semibold)),
              SizedBox(height: CustomTheme.screenHeight! * 0.03),
              Container(
                height: CustomTheme.screenHeight! * 0.06,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Employee Name",
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium)),
                    Text("Raditya  Pratama",
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold)),
                  ],
                ),
              ),
              Container(
                height: CustomTheme.screenHeight! * 0.06,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Role",
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium)),
                    Text("BackEnd Engineer",
                        textAlign: TextAlign.end,
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold)),
                  ],
                ),
              ),
              Container(
                height: CustomTheme.screenHeight! * 0.06,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Employee ID",
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium)),
                    Text("ACI 00123",
                        textAlign: TextAlign.end,
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold)),
                  ],
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.03),
        Container(
          padding: EdgeInsets.all(12),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              border: Border.all(width: 1, color: Color(0xFFB8CADB)),
              color: Colors.white),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Payslip",
                  style: CustomTheme.body1(context,
                      color: Colors.black, fontWeight: CustomTheme.semibold)),
              SizedBox(height: CustomTheme.screenHeight! * 0.03),
              Container(
                height: CustomTheme.screenHeight! * 0.06,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Periode",
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium)),
                    Text(widget.titleAppbar,
                        textAlign: TextAlign.end,
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold)),
                  ],
                ),
              ),
              Container(
                height: CustomTheme.screenHeight! * 0.06,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Gross Earning",
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium)),
                    Text("35.000.000",
                        textAlign: TextAlign.end,
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold)),
                  ],
                ),
              ),
              Container(
                height: CustomTheme.screenHeight! * 0.06,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Net Pay",
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium)),
                    Container(
                      padding: EdgeInsets.all(8),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          color: Color(0xFFF2F4F9)),
                      child: Text("30.000.000",
                          style: CustomTheme.body2(context,
                              color: Color(0xFF5981EA),
                              fontWeight: CustomTheme.semibold)),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.03),
        Container(
          padding: EdgeInsets.all(12),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              border: Border.all(width: 1, color: Color(0xFFB8CADB)),
              color: Colors.white),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Earning",
                  style: CustomTheme.body1(context,
                      color: Color(0xFF27AE60),
                      fontWeight: CustomTheme.semibold)),
              SizedBox(height: CustomTheme.screenHeight! * 0.03),
              Container(
                height: CustomTheme.screenHeight! * 0.06,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Basic",
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium)),
                    Text("20.000.000",
                        textAlign: TextAlign.end,
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold)),
                  ],
                ),
              ),
              Container(
                height: CustomTheme.screenHeight! * 0.06,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Tunjangan Telekomunikasi",
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium)),
                    Text("5.000.000",
                        textAlign: TextAlign.end,
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold)),
                  ],
                ),
              ),
              Container(
                height: CustomTheme.screenHeight! * 0.06,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Tunjangan Transportasi",
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium)),
                    Text("5.000.000",
                        textAlign: TextAlign.end,
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold)),
                  ],
                ),
              ),
              Container(
                height: CustomTheme.screenHeight! * 0.06,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Tunjangan Rumah",
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium)),
                    Text("5.000.000",
                        textAlign: TextAlign.end,
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold)),
                  ],
                ),
              ),
              SizedBox(height: CustomTheme.screenHeight! * 0.02),
              Divider(
                  color: Color(0xFFF2F4F9),
                  height: 0,
                  thickness: 1,
                  endIndent: 17,
                  indent: 17),
              SizedBox(height: CustomTheme.screenHeight! * 0.02),
              Container(
                height: CustomTheme.screenHeight! * 0.06,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Total",
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium)),
                    Text("35.000.000",
                        textAlign: TextAlign.end,
                        style: CustomTheme.body2(context,
                            color: Colors.black, fontWeight: CustomTheme.bold)),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  info2() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.all(12),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              border: Border.all(width: 1, color: Color(0xFFB8CADB)),
              color: Colors.white),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Deductions",
                  style: CustomTheme.body1(context,
                      color: Color(0xFFF44336),
                      fontWeight: CustomTheme.semibold)),
              SizedBox(height: CustomTheme.screenHeight! * 0.03),
              Container(
                height: CustomTheme.screenHeight! * 0.06,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Iuran Pensiun/THT",
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium)),
                    Text("1.000.000",
                        textAlign: TextAlign.end,
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold)),
                  ],
                ),
              ),
              Container(
                height: CustomTheme.screenHeight! * 0.06,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Potongan Premi Kesehatan",
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium)),
                    Text("1.000.000",
                        textAlign: TextAlign.end,
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold)),
                  ],
                ),
              ),
              Container(
                height: CustomTheme.screenHeight! * 0.06,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Iuran JHT",
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium)),
                    Text("1.000.000",
                        textAlign: TextAlign.end,
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold)),
                  ],
                ),
              ),
              Container(
                height: CustomTheme.screenHeight! * 0.06,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Pajak Penghasilan",
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium)),
                    Text("1.000.000",
                        textAlign: TextAlign.end,
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold)),
                  ],
                ),
              ),
              SizedBox(height: CustomTheme.screenHeight! * 0.02),
              Divider(
                  color: Color(0xFFF2F4F9),
                  height: 0,
                  thickness: 1,
                  endIndent: 17,
                  indent: 17),
              SizedBox(height: CustomTheme.screenHeight! * 0.02),
              Container(
                height: CustomTheme.screenHeight! * 0.06,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Total",
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium)),
                    Text("4.000.000",
                        textAlign: TextAlign.end,
                        style: CustomTheme.body2(context,
                            color: Colors.black, fontWeight: CustomTheme.bold)),
                  ],
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.03),
        Container(
          padding: EdgeInsets.all(12),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              border: Border.all(width: 1, color: Color(0xFFB8CADB)),
              color: Colors.white),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Loans",
                  style: CustomTheme.body1(context,
                      color: Color(0xFFF44336),
                      fontWeight: CustomTheme.semibold)),
              SizedBox(height: CustomTheme.screenHeight! * 0.03),
              Container(
                height: CustomTheme.screenHeight! * 0.06,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Pinjaman Karyawan",
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium)),
                    Text("1.000.000",
                        textAlign: TextAlign.end,
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold)),
                  ],
                ),
              ),
              SizedBox(height: CustomTheme.screenHeight! * 0.02),
              Divider(
                  color: Color(0xFFF2F4F9),
                  height: 0,
                  thickness: 1,
                  endIndent: 17,
                  indent: 17),
              SizedBox(height: CustomTheme.screenHeight! * 0.02),
              Container(
                height: CustomTheme.screenHeight! * 0.06,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Total",
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium)),
                    Text("1.000.000",
                        textAlign: TextAlign.end,
                        style: CustomTheme.body2(context,
                            color: Colors.black, fontWeight: CustomTheme.bold)),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
