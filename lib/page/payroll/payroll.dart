import 'package:flutter/material.dart';
import 'package:hr_project/page/payroll/payroll_detail.dart';
import 'package:hr_project/styling_theme.dart';
import 'package:hr_project/widget/custom_appbar.dart';

class PayrollPage extends StatefulWidget {
  const PayrollPage({Key? key}) : super(key: key);

  @override
  State<PayrollPage> createState() => _PayrollPageState();
}

class _PayrollPageState extends State<PayrollPage> {
  String title1 = "February 2023";
  String title2 = "January 2023";
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFFF6F9FF),
        appBar: CustomAppbar(title: "Payroll"),
        body: cardBody(),
      ),
    );
  }

  cardBody() {
    return Padding(
      padding: EdgeInsets.all(12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          PayrollDetailPage(titleAppbar: title1)));
            },
            child: Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)),
              child: Padding(
                padding: EdgeInsets.all(12),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Container(
                            decoration: BoxDecoration(shape: BoxShape.circle),
                            child: AvatarAsset.avatar27(1)),
                        SizedBox(width: CustomTheme.screenWidth! * 0.03),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Payroll February 2023",
                                style: CustomTheme.body1(context,
                                    color: Colors.black,
                                    fontWeight: CustomTheme.semibold)),
                            SizedBox(height: CustomTheme.screenHeight! * 0.01),
                            Text(title1,
                                style: CustomTheme.body1(context,
                                    color: Colors.black,
                                    fontWeight: CustomTheme.medium)),
                          ],
                        ),
                        Expanded(child: Container()),
                        Container(
                          padding: EdgeInsets.all(8),
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8)),
                              color: Color(0xFFF3F6F9)),
                          child: Icon(Icons.arrow_forward_ios_rounded,
                              size: 20, color: Color(0xFF828282)),
                        ),
                      ],
                    ),
                    SizedBox(height: CustomTheme.screenHeight! * 0.02),
                    Divider(
                        color: Color(0xFFF2F4F9),
                        height: 0,
                        thickness: 1,
                        endIndent: 17,
                        indent: 17),
                    SizedBox(height: CustomTheme.screenHeight! * 0.02),
                    Container(
                      height: CustomTheme.screenHeight! * 0.07,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Pay Date",
                              style: CustomTheme.body2(context,
                                  color: Colors.black,
                                  fontWeight: CustomTheme.medium)),
                          Text("27 February 2023",
                              style: CustomTheme.body2(context,
                                  color: Colors.black,
                                  fontWeight: CustomTheme.medium)),
                        ],
                      ),
                    ),
                    Container(
                      height: CustomTheme.screenHeight! * 0.07,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Hours Total",
                              style: CustomTheme.body2(context,
                                  color: Colors.black,
                                  fontWeight: CustomTheme.medium)),
                          Text("160 Hours",
                              textAlign: TextAlign.end,
                              style: CustomTheme.body2(context,
                                  color: Colors.black,
                                  fontWeight: CustomTheme.medium)),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          SizedBox(height: CustomTheme.screenHeight! * 0.02),
          GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => PayrollDetailPage(
                            titleAppbar: title2,
                          )));
            },
            child: Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)),
              child: Padding(
                padding: EdgeInsets.all(12),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Container(
                            decoration: BoxDecoration(shape: BoxShape.circle),
                            child: AvatarAsset.avatar27(1)),
                        SizedBox(width: CustomTheme.screenWidth! * 0.03),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Payroll January 2023",
                                style: CustomTheme.body1(context,
                                    color: Colors.black,
                                    fontWeight: CustomTheme.semibold)),
                            SizedBox(height: CustomTheme.screenHeight! * 0.01),
                            Text(title2,
                                style: CustomTheme.body1(context,
                                    color: Colors.black,
                                    fontWeight: CustomTheme.medium)),
                          ],
                        ),
                        Expanded(child: Container()),
                        Container(
                          padding: EdgeInsets.all(8),
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8)),
                              color: Color(0xFFF3F6F9)),
                          child: Icon(Icons.arrow_forward_ios_rounded,
                              size: 20, color: Color(0xFF828282)),
                        ),
                      ],
                    ),
                    SizedBox(height: CustomTheme.screenHeight! * 0.02),
                    Divider(
                        color: Color(0xFFF2F4F9),
                        height: 0,
                        thickness: 1,
                        endIndent: 17,
                        indent: 17),
                    SizedBox(height: CustomTheme.screenHeight! * 0.02),
                    Container(
                      height: CustomTheme.screenHeight! * 0.07,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Pay Date",
                              style: CustomTheme.body2(context,
                                  color: Colors.black,
                                  fontWeight: CustomTheme.medium)),
                          Text("30 January 2023",
                              style: CustomTheme.body2(context,
                                  color: Colors.black,
                                  fontWeight: CustomTheme.medium)),
                        ],
                      ),
                    ),
                    Container(
                      height: CustomTheme.screenHeight! * 0.07,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Hours Total",
                              style: CustomTheme.body2(context,
                                  color: Colors.black,
                                  fontWeight: CustomTheme.medium)),
                          Text("168 Hours",
                              textAlign: TextAlign.end,
                              style: CustomTheme.body2(context,
                                  color: Colors.black,
                                  fontWeight: CustomTheme.medium)),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
