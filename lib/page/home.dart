import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:hr_project/list_dummy.dart';
import 'package:hr_project/page/detail_list.dart';
import 'package:hr_project/page/viewlist.dart';
import 'package:hr_project/styling_theme.dart';
import 'package:hr_project/widget/grid_menu_home.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../widget/notif_list.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  String? username;
  String? email;
  String? jobTitle;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
  late AndroidNotificationChannel channel;
  late FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  String? deviceId;
  List<RemoteMessage> _messages = [];

  void getToken() async {
    _firebaseMessaging.getToken().then((token) {
      print('Token Notification: $token');
      FirebaseFirestore.instance
          .collection("UserTokens")
          .doc(deviceId)
          .get()
          .then((doc) async {
        if (doc.exists) {
          await FirebaseFirestore.instance
              .collection("UserTokens")
              .doc(deviceId)
              .update({
            'token': token,
          });
        } else {
          await FirebaseFirestore.instance
              .collection("UserTokens")
              .doc(deviceId)
              .set({
            'token': token,
          });
        }
      });
    });
  }

  Future<String?> getDeviceId() async {
    var deviceInfo = DeviceInfoPlugin();
    if (Platform.isIOS) {
      var iosDeviceInfo = await deviceInfo.iosInfo;
      return iosDeviceInfo.identifierForVendor;
    } else if (Platform.isAndroid) {
      var androidDeviceInfo = await deviceInfo.androidInfo;
      return androidDeviceInfo.id;
    }
    return null;
  }

  void requestPermission() async {
    deviceId = await getDeviceId();
    NotificationSettings settings =
        await FirebaseMessaging.instance.requestPermission(
      alert: true,
      announcement: true,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );

    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      print('User granted permission');
    } else if (settings.authorizationStatus ==
        AuthorizationStatus.provisional) {
      print('User granted provisional permission');
    } else {
      print('User declined or has not accepted permission');
    }
  }

  void loadFCM() async {
    channel = const AndroidNotificationChannel(
      'high_importance_channel',
      'High Importance Notifications',
      importance: Importance.high,
    );

    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);

    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true,
    );
  }

  void listenFCM() {
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification? notification = message.notification;
      AndroidNotification? android = message.notification?.android;
      if (notification != null && android != null) {
        flutterLocalNotificationsPlugin.show(
          notification.hashCode,
          notification.title,
          notification.body,
          NotificationDetails(
            android: AndroidNotificationDetails(
              channel.id,
              channel.name,
              icon: 'launch_background',
            ),
          ),
        );
      }
    });
  }

  void onClickMessage() {
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      print('A new onMessageOpenedApp event was published!');
      Navigator.pushNamed(
        context,
        '/message',
        arguments: MessageArguments(message, true),
      );
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loadUsername();
    requestPermission();
    getToken();
    loadFCM();

    FirebaseMessaging.instance
        .getInitialMessage()
        .then((RemoteMessage? message) {
      if (message != null) {
        Navigator.pushNamed(
          context,
          '/message',
          arguments: MessageArguments(message, true),
        );
      }
    });

    listenFCM();
    onClickMessage();

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      setState(() {
        _messages = [..._messages, message];
      });
    });
  }

  Future<void> _loadUsername() async {
    final SharedPreferences prefs = await _prefs;
    setState(() {
      username = prefs.getString('fullName') ?? "";
      email = prefs.getString('email') ?? "";
      jobTitle = prefs.getString('jobTitle') ?? "";
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Stack(
          children: [
            Positioned.fill(
              child: Align(
                alignment: Alignment.topCenter,
                child: ImageAsset.headerBackground(0.25),
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(25, 25, 25, 0),
              child: Column(
                children: [
                  header(),
                  SizedBox(height: CustomTheme.screenHeight! * 0.05),
                  menu(),
                  SizedBox(height: CustomTheme.screenHeight! * 0.05),
                  view_all(),
                  SizedBox(height: CustomTheme.screenHeight! * 0.02),
                  list(),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  header() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconAsset.avatar(1),
        SizedBox(width: CustomTheme.screenWidth! * 0.07),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              username ?? "",
              style: CustomTheme.body1(context,
                  color: Colors.black, fontWeight: CustomTheme.semibold),
            ),
            SizedBox(height: CustomTheme.screenHeight! * 0.01),
            Text(
              "${jobTitle ?? ""}\n${email ?? ""}",
              style: CustomTheme.body2(context,
                  color: Colors.white, fontWeight: CustomTheme.medium),
            ),
          ],
        ),
        Expanded(child: Container()),
        Container(
          padding: EdgeInsets.all(8),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(12)),
              color: Colors.white),
          child: IconAsset.bell(),
        )
      ],
    );
  }

  menu() {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      child: Padding(
          padding: EdgeInsets.all(15),
          child: GridView.builder(
            shrinkWrap: true,
            itemCount: ListDummy.gridMenu.length,
            physics: NeverScrollableScrollPhysics(),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 4,
              mainAxisSpacing: 20,
            ),
            itemBuilder: (BuildContext context, int index) {
              return GridMenuHome(
                  icon: ListDummy.gridMenu[index].icon,
                  title: ListDummy.gridMenu[index].title,
                  directPage: ListDummy.gridMenu[index].directPage);
            },
          )),
    );
  }

  list() {
    return Expanded(child: MessageList(messages: _messages));
  }

  view_all() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          "Latest Updates",
          style: CustomTheme.body1(context,
              color: Colors.black, fontWeight: CustomTheme.semibold),
        ),
        GestureDetector(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ViewList(listAll: _messages)));
          },
          child: Text(
            "View All",
            style: CustomTheme.body1(context,
                color: Color(0xFF5981EA), fontWeight: CustomTheme.semibold),
          ),
        ),
      ],
    );
  }
}
