import 'dart:convert';

import 'package:hr_project/config/global_config.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

Future<Response> employeeChangeProject_API(
    {required int project_id, required String token}) async {
  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Accept': 'application/hal+json',
    'Content-Type': 'application/json',
  };
  final body = {
    "project_id": project_id,
    "start_date": "2023-06-28",
    "end_date": "2023-06-28"
  };
  print("RAW Employee Change Project: " + body.toString());
  print("URL Employee Change Project: " +
      ServerUrl.baseUrl +
      ServerUrl.employeeChangeProject);
  final res = await http.post(
      Uri.parse(ServerUrl.baseUrl + ServerUrl.employeeChangeProject),
      headers: headers,
      body: jsonEncode(body));
  print("STATUS CODE(Employee Change Project): " + res.statusCode.toString());
  print("BODY(Employee Change Project): " + res.body);
  return res;
}
