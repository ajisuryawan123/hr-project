import 'package:hr_project/config/global_config.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

Future<Response> employeeCurrentProject_API({required String token}) async {
  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Accept': 'application/hal+json',
    'Content-Type': 'application/json',
  };
  print("URL Employee Current Project: " +
      ServerUrl.baseUrl +
      ServerUrl.employeeCurrentProject);
  final res = await http.get(
      Uri.parse(ServerUrl.baseUrl + ServerUrl.employeeCurrentProject),
      headers: headers);
  print("STATUS CODE(Employee Current Project): " + res.statusCode.toString());
  print("BODY(Employee Current Project): " + res.body);
  return res;
}
