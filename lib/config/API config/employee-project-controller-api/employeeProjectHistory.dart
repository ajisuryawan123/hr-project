import 'package:hr_project/config/global_config.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

Future<Response> employeeProjectHistory_API({required String token}) async {
  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Accept': 'application/hal+json',
    'Content-Type': 'application/json',
  };
  print("URL Employee Project History: " +
      ServerUrl.baseUrl +
      ServerUrl.employeeProjectHistory);
  final res = await http.get(
      Uri.parse(ServerUrl.baseUrl + ServerUrl.employeeProjectHistory),
      headers: headers);
  print("STATUS CODE(Employee Project History): " + res.statusCode.toString());
  print("BODY(Employee Project History): " + res.body);
  return res;
}
