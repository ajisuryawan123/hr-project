import 'dart:convert';

import 'package:hr_project/config/global_config.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<Response> attendanceLog_API({
  required int pageNo,
  required String startDate,
  required String endDate,
  String? attendanceStatus,
  String? searchByName,
}) async {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  final SharedPreferences prefs = await _prefs;

  String token = prefs.getString("accessToken")!;

  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Accept': 'application/hal+json',
    'Content-Type': 'application/json',
  };

  final body = {
    "page": {
      "descending": true,
      "page_no": pageNo,
      "page_size": 10,
    },
    "start_date": startDate,
    "end_date": endDate
  };

  if (attendanceStatus is String) {
    body['attendance_status'] = attendanceStatus;
  }

  if (searchByName is String) {
    body['search_by_name'] = searchByName;
  }

  print("RAW Attendance Log: " + body.toString());
  print("URL Attendance Log: " + ServerUrl.baseUrl + ServerUrl.attendances_log);

  final res = await http.post(
    Uri.parse(ServerUrl.baseUrl + ServerUrl.attendances_log),
    headers: headers,
    body: jsonEncode(body),
  );

  print("STATUS CODE(Attendance Log): " + res.statusCode.toString());
  print("BODY(Attendance Log): " + res.body);

  return res;
}
