import 'dart:convert';

import 'package:hr_project/config/global_config.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<Response> attendanceLogExport_API({
  required String startDate,
  required String endDate,
  // required int pageSize,
}) async {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  final SharedPreferences prefs = await _prefs;

  String token = prefs.getString("accessToken")!;

  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Accept': 'application/hal+json',
    'Content-Type': 'application/json',
  };

  final body = {
    // "page": {
    //   "descending": true,
    //   "page_no": 0,
    //   "page_size": pageSize,
    // },
    "start_date": startDate,
    "end_date": endDate
  };

  print("RAW Attendance Log Export: " + body.toString());
  print("URL Attendance Log Export: " +
      ServerUrl.baseUrl +
      ServerUrl.attendances_logExport);

  final res = await http.post(
    Uri.parse(ServerUrl.baseUrl + ServerUrl.attendances_logExport),
    headers: headers,
    body: jsonEncode(body),
  );

  print("STATUS CODE(Attendance Log Export): " + res.statusCode.toString());
  // print("BODY(Attendance Log Export): " + res.body);

  return res;
}
