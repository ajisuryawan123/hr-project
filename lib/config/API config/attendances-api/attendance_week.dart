import 'package:hr_project/config/global_config.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

Future<Response> attendancesWeek_API({required String token}) async {
  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Accept': 'application/hal+json',
    'Content-Type': 'application/json',
  };
  print("URL Attendances Week: " +
      ServerUrl.baseUrl +
      ServerUrl.attendances_week);
  final res = await http.get(
      Uri.parse(ServerUrl.baseUrl + ServerUrl.attendances_week),
      headers: headers);
  print("STATUS CODE(Attendances Week): " + res.statusCode.toString());
  print("BODY(Attendances Week): " + res.body);
  return res;
}
