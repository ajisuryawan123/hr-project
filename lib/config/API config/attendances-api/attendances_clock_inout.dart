import 'dart:convert';

import 'package:hr_project/config/global_config.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:intl/intl.dart';

Future<Response> attendancesClockInOut_API(
    {required String type,
    required String dailyTask,
    required String workplace,
    required int projectId,
    required String clock_in,
    required String clock_out,
    required String attendance_id,
    required String token}) async {
  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Accept': 'application/hal+json',
    'Content-Type': 'application/json',
  };
  final body = {
    "page": {"descending": true, "page_no": 0, "page_size": 0, "order_by": ""},
    "attendance_id": attendance_id,
    "clock_in": clock_in,
    "clock_out": clock_out,
    "workplace": workplace,
    "daily_task": dailyTask,
    "project_id": projectId,
    "attendance_date":
        DateFormat('yyyy-MM-dd', 'en_US').format(DateTime.now()).toString(),
    "start_date":
        DateFormat('yyyy-MM-dd', 'en_US').format(DateTime.now()).toString(),
    "end_date":
        DateFormat('yyyy-MM-dd', 'en_US').format(DateTime.now()).toString(),
    "attendance_status": "string",
    "search_by_name": "string",
    "created_by": "string"
  };
  if (type == "Clock Out") {
    print("RAW attendancesClockOut: " + body.toString());
    print("URL attendancesClockOut: " +
        ServerUrl.baseUrl +
        ServerUrl.attendances_clockOut);

    final res = await http.post(
        Uri.parse(ServerUrl.baseUrl + ServerUrl.attendances_clockOut),
        headers: headers,
        body: jsonEncode(body));
    print("STATUS CODE(attendancesClockOut): " + res.statusCode.toString());
    print("BODY(attendancesClockIn): " + res.body);
    return res;
  } else {
    print("RAW attendancesClockIn: " + body.toString());
    print("URL attendancesClockIn: " +
        ServerUrl.baseUrl +
        ServerUrl.attendances_clockIn);

    final res = await http.post(
        Uri.parse(ServerUrl.baseUrl + ServerUrl.attendances_clockIn),
        headers: headers,
        body: jsonEncode(body));
    print("STATUS CODE(attendancesClockIn): " + res.statusCode.toString());
    print("BODY(attendancesClockIn): " + res.body);
    return res;
  }
}
