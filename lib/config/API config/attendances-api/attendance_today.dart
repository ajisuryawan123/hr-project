import 'package:hr_project/config/global_config.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

Future<Response> attendancesToday_API({required String token}) async {
  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Accept': 'application/hal+json',
    'Content-Type': 'application/json',
  };
  print("URL Attendances Today: " +
      ServerUrl.baseUrl +
      ServerUrl.attendances_today);
  final res = await http.get(
      Uri.parse(ServerUrl.baseUrl + ServerUrl.attendances_today),
      headers: headers);
  print("STATUS CODE(Attendances Today): " + res.statusCode.toString());
  print("BODY(Attendances Today): " + res.body);
  return res;
}
