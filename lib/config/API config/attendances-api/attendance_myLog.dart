import 'dart:convert';

import 'package:hr_project/config/global_config.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<Response> attendanceMyLog_API({
  required int pageNo,
  required String startDate,
  required String endDate,
}) async {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  final SharedPreferences prefs = await _prefs;

  String token = prefs.getString("accessToken")!;

  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Accept': 'application/hal+json',
    'Content-Type': 'application/json',
  };

  final body = {
    "page": {
      "descending": true,
      "page_no": pageNo,
      "page_size": 10,
    },
    "start_date": startDate,
    "end_date": endDate
  };

  print("RAW Attendance My Log: " + body.toString());
  print("URL Attendance My Log: " +
      ServerUrl.baseUrl +
      ServerUrl.attendances_myLog);

  final res = await http.post(
    Uri.parse(ServerUrl.baseUrl + ServerUrl.attendances_myLog),
    headers: headers,
    body: jsonEncode(body),
  );

  print("STATUS CODE(Attendance My Log): " + res.statusCode.toString());
  print("BODY(Attendance My Log): " + res.body);

  return res;
}
