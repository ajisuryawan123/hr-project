import 'dart:convert';

import 'package:hr_project/config/global_config.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

Future<Response> updatePasswordAPI(
    {required String old_password,
    required String new_password,
    required String token}) async {
  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Accept': 'application/hal+json',
    'Content-Type': 'application/json',
  };
  final body = {"old_password": old_password, "new_password": new_password};
  print("RAW Update Password: " + body.toString());
  print(
      "URL Update Password: " + ServerUrl.baseUrl + ServerUrl.update_password);
  final res = await http.post(
      Uri.parse(ServerUrl.baseUrl + ServerUrl.update_password),
      headers: headers,
      body: jsonEncode(body));
  print("STATUS CODE(Update Password): " + res.statusCode.toString());
  print("BODY(Update Password): " + res.body);
  return res;
}
