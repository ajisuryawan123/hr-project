import 'dart:convert';

import 'package:hr_project/config/global_config.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

Future<Response> changePasswordAPI({
  required String email,
  required String password,
}) async {
  Map<String, String> headers = {
    'Accept': 'application/hal+json',
    'Content-Type': 'application/json',
  };

  final body = {
    "email": email,
    "password": password,
  };

  print("RAW Change Password: " + body.toString());
  print(
      "URL Change Password: " + ServerUrl.baseUrl + ServerUrl.change_password);

  final res = await http.post(
    Uri.parse(ServerUrl.baseUrl + ServerUrl.change_password),
    headers: headers,
    body: jsonEncode(body),
  );

  print("STATUS CODE(Change Password): " + res.statusCode.toString());
  print("BODY(Change Password): " + res.body);

  return res;
}
