import 'dart:convert';

import 'package:hr_project/config/global_config.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

Future<Response> forgotPasswordAPI({
  required String email,
}) async {
  Map<String, String> headers = {
    'Accept': 'application/hal+json',
    'Content-Type': 'application/json',
  };

  final body = {
    "email": email,
  };

  print("RAW Forgot Password: " + body.toString());
  print(
      "URL Forgot Password: " + ServerUrl.baseUrl + ServerUrl.forgot_password);

  final res = await http.post(
    Uri.parse(ServerUrl.baseUrl + ServerUrl.forgot_password),
    headers: headers,
    body: jsonEncode(body),
  );

  print("STATUS CODE(Forgot Password): " + res.statusCode.toString());
  print("BODY(Forgot Password): " + res.body);

  return res;
}
