import 'dart:convert';

import 'package:hr_project/config/global_config.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

Future<Response> verifyOtpAPI({
  required String email,
  required String otp,
}) async {
  Map<String, String> headers = {
    'Accept': 'application/hal+json',
    'Content-Type': 'application/json',
  };

  final body = {
    "email": email,
    "otp_code": otp,
  };

  print("RAW Verify OTP: " + body.toString());
  print("URL Verify OTP: " + ServerUrl.baseUrl + ServerUrl.verify_otp);

  final res = await http.post(
    Uri.parse(ServerUrl.baseUrl + ServerUrl.verify_otp),
    headers: headers,
    body: jsonEncode(body),
  );

  print("STATUS CODE(Verify OTP): " + res.statusCode.toString());
  print("BODY(Verify OTP): " + res.body);

  return res;
}
