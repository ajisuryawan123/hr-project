import 'dart:convert';

import 'package:hr_project/config/global_config.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

Future<Response> authenticateAPI(
    {required String email, required String password}) async {
  Map<String, String> headers = {
    'Accept': 'application/hal+json',
    'Content-Type': 'application/json',
  };
  final body = {"email": email, "password": password};
  print("RAW Authenticate: " + body.toString());
  print("URL Authenticate: " + ServerUrl.baseUrl + ServerUrl.authenticate);
  final res = await http.post(
      Uri.parse(ServerUrl.baseUrl + ServerUrl.authenticate),
      headers: headers,
      body: jsonEncode(body));
  print("STATUS CODE(Authenticate): " + res.statusCode.toString());
  print("BODY(Authenticate): " + res.body);
  return res;
}
