import 'package:hr_project/config/global_config.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

Future<Response> leaveDetail_API(
    {required String id_leave, required String token}) async {
  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Accept': 'application/hal+json',
    'Content-Type': 'application/json',
  };
  print("URL Leave Detail: " +
      ServerUrl.baseUrl +
      ServerUrl.leave_detail +
      "/${id_leave}");
  final res = await http.get(
      Uri.parse(ServerUrl.baseUrl + ServerUrl.leave_detail + "/${id_leave}"),
      headers: headers);
  print("STATUS CODE(Leave Detail): " + res.statusCode.toString());
  print("BODY(Leave Detail): " + res.body);
  return res;
}
