import 'package:hr_project/config/global_config.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

Future<Response> leaveType_API({required String token}) async {
  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Accept': 'application/hal+json',
    'Content-Type': 'application/json',
  };
  print("URL Leave Type: " + ServerUrl.baseUrl + ServerUrl.leave_type);
  final res = await http.get(
      Uri.parse(ServerUrl.baseUrl + ServerUrl.leave_type),
      headers: headers);
  print("STATUS CODE(Leave Type): " + res.statusCode.toString());
  print("BODY(Leave Type): " + res.body);
  return res;
}
