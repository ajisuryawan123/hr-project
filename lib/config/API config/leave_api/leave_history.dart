import 'dart:convert';

import 'package:hr_project/config/global_config.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

Future<Response> leaveHistroy_API({required String token}) async {
  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Accept': 'application/hal+json',
    'Content-Type': 'application/json',
  };
  final body = {
    "page": {"page_no": 0, "page_size": 30}
  };
  print("RAW Leave History: " + body.toString());
  print("URL Leave History: " + ServerUrl.baseUrl + ServerUrl.leave_history);
  final res = await http.post(
      Uri.parse(ServerUrl.baseUrl + ServerUrl.leave_history),
      headers: headers,
      body: jsonEncode(body));
  print("STATUS CODE(Leave History): " + res.statusCode.toString());
  print("BODY(Leave History): " + res.body);
  return res;
}
