import 'dart:convert';

import 'package:hr_project/config/global_config.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

Future<Response> leaveAssesor1_2API(
    {required String type, required String token}) async {
  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Accept': 'application/hal+json',
    'Content-Type': 'application/json',
  };
  final body = {
    "search_by_name": "",
    "page": {"page_no": 0, "page_size": 30}
  };
  if (type == "Assesor1") {
    print("RAW Assesor1: " + body.toString());
    print("URL Assesor1: " + ServerUrl.baseUrl + ServerUrl.leave_assesor1);

    final res = await http.post(
        Uri.parse(ServerUrl.baseUrl + ServerUrl.leave_assesor1),
        headers: headers,
        body: jsonEncode(body));
    print("STATUS CODE(Assesor1): " + res.statusCode.toString());
    print("BODY(Assesor1): " + res.body);
    return res;
  } else {
    print("RAW Assesor2: " + body.toString());
    print("URL Assesor2: " + ServerUrl.baseUrl + ServerUrl.leave_assesor2);

    final res = await http.post(
        Uri.parse(ServerUrl.baseUrl + ServerUrl.leave_assesor2),
        headers: headers,
        body: jsonEncode(body));
    print("STATUS CODE(Assesor2): " + res.statusCode.toString());
    print("BODY(Assesor2): " + res.body);
    return res;
  }
}
