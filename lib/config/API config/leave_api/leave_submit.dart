import 'dart:io';

import 'package:hr_project/config/global_config.dart';
import 'package:path/path.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:mime/mime.dart';
import 'package:http_parser/http_parser.dart';

Future<Response> leaveSubmit_API(
    {required File file,
    required String start_date,
    required String end_date,
    required String leave_type_id,
    required String reason,
    required String first_assesor,
    required String second_assesor,
    required String type,
    required String token}) async {
  var mimeType = lookupMimeType(file.path);
  var bytes = await File.fromUri(Uri.parse(file.path)).readAsBytes();
  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Accept': 'application/hal+json',
    'Content-Type': 'application/json',
  };
  final body = {
    "start_date": start_date,
    "end_date": end_date,
    "leave_type_id": leave_type_id,
    "reason": reason,
    "first_assesor_employee_id": first_assesor,
    "second_assesor_employee_id": second_assesor,
    "type": type
  };
  print("RAW Leave Submit: " + body.toString());
  print("URL Leave Submit: " + ServerUrl.baseUrl + ServerUrl.leave_submit);
  http.MultipartRequest res = await http.MultipartRequest(
      "POST", Uri.parse(ServerUrl.baseUrl + ServerUrl.leave_submit));
  http.MultipartFile multipartFile = await http.MultipartFile.fromBytes(
      'attachment', bytes,
      filename: basename(file.path),
      contentType: MediaType.parse(mimeType.toString()));
  res.fields.addAll(body);
  res.headers.addAll(headers);
  res.files.add(multipartFile);
  var streamedResponse = await res.send();
  var response = await http.Response.fromStream(streamedResponse);
  print("STATUS CODE(Leave Submit): " + response.statusCode.toString());
  print("BODY(Leave Submit): " + response.body);
  return response;
}
