class ServerUrl {
  ///baseUrl
  static String baseUrl = "https://api.appfuxion.id/v1/timesheet";

  ///Authenticate
  static String authenticate = "/auth/authenticate";
  static String forgot_password = "/auth/request-forgot-password";
  static String verify_otp = "/auth/verify-otp";
  static String change_password = "/auth/change-password";
  static String update_password = "/auth/update-password";

  ///Attendances
  static String attendances_clockIn = "/attendances/clock-in";
  static String attendances_clockOut = "/attendances/clock-out";
  static String attendances_today = "/attendances/today";
  static String attendances_week = "/attendances/week";
  static String attendances_log = "/attendances/log";
  static String attendances_logExport = "/attendances/log/export";
  static String attendances_myLog = "/attendances/my-log";
  static String attendances_myLogExport = "/attendances/my-log/export";

  ///Leave
  static String leave_submit = "/leave/submit";
  static String leave_type = "/leave/type";
  static String leave_assesor1 = "/leave/assesor_1";
  static String leave_assesor2 = "/leave/assesor_2";
  static String leave_history = "/leave/history";
  static String leave_detail = "/leave/details";

  ///Employee Project Controller
  static String employeeProjectHistory = "/employee-project/my-history";
  static String employeeCurrentProject = "/employee-project/my-current-project";
  static String employeeChangeProject = "/employee-project/change-my-project";
}
