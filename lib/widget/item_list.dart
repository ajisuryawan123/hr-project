import 'package:flutter/material.dart';
import 'package:hr_project/styling_theme.dart';

class ItemList extends StatefulWidget {
  final Image image;
  final String type;
  final String title;
  final String subtitle;
  final Widget detail;
  const ItemList(
      {Key? key,
      required this.image,
      required this.type,
      required this.title,
      required this.subtitle,
      required this.detail})
      : super(key: key);

  @override
  State<ItemList> createState() => _ItemListState();
}

class _ItemListState extends State<ItemList> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => widget.detail));
      },
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
        child: Padding(
          padding: EdgeInsets.all(12),
          child: Row(
            children: [
              ClipRRect(
                  borderRadius: BorderRadius.circular(12),
                  child: Container(
                      width: CustomTheme.screenWidth! * 0.25,
                      child: widget.image)),
              SizedBox(width: CustomTheme.screenWidth! * 0.03),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    top(),
                    SizedBox(height: CustomTheme.screenHeight! * 0.01),
                    bottom(),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  top() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          padding: EdgeInsets.all(5),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              color: Color(0xFFFFEEE2)),
          child: Text(
            "All Countries",
            style: CustomTheme.caption(context,
                color: Color(0xFFEB7D2E), fontWeight: CustomTheme.medium),
          ),
        ),
        Icon(Icons.arrow_forward_ios_rounded,
            color: Color(0xFFE0E0E0), size: 12)
      ],
    );
  }

  bottom() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.title,
          overflow: TextOverflow.ellipsis,
          style: CustomTheme.body1(context,
              color: Colors.black, fontWeight: CustomTheme.semibold),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.01),
        Text(
          widget.subtitle,
          maxLines: 2,
          overflow: TextOverflow.ellipsis,
          style: CustomTheme.body2(context,
              color: Colors.black, fontWeight: CustomTheme.medium),
        ),
      ],
    );
  }
}
