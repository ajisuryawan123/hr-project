import 'package:flutter/material.dart';
import 'package:hr_project/model/attendances-model/attendances_log.dart';
import 'package:hr_project/styling_theme.dart';
import 'package:moment_dart/moment_dart.dart';

class AttendDetailItem extends StatefulWidget {
  final Content element;
  const AttendDetailItem({Key? key, required this.element}) : super(key: key);

  @override
  State<AttendDetailItem> createState() => _AttendDetailItemState();
}

class _AttendDetailItemState extends State<AttendDetailItem> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (widget.element.dateGroupLabel is String)
          dateGroup(widget.element.dateGroupLabel ?? ''),
        Container(
          margin: EdgeInsets.fromLTRB(20, 0, 20, 10),
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            border: Border.all(color: Color(0xFFD8E4EE)),
            borderRadius: BorderRadius.all(Radius.circular(5)),
            color: Colors.white,
          ),
          child: Row(
            children: [
              // TODO: employee profile picture
              IconAsset.avatar(1.25),
              SizedBox(width: CustomTheme.screenWidth! * 0.03),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(widget.element.employee.fullName,
                            style: CustomTheme.body1(context,
                                color: Colors.black,
                                fontWeight: CustomTheme.bold)),
                        widget.element.attendanceStatus ==
                                AttendanceStatus.NORECORD
                            ? Container(
                                padding: EdgeInsets.all(8),
                                child: Text(' ',
                                    style: CustomTheme.overline(context,
                                        color: Colors.black,
                                        fontWeight: CustomTheme.regular)),
                              )
                            : Container(
                                padding: EdgeInsets.all(8),
                                decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(8)),
                                    color: Color(0xFFEEEFF1)),
                                child: Text(
                                    widget.element.attendanceStatus ==
                                            AttendanceStatus.ONTIME
                                        ? 'On Time'
                                        : widget.element.attendanceStatus ==
                                                AttendanceStatus.LATE
                                            ? 'Late'
                                            : 'Leave',
                                    style: CustomTheme.overline(context,
                                        color: Colors.black,
                                        fontWeight: CustomTheme.regular)),
                              )
                      ],
                    ),
                    clockInOut(widget.element.clockIn, widget.element.clockOut),
                  ],
                ),
              )
            ],
          ),
        ),
      ],
    );
    // Container(
    //   margin: EdgeInsets.all(8),
    //   padding: EdgeInsets.all(12),
    //   decoration: BoxDecoration(
    //       borderRadius: BorderRadius.all(Radius.circular(8)),
    //       color: Colors.white),
    //   child: Row(
    //     children: [
    //       Container(
    //           decoration: BoxDecoration(shape: BoxShape.circle),
    //           child: widget.element.avatar),
    //       SizedBox(width: CustomTheme.screenWidth! * 0.03),
    //       Expanded(
    //         child: Column(
    //           crossAxisAlignment: CrossAxisAlignment.start,
    //           children: [
    //             Text(widget.element.name,
    //                 style: CustomTheme.body1(context,
    //                     color: Colors.black, fontWeight: CustomTheme.semibold)),
    //             SizedBox(height: CustomTheme.screenHeight! * 0.02),
    //             Row(
    //               children: [
    //                 Expanded(
    //                   flex: 3,
    //                   child: Row(
    //                     children: [
    //                       IconAsset.door_enter_green(),
    //                       SizedBox(width: CustomTheme.screenWidth! * 0.02),
    //                       Text(
    //                         widget.element.clock_in,
    //                         style: CustomTheme.body2(
    //                           context,
    //                           color: Colors.black,
    //                           fontWeight: CustomTheme.semibold,
    //                         ),
    //                       ),
    //                     ],
    //                   ),
    //                 ),
    //                 Expanded(
    //                   flex: 2,
    //                   child: Row(
    //                     children: [
    //                       IconAsset.door_exit_red(),
    //                       SizedBox(width: CustomTheme.screenWidth! * 0.02),
    //                       Text(
    //                         widget.element.clock_out,
    //                         style: CustomTheme.body2(
    //                           context,
    //                           color: Colors.black,
    //                           fontWeight: CustomTheme.semibold,
    //                         ),
    //                       ),
    //                     ],
    //                   ),
    //                 ),
    //               ],
    //             )
    //           ],
    //         ),
    //       )
    //     ],
    //   ),
    // );
  }

  dateGroup(String dateGroupLabel) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 12),
      margin: EdgeInsets.only(top: 10, bottom: 20),
      width: CustomTheme.screenWidth,
      decoration: BoxDecoration(color: Color(0xFFE7F1FA)),
      child: Text(
        dateGroupLabel,
        style: CustomTheme.body1(context,
            color: Color(0xFF333333), fontWeight: CustomTheme.bold),
      ),
    );
  }

  clockInOut(DateTime? clockIn, DateTime? clockOut) {
    return Row(
      children: [
        Expanded(
          flex: 3,
          child: Row(
            children: [
              IconAsset.door_enter_green(),
              SizedBox(width: CustomTheme.screenWidth! * 0.02),
              Text(
                clockIn is DateTime ? Moment(clockIn).format('hh:mm A') : '-',
                style: CustomTheme.body2(
                  context,
                  color: Colors.black,
                  fontWeight: CustomTheme.semibold,
                ),
              ),
            ],
          ),
        ),
        Expanded(
          flex: 2,
          child: Row(
            children: [
              IconAsset.door_exit_red(),
              SizedBox(width: CustomTheme.screenWidth! * 0.02),
              Text(
                clockOut is DateTime ? Moment(clockOut).format('hh:mm A') : '-',
                style: CustomTheme.body2(
                  context,
                  color: Colors.black,
                  fontWeight: CustomTheme.semibold,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
