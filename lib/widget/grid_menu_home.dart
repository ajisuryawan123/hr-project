import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hr_project/page/blank_page.dart';
import 'package:hr_project/styling_theme.dart';

class GridMenuHome extends StatefulWidget {
  final SvgPicture icon;
  final String title;
  final Widget? directPage;
  const GridMenuHome(
      {Key? key,
      required this.icon,
      required this.title,
      required this.directPage})
      : super(key: key);

  @override
  State<GridMenuHome> createState() => _GridMenuHomeState();
}

class _GridMenuHomeState extends State<GridMenuHome> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        InkWell(
          onTap: widget.directPage == null
              ? null
              : () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => widget.directPage!));
                },
          child: Container(
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color(0xFFEB7D2E),
                  Colors.white,
                ],
              ),
            ),
            child: widget.icon,
          ),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.01),
        Text(
          widget.title,
          textAlign: TextAlign.center,
          style: CustomTheme.body2(context,
              color: Colors.black, fontWeight: CustomTheme.medium),
        ),
      ],
    );
  }
}
