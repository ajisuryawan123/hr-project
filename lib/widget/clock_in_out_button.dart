import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hr_project/styling_theme.dart';

class CustomHeaderButton extends StatefulWidget {
  final Color color;
  final Color colorText;
  final SvgPicture icon;
  final String text;
  final double customWidth;
  final Function()? onPress;
  final bool isCenter;
  const CustomHeaderButton(
      {Key? key,
      required this.color,
      required this.icon,
      required this.text,
      required this.colorText,
      required this.onPress,
      required this.customWidth,
      required this.isCenter})
      : super(key: key);

  @override
  State<CustomHeaderButton> createState() => _CustomHeaderButtonState();
}

class _CustomHeaderButtonState extends State<CustomHeaderButton> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.onPress,
      child: Container(
        width: widget.customWidth,
        child: Card(
          elevation: 2,
          color: widget.color,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(5))),
          child: Padding(
            padding: EdgeInsets.all(12),
            child: widget.isCenter
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      widget.icon,
                      SizedBox(width: CustomTheme.screenWidth! * 0.03),
                      Text(
                        widget.text,
                        style: CustomTheme.subtitle(context,
                            color: widget.colorText,
                            fontWeight: CustomTheme.semibold),
                      ),
                    ],
                  )
                : Row(
                    children: [
                      widget.icon,
                      SizedBox(width: CustomTheme.screenWidth! * 0.03),
                      Text(
                        widget.text,
                        style: CustomTheme.subtitle(context,
                            color: widget.colorText,
                            fontWeight: CustomTheme.semibold),
                      ),
                    ],
                  ),
          ),
        ),
      ),
    );
  }
}
