import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

import '../page/detail_list.dart';
import '../styling_theme.dart';

class MessageListAll extends StatefulWidget {
  const MessageListAll({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MessageList();
}

class _MessageList extends State<MessageListAll> {
  List<RemoteMessage> _messages = [];

  @override
  void initState() {
    super.initState();
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      setState(() {
        _messages = [..._messages, message];
      });
    });
  }

  noList() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.all(12),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color(0xFFD8E4EE),
                Color(0xFFD8E4EE).withOpacity(0.0),
              ],
            ),
          ),
          child: IconAsset.notification(),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.03),
        Text(
          "No updates right now!",
          style: CustomTheme.body1(context,
              color: Color(0xFF525F71), fontWeight: CustomTheme.semibold),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.01),
        Text(
          "Enjoy a well-deserved break from the\nconstant stream of notifications.",
          textAlign: TextAlign.center,
          style: CustomTheme.body1(context,
              color: Color(0xFFB8CADB), fontWeight: CustomTheme.medium),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    if (_messages.isEmpty) {
      return Center(child: noList());
    }

    return ListView.builder(
        shrinkWrap: true,
        itemCount: _messages.length,
        itemBuilder: (context, index) {
          RemoteMessage message = _messages[index];

          return InkWell(
            onTap: () {
              Navigator.pushNamed(context, '/message',
                  arguments: MessageArguments(message, false));
            },
            child: Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12)),
              child: Padding(
                padding: EdgeInsets.all(12),
                child: Row(
                  children: [
                    ClipRRect(
                        borderRadius: BorderRadius.circular(12),
                        child: Container(
                            width: CustomTheme.screenWidth! * 0.25,
                            child: Image.network(
                              message.notification!.android!.imageUrl ??
                                  'https://img.icons8.com/bubbles/2x/appointment-reminders.png',
                              fit: BoxFit.fill,
                            ))),
                    SizedBox(width: CustomTheme.screenWidth! * 0.03),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          top(message),
                          SizedBox(height: CustomTheme.screenHeight! * 0.01),
                          bottom(message),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }

  top(RemoteMessage message) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          padding: EdgeInsets.all(5),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              color: Color(0xFFFFEEE2)),
          child: Text(
            "All Countries",
            style: CustomTheme.caption(context,
                color: Color(0xFFEB7D2E), fontWeight: CustomTheme.medium),
          ),
        ),
        Icon(Icons.arrow_forward_ios_rounded,
            color: Color(0xFFE0E0E0), size: 12)
      ],
    );
  }

  bottom(RemoteMessage message) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          message.notification!.title!,
          overflow: TextOverflow.ellipsis,
          style: CustomTheme.body1(context,
              color: Colors.black, fontWeight: CustomTheme.semibold),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.01),
        Text(
          message.notification!.body!,
          maxLines: 2,
          overflow: TextOverflow.ellipsis,
          style: CustomTheme.body2(context,
              color: Colors.black, fontWeight: CustomTheme.medium),
        ),
      ],
    );
  }
}
