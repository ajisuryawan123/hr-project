import 'package:flutter/material.dart';
import 'package:hr_project/styling_theme.dart';

class StatHeader extends StatefulWidget {
  final bool isSecond;
  final String pointValue;
  final String titlePoint;
  final String percentValue;
  final String titleValue;
  const StatHeader(
      {Key? key,
      required this.isSecond,
      required this.pointValue,
      required this.titlePoint,
      required this.percentValue,
      required this.titleValue})
      : super(key: key);

  @override
  State<StatHeader> createState() => _StatHeaderState();
}

class _StatHeaderState extends State<StatHeader> {
  @override
  Widget build(BuildContext context) {
    return widget.isSecond
        ? Container(
            width: CustomTheme.screenWidth! * 0.43,
            padding: EdgeInsets.all(12),
            decoration: BoxDecoration(
              border: Border(
                  left: BorderSide(width: 0.5, color: Color(0xFFB8CADB))),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  widget.pointValue,
                  style: CustomTheme.headline4(context,
                      color: Colors.black, fontWeight: CustomTheme.bold),
                ),
                SizedBox(height: CustomTheme.screenHeight! * 0.01),
                Text(
                  widget.titlePoint,
                  style: CustomTheme.subtitle(context,
                      color: Color(0xFF757575), fontWeight: CustomTheme.medium),
                ),
              ],
            ),
          )
        : Container(
            width: CustomTheme.screenWidth! * 0.43,
            padding: EdgeInsets.all(12),
            decoration: BoxDecoration(
              border: Border(
                  right: BorderSide(width: 0.5, color: Color(0xFFB8CADB))),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  widget.percentValue,
                  style: CustomTheme.headline4(context,
                      color: Colors.black, fontWeight: CustomTheme.bold),
                ),
                SizedBox(height: CustomTheme.screenHeight! * 0.01),
                Text(
                  widget.titleValue,
                  style: CustomTheme.subtitle(context,
                      color: Color(0xFF757575), fontWeight: CustomTheme.medium),
                ),
              ],
            ),
          );
  }
}
