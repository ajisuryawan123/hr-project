import 'package:flutter/material.dart';
import 'package:hr_project/dummy_data.dart';
import 'package:hr_project/styling_theme.dart';

import '../page/log/attendance_detail.dart';

class LegendLog extends StatefulWidget {
  final PieData data;
  final DateTime startDate, endDate;
  const LegendLog(
      {Key? key,
      required this.data,
      required this.startDate,
      required this.endDate})
      : super(key: key);

  @override
  State<LegendLog> createState() => _LegendLogState();
}

class _LegendLogState extends State<LegendLog> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        int idx;
        if (widget.data.name == "On Time") {
          idx = 0;
        } else if (widget.data.name == "Late") {
          idx = 1;
        } else {
          idx = 2;
        }
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => AttendDetailPage(
                      initialIndexTab: idx,
                      startDate: widget.startDate,
                      endDate: widget.endDate,
                    )));
      },
      child: Row(
        children: [
          CircleAvatar(
            radius: 4,
            backgroundColor: widget.data.color,
          ),
          SizedBox(width: CustomTheme.screenWidth! * 0.02),
          SizedBox(
              width: CustomTheme.screenWidth! * 0.18,
              child: Text(widget.data.name,
                  style: CustomTheme.body2(context,
                      color: Colors.black, fontWeight: CustomTheme.medium))),
          SizedBox(
              width: CustomTheme.screenWidth! * 0.01,
              child: Text(":",
                  style: CustomTheme.body2(context,
                      color: Colors.black, fontWeight: CustomTheme.medium))),
          SizedBox(
              width: CustomTheme.screenWidth! * 0.08,
              child: Text(widget.data.value.toString(),
                  textAlign: TextAlign.end,
                  style: CustomTheme.body2(context,
                      color: Colors.black, fontWeight: CustomTheme.semibold))),
          SizedBox(
              width: CustomTheme.screenWidth! * 0.11,
              child: Text("(" + widget.data.percent.toInt().toString() + "%)",
                  textAlign: TextAlign.end,
                  style: CustomTheme.body2(context,
                      color: Color(0xFFB8CADB),
                      fontWeight: CustomTheme.semibold))),
          SizedBox(width: CustomTheme.screenWidth! * 0.01),
          Icon(Icons.keyboard_arrow_down_outlined, color: Color(0xFFB8CADB))
        ],
      ),
    );
  }
}
