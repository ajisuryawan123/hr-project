import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hr_project/page/blank_page.dart';
import 'package:hr_project/styling_theme.dart';

class ProfileItem extends StatefulWidget {
  final SvgPicture icon;
  final String title;
  final Widget? directPage;
  const ProfileItem(
      {Key? key, required this.icon, required this.title, this.directPage})
      : super(key: key);

  @override
  State<ProfileItem> createState() => _ProfileItemState();
}

class _ProfileItemState extends State<ProfileItem> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (context) {
          if (widget.directPage == null) {
            return BlankPage();
          } else {
            return widget.directPage!;
          }
        }));
      },
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                widget.icon,
                SizedBox(width: CustomTheme.screenWidth! * 0.03),
                Text(widget.title,
                    style: CustomTheme.body1(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
              ],
            ),
            Icon(Icons.arrow_forward_ios_rounded,
                color: Color(0xFFE0E0E0), size: 16)
          ],
        ),
      ),
    );
  }
}
