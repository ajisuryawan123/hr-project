import 'package:flutter/material.dart';
import 'package:hr_project/styling_theme.dart';

class CustomAppbar extends StatefulWidget implements PreferredSizeWidget {
  final String title;
  const CustomAppbar({Key? key, required this.title}) : super(key: key);

  @override
  State<CustomAppbar> createState() => _CustomAppbarState();

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}

class _CustomAppbarState extends State<CustomAppbar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: true,
      iconTheme: IconThemeData(color: Color(0xFF2F80ED)),
      backgroundColor: Color(0xFFF2C94C),
      title: Text(
        widget.title,
        style: CustomTheme.subtitle(context,
            color: Colors.black, fontWeight: CustomTheme.semibold),
      ),
    );
  }
}
