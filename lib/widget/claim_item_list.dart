import 'package:flutter/material.dart';
import 'package:hr_project/dummy_data.dart';
import 'package:hr_project/page/leave/leave_detail.dart';
import 'package:hr_project/styling_theme.dart';
import 'package:intl/intl.dart';

import '../model/leave-model/leave_history.dart';
import '../page/leave/claim/claim_detail.dart';

class ClaimItem extends StatefulWidget {
  final DummyListClaim item;
  const ClaimItem({
    Key? key,
    required this.item,
  }) : super(key: key);

  @override
  State<ClaimItem> createState() => _ClaimItemState();
}

class _ClaimItemState extends State<ClaimItem> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => DetailClaim()));
      },
      child: Container(
        padding: EdgeInsets.all(12),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(8)),
            color: Colors.white),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  padding: EdgeInsets.all(8),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      color: widget.item.status == "Waiting Response"
                          ? Color(0xFFFFF4ED)
                          : widget.item.status == "Declined"
                              ? Color(0xFFFFEEEC)
                              : Color(0xFFE9F7EF)),
                  child: Text(
                      widget.item.status == "Waiting Response"
                          ? "Waiting Response"
                          : widget.item.status == "Declined"
                              ? "Declined"
                              : "Approved",
                      style: CustomTheme.caption(context,
                          color: widget.item.status == "Waiting Response"
                              ? Color(0xFFEB7D2E)
                              : widget.item.status == "Declined"
                                  ? Color(0xFFFF5245)
                                  : Color(0xFF27AE60),
                          fontWeight: CustomTheme.semibold)),
                ),
                // IconButton(
                //     icon: Icon(Icons.more_vert, color: Color(0xFF828282)),
                //     onPressed: () {})
              ],
            ),
            SizedBox(height: CustomTheme.screenHeight! * 0.01),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    IconAsset.calender_grey(),
                    SizedBox(width: CustomTheme.screenWidth! * 0.02),
                    Text(widget.item.startDate,
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold)),
                  ],
                ),
                Row(
                  children: [
                    IconAsset.receipt_grey(),
                    SizedBox(width: CustomTheme.screenWidth! * 0.02),
                    Text(widget.item.price,
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold)),
                  ],
                ),
              ],
            ),
            SizedBox(height: CustomTheme.screenHeight! * 0.01),
            Container(
              width: CustomTheme.screenWidth! * 1,
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 15),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  border: Border.all(width: 1, color: Color(0xFFD8E4EE)),
                  color: Color(0xFFF3F6F9)),
              child: RichText(
                textAlign: TextAlign.left,
                text: TextSpan(
                  children: [
                    TextSpan(
                        text: widget.item.title,
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold)),
                    TextSpan(
                        text: widget.item.contain,
                        style: CustomTheme.body1(context,
                            color: Color(0xFF828282),
                            fontWeight: CustomTheme.medium)),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
