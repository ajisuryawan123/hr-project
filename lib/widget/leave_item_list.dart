import 'package:flutter/material.dart';
import 'package:hr_project/page/leave/leave_detail.dart';
import 'package:hr_project/styling_theme.dart';
import 'package:intl/intl.dart';

import '../model/leave-model/leave_history.dart';

class LeaveItem extends StatefulWidget {
  final Content item;
  const LeaveItem({
    Key? key,
    required this.item,
  }) : super(key: key);

  @override
  State<LeaveItem> createState() => _LeaveItemState();
}

class _LeaveItemState extends State<LeaveItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8)),
          color: Colors.white),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                padding: EdgeInsets.all(8),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    color: widget.item.status == "WAITING"
                        ? Color(0xFFFFF4ED)
                        : widget.item.status == "DECLINED"
                            ? Color(0xFFFFEEEC)
                            : Color(0xFFE9F7EF)),
                child: Text(
                    widget.item.status == "WAITING"
                        ? "Waiting Response"
                        : widget.item.status == "DECLINED"
                            ? "Declined"
                            : "Approved",
                    style: CustomTheme.caption(context,
                        color: widget.item.status == "WAITING"
                            ? Color(0xFFEB7D2E)
                            : widget.item.status == "DECLINED"
                                ? Color(0xFFFF5245)
                                : Color(0xFF27AE60),
                        fontWeight: CustomTheme.semibold)),
              ),
              IconButton(
                  icon: Icon(Icons.more_vert, color: Color(0xFF828282)),
                  onPressed: () {
                    showOption();
                  })
            ],
          ),
          SizedBox(height: CustomTheme.screenHeight! * 0.01),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  IconAsset.calender_black(),
                  SizedBox(width: CustomTheme.screenWidth! * 0.02),
                  Text(
                      DateFormat('dd MMM yy', 'en_US')
                          .format(widget.item.startDate!),
                      style: CustomTheme.body1(context,
                          color: Colors.black,
                          fontWeight: CustomTheme.semibold)),
                ],
              ),
              Container(
                padding: EdgeInsets.all(8),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    color: Color(0xFFF2F4F9)),
                child: Text("${widget.item.daysTotal} days",
                    style: CustomTheme.caption(context,
                        color: Color(0xFF5981EA),
                        fontWeight: CustomTheme.medium)),
              ),
              Row(
                children: [
                  IconAsset.calender_black(),
                  SizedBox(width: CustomTheme.screenWidth! * 0.02),
                  Text(
                      DateFormat('dd MMM yy', 'en_US')
                          .format(widget.item.endDate!),
                      style: CustomTheme.body1(context,
                          color: Colors.black,
                          fontWeight: CustomTheme.semibold)),
                ],
              ),
            ],
          ),
          SizedBox(height: CustomTheme.screenHeight! * 0.01),
          Container(
            width: CustomTheme.screenWidth! * 1,
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 15),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(8)),
                border: Border.all(width: 1, color: Color(0xFFD8E4EE)),
                color: Color(0xFFF3F6F9)),
            child: RichText(
              textAlign: TextAlign.left,
              text: TextSpan(
                children: [
                  TextSpan(
                      text: widget.item.type == "PAID"
                          ? "Paid Leave\n\n"
                          : "Unpaid Leave\n\n",
                      style: CustomTheme.body1(context,
                          color: Colors.black,
                          fontWeight: CustomTheme.semibold)),
                  TextSpan(
                      text: widget.item.reason,
                      style: CustomTheme.body1(context,
                          color: Color(0xFF828282),
                          fontWeight: CustomTheme.medium)),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  showOption() {
    return showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(15))),
        builder: (BuildContext context) {
          return Padding(
            padding: EdgeInsets.only(top: 15),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text('Leave Options',
                    style: CustomTheme.subtitle(context,
                        color: Colors.black, fontWeight: CustomTheme.medium)),
                SizedBox(height: CustomTheme.screenHeight! * 0.02),
                ListTile(
                  title: Text('View Request',
                      style: CustomTheme.subtitle(context,
                          color: Colors.black, fontWeight: CustomTheme.medium)),
                  trailing: Icon(Icons.arrow_forward_ios_rounded,
                      size: 17, color: Color(0xFFB8CADB)),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => LeaveDetailPage(
                                id_leave: widget.item.leaveId.toString())));
                  },
                ),
                Divider(
                    color: Color(0xFFD8E4EE),
                    height: 0,
                    thickness: 1,
                    endIndent: 20,
                    indent: 20),
                ListTile(
                  title: Text('Resubmit Request',
                      style: CustomTheme.subtitle(context,
                          color: Color(0xFFE0E0E0),
                          fontWeight: CustomTheme.medium)),
                  trailing: Icon(Icons.arrow_forward_ios_rounded,
                      size: 17, color: Color(0xFFE0E0E0)),
                  onTap: () {},
                ),
              ],
            ),
          );
        });
  }
}
