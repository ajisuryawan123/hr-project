import 'package:flutter/material.dart';
import 'package:hr_project/styling_theme.dart';

class PassTextField extends StatefulWidget {
  final TextEditingController controller;
  bool passVisible;
  final String? Function(String?)? validateCheck;
  final String title;
  PassTextField(
      {Key? key,
      required this.controller,
      required this.passVisible,
      required this.title,
      this.validateCheck})
      : super(key: key);

  @override
  State<PassTextField> createState() => _PassTextFieldState();
}

class _PassTextFieldState extends State<PassTextField> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: CustomTheme.screenHeight! * 0.03),
        Text(
          widget.title,
          style: CustomTheme.body2(context,
              color: Colors.black, fontWeight: CustomTheme.semibold),
        ),
        SizedBox(height: CustomTheme.screenHeight! * 0.01),
        Container(
          height: CustomTheme.screenHeight! * 0.1,
          child: TextFormField(
            controller: widget.controller,
            validator: widget.validateCheck,
            obscureText: !widget.passVisible,
            style: CustomTheme.body2(context,
                color: Colors.black, fontWeight: CustomTheme.medium),
            decoration: InputDecoration(
              helperText: "",
              floatingLabelBehavior: FloatingLabelBehavior.never,
              contentPadding:
                  EdgeInsets.symmetric(vertical: 30, horizontal: 10),
              filled: true,
              fillColor: Colors.white,
              suffixIcon: IconButton(
                icon: Icon(
                  widget.passVisible ? Icons.visibility : Icons.visibility_off,
                  color: Color(0xFF828282),
                ),
                onPressed: () {
                  setState(() {
                    widget.passVisible = !widget.passVisible;
                  });
                },
              ),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Color(0xFFBDBDBD))),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Color(0xFFBDBDBD))),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Color(0xFFBDBDBD))),
            ),
          ),
        ),
      ],
    );
  }
}
