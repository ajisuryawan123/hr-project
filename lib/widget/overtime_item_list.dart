import 'package:flutter/material.dart';
import 'package:hr_project/dummy_data.dart';
import 'package:hr_project/page/leave/leave_detail.dart';
import 'package:hr_project/page/leave/claim/claim_detail.dart';
import 'package:hr_project/styling_theme.dart';
import 'package:intl/intl.dart';

import '../model/leave-model/leave_history.dart';
import '../page/leave/overtime/overtime_detail.dart';

class OvertimeItem extends StatefulWidget {
  final DummyListOvertime item;
  const OvertimeItem({
    Key? key,
    required this.item,
  }) : super(key: key);

  @override
  State<OvertimeItem> createState() => _OvertimeItemState();
}

class _OvertimeItemState extends State<OvertimeItem> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => DetailOvertime()));
      },
      child: Container(
        margin: EdgeInsets.all(8),
        padding: EdgeInsets.fromLTRB(12, 0, 12, 12),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(8)),
            color: Colors.white),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    IconAsset.calender_grey(),
                    SizedBox(width: CustomTheme.screenWidth! * 0.02),
                    Text(widget.item.startDate,
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold)),
                  ],
                ),
                // IconButton(
                //     icon: Icon(Icons.more_vert, color: Color(0xFF828282)),
                //     onPressed: () {})
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    IconAsset.time_grey(),
                    SizedBox(width: CustomTheme.screenWidth! * 0.02),
                    Text(widget.item.startTime,
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold)),
                  ],
                ),
                Row(
                  children: [
                    IconAsset.time_grey(),
                    SizedBox(width: CustomTheme.screenWidth! * 0.02),
                    Text(widget.item.endTime,
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold)),
                  ],
                ),
              ],
            ),
            SizedBox(height: CustomTheme.screenHeight! * 0.01),
            Container(
              width: CustomTheme.screenWidth! * 1,
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 15),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  border: Border.all(width: 1, color: Color(0xFFD8E4EE)),
                  color: Color(0xFFF3F6F9)),
              child: RichText(
                textAlign: TextAlign.left,
                text: TextSpan(
                  children: [
                    TextSpan(
                        text: widget.item.title,
                        style: CustomTheme.body1(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold)),
                    TextSpan(
                        text: widget.item.contain,
                        style: CustomTheme.body1(context,
                            color: Color(0xFF828282),
                            fontWeight: CustomTheme.medium)),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
