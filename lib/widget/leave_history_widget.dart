import 'package:flutter/material.dart';
import 'package:hr_project/dummy_data.dart';
import 'package:hr_project/page/approval_management/leave_management/leave_detail_history.dart';
import 'package:hr_project/styling_theme.dart';

class LeaveHistoryWidget extends StatefulWidget {
  final DummyManagePendingHistory item;
  const LeaveHistoryWidget({Key? key, required this.item}) : super(key: key);

  @override
  State<LeaveHistoryWidget> createState() => _LeaveHistoryWidgetState();
}

class _LeaveHistoryWidgetState extends State<LeaveHistoryWidget> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => LeaveDetailHistory()));
      },
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
        elevation: 8,
        child: Padding(
          padding: EdgeInsets.all(12),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Container(
                      decoration: BoxDecoration(shape: BoxShape.circle),
                      child: widget.item.avatar),
                  SizedBox(width: CustomTheme.screenWidth! * 0.03),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(widget.item.name,
                          style: CustomTheme.body1(context,
                              color: Colors.black,
                              fontWeight: CustomTheme.semibold)),
                      SizedBox(height: CustomTheme.screenHeight! * 0.01),
                      Text(widget.item.role,
                          style: CustomTheme.body1(context,
                              color: Colors.black,
                              fontWeight: CustomTheme.medium)),
                    ],
                  ),
                  Expanded(child: Container()),
                  Container(
                    padding: EdgeInsets.all(8),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        color: Color(0xFFF3F6F9)),
                    child: Icon(Icons.arrow_forward_ios_rounded,
                        size: 20, color: Color(0xFF828282)),
                  ),
                ],
              ),
              SizedBox(height: CustomTheme.screenHeight! * 0.02),
              Divider(
                  color: Color(0xFFF2F4F9),
                  height: 0,
                  thickness: 1,
                  endIndent: 17,
                  indent: 17),
              SizedBox(height: CustomTheme.screenHeight! * 0.02),
              Container(
                height: CustomTheme.screenHeight! * 0.07,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Paid Leave",
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium)),
                    Container(
                      padding: EdgeInsets.all(8),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          color: widget.item.statusLeave == "Waiting Response"
                              ? Color(0xFFFFF4ED)
                              : widget.item.statusLeave == "Declined"
                                  ? Color(0xFFFFEEEC)
                                  : Color(0xFFE9F7EF)),
                      child: Text(widget.item.statusLeave,
                          style: CustomTheme.caption(context,
                              color:
                                  widget.item.statusLeave == "Waiting Response"
                                      ? Color(0xFFEB7D2E)
                                      : widget.item.statusLeave == "Declined"
                                          ? Color(0xFFFF5245)
                                          : Color(0xFF27AE60),
                              fontWeight: CustomTheme.medium)),
                    ),
                  ],
                ),
              ),
              Container(
                height: CustomTheme.screenHeight! * 0.07,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Leave Type",
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium)),
                    Text(widget.item.typeLeave,
                        textAlign: TextAlign.end,
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.semibold)),
                  ],
                ),
              ),
              Container(
                height: CustomTheme.screenHeight! * 0.07,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Days Total",
                        style: CustomTheme.body2(context,
                            color: Colors.black,
                            fontWeight: CustomTheme.medium)),
                    Container(
                      padding: EdgeInsets.all(8),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          color: Color(0xFFF2F4F9)),
                      child: Text(widget.item.daysTotal,
                          style: CustomTheme.caption(context,
                              color: Color(0xFF5981EA),
                              fontWeight: CustomTheme.medium)),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
