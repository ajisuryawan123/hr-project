import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lottie/lottie.dart';

class CustomTheme {
  static MediaQueryData? _mediaQueryData;
  static double? pixelData;
  static double? screenWidth;
  static double? screenHeight;
  static Orientation? orientation;

  void init(BuildContext context) {
    _mediaQueryData = MediaQuery.of(context);
    pixelData = _mediaQueryData!.devicePixelRatio;
    screenWidth = _mediaQueryData!.size.width;
    screenHeight = _mediaQueryData!.size.height;
    orientation = _mediaQueryData!.orientation;
  }

  // static Color colorPrimary1 = Color(0xFFEB7D2E);
  // static Color colorPrimary2 = Color(0xFF2D3643);
  // static Color colorSecondary1 = Color(0xFFFFA15E);
  // static Color colorSecondary2 = Color(0xFF525F71);
  // static Color variantPrimary1 = Color(0xFFF2F4F9);
  // static Color variantPrimary2 = Color(0xFFB8CADB);
  // static Color variantSecondary1 = Color(0xFF5981EA);
  // static Color variantSecondary2 = Color(0xFF93B1FF);
  // static Color iconsPrimary1 = Color(0xFF828282);
  // static Color iconsPrimary2 = Color(0xFFFFFFFF);
  // static Color neutralPrimary1 = Color(0xFF333333);
  // static Color neutralPrimary2 = Color(0xFFFFFFFF);
  // static Color graysPrimary1 = Color(0xFF828282);
  // static Color graysPrimary2 = Color(0xFFBDBDBD);
  // static Color graysSecondary1 = Color(0xFFE0E0E0);
  // static Color graysSecondary2 = Color(0xFFF2F2F2);

  static FontWeight regular = FontWeight.w400;
  static FontWeight medium = FontWeight.w500;
  static FontWeight semibold = FontWeight.w600;
  static FontWeight bold = FontWeight.w700;

  static TextStyle headline1(BuildContext context,
      {required Color color, required FontWeight fontWeight}) {
    return GoogleFonts.lato(fontWeight: fontWeight, color: color, fontSize: 35);
  }

  static TextStyle headline2(BuildContext context,
      {required Color color, required FontWeight fontWeight}) {
    return GoogleFonts.lato(fontWeight: fontWeight, color: color, fontSize: 30);
  }

  static TextStyle headline3(BuildContext context,
      {required Color color, required FontWeight fontWeight}) {
    return GoogleFonts.lato(fontWeight: fontWeight, color: color, fontSize: 25);
  }

  static TextStyle headline4(BuildContext context,
      {required Color color, required FontWeight fontWeight}) {
    return GoogleFonts.lato(fontWeight: fontWeight, color: color, fontSize: 20);
  }

  static TextStyle subtitle(BuildContext context,
      {required Color color, required FontWeight fontWeight}) {
    return GoogleFonts.lato(fontWeight: fontWeight, color: color, fontSize: 17);
  }

  static TextStyle body1(BuildContext context,
      {required Color color, required FontWeight fontWeight}) {
    return GoogleFonts.lato(fontWeight: fontWeight, color: color, fontSize: 15);
  }

  static TextStyle body2(BuildContext context,
      {required Color color, required FontWeight fontWeight}) {
    return GoogleFonts.lato(fontWeight: fontWeight, color: color, fontSize: 13);
  }

  static TextStyle button(BuildContext context,
      {required Color color, required FontWeight fontWeight}) {
    return GoogleFonts.lato(fontWeight: fontWeight, color: color, fontSize: 15);
  }

  static TextStyle caption(BuildContext context,
      {required Color color, required FontWeight fontWeight}) {
    return GoogleFonts.lato(fontWeight: fontWeight, color: color, fontSize: 10);
  }

  static TextStyle overline(BuildContext context,
      {required Color color, required FontWeight fontWeight}) {
    return GoogleFonts.lato(fontWeight: fontWeight, color: color, fontSize: 8);
  }
}

class ImageAsset {
  static Image logoHR(double scale) {
    return Image.asset("assets/logo/logoHR.png", scale: scale);
  }

  static Image headerBackground(double scale) {
    return Image.asset("assets/background/headerBackground.png", scale: scale);
  }

  static Image headerBackground2(double scale) {
    return Image.asset("assets/background/headerBackground2.png", scale: scale);
  }

  static Image image1(double scale) {
    return Image.asset("assets/image/image1.png", scale: scale);
  }

  static Image image2(double scale) {
    return Image.asset("assets/image/image2.png", scale: scale);
  }

  static Image image3(double scale) {
    return Image.asset("assets/image/image3.png", scale: scale);
  }

  static Image image4(double scale) {
    return Image.asset("assets/image/image4.png", scale: scale);
  }

  static Image image5(double scale) {
    return Image.asset("assets/image/image5.png", scale: scale);
  }

  static Image image6(double scale) {
    return Image.asset("assets/image/image6.png", scale: scale);
  }

  static Image image7(double scale) {
    return Image.asset("assets/image/image7.png", scale: scale);
  }

  static Image image8(double scale) {
    return Image.asset("assets/image/image8.png", scale: scale);
  }
}

class IconAsset {
  static SvgPicture chat() {
    return SvgPicture.asset("assets/icon/chat.svg");
  }

  static SvgPicture clock() {
    return SvgPicture.asset("assets/icon/clock.svg");
  }

  static SvgPicture date() {
    return SvgPicture.asset("assets/icon/date.svg");
  }

  static SvgPicture leave() {
    return SvgPicture.asset("assets/icon/leave.svg");
  }

  static SvgPicture form() {
    return SvgPicture.asset("assets/icon/form.svg");
  }

  static SvgPicture log() {
    return SvgPicture.asset("assets/icon/log.svg");
  }

  static SvgPicture more() {
    return SvgPicture.asset("assets/icon/more.svg");
  }

  static SvgPicture payroll() {
    return SvgPicture.asset("assets/icon/payroll.svg");
  }

  static SvgPicture profile() {
    return SvgPicture.asset("assets/icon/profile.svg");
  }

  static SvgPicture bell() {
    return SvgPicture.asset("assets/icon/bell.svg");
  }

  static SvgPicture notification() {
    return SvgPicture.asset("assets/icon/notification.svg");
  }

  static SvgPicture leaderboard() {
    return SvgPicture.asset("assets/icon/leaderboard.svg");
  }

  static SvgPicture approval_management() {
    return SvgPicture.asset("assets/icon/approval_management.svg");
  }

  static SvgPicture time_setting() {
    return SvgPicture.asset("assets/icon/time_setting.svg");
  }

  static SvgPicture user_management() {
    return SvgPicture.asset("assets/icon/user_management.svg");
  }

  static SvgPicture bulb() {
    return SvgPicture.asset("assets/icon/bulb.svg");
  }

  static SvgPicture door_enter() {
    return SvgPicture.asset("assets/icon/door_enter.svg");
  }

  static SvgPicture door_exit() {
    return SvgPicture.asset("assets/icon/door_exit.svg");
  }

  static SvgPicture leaves() {
    return SvgPicture.asset("assets/icon/leaves.svg");
  }

  static SvgPicture door_enter_green() {
    return SvgPicture.asset("assets/icon/door_enter_green.svg");
  }

  static SvgPicture door_exit_red() {
    return SvgPicture.asset("assets/icon/door_exit_red.svg");
  }

  static SvgPicture door_exit_grey() {
    return SvgPicture.asset("assets/icon/door_exit_grey.svg");
  }

  static SvgPicture door_enter_grey() {
    return SvgPicture.asset("assets/icon/door_enter_grey.svg");
  }

  static SvgPicture badge_account() {
    return SvgPicture.asset("assets/icon/badge_account.svg");
  }

  static SvgPicture clipboard() {
    return SvgPicture.asset("assets/icon/clipboard.svg");
  }

  static SvgPicture lock() {
    return SvgPicture.asset("assets/icon/lock.svg");
  }

  static SvgPicture log_out() {
    return SvgPicture.asset("assets/icon/log_out.svg");
  }

  static SvgPicture download() {
    return SvgPicture.asset("assets/icon/download.svg");
  }

  static SvgPicture search_blue() {
    return SvgPicture.asset("assets/icon/search_blue.svg");
  }

  static SvgPicture search_grey() {
    return SvgPicture.asset("assets/icon/search_grey.svg");
  }

  static SvgPicture calender_blue() {
    return SvgPicture.asset("assets/icon/calender_blue.svg");
  }

  static SvgPicture calender_white() {
    return SvgPicture.asset("assets/icon/calender_white.svg");
  }

  static SvgPicture calender_black() {
    return SvgPicture.asset("assets/icon/calender_black.svg");
  }

  static SvgPicture calender_grey() {
    return SvgPicture.asset("assets/icon/calender_grey.svg");
  }

  static SvgPicture add_square() {
    return SvgPicture.asset("assets/icon/add_square.svg");
  }

  static SvgPicture delete() {
    return SvgPicture.asset("assets/icon/delete.svg");
  }

  static SvgPicture medal1() {
    return SvgPicture.asset("assets/icon/medal1.svg");
  }

  static SvgPicture medal2() {
    return SvgPicture.asset("assets/icon/medal2.svg");
  }

  static SvgPicture medal3() {
    return SvgPicture.asset("assets/icon/medal3.svg");
  }

  static SvgPicture time_light_blue() {
    return SvgPicture.asset("assets/icon/time_light_blue.svg");
  }

  static SvgPicture time_grey() {
    return SvgPicture.asset("assets/icon/time_grey.svg");
  }

  static SvgPicture indonesia() {
    return SvgPicture.asset("assets/icon/indonesia.svg");
  }

  static SvgPicture malaysia() {
    return SvgPicture.asset("assets/icon/malaysia.svg");
  }

  static SvgPicture singapore() {
    return SvgPicture.asset("assets/icon/singapore.svg");
  }

  static SvgPicture calendar_event() {
    return SvgPicture.asset("assets/icon/calendar_event.svg");
  }

  static SvgPicture receipt_white() {
    return SvgPicture.asset("assets/icon/receipt_white.svg");
  }

  static SvgPicture receipt_grey() {
    return SvgPicture.asset("assets/icon/receipt_grey.svg");
  }

  static SvgPicture timer() {
    return SvgPicture.asset("assets/icon/timer.svg");
  }

  static Image avatar(double scale) {
    return Image.asset("assets/icon/avatar.png", scale: scale);
  }
}

class LottieAsset {
  static LottieBuilder success() {
    return Lottie.asset("assets/lottie/success.json");
  }
}

class AvatarAsset {
  static Image avatar1(double scale) {
    return Image.asset("assets/avatar/avatar1.png", scale: scale);
  }

  static Image avatar2(double scale) {
    return Image.asset("assets/avatar/avatar2.png", scale: scale);
  }

  static Image avatar3(double scale) {
    return Image.asset("assets/avatar/avatar3.png", scale: scale);
  }

  static Image avatar4(double scale) {
    return Image.asset("assets/avatar/avatar4.png", scale: scale);
  }

  static Image avatar5(double scale) {
    return Image.asset("assets/avatar/avatar5.png", scale: scale);
  }

  static Image avatar6(double scale) {
    return Image.asset("assets/avatar/avatar6.png", scale: scale);
  }

  static Image avatar7(double scale) {
    return Image.asset("assets/avatar/avatar7.png", scale: scale);
  }

  static Image avatar8(double scale) {
    return Image.asset("assets/avatar/avatar8.png", scale: scale);
  }

  static Image avatar9(double scale) {
    return Image.asset("assets/avatar/avatar9.png", scale: scale);
  }

  static Image avatar10(double scale) {
    return Image.asset("assets/avatar/avatar10.png", scale: scale);
  }

  static Image avatar11(double scale) {
    return Image.asset("assets/avatar/avatar11.png", scale: scale);
  }

  static Image avatar12(double scale) {
    return Image.asset("assets/avatar/avatar12.png", scale: scale);
  }

  static Image avatar13(double scale) {
    return Image.asset("assets/avatar/avatar13.png", scale: scale);
  }

  static Image avatar14(double scale) {
    return Image.asset("assets/avatar/avatar14.png", scale: scale);
  }

  static Image avatar15(double scale) {
    return Image.asset("assets/avatar/avatar15.png", scale: scale);
  }

  static Image avatar15_bigger(double scale) {
    return Image.asset("assets/avatar/avatar15_bigger.png", scale: scale);
  }

  static Image avatar16(double scale) {
    return Image.asset("assets/avatar/avatar16.png", scale: scale);
  }

  static Image avatar17(double scale) {
    return Image.asset("assets/avatar/avatar17.png", scale: scale);
  }

  static Image avatar18(double scale) {
    return Image.asset("assets/avatar/avatar18.png", scale: scale);
  }

  static Image avatar19(double scale) {
    return Image.asset("assets/avatar/avatar19.png", scale: scale);
  }

  static Image avatar20(double scale) {
    return Image.asset("assets/avatar/avatar20.png", scale: scale);
  }

  static Image avatar21(double scale) {
    return Image.asset("assets/avatar/avatar21.png", scale: scale);
  }

  static Image avatar22(double scale) {
    return Image.asset("assets/avatar/avatar22.png", scale: scale);
  }

  static Image avatar23(double scale) {
    return Image.asset("assets/avatar/avatar23.png", scale: scale);
  }

  static Image avatar24(double scale) {
    return Image.asset("assets/avatar/avatar24.png", scale: scale);
  }

  static Image avatar25(double scale) {
    return Image.asset("assets/avatar/avatar25.png", scale: scale);
  }

  static Image avatar26(double scale) {
    return Image.asset("assets/avatar/avatar26.png", scale: scale);
  }

  static Image avatar27(double scale) {
    return Image.asset("assets/avatar/avatar27.png", scale: scale);
  }
}
