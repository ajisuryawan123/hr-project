// To parse this JSON data, do
//
//     final attendanceToday = attendanceTodayFromJson(jsonString);

import 'dart:convert';

AttendanceTodayModel attendanceTodayModelFromJson(String str) =>
    AttendanceTodayModel.fromJson(json.decode(str));

String attendanceTodayModelToJson(AttendanceTodayModel data) =>
    json.encode(data.toJson());

class AttendanceTodayModel {
  String? errorCode;
  String? errorMessage;
  DataToday? data;

  AttendanceTodayModel({
    this.errorCode,
    this.errorMessage,
    this.data,
  });

  factory AttendanceTodayModel.fromJson(Map<String, dynamic> json) =>
      AttendanceTodayModel(
        errorCode: json["error_code"],
        errorMessage: json["error_message"],
        data: json["data"] == null ? null : DataToday.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "error_code": errorCode,
        "error_message": errorMessage,
        "data": data?.toJson(),
      };
}

class DataToday {
  String? event;
  Project? project;
  Employee? employee;
  String? attendanceId;
  String? clockIn;
  String? clockOut;
  DateTime? attendanceDate;
  String? workPlace;
  String? dailyTask;
  int? totalWork;
  String? attendanceStatus;
  String? createdBy;

  DataToday({
    this.event,
    this.project,
    this.employee,
    this.attendanceId,
    this.clockIn,
    this.clockOut,
    this.attendanceDate,
    this.workPlace,
    this.dailyTask,
    this.totalWork,
    this.attendanceStatus,
    this.createdBy,
  });

  factory DataToday.fromJson(Map<String, dynamic> json) => DataToday(
        event: json["event"],
        project:
            json["project"] == null ? null : Project.fromJson(json["project"]),
        employee: json["employee"] == null
            ? null
            : Employee.fromJson(json["employee"]),
        attendanceId: json["attendance_id"],
        clockIn: json["clock_in"] == null ? null : json["clock_in"],
        clockOut:
            json.containsKey("clock_out") == false ? null : json["clock_out"],
        attendanceDate: json["attendance_date"] == null
            ? null
            : DateTime.parse(json["attendance_date"]),
        workPlace: json["work_place"],
        dailyTask: json["daily_task"],
        totalWork: json["total_work"],
        attendanceStatus: json["attendance_status"],
        createdBy: json["created_by"],
      );

  Map<String, dynamic> toJson() => {
        "event": event,
        "project": project?.toJson(),
        "employee": employee?.toJson(),
        "attendance_id": attendanceId,
        "clock_in": clockIn,
        "clock_out": clockOut,
        "attendance_date":
            "${attendanceDate!.year.toString().padLeft(4, '0')}-${attendanceDate!.month.toString().padLeft(2, '0')}-${attendanceDate!.day.toString().padLeft(2, '0')}",
        "work_place": workPlace,
        "daily_task": dailyTask,
        "total_work": totalWork,
        "attendance_status": attendanceStatus,
        "created_by": createdBy,
      };
}

class Employee {
  Client? client;
  String? fullName;
  String? email;
  String? phoneNumber;
  DateTime? joinDate;
  String? jobTitle;

  Employee({
    this.client,
    this.fullName,
    this.email,
    this.phoneNumber,
    this.joinDate,
    this.jobTitle,
  });

  factory Employee.fromJson(Map<String, dynamic> json) => Employee(
        client: json["client"] == null ? null : Client.fromJson(json["client"]),
        fullName: json["full_name"],
        email: json["email"],
        phoneNumber: json["phone_number"],
        joinDate: json["join_date"] == null
            ? null
            : DateTime.parse(json["join_date"]),
        jobTitle: json["job_title"],
      );

  Map<String, dynamic> toJson() => {
        "client": client?.toJson(),
        "full_name": fullName,
        "email": email,
        "phone_number": phoneNumber,
        "join_date": joinDate?.toIso8601String(),
        "job_title": jobTitle,
      };
}

class Client {
  int? clientId;
  String? clientName;
  String? email;
  String? notes;

  Client({
    this.clientId,
    this.clientName,
    this.email,
    this.notes,
  });

  factory Client.fromJson(Map<String, dynamic> json) => Client(
        clientId: json["client_id"],
        clientName: json["client_name"],
        email: json["email"],
        notes: json["notes"],
      );

  Map<String, dynamic> toJson() => {
        "client_id": clientId,
        "client_name": clientName,
        "email": email,
        "notes": notes,
      };
}

class Project {
  int? projectId;
  String? projectName;

  Project({
    this.projectId,
    this.projectName,
  });

  factory Project.fromJson(Map<String, dynamic> json) => Project(
        projectId: json["project_id"],
        projectName: json["project_name"],
      );

  Map<String, dynamic> toJson() => {
        "project_id": projectId,
        "project_name": projectName,
      };
}
