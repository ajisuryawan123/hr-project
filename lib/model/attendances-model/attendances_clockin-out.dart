// To parse this JSON data, do
//
//     final attendancesClockIn = attendancesClockInFromJson(jsonString);

import 'dart:convert';

AttendancesClockInOutModel attendancesClockInModelFromJson(String str) =>
    AttendancesClockInOutModel.fromJson(json.decode(str));

String attendancesClockInModelToJson(AttendancesClockInOutModel data) =>
    json.encode(data.toJson());

class AttendancesClockInOutModel {
  String? errorCode;
  String? errorMessage;
  DataClockInOut? data;

  AttendancesClockInOutModel({
    this.errorCode,
    this.errorMessage,
    this.data,
  });

  factory AttendancesClockInOutModel.fromJson(Map<String, dynamic> json) =>
      AttendancesClockInOutModel(
        errorCode: json["error_code"],
        errorMessage: json["error_message"],
        data:
            json["data"] == null ? null : DataClockInOut.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "error_code": errorCode,
        "error_message": errorMessage,
        "data": data?.toJson(),
      };
}

class DataClockInOut {
  String? event;
  Project? project;
  Employee? employee;
  String? attendanceId;
  DateTime? clockIn;
  DateTime? attendanceDate;
  String? workPlace;
  String? dailyTask;
  int? totalWork;
  String? attendanceStatus;
  String? createdBy;

  DataClockInOut({
    this.event,
    this.project,
    this.employee,
    this.attendanceId,
    this.clockIn,
    this.attendanceDate,
    this.workPlace,
    this.dailyTask,
    this.totalWork,
    this.attendanceStatus,
    this.createdBy,
  });

  factory DataClockInOut.fromJson(Map<String, dynamic> json) => DataClockInOut(
        event: json["event"],
        project:
            json["project"] == null ? null : Project.fromJson(json["project"]),
        employee: json["employee"] == null
            ? null
            : Employee.fromJson(json["employee"]),
        attendanceId: json["attendance_id"],
        clockIn:
            json["clock_in"] == null ? null : DateTime.parse(json["clock_in"]),
        attendanceDate: json["attendance_date"] == null
            ? null
            : DateTime.parse(json["attendance_date"]),
        workPlace: json["work_place"],
        dailyTask: json["daily_task"],
        totalWork: json["total_work"],
        attendanceStatus: json["attendance_status"],
        createdBy: json["created_by"],
      );

  Map<String, dynamic> toJson() => {
        "event": event,
        "project": project?.toJson(),
        "employee": employee?.toJson(),
        "attendance_id": attendanceId,
        "clock_in": clockIn?.toIso8601String(),
        "attendance_date":
            "${attendanceDate!.year.toString().padLeft(4, '0')}-${attendanceDate!.month.toString().padLeft(2, '0')}-${attendanceDate!.day.toString().padLeft(2, '0')}",
        "work_place": workPlace,
        "daily_task": dailyTask,
        "total_work": totalWork,
        "attendance_status": attendanceStatus,
        "created_by": createdBy,
      };
}

class Employee {
  Client? client;
  String? fullName;
  String? email;
  String? phoneNumber;
  String? jobTitle;

  Employee({
    this.client,
    this.fullName,
    this.email,
    this.phoneNumber,
    this.jobTitle,
  });

  factory Employee.fromJson(Map<String, dynamic> json) => Employee(
        client: json["client"] == null ? null : Client.fromJson(json["client"]),
        fullName: json["full_name"],
        email: json["email"],
        phoneNumber: json["phone_number"],
        jobTitle: json["job_title"],
      );

  Map<String, dynamic> toJson() => {
        "client": client?.toJson(),
        "full_name": fullName,
        "email": email,
        "phone_number": phoneNumber,
        "job_title": jobTitle,
      };
}

class Client {
  int? clientId;
  String? clientName;
  String? email;
  String? notes;

  Client({
    this.clientId,
    this.clientName,
    this.email,
    this.notes,
  });

  factory Client.fromJson(Map<String, dynamic> json) => Client(
        clientId: json["client_id"],
        clientName: json["client_name"],
        email: json["email"],
        notes: json["notes"],
      );

  Map<String, dynamic> toJson() => {
        "client_id": clientId,
        "client_name": clientName,
        "email": email,
        "notes": notes,
      };
}

class Project {
  int? projectId;
  String? projectName;

  Project({
    this.projectId,
    this.projectName,
  });

  factory Project.fromJson(Map<String, dynamic> json) => Project(
        projectId: json["project_id"],
        projectName: json["project_name"],
      );

  Map<String, dynamic> toJson() => {
        "project_id": projectId,
        "project_name": projectName,
      };
}
