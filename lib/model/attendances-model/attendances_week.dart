// To parse this JSON data, do
//
//     final attendanceWeek = attendanceWeekFromJson(jsonString);

import 'dart:convert';

AttendanceWeekModel attendanceWeekModelFromJson(String str) =>
    AttendanceWeekModel.fromJson(json.decode(str));

String attendanceWeekModelToJson(AttendanceWeekModel data) =>
    json.encode(data.toJson());

class AttendanceWeekModel {
  String? errorCode;
  String? errorMessage;
  DataWeek? data;

  AttendanceWeekModel({
    this.errorCode,
    this.errorMessage,
    this.data,
  });

  factory AttendanceWeekModel.fromJson(Map<String, dynamic> json) =>
      AttendanceWeekModel(
        errorCode: json["error_code"],
        errorMessage: json["error_message"],
        data: json["data"] == null ? null : DataWeek.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "error_code": errorCode,
        "error_message": errorMessage,
        "data": data?.toJson(),
      };
}

class DataWeek {
  List<Attendance>? attendances;
  DateTime? startDate;
  DateTime? endDate;
  String? dateRange;
  int? punctualityValue;
  String? punctualityMessage;
  String? totalEmployeeClockIn;

  DataWeek({
    this.attendances,
    this.startDate,
    this.endDate,
    this.dateRange,
    this.punctualityValue,
    this.punctualityMessage,
    this.totalEmployeeClockIn,
  });

  factory DataWeek.fromJson(Map<String, dynamic> json) => DataWeek(
        attendances: json["attendances"] == null
            ? []
            : List<Attendance>.from(
                json["attendances"]!.map((x) => Attendance.fromJson(x))),
        startDate: json["start_date"] == null
            ? null
            : DateTime.parse(json["start_date"]),
        endDate:
            json["end_date"] == null ? null : DateTime.parse(json["end_date"]),
        dateRange: json["date_range"],
        punctualityValue: json["punctuality_value"],
        punctualityMessage: json["punctuality_message"],
        totalEmployeeClockIn: json["total_employee_clock_in"],
      );

  Map<String, dynamic> toJson() => {
        "attendances": attendances == null
            ? []
            : List<dynamic>.from(attendances!.map((x) => x.toJson())),
        "start_date":
            "${startDate!.year.toString().padLeft(4, '0')}-${startDate!.month.toString().padLeft(2, '0')}-${startDate!.day.toString().padLeft(2, '0')}",
        "end_date":
            "${endDate!.year.toString().padLeft(4, '0')}-${endDate!.month.toString().padLeft(2, '0')}-${endDate!.day.toString().padLeft(2, '0')}",
        "date_range": dateRange,
        "punctuality_value": punctualityValue,
        "punctuality_message": punctualityMessage,
        "total_employee_clock_in": totalEmployeeClockIn,
      };
}

class Attendance {
  String? event;
  Project? project;
  Employee? employee;
  String? attendanceId;
  DateTime? clockIn;
  DateTime? clockOut;
  DateTime? attendanceDate;
  String? workPlace;
  String? dailyTask;
  int? totalWork;
  String? attendanceStatus;
  String? dateGroupLabel;
  String? createdBy;

  Attendance({
    this.event,
    this.project,
    this.employee,
    this.attendanceId,
    this.clockIn,
    this.clockOut,
    this.attendanceDate,
    this.workPlace,
    this.dailyTask,
    this.totalWork,
    this.attendanceStatus,
    this.dateGroupLabel,
    this.createdBy,
  });

  factory Attendance.fromJson(Map<String, dynamic> json) => Attendance(
        event: json.containsKey("event") ? json["event"] : "-",
        project:
            json["project"] == null ? null : Project.fromJson(json["project"]),
        employee: json["employee"] == null
            ? null
            : Employee.fromJson(json["employee"]),
        attendanceId: json["attendance_id"],
        clockIn:
            json["clock_in"] == null ? null : DateTime.parse(json["clock_in"]),
        clockOut: json["clock_out"] == null
            ? null
            : DateTime.parse(json["clock_out"]),
        attendanceDate: json["attendance_date"] == null
            ? null
            : DateTime.parse(json["attendance_date"]),
        workPlace: json["work_place"],
        dailyTask: json.containsKey("daily_task") ? json["daily_task"] : "-",
        totalWork: json["total_work"],
        attendanceStatus: json["attendance_status"],
        dateGroupLabel: json["date_group_label"],
        createdBy: json["created_by"],
      );

  Map<String, dynamic> toJson() => {
        "event": event,
        "project": project?.toJson(),
        "employee": employee?.toJson(),
        "attendance_id": attendanceId,
        "clock_in": clockIn?.toIso8601String(),
        "clock_out": clockOut?.toIso8601String(),
        "attendance_date":
            "${attendanceDate!.year.toString().padLeft(4, '0')}-${attendanceDate!.month.toString().padLeft(2, '0')}-${attendanceDate!.day.toString().padLeft(2, '0')}",
        "work_place": workPlace,
        "daily_task": dailyTask,
        "total_work": totalWork,
        "attendance_status": attendanceStatus,
        "date_group_label": dateGroupLabel,
        "created_by": createdBy,
      };
}

class Employee {
  Client? client;
  String? fullName;
  String? email;
  String? phoneNumber;
  DateTime? joinDate;
  String? jobTitle;

  Employee({
    this.client,
    this.fullName,
    this.email,
    this.phoneNumber,
    this.joinDate,
    this.jobTitle,
  });

  factory Employee.fromJson(Map<String, dynamic> json) => Employee(
        client: json["client"] == null ? null : Client.fromJson(json["client"]),
        fullName: json["full_name"],
        email: json["email"],
        phoneNumber: json["phone_number"],
        joinDate: json["join_date"] == null
            ? null
            : DateTime.parse(json["join_date"]),
        jobTitle: json["job_title"],
      );

  Map<String, dynamic> toJson() => {
        "client": client?.toJson(),
        "full_name": fullName,
        "email": email,
        "phone_number": phoneNumber,
        "join_date": joinDate?.toIso8601String(),
        "job_title": jobTitle,
      };
}

class Client {
  int? clientId;
  String? clientName;
  String? email;
  String? notes;

  Client({
    this.clientId,
    this.clientName,
    this.email,
    this.notes,
  });

  factory Client.fromJson(Map<String, dynamic> json) => Client(
        clientId: json["client_id"],
        clientName: json["client_name"],
        email: json["email"],
        notes: json["notes"],
      );

  Map<String, dynamic> toJson() => {
        "client_id": clientId,
        "client_name": clientName,
        "email": email,
        "notes": notes,
      };
}

class Project {
  int? projectId;
  String? projectName;

  Project({
    this.projectId,
    this.projectName,
  });

  factory Project.fromJson(Map<String, dynamic> json) => Project(
        projectId: json["project_id"],
        projectName: json["project_name"],
      );

  Map<String, dynamic> toJson() => {
        "project_id": projectId,
        "project_name": projectName,
      };
}
