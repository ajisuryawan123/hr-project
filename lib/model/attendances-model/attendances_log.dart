// To parse this JSON data, do
//
//     final attendanceLogModel = attendanceLogModelFromJson(jsonString);

import 'dart:convert';

AttendanceLogModel attendanceLogModelFromJson(String str) =>
    AttendanceLogModel.fromJson(json.decode(str));

String attendanceLogModelToJson(AttendanceLogModel data) =>
    json.encode(data.toJson());

class AttendanceLogModel {
  String errorCode;
  String errorMessage;
  Data? data;

  AttendanceLogModel({
    required this.errorCode,
    required this.errorMessage,
    this.data,
  });

  factory AttendanceLogModel.fromJson(Map<String, dynamic> json) =>
      AttendanceLogModel(
        errorCode: json["error_code"],
        errorMessage: json["error_message"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "error_code": errorCode,
        "error_message": errorMessage,
        "data": data?.toJson(),
      };
}

class Data {
  List<Statistic> statistic;
  String startDate;
  String endDate;
  String dateRange;
  AttendancePagination attendancePagination;

  Data({
    required this.statistic,
    required this.startDate,
    required this.endDate,
    required this.dateRange,
    required this.attendancePagination,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        statistic: List<Statistic>.from(
            json["statistic"].map((x) => Statistic.fromJson(x))),
        startDate: (json["start_date"]),
        endDate: (json["end_date"]),
        dateRange: json["date_range"],
        attendancePagination:
            AttendancePagination.fromJson(json["attendance_pagination"]),
      );

  Map<String, dynamic> toJson() => {
        "statistic": List<dynamic>.from(statistic.map((x) => x.toJson())),
        "start_date": startDate,
        "end_date": endDate,
        "date_range": dateRange,
        "attendance_pagination": attendancePagination.toJson(),
      };
}

class AttendancePagination {
  List<Content> content;
  Pageable pageable;
  int totalElements;
  int totalPages;
  bool last;
  int size;
  int number;
  Sort sort;
  int numberOfElements;
  bool first;
  bool empty;

  AttendancePagination({
    required this.content,
    required this.pageable,
    required this.totalElements,
    required this.totalPages,
    required this.last,
    required this.size,
    required this.number,
    required this.sort,
    required this.numberOfElements,
    required this.first,
    required this.empty,
  });

  factory AttendancePagination.fromJson(Map<String, dynamic> json) =>
      AttendancePagination(
        content:
            List<Content>.from(json["content"].map((x) => Content.fromJson(x))),
        pageable: Pageable.fromJson(json["pageable"]),
        totalElements: json["totalElements"],
        totalPages: json["totalPages"],
        last: json["last"],
        size: json["size"],
        number: json["number"],
        sort: Sort.fromJson(json["sort"]),
        numberOfElements: json["numberOfElements"],
        first: json["first"],
        empty: json["empty"],
      );

  Map<String, dynamic> toJson() => {
        "content": List<dynamic>.from(content.map((x) => x.toJson())),
        "pageable": pageable.toJson(),
        "totalElements": totalElements,
        "totalPages": totalPages,
        "last": last,
        "size": size,
        "number": number,
        "sort": sort.toJson(),
        "numberOfElements": numberOfElements,
        "first": first,
        "empty": empty,
      };
}

class Content {
  Project project;
  Employee employee;
  String attendanceId;
  DateTime attendanceDate;
  int? totalWork;
  AttendanceStatus attendanceStatus;
  String? dateGroupLabel;
  String? createdBy;
  String? event;
  DateTime? clockIn;
  DateTime? clockOut;
  String? workPlace;
  String? dailyTask;

  Content({
    required this.project,
    required this.employee,
    required this.attendanceId,
    required this.attendanceDate,
    this.totalWork,
    required this.attendanceStatus,
    this.dateGroupLabel,
    this.createdBy,
    this.event,
    this.clockIn,
    this.clockOut,
    this.workPlace,
    this.dailyTask,
  });

  factory Content.fromJson(Map<String, dynamic> json) => Content(
        project: Project.fromJson(json["project"]),
        employee: Employee.fromJson(json["employee"]),
        attendanceId: json["attendance_id"],
        attendanceDate: DateTime.parse(json["attendance_date"]),
        totalWork: json["total_work"],
        attendanceStatus:
            attendanceStatusValues.map[json["attendance_status"]]!,
        dateGroupLabel: json["date_group_label"],
        createdBy: json["created_by"],
        event: json["event"],
        clockIn:
            json["clock_in"] == null ? null : DateTime.parse(json["clock_in"]),
        clockOut: json["clock_out"] == null
            ? null
            : DateTime.parse(json["clock_out"]),
        workPlace: json["work_place"],
        dailyTask: json["daily_task"],
      );

  Map<String, dynamic> toJson() => {
        "project": project.toJson(),
        "employee": employee.toJson(),
        "attendance_id": attendanceId,
        "attendance_date":
            "${attendanceDate.year.toString().padLeft(4, '0')}-${attendanceDate.month.toString().padLeft(2, '0')}-${attendanceDate.day.toString().padLeft(2, '0')}",
        "total_work": totalWork,
        "attendance_status": attendanceStatusValues.reverse[attendanceStatus],
        "date_group_label": dateGroupLabel,
        "created_by": createdBy,
        "event": event,
        "clock_in": clockIn?.toIso8601String(),
        "clock_out": clockOut?.toIso8601String(),
        "work_place": workPlace,
        "daily_task": dailyTask,
      };
}

enum AttendanceStatus { NORECORD, LATE, ONTIME }

final attendanceStatusValues = EnumValues({
  "LATE": AttendanceStatus.LATE,
  "NORECORD": AttendanceStatus.NORECORD,
  "ONTIME": AttendanceStatus.ONTIME,
});

class Employee {
  Client client;
  String fullName;
  String email;
  DateTime joinDate;
  String? jobTitle;
  String? phoneNumber;

  Employee({
    required this.client,
    required this.fullName,
    required this.email,
    required this.joinDate,
    this.jobTitle,
    this.phoneNumber,
  });

  factory Employee.fromJson(Map<String, dynamic> json) => Employee(
        client: Client.fromJson(json["client"]),
        fullName: json["full_name"],
        email: json["email"],
        phoneNumber: json["phone_number"],
        joinDate: DateTime.parse(json["join_date"]),
        jobTitle: json["job_title"],
      );

  Map<String, dynamic> toJson() => {
        "client": client.toJson(),
        "full_name": fullName,
        "email": email,
        "phone_number": phoneNumber,
        "join_date": joinDate.toIso8601String(),
        "job_title": jobTitle,
      };
}

class Client {
  int clientId;
  String clientName;
  String? email;
  String? notes;
  String? phoneNumber;

  Client({
    required this.clientId,
    required this.clientName,
    this.email,
    this.notes,
    this.phoneNumber,
  });

  factory Client.fromJson(Map<String, dynamic> json) => Client(
        clientId: json["client_id"],
        clientName: json["client_name"],
        email: json["email"],
        notes: json["notes"],
        phoneNumber: json["phoneNumber"],
      );

  Map<String, dynamic> toJson() => {
        "client_id": clientId,
        "client_name": clientName,
        "email": email,
        "notes": notes,
        "phoneNumber": phoneNumber,
      };
}

class Project {
  int projectId;
  String projectName;
  String? projectNotes;

  Project({
    required this.projectId,
    required this.projectName,
    this.projectNotes,
  });

  factory Project.fromJson(Map<String, dynamic> json) => Project(
        projectId: json["project_id"],
        projectName: json["project_name"],
        projectNotes: json["project_notes"],
      );

  Map<String, dynamic> toJson() => {
        "project_id": projectId,
        "project_name": projectName,
        "project_notes": projectNotes,
      };
}

class Pageable {
  Sort sort;
  int offset;
  int pageNumber;
  int pageSize;
  bool paged;
  bool unpaged;

  Pageable({
    required this.sort,
    required this.offset,
    required this.pageNumber,
    required this.pageSize,
    required this.paged,
    required this.unpaged,
  });

  factory Pageable.fromJson(Map<String, dynamic> json) => Pageable(
        sort: Sort.fromJson(json["sort"]),
        offset: json["offset"],
        pageNumber: json["pageNumber"],
        pageSize: json["pageSize"],
        paged: json["paged"],
        unpaged: json["unpaged"],
      );

  Map<String, dynamic> toJson() => {
        "sort": sort.toJson(),
        "offset": offset,
        "pageNumber": pageNumber,
        "pageSize": pageSize,
        "paged": paged,
        "unpaged": unpaged,
      };
}

class Sort {
  bool empty;
  bool unsorted;
  bool sorted;

  Sort({
    required this.empty,
    required this.unsorted,
    required this.sorted,
  });

  factory Sort.fromJson(Map<String, dynamic> json) => Sort(
        empty: json["empty"],
        unsorted: json["unsorted"],
        sorted: json["sorted"],
      );

  Map<String, dynamic> toJson() => {
        "empty": empty,
        "unsorted": unsorted,
        "sorted": sorted,
      };
}

class Statistic {
  int value;
  double percentage;
  String groupingBy;

  Statistic({
    required this.value,
    required this.percentage,
    required this.groupingBy,
  });

  factory Statistic.fromJson(Map<String, dynamic> json) => Statistic(
        value: json["value"],
        percentage: json["percentage"]?.toDouble(),
        groupingBy: json["grouping_by"],
      );

  Map<String, dynamic> toJson() => {
        "value": value,
        "percentage": percentage,
        "grouping_by": groupingBy,
      };
}

class EnumValues<T> {
  Map<String, T> map;
  late Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    reverseMap = map.map((k, v) => MapEntry(v, k));
    return reverseMap;
  }
}
