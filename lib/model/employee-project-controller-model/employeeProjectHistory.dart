// To parse this JSON data, do
//
//     final employeeProjectHistoryModel = employeeProjectHistoryModelFromJson(jsonString);

import 'dart:convert';

EmployeeProjectHistoryModel employeeProjectHistoryModelFromJson(String str) =>
    EmployeeProjectHistoryModel.fromJson(json.decode(str));

String employeeProjectHistoryModelToJson(EmployeeProjectHistoryModel data) =>
    json.encode(data.toJson());

class EmployeeProjectHistoryModel {
  String? errorCode;
  String? errorMessage;
  EmployeeProjectData? data;

  EmployeeProjectHistoryModel({
    this.errorCode,
    this.errorMessage,
    this.data,
  });

  factory EmployeeProjectHistoryModel.fromJson(Map<String, dynamic> json) =>
      EmployeeProjectHistoryModel(
        errorCode: json["error_code"],
        errorMessage: json["error_message"],
        data: json["data"] == null
            ? null
            : EmployeeProjectData.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "error_code": errorCode,
        "error_message": errorMessage,
        "data": data?.toJson(),
      };
}

class EmployeeProjectData {
  String? errorCode;
  String? errorMessage;
  List<EmployeeHistoryData>? data;

  EmployeeProjectData({
    this.errorCode,
    this.errorMessage,
    this.data,
  });

  factory EmployeeProjectData.fromJson(Map<String, dynamic> json) =>
      EmployeeProjectData(
        errorCode: json["error_code"],
        errorMessage: json["error_message"],
        data: json["data"] == null
            ? []
            : List<EmployeeHistoryData>.from(json["data"]!.map((x) => EmployeeHistoryData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "error_code": errorCode,
        "error_message": errorMessage,
        "data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class EmployeeHistoryData {
  Project? project;
  DateTime? startDate;
  DateTime? endDate;
  bool? isActive;

  EmployeeHistoryData({
    this.project,
    this.startDate,
    this.endDate,
    this.isActive,
  });

  factory EmployeeHistoryData.fromJson(Map<String, dynamic> json) => EmployeeHistoryData(
        project:
            json["project"] == null ? null : Project.fromJson(json["project"]),
        startDate: json["start_date"] == null
            ? null
            : DateTime.parse(json["start_date"]),
        endDate:
            json["end_date"] == null ? null : DateTime.parse(json["end_date"]),
        isActive: json["is_active"],
      );

  Map<String, dynamic> toJson() => {
        "project": project?.toJson(),
        "start_date":
            "${startDate!.year.toString().padLeft(4, '0')}-${startDate!.month.toString().padLeft(2, '0')}-${startDate!.day.toString().padLeft(2, '0')}",
        "end_date":
            "${endDate!.year.toString().padLeft(4, '0')}-${endDate!.month.toString().padLeft(2, '0')}-${endDate!.day.toString().padLeft(2, '0')}",
        "is_active": isActive,
      };
}

class Project {
  int? projectId;
  String? projectName;
  String? projectNotes;
  Client? client;

  Project({
    this.projectId,
    this.projectName,
    this.projectNotes,
    this.client,
  });

  factory Project.fromJson(Map<String, dynamic> json) => Project(
        projectId: json["project_id"],
        projectName: json["project_name"],
        projectNotes: json["project_notes"],
        client: json["client"] == null ? null : Client.fromJson(json["client"]),
      );

  Map<String, dynamic> toJson() => {
        "project_id": projectId,
        "project_name": projectName,
        "project_notes": projectNotes,
        "client": client?.toJson(),
      };
}

class Client {
  int? clientId;
  String? clientName;

  Client({
    this.clientId,
    this.clientName,
  });

  factory Client.fromJson(Map<String, dynamic> json) => Client(
        clientId: json["client_id"],
        clientName: json["client_name"],
      );

  Map<String, dynamic> toJson() => {
        "client_id": clientId,
        "client_name": clientName,
      };
}
