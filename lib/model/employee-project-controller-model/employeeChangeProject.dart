// To parse this JSON data, do
//
//     final employeeChangeProjectModel = employeeChangeProjectModelFromJson(jsonString);

import 'dart:convert';

EmployeeChangeProjectModel employeeChangeProjectModelFromJson(String str) =>
    EmployeeChangeProjectModel.fromJson(json.decode(str));

String employeeChangeProjectModelToJson(EmployeeChangeProjectModel data) =>
    json.encode(data.toJson());

class EmployeeChangeProjectModel {
  String? errorCode;
  String? errorMessage;
  EmployeeChangeProjectData? data;

  EmployeeChangeProjectModel({
    this.errorCode,
    this.errorMessage,
    this.data,
  });

  factory EmployeeChangeProjectModel.fromJson(Map<String, dynamic> json) =>
      EmployeeChangeProjectModel(
        errorCode: json["error_code"],
        errorMessage: json["error_message"],
        data: json["data"] == null
            ? null
            : EmployeeChangeProjectData.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "error_code": errorCode,
        "error_message": errorMessage,
        "data": data?.toJson(),
      };
}

class EmployeeChangeProjectData {
  Project? project;
  DateTime? startDate;
  DateTime? endDate;
  bool? isActive;

  EmployeeChangeProjectData({
    this.project,
    this.startDate,
    this.endDate,
    this.isActive,
  });

  factory EmployeeChangeProjectData.fromJson(Map<String, dynamic> json) =>
      EmployeeChangeProjectData(
        project:
            json["project"] == null ? null : Project.fromJson(json["project"]),
        startDate: json["start_date"] == null
            ? null
            : DateTime.parse(json["start_date"]),
        endDate:
            json["end_date"] == null ? null : DateTime.parse(json["end_date"]),
        isActive: json["is_active"],
      );

  Map<String, dynamic> toJson() => {
        "project": project?.toJson(),
        "start_date":
            "${startDate!.year.toString().padLeft(4, '0')}-${startDate!.month.toString().padLeft(2, '0')}-${startDate!.day.toString().padLeft(2, '0')}",
        "end_date":
            "${endDate!.year.toString().padLeft(4, '0')}-${endDate!.month.toString().padLeft(2, '0')}-${endDate!.day.toString().padLeft(2, '0')}",
        "is_active": isActive,
      };
}

class Project {
  int? projectId;
  String? projectName;
  String? projectNotes;
  Client? client;

  Project({
    this.projectId,
    this.projectName,
    this.projectNotes,
    this.client,
  });

  factory Project.fromJson(Map<String, dynamic> json) => Project(
        projectId: json["project_id"],
        projectName: json["project_name"],
        projectNotes: json["project_notes"],
        client: json["client"] == null ? null : Client.fromJson(json["client"]),
      );

  Map<String, dynamic> toJson() => {
        "project_id": projectId,
        "project_name": projectName,
        "project_notes": projectNotes,
        "client": client?.toJson(),
      };
}

class Client {
  int? clientId;
  String? clientName;

  Client({
    this.clientId,
    this.clientName,
  });

  factory Client.fromJson(Map<String, dynamic> json) => Client(
        clientId: json["client_id"],
        clientName: json["client_name"],
      );

  Map<String, dynamic> toJson() => {
        "client_id": clientId,
        "client_name": clientName,
      };
}
