// To parse this JSON data, do
//
//     final changePasswordModel = changePasswordModelFromJson(jsonString);

import 'dart:convert';

ChangePasswordModel changePasswordModelFromJson(String str) =>
    ChangePasswordModel.fromJson(json.decode(str));

String changePasswordModelToJson(ChangePasswordModel data) =>
    json.encode(data.toJson());

class ChangePasswordModel {
  String errorCode;
  String errorMessage;
  Data? data;

  ChangePasswordModel({
    required this.errorCode,
    required this.errorMessage,
    required this.data,
  });

  factory ChangePasswordModel.fromJson(Map<String, dynamic> json) =>
      ChangePasswordModel(
        errorCode: json["error_code"],
        errorMessage: json["error_message"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "error_code": errorCode,
        "error_message": errorMessage,
        "data": data?.toJson(),
      };
}

class Data {
  String accessToken;
  String refreshToken;
  Client client;
  Employee employee;
  List<ProjectList> projectList;
  String roleType;
  User user;

  Data({
    required this.accessToken,
    required this.refreshToken,
    required this.client,
    required this.employee,
    required this.projectList,
    required this.roleType,
    required this.user,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        accessToken: json["access_token"],
        refreshToken: json["refresh_token"],
        client: Client.fromJson(json["client"]),
        employee: Employee.fromJson(json["employee"]),
        projectList: List<ProjectList>.from(
            json["project_list"].map((x) => ProjectList.fromJson(x))),
        roleType: json["role_type"],
        user: User.fromJson(json["user"]),
      );

  Map<String, dynamic> toJson() => {
        "access_token": accessToken,
        "refresh_token": refreshToken,
        "client": client.toJson(),
        "employee": employee.toJson(),
        "project_list": List<dynamic>.from(projectList.map((x) => x.toJson())),
        "role_type": roleType,
        "user": user.toJson(),
      };
}

class Client {
  int clientId;
  String clientName;
  String email;
  String notes;

  Client({
    required this.clientId,
    required this.clientName,
    required this.email,
    required this.notes,
  });

  factory Client.fromJson(Map<String, dynamic> json) => Client(
        clientId: json["client_id"],
        clientName: json["client_name"],
        email: json["email"],
        notes: json["notes"],
      );

  Map<String, dynamic> toJson() => {
        "client_id": clientId,
        "client_name": clientName,
        "email": email,
        "notes": notes,
      };
}

class Employee {
  int employeeId;
  String fullName;
  String email;
  String phoneNumber;
  String jobTitle;

  Employee({
    required this.employeeId,
    required this.fullName,
    required this.email,
    required this.phoneNumber,
    required this.jobTitle,
  });

  factory Employee.fromJson(Map<String, dynamic> json) => Employee(
        employeeId: json["employee_id"],
        fullName: json["full_name"],
        email: json["email"],
        phoneNumber: json["phone_number"],
        jobTitle: json["job_title"],
      );

  Map<String, dynamic> toJson() => {
        "employee_id": employeeId,
        "full_name": fullName,
        "email": email,
        "phone_number": phoneNumber,
        "job_title": jobTitle,
      };
}

class ProjectList {
  int projectId;
  String projectName;
  String projectNotes;

  ProjectList({
    required this.projectId,
    required this.projectName,
    required this.projectNotes,
  });

  factory ProjectList.fromJson(Map<String, dynamic> json) => ProjectList(
        projectId: json["project_id"],
        projectName: json["project_name"],
        projectNotes: json["project_notes"],
      );

  Map<String, dynamic> toJson() => {
        "project_id": projectId,
        "project_name": projectName,
        "project_notes": projectNotes,
      };
}

class User {
  int userId;
  String fullName;
  String email;

  User({
    required this.userId,
    required this.fullName,
    required this.email,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
        userId: json["user_id"],
        fullName: json["full_name"],
        email: json["email"],
      );

  Map<String, dynamic> toJson() => {
        "user_id": userId,
        "full_name": fullName,
        "email": email,
      };
}
