// To parse this JSON data, do
//
//     final authenticateModel = authenticateModelFromJson(jsonString);

import 'dart:convert';

AuthenticateModel authenticateModelFromJson(String str) => AuthenticateModel.fromJson(json.decode(str));

String authenticateModelToJson(AuthenticateModel data) => json.encode(data.toJson());

class AuthenticateModel {
  String? errorCode;
  String? errorMessage;
  Data? data;

  AuthenticateModel({
    this.errorCode,
    this.errorMessage,
    this.data,
  });

  factory AuthenticateModel.fromJson(Map<String, dynamic> json) => AuthenticateModel(
    errorCode: json["error_code"],
    errorMessage: json["error_message"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "error_code": errorCode,
    "error_message": errorMessage,
    "data": data?.toJson(),
  };
}

class Data {
  String? accessToken;
  String? refreshToken;
  Client? client;
  Employee? employee;
  List<ProjectList>? projectList;
  String? roleType;
  User? user;

  Data({
    this.accessToken,
    this.refreshToken,
    this.client,
    this.employee,
    this.projectList,
    this.roleType,
    this.user,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    accessToken: json["access_token"],
    refreshToken: json["refresh_token"],
    client: json["client"] == null ? null : Client.fromJson(json["client"]),
    employee: json["employee"] == null ? null : Employee.fromJson(json["employee"]),
    projectList: json["project_list"] == null ? [] : List<ProjectList>.from(json["project_list"]!.map((x) => ProjectList.fromJson(x))),
    roleType: json["role_type"],
    user: json["user"] == null ? null : User.fromJson(json["user"]),
  );

  Map<String, dynamic> toJson() => {
    "access_token": accessToken,
    "refresh_token": refreshToken,
    "client": client?.toJson(),
    "employee": employee?.toJson(),
    "project_list": projectList == null ? [] : List<dynamic>.from(projectList!.map((x) => x.toJson())),
    "role_type": roleType,
    "user": user?.toJson(),
  };
}

class Client {
  int? clientId;
  String? clientName;
  String? email;
  String? notes;

  Client({
    this.clientId,
    this.clientName,
    this.email,
    this.notes,
  });

  factory Client.fromJson(Map<String, dynamic> json) => Client(
    clientId: json["client_id"],
    clientName: json["client_name"],
    email: json["email"],
    notes: json["notes"],
  );

  Map<String, dynamic> toJson() => {
    "client_id": clientId,
    "client_name": clientName,
    "email": email,
    "notes": notes,
  };
}

class Employee {
  int? employeeId;
  String? fullName;
  String? email;
  String? phoneNumber;
  String? jobTitle;

  Employee({
    this.employeeId,
    this.fullName,
    this.email,
    this.phoneNumber,
    this.jobTitle,
  });

  factory Employee.fromJson(Map<String, dynamic> json) => Employee(
    employeeId: json["employee_id"],
    fullName: json["full_name"],
    email: json["email"],
    phoneNumber: json["phone_number"],
    jobTitle: json["job_title"],
  );

  Map<String, dynamic> toJson() => {
    "employee_id": employeeId,
    "full_name": fullName,
    "email": email,
    "phone_number": phoneNumber,
    "job_title": jobTitle,
  };
}

class ProjectList {
  int? projectId;
  String? projectName;

  ProjectList({
    this.projectId,
    this.projectName,
  });

  factory ProjectList.fromJson(Map<String, dynamic> json) => ProjectList(
    projectId: json["project_id"],
    projectName: json["project_name"],
  );

  Map<String, dynamic> toJson() => {
    "project_id": projectId,
    "project_name": projectName,
  };
}

class User {
  int? userId;
  String? fullName;
  String? email;

  User({
    this.userId,
    this.fullName,
    this.email,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
    userId: json["user_id"],
    fullName: json["full_name"],
    email: json["email"],
  );

  Map<String, dynamic> toJson() => {
    "user_id": userId,
    "full_name": fullName,
    "email": email,
  };
}
