// To parse this JSON data, do
//
//     final leaveAssesor1Model = leaveAssesor1ModelFromJson(jsonString);

import 'dart:convert';

LeaveAssesor1_2Model leaveAssesor1ModelFromJson(String str) =>
    LeaveAssesor1_2Model.fromJson(json.decode(str));

String leaveAssesor1ModelToJson(LeaveAssesor1_2Model data) =>
    json.encode(data.toJson());

class LeaveAssesor1_2Model {
  String? errorCode;
  String? errorMessage;
  LeaveAssesor1_2Data? data;

  LeaveAssesor1_2Model({
    this.errorCode,
    this.errorMessage,
    this.data,
  });

  factory LeaveAssesor1_2Model.fromJson(Map<String, dynamic> json) =>
      LeaveAssesor1_2Model(
        errorCode: json["error_code"],
        errorMessage: json["error_message"],
        data: json["data"] == null
            ? null
            : LeaveAssesor1_2Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "error_code": errorCode,
        "error_message": errorMessage,
        "data": data?.toJson(),
      };
}

class LeaveAssesor1_2Data {
  List<Content>? content;
  Pageable? pageable;
  int? totalElements;
  int? totalPages;
  bool? last;
  int? size;
  int? number;
  Sort? sort;
  int? numberOfElements;
  bool? first;
  bool? empty;

  LeaveAssesor1_2Data({
    this.content,
    this.pageable,
    this.totalElements,
    this.totalPages,
    this.last,
    this.size,
    this.number,
    this.sort,
    this.numberOfElements,
    this.first,
    this.empty,
  });

  factory LeaveAssesor1_2Data.fromJson(Map<String, dynamic> json) =>
      LeaveAssesor1_2Data(
        content: json["content"] == null
            ? []
            : List<Content>.from(
                json["content"]!.map((x) => Content.fromJson(x))),
        pageable: json["pageable"] == null
            ? null
            : Pageable.fromJson(json["pageable"]),
        totalElements: json["totalElements"],
        totalPages: json["totalPages"],
        last: json["last"],
        size: json["size"],
        number: json["number"],
        sort: json["sort"] == null ? null : Sort.fromJson(json["sort"]),
        numberOfElements: json["numberOfElements"],
        first: json["first"],
        empty: json["empty"],
      );

  Map<String, dynamic> toJson() => {
        "content": content == null
            ? []
            : List<dynamic>.from(content!.map((x) => x.toJson())),
        "pageable": pageable?.toJson(),
        "totalElements": totalElements,
        "totalPages": totalPages,
        "last": last,
        "size": size,
        "number": number,
        "sort": sort?.toJson(),
        "numberOfElements": numberOfElements,
        "first": first,
        "empty": empty,
      };
}

class Content {
  Employee? employee;

  Content({
    this.employee,
  });

  factory Content.fromJson(Map<String, dynamic> json) => Content(
        employee: json["employee"] == null
            ? null
            : Employee.fromJson(json["employee"]),
      );

  Map<String, dynamic> toJson() => {
        "employee": employee?.toJson(),
      };
}

class Employee {
  int? employeeId;
  String? fullName;

  Employee({
    this.employeeId,
    this.fullName,
  });

  factory Employee.fromJson(Map<String, dynamic> json) => Employee(
        employeeId: json["employee_id"],
        fullName: json["full_name"],
      );

  Map<String, dynamic> toJson() => {
        "employee_id": employeeId,
        "full_name": fullName,
      };
}

class Pageable {
  Sort? sort;
  int? offset;
  int? pageNumber;
  int? pageSize;
  bool? paged;
  bool? unpaged;

  Pageable({
    this.sort,
    this.offset,
    this.pageNumber,
    this.pageSize,
    this.paged,
    this.unpaged,
  });

  factory Pageable.fromJson(Map<String, dynamic> json) => Pageable(
        sort: json["sort"] == null ? null : Sort.fromJson(json["sort"]),
        offset: json["offset"],
        pageNumber: json["pageNumber"],
        pageSize: json["pageSize"],
        paged: json["paged"],
        unpaged: json["unpaged"],
      );

  Map<String, dynamic> toJson() => {
        "sort": sort?.toJson(),
        "offset": offset,
        "pageNumber": pageNumber,
        "pageSize": pageSize,
        "paged": paged,
        "unpaged": unpaged,
      };
}

class Sort {
  bool? empty;
  bool? unsorted;
  bool? sorted;

  Sort({
    this.empty,
    this.unsorted,
    this.sorted,
  });

  factory Sort.fromJson(Map<String, dynamic> json) => Sort(
        empty: json["empty"],
        unsorted: json["unsorted"],
        sorted: json["sorted"],
      );

  Map<String, dynamic> toJson() => {
        "empty": empty,
        "unsorted": unsorted,
        "sorted": sorted,
      };
}
