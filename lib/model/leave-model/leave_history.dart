// To parse this JSON data, do
//
//     final leaveHistoryModel = leaveHistoryModelFromJson(jsonString);

import 'dart:convert';

LeaveHistoryModel leaveHistoryModelFromJson(String str) =>
    LeaveHistoryModel.fromJson(json.decode(str));

String leaveHistoryModelToJson(LeaveHistoryModel data) =>
    json.encode(data.toJson());

class LeaveHistoryModel {
  String? errorCode;
  String? errorMessage;
  LeaveHistoryData? data;

  LeaveHistoryModel({
    this.errorCode,
    this.errorMessage,
    this.data,
  });

  factory LeaveHistoryModel.fromJson(Map<String, dynamic> json) =>
      LeaveHistoryModel(
        errorCode: json["error_code"],
        errorMessage: json["error_message"],
        data: json["data"] == null
            ? null
            : LeaveHistoryData.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "error_code": errorCode,
        "error_message": errorMessage,
        "data": data?.toJson(),
      };
}

class LeaveHistoryData {
  Leave? leave;
  LeavePagination? leavePagination;

  LeaveHistoryData({
    this.leave,
    this.leavePagination,
  });

  factory LeaveHistoryData.fromJson(Map<String, dynamic> json) =>
      LeaveHistoryData(
        leave: json["leave"] == null ? null : Leave.fromJson(json["leave"]),
        leavePagination: json["leave_pagination"] == null
            ? null
            : LeavePagination.fromJson(json["leave_pagination"]),
      );

  Map<String, dynamic> toJson() => {
        "leave": leave?.toJson(),
        "leave_pagination": leavePagination?.toJson(),
      };
}

class Leave {
  int? remains;
  int? total;
  int? taken;
  int? unpaidLeaveTaken;

  Leave({
    this.remains,
    this.total,
    this.taken,
    this.unpaidLeaveTaken,
  });

  factory Leave.fromJson(Map<String, dynamic> json) => Leave(
        remains: json["remains"],
        total: json["total"],
        taken: json["taken"],
        unpaidLeaveTaken: json["unpaid_leave_taken"],
      );

  Map<String, dynamic> toJson() => {
        "remains": remains,
        "total": total,
        "taken": taken,
        "unpaid_leave_taken": unpaidLeaveTaken,
      };
}

class LeavePagination {
  List<Content>? content;
  Pageable? pageable;
  int? totalElements;
  int? totalPages;
  bool? last;
  int? size;
  int? number;
  Sort? sort;
  int? numberOfElements;
  bool? first;
  bool? empty;

  LeavePagination({
    this.content,
    this.pageable,
    this.totalElements,
    this.totalPages,
    this.last,
    this.size,
    this.number,
    this.sort,
    this.numberOfElements,
    this.first,
    this.empty,
  });

  factory LeavePagination.fromJson(Map<String, dynamic> json) =>
      LeavePagination(
        content: json["content"] == null
            ? []
            : List<Content>.from(
                json["content"]!.map((x) => Content.fromJson(x))),
        pageable: json["pageable"] == null
            ? null
            : Pageable.fromJson(json["pageable"]),
        totalElements: json["totalElements"],
        totalPages: json["totalPages"],
        last: json["last"],
        size: json["size"],
        number: json["number"],
        sort: json["sort"] == null ? null : Sort.fromJson(json["sort"]),
        numberOfElements: json["numberOfElements"],
        first: json["first"],
        empty: json["empty"],
      );

  Map<String, dynamic> toJson() => {
        "content": content == null
            ? []
            : List<dynamic>.from(content!.map((x) => x.toJson())),
        "pageable": pageable?.toJson(),
        "totalElements": totalElements,
        "totalPages": totalPages,
        "last": last,
        "size": size,
        "number": number,
        "sort": sort?.toJson(),
        "numberOfElements": numberOfElements,
        "first": first,
        "empty": empty,
      };
}

class Content {
  String? type;
  Employee? employee;
  Assessors? assessors;
  String? reason;
  int? leaveId;
  DateTime? startDate;
  DateTime? endDate;
  LeaveType? leaveType;
  String? status;
  String? attachmentUrl;
  int? daysTotal;

  Content({
    this.type,
    this.employee,
    this.assessors,
    this.reason,
    this.leaveId,
    this.startDate,
    this.endDate,
    this.leaveType,
    this.status,
    this.attachmentUrl,
    this.daysTotal,
  });

  factory Content.fromJson(Map<String, dynamic> json) => Content(
        type: json["type"],
        employee: json["employee"] == null
            ? null
            : Employee.fromJson(json["employee"]),
        assessors: json["assessors"] == null
            ? null
            : Assessors.fromJson(json["assessors"]),
        reason: json["reason"],
        leaveId: json["leave_id"],
        startDate: json["start_date"] == null
            ? null
            : DateTime.parse(json["start_date"]),
        endDate:
            json["end_date"] == null ? null : DateTime.parse(json["end_date"]),
        leaveType: json["leave_type"] == null
            ? null
            : LeaveType.fromJson(json["leave_type"]),
        status: json["status"],
        attachmentUrl: json["attachment_url"],
        daysTotal: json["days_total"],
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        "employee": employee?.toJson(),
        "assessors": assessors?.toJson(),
        "reason": reason,
        "leave_id": leaveId,
        "start_date":
            "${startDate!.year.toString().padLeft(4, '0')}-${startDate!.month.toString().padLeft(2, '0')}-${startDate!.day.toString().padLeft(2, '0')}",
        "end_date":
            "${endDate!.year.toString().padLeft(4, '0')}-${endDate!.month.toString().padLeft(2, '0')}-${endDate!.day.toString().padLeft(2, '0')}",
        "leave_type": leaveType?.toJson(),
        "status": status,
        "attachment_url": attachmentUrl,
        "days_total": daysTotal,
      };
}

class Assessors {
  First? first;
  First? second;

  Assessors({
    this.first,
    this.second,
  });

  factory Assessors.fromJson(Map<String, dynamic> json) => Assessors(
        first: json["first"] == null ? null : First.fromJson(json["first"]),
        second: json["second"] == null ? null : First.fromJson(json["second"]),
      );

  Map<String, dynamic> toJson() => {
        "first": first?.toJson(),
        "second": second?.toJson(),
      };
}

class First {
  String? name;
  String? email;
  String? status;
  int? employeeId;

  First({
    this.name,
    this.email,
    this.status,
    this.employeeId,
  });

  factory First.fromJson(Map<String, dynamic> json) => First(
        name: json["name"],
        email: json["email"],
        status: json["status"],
        employeeId: json["employee_id"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "email": email,
        "status": status,
        "employee_id": employeeId,
      };
}

class Employee {
  Client? client;
  String? fullName;
  String? email;
  String? phoneNumber;
  DateTime? joinDate;
  String? jobTitle;

  Employee({
    this.client,
    this.fullName,
    this.email,
    this.phoneNumber,
    this.joinDate,
    this.jobTitle,
  });

  factory Employee.fromJson(Map<String, dynamic> json) => Employee(
        client: json["client"] == null ? null : Client.fromJson(json["client"]),
        fullName: json["full_name"],
        email: json["email"],
        phoneNumber: json["phone_number"],
        joinDate: json["join_date"] == null
            ? null
            : DateTime.parse(json["join_date"]),
        jobTitle: json["job_title"],
      );

  Map<String, dynamic> toJson() => {
        "client": client?.toJson(),
        "full_name": fullName,
        "email": email,
        "phone_number": phoneNumber,
        "join_date": joinDate?.toIso8601String(),
        "job_title": jobTitle,
      };
}

class Client {
  int? clientId;
  String? clientName;
  String? email;
  String? notes;
  String? phoneNumber;

  Client({
    this.clientId,
    this.clientName,
    this.email,
    this.notes,
    this.phoneNumber,
  });

  factory Client.fromJson(Map<String, dynamic> json) => Client(
        clientId: json["client_id"],
        clientName: json["client_name"],
        email: json["email"],
        notes: json["notes"],
        phoneNumber: json["phoneNumber"],
      );

  Map<String, dynamic> toJson() => {
        "client_id": clientId,
        "client_name": clientName,
        "email": email,
        "notes": notes,
        "phoneNumber": phoneNumber,
      };
}

class LeaveType {
  int? id;
  String? name;

  LeaveType({
    this.id,
    this.name,
  });

  factory LeaveType.fromJson(Map<String, dynamic> json) => LeaveType(
        id: json["id"],
        name: json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
      };
}

class Pageable {
  Sort? sort;
  int? offset;
  int? pageNumber;
  int? pageSize;
  bool? paged;
  bool? unpaged;

  Pageable({
    this.sort,
    this.offset,
    this.pageNumber,
    this.pageSize,
    this.paged,
    this.unpaged,
  });

  factory Pageable.fromJson(Map<String, dynamic> json) => Pageable(
        sort: json["sort"] == null ? null : Sort.fromJson(json["sort"]),
        offset: json["offset"],
        pageNumber: json["pageNumber"],
        pageSize: json["pageSize"],
        paged: json["paged"],
        unpaged: json["unpaged"],
      );

  Map<String, dynamic> toJson() => {
        "sort": sort?.toJson(),
        "offset": offset,
        "pageNumber": pageNumber,
        "pageSize": pageSize,
        "paged": paged,
        "unpaged": unpaged,
      };
}

class Sort {
  bool? empty;
  bool? unsorted;
  bool? sorted;

  Sort({
    this.empty,
    this.unsorted,
    this.sorted,
  });

  factory Sort.fromJson(Map<String, dynamic> json) => Sort(
        empty: json["empty"],
        unsorted: json["unsorted"],
        sorted: json["sorted"],
      );

  Map<String, dynamic> toJson() => {
        "empty": empty,
        "unsorted": unsorted,
        "sorted": sorted,
      };
}
