// To parse this JSON data, do
//
//     final leaveSubmitModel = leaveSubmitModelFromJson(jsonString);

import 'dart:convert';

LeaveSubmitModel leaveSubmitModelFromJson(String str) =>
    LeaveSubmitModel.fromJson(json.decode(str));

String leaveSubmitModelToJson(LeaveSubmitModel data) =>
    json.encode(data.toJson());

class LeaveSubmitModel {
  String? errorCode;
  String? errorMessage;
  DataLeaveSubmit? data;

  LeaveSubmitModel({
    this.errorCode,
    this.errorMessage,
    this.data,
  });

  factory LeaveSubmitModel.fromJson(Map<String, dynamic> json) =>
      LeaveSubmitModel(
        errorCode: json["error_code"],
        errorMessage: json["error_message"],
        data: json["data"] == null ? null : DataLeaveSubmit.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "error_code": errorCode,
        "error_message": errorMessage,
        "data": data?.toJson(),
      };
}

class DataLeaveSubmit {
  String? type;
  Employee? employee;
  Assessors? assessors;
  String? reason;
  int? leaveId;
  DateTime? startDate;
  DateTime? endDate;
  LeaveType? leaveType;
  String? status;
  String? attachmentUrl;
  int? daysTotal;

  DataLeaveSubmit({
    this.type,
    this.employee,
    this.assessors,
    this.reason,
    this.leaveId,
    this.startDate,
    this.endDate,
    this.leaveType,
    this.status,
    this.attachmentUrl,
    this.daysTotal,
  });

  factory DataLeaveSubmit.fromJson(Map<String, dynamic> json) => DataLeaveSubmit(
        type: json["type"],
        employee: json["employee"] == null
            ? null
            : Employee.fromJson(json["employee"]),
        assessors: json["assessors"] == null
            ? null
            : Assessors.fromJson(json["assessors"]),
        reason: json["reason"],
        leaveId: json["leave_id"],
        startDate: json["start_date"] == null
            ? null
            : DateTime.parse(json["start_date"]),
        endDate:
            json["end_date"] == null ? null : DateTime.parse(json["end_date"]),
        leaveType: json["leave_type"] == null
            ? null
            : LeaveType.fromJson(json["leave_type"]),
        status: json["status"],
        attachmentUrl: json["attachment_url"],
        daysTotal: json["days_total"],
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        "employee": employee?.toJson(),
        "assessors": assessors?.toJson(),
        "reason": reason,
        "leave_id": leaveId,
        "start_date":
            "${startDate!.year.toString().padLeft(4, '0')}-${startDate!.month.toString().padLeft(2, '0')}-${startDate!.day.toString().padLeft(2, '0')}",
        "end_date":
            "${endDate!.year.toString().padLeft(4, '0')}-${endDate!.month.toString().padLeft(2, '0')}-${endDate!.day.toString().padLeft(2, '0')}",
        "leave_type": leaveType?.toJson(),
        "status": status,
        "attachment_url": attachmentUrl,
        "days_total": daysTotal,
      };
}

class Assessors {
  First? first;
  First? second;

  Assessors({
    this.first,
    this.second,
  });

  factory Assessors.fromJson(Map<String, dynamic> json) => Assessors(
        first: json["first"] == null ? null : First.fromJson(json["first"]),
        second: json["second"] == null ? null : First.fromJson(json["second"]),
      );

  Map<String, dynamic> toJson() => {
        "first": first?.toJson(),
        "second": second?.toJson(),
      };
}

class First {
  String? status;
  int? employeeId;

  First({
    this.status,
    this.employeeId,
  });

  factory First.fromJson(Map<String, dynamic> json) => First(
        status: json["status"],
        employeeId: json["employee_id"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "employee_id": employeeId,
      };
}

class Employee {
  Client? client;
  String? fullName;
  String? email;
  String? phoneNumber;
  String? jobTitle;

  Employee({
    this.client,
    this.fullName,
    this.email,
    this.phoneNumber,
    this.jobTitle,
  });

  factory Employee.fromJson(Map<String, dynamic> json) => Employee(
        client: json["client"] == null ? null : Client.fromJson(json["client"]),
        fullName: json["full_name"],
        email: json["email"],
        phoneNumber: json["phone_number"],
        jobTitle: json["job_title"],
      );

  Map<String, dynamic> toJson() => {
        "client": client?.toJson(),
        "full_name": fullName,
        "email": email,
        "phone_number": phoneNumber,
        "job_title": jobTitle,
      };
}

class Client {
  int? clientId;
  String? clientName;
  String? email;
  String? notes;
  String? phoneNumber;

  Client({
    this.clientId,
    this.clientName,
    this.email,
    this.notes,
    this.phoneNumber,
  });

  factory Client.fromJson(Map<String, dynamic> json) => Client(
        clientId: json["client_id"],
        clientName: json["client_name"],
        email: json["email"],
        notes: json["notes"],
        phoneNumber: json["phoneNumber"],
      );

  Map<String, dynamic> toJson() => {
        "client_id": clientId,
        "client_name": clientName,
        "email": email,
        "notes": notes,
        "phoneNumber": phoneNumber,
      };
}

class LeaveType {
  int? id;
  String? name;

  LeaveType({
    this.id,
    this.name,
  });

  factory LeaveType.fromJson(Map<String, dynamic> json) => LeaveType(
        id: json["id"],
        name: json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
      };
}
