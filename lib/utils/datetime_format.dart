import 'package:moment_dart/moment_dart.dart';

String datetime_format(DateTime? dateTime, String format) =>
    dateTime is DateTime ? Moment(dateTime).format(format) : '-';
