import 'package:flutter/material.dart';
import 'package:hr_project/page/authenticate/login.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

unauthorized(context) async {
  final SharedPreferences prefs = await _prefs;

  await prefs.clear();
  return Navigator.of(context).pushAndRemoveUntil(
      MaterialPageRoute(builder: (context) => LoginPage()),
      (Route<dynamic> route) => false);
}
