import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'projectList-model.dart';

class ProjectDatabase {
  static final ProjectDatabase instance = ProjectDatabase._init();

  static Database? _database;

  ProjectDatabase._init();

  Future<Database> get database async {
    if (_database != null) {
      return _database!;
    } else {
      _database = await _initDB('projectList.db');
      return _database!;
    }
  }

  Future<Database> _initDB(String filePath) async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, filePath);
    return await openDatabase(path, version: 1, onCreate: _createDB);
  }

  Future _createDB(Database db, int version) async {
    final idType = 'INTEGER PRIMARY KEY AUTOINCREMENT';
    // final boolType = 'BOOLEAN NOT NULL';
    final integerType = 'INTEGER NOT NULL';
    final textType = 'TEXT NOT NULL';

    await db.execute('''
CREATE TABLE $tableProject ( 
  ${ProjectFields.id} $idType, 
  ${ProjectFields.idProject} $integerType,
  ${ProjectFields.nameProject} $textType
  )
''');
  }

  Future<TodayProjectList> create(TodayProjectList project) async {
    final db = await instance.database;
    final id = await db.insert(tableProject, project.toJson());
    return project.copy(id: id);
  }

  Future<TodayProjectList> read(int id) async {
    final db = await instance.database;
    final maps = await db.query(
      tableProject,
      columns: ProjectFields.values,
      where: '${ProjectFields.id} = ?',
      whereArgs: [id],
    );

    if (maps.isNotEmpty) {
      return TodayProjectList.fromJson(maps.first);
    } else {
      throw Exception('ID $id not found');
    }
  }

  Future<List<TodayProjectList>> readAll() async {
    final db = await instance.database;
    final result = await db.query(tableProject);
    return result.map((json) => TodayProjectList.fromJson(json)).toList();
  }

  Future<int> update(TodayProjectList kost) async {
    final db = await instance.database;
    return db.update(
      tableProject,
      kost.toJson(),
      where: '${ProjectFields.id} = ?',
      whereArgs: [kost.id],
    );
  }

  Future<int> delete(int id) async {
    final db = await instance.database;
    return await db.delete(
      tableProject,
      where: '${ProjectFields.id} = ?',
      whereArgs: [id],
    );
  }

  Future deleteAll() async {
    final db = await instance.database;
    return await db.rawQuery('DELETE FROM ${tableProject}');
  }

  Future close() async {
    final db = await instance.database;
    db.close();
  }
}
