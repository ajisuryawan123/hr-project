final String tableProject = 'project_list';

class ProjectFields {
  static final List<String> values = [
    id,
    idProject,
    nameProject,
  ];

  static final String id = '_id';
  static final String idProject = 'project_id';
  static final String nameProject = 'project_name';
}

class TodayProjectList {
  final int? id;
  final int idProject;
  final String nameProject;

  TodayProjectList({
    this.id,
    required this.idProject,
    required this.nameProject,
  });

  Map<String, Object?> toJson() => {
        ProjectFields.id: id,
        ProjectFields.idProject: idProject,
        ProjectFields.nameProject: nameProject,
      };

  static TodayProjectList fromJson(Map<String, Object?> json) => TodayProjectList(
        id: json[ProjectFields.id] as int?,
        idProject: json[ProjectFields.idProject] as int,
        nameProject: json[ProjectFields.nameProject] as String,
      );

  TodayProjectList copy({
    int? id,
    int? idProject,
    String? nameProject,
  }) =>
      TodayProjectList(
        id: id ?? this.id,
        idProject: idProject ?? this.idProject,
        nameProject: nameProject ?? this.nameProject,
      );
}
